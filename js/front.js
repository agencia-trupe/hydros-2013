function fbs_click(obj){

	if($(obj).attr('data-fb-title') != undefined)
		t = $(obj).attr('data-fb-title')
	else
		t = document.title;

	if($(obj).attr('data-fb-description') != undefined)
		s = $(obj).attr('data-fb-description')
	else
		s = $('meta[name=description]').attr("content");
	
	u = location.href;	
	
	window.open('http://www.facebook.com/sharer.php?u='+encodeURIComponent(u)+'&t='+encodeURIComponent(t)+'&s='+encodeURIComponent(s),'sharer','toolbar=0,status=0,width=626,height=436');
	return false;
}

function fbs_click_game(obj, frase, titulo, lang){

	t = frase;
	s = titulo;
	u = location.href;
	
	window.open('http://www.facebook.com/sharer.php?s=100&p[title]='+encodeURIComponent(s)+'&p[summary]='+encodeURIComponent(t)+'&p[url]='+encodeURIComponent(u)+'&p[images][0]='+encodeURIComponent('http://www.projetohydros.com/_imgs/layout/'+lang+'fb.jpg'),'sharer','toolbar=0,status=0,width=626,height=436');
	return false;
}

function jsTraduz(str){
    jQuery.ajaxSetup({async:false, scriptCharset: "utf-8"});
    var traduzida;
    $.post(BASE+'/linguagem/jsTraduz', {string : str}, function(retorno){
            traduzida = retorno;
    });
    return traduzida;
}

$('document').ready( function(){

	Modernizr.load([
		{
			test: Modernizr.input.placeholder,
		  	nope: 'js/polyfill-placeholder.js'
		}
	]);

	if($('#lista-tweets').length){
		$('#lista-tweets').jtwt({
			username : $('#share-box .share-twitter a').attr('href').split('/').pop(),
			count : 3,
			image_size : 0,
			loader_text : 'Carregando...'
		});
	}
	
	$('#animate').cycle({
		pager:  $('#paginacao .centro .pad'),
		pause : 1,
		timeout : 6000,
		slideExpr: '.slide',
		width : 650,
		height: 569,
	    pagerAnchorBuilder: function(idx, slide) { 
	        return '<a href="#"></a>';
	    }
	});

	$('#faixa-slides #chamada').cycle({
		pause : 1,
		timeout : 9000,
	});

	$('#empresas').cycle({
		prev : $('#paginacao-empresas #emp-nav-prev'),
		next : $('#paginacao-empresas #emp-nav-next'),
		timeout : 0,
		fx : 'scrollHorz'
	});

	$('#pessoas .animate').cycle({
		prev : $('#paginacao-pessoas #pess-nav-prev'),
		next : $('#paginacao-pessoas #pess-nav-next'),
		timeout : 0,
		fx : 'scrollHorz'
	});	

	$.fn.qtip.styles.padrao = {
    	padding    : 0,
    	color      : '#fff',
    	background : '#430E5D',
    	textAlign  : 'center',
    	width      : 400,
	    height     : 27,
    	tip : {
    		size  : {
    			x : 10,
    			y : 4
    		}
    	},
    	border     : {
    		width  : 1,
    		radius : 7,
    		color  : '#430E5D'
    	}		
	}

	if($('body').hasClass('en')){
		var tooltip_projeto = "Here you keep up to date of all <span>Hydros Project</span> challenges and find out how to participate and become and <span>“Water Ambassador”</span>.";
		var tooltip_novidades = "Check in this space for <span>news, information and updates about the water</span> in the world.";
		var tooltip_materiais = "In this space you can download for free several materials which will support your decisions to become a <span>Water Ambassador<span>, as images, videos, posters, stickers, and others.";
		var tooltip_pegada = "You will find here the <span>Hydros Footprint</span> app and game for your smartphone or tablet.";
		var tooltip_inspira = "Here you know the initiatives and work done by people who have been sensitized by the <span>Hydros Project</span>;";
	}else if($('body').hasClass('es')){
		var tooltip_projeto = "Aquí podrás estar al día sobre los desafíos del Proyecto Hydros y descubrir cómo participar y convertirte en un <span>“Embajador del Agua”.</span>";
		var tooltip_novidades = "Verifica en este espacio <strong>noticias, informaciones y actualizaciones<strong> sobre el agua en el mundo.";
		var tooltip_materiais = "En este espacio puedes descargar gratuitamente diversos materiales que apoyarán tu decisión de convertirte en  un <span>Embajador del Agua</span>, como imágenes, vídeos, carteles, adhesivo, y otras piezas para divulgación del <span>Proyecto Hydros</span>."
		var tooltip_pegada = "Encontrarás aquí la aplicativa y el juego <span>Huella Hydros</span> para su smartphone o tablet.";
		var tooltip_inspira = "Aquí conocerás las iniciativas y los trabajos realizados por las personas que han sido sensibilizadas por el <span>Proyecto Hydros</span>.";
	}else {
		var tooltip_projeto = "Aqui você fica por dentro dos desafios do <span>Projeto Hydros</span> e descobre como participar e se tornar um <span>“Embaixador da Água”.</span>";
		var tooltip_novidades = "Confira nesse espaço <span>notícias, informações e dados atualizados</span> sobre a água no mundo.";
		var tooltip_materiais = "Nesse espaço você pode fazer o download gratuito de diversos materiais que apoiarão a sua decisão de tornar-se um <span>Embaixador da Água</span>, como imagens, vídeos, cartazes, adesivos e outras peças para divulgação do <span>Projeto Hydros.</span>";		
		var tooltip_pegada = "Você encontra aqui o aplicativo e o game <span>Pegada Hydros</span> para seu smartphone ou tablet.";
		var tooltip_inspira = "Aqui você conhece as iniciativas e trabalhos feitos por pessoas que foram sensibilizados pelo <span>Projeto Hydros</span>.";
	}

	$('#mn-projeto').qtip({
	    content  : tooltip_projeto,
	    show     : 'mouseover',
	    hide     : 'mouseout',
	    position : {
	   		corner : {
	   			target  : 'topMiddle',
	   			tooltip : 'bottomMiddle'
	   		}
	    },
	    style : {
	    	tip : {
	    		corner : 'bottomMiddle',
	    	},
	    	name : 'padrao'
	    }
	});

	$('#mn-novidades').qtip({
	    content  : tooltip_novidades,
	    show     : 'mouseover',
	    hide     : 'mouseout',
	    position : {
	   		corner : {
	   			target  : 'bottomMiddletopMiddle',
	   			tooltip : 'topMiddle'
	   		}
	    },
	    style : {
	    	height : 27,
	    	width: 350,
	    	tip : {
	    		corner : 'topMiddle',
	    	},
	    	name : 'padrao'
	    }
	});	

	$('#mn-materiais').qtip({
	    content  : tooltip_materiais,
	    show     : 'mouseover',
	    hide     : 'mouseout',
	    position : {
	   		corner : {
	   			target  : 'topMiddle',
	   			tooltip : 'bottomMiddle'
	   		}
	    },
	    style : {
	    	width : 450,
	    	tip : {
	    		corner : 'bottomMiddle'
	    	},
	    	name : 'padrao'
	    }
	});

	$('#mn-pegada').qtip({
	    content  : tooltip_pegada,
	    show     : 'mouseover',
	    hide     : 'mouseout',
	    position : {
	   		corner : {
	   			target  : 'bottomMiddletopMiddle',
	   			tooltip : 'topMiddle'
	   		}
	    },
	    style : {
	    	height : 27,
	    	width: 350,
	    	tip : {
	    		corner : 'topMiddle',
	    	},
	    	name : 'padrao'
	    }
	});		

	$('#mn-inspira').qtip({
	    content  : tooltip_inspira,
	    show     : 'mouseover',
	    hide     : 'mouseout',
	    position : {
	   		corner : {
	   			target  : 'bottomMiddletopMiddle',
	   			tooltip : 'topMiddle'
	   		}
	    },
	    style : {
	    	height : 27,
	    	width: 350,
	    	tip : {
	    		corner : 'topMiddle',
	    	},
	    	name : 'padrao'
	    }
	});		

	$('#form-newsletter').submit( function(e){
		e.preventDefault();

		var nome  = $('#input-nome').val();
		var email = $('#input-email').val();
		var idioma = $('#input-idioma').val();
		var re = /\S+@\S+\.\S+/;
		var checkemail = re.test(email)
    	
		if(nome && email && idioma && checkemail){

			$.post(BASE+'home/cadastro', { nome : nome, email: email, idioma: idioma }, function(retorno){
				retorno = JSON.parse(retorno);
				$('#resposta-newsletter').html(retorno.mensagem);
				$('#caixa-newsletter #move-newsletter').addClass('hid');
				if(retorno.codigo == 1){
					setTimeout( function(){
						$('#input-nome').val('');
						$('#input-email').val('');
						$('#caixa-newsletter #move-newsletter').removeClass('hid');
					}, 6000);
				}else{
					setTimeout( function(){
						$('#caixa-newsletter #move-newsletter').removeClass('hid');
					}, 2000);
				}
			});

		}else{
			$('#resposta-newsletter').html(jsTraduz("Insira seu nome e um email válido!"));
			$('#caixa-newsletter #move-newsletter').addClass('hid');
			setTimeout( function(){
				$('#caixa-newsletter #move-newsletter').removeClass('hid');
			}, 2000);
		}
	});

	$('#lanca-modal-adesao').live('click', function(e){
		e.preventDefault();

		$.fancybox({
			'padding'	: 0,
			'width' 	: 900,
			'height'	: 340,
			'type'		: 'iframe',
			'href'		: BASE+'projeto/adesao',
			'onClosed'	: 	function() { parent.location.reload(true); }
		});
	});

	tamMaximo = parseInt($('.imagens').css('width')) - parseInt($('.galeria-imagens').css('width'));

    $('#slider1').slider({
        min : 0,
        max : tamMaximo,
        animate : 'normal',
        slide : function(event, ui){
            $('.imagens').stop().animate({'marginLeft' : -ui.value}, 'fast');
            $('#slider2').slider("option", "value", ui.value);
        }
    });	

    $('#slider2').slider({
        min : 0,
        max : tamMaximo,
        animate : 'normal',
        slide : function(event, ui){
            $('.imagens').stop().animate({'marginLeft' : -ui.value}, 'fast');
            $('#slider1').slider("option", "value", ui.value);
        }
    });


	if($('.barra-share-large').length){
		$('.barra-share-large').each( function(){
			var barra = $(this);
			var elemento = barra.prev();
			elemento.imagesLoaded( function(){
				var largura = elemento.css('width')	;
				barra.css('width', largura);
				if(parseInt(largura) >= 400 && parseInt(largura) < 500)
					barra.addClass('semiwidth');
				if(parseInt(largura) < 400)
					barra.addClass('halfwidth');
			});	
		});
	}

	if($('.coluna.detalhe .titulo').length){
		var contexto_titulo = $('.coluna.detalhe .titulo');
		contexto_titulo.imagesLoaded( function(){
			var altura = parseInt($('img', contexto_titulo).css('height'));
			contexto_titulo.css('height', altura);
		});
	}

	if($('#comentarios').length){

		var contexto = $('#comentarios');
		var original;

		$('#btn-comente', contexto).live('click', function(e){
			e.preventDefault();

			$('#inner', contexto).addClass('hid');
			original = $('#inner', contexto).html();

			setTimeout( function(){
				$('#inner', contexto).load(BASE+'/comentarios/comentar', function(){
					window.location.hash = "";
					$('#inner', contexto).removeClass('hid');
					$('#input-comentario').focus();
					$('#input-redirect').val(window.location);
				});
			}, 400);
		});

		$('#btn-cadastre', contexto).live('click', function(e){
			e.preventDefault();

			$('#inner', contexto).addClass('hid');

			setTimeout( function(){
				$('#inner', contexto).load(BASE+'/comentarios/formCadastro', function(){
					window.location.hash = "";
					$('#inner', contexto).removeClass('hid');
				});
			}, 400);
		});

		$('#botao-cancelar-post').live('click', function(e){
			e.preventDefault();
			$('#inner', contexto).html(original);
			$('#comentarios #inner #comment-list-wrapper').cycle({
				slideExpr: $('.lista-comentarios'),
				timeout : 0,
				next : $('#btn-mais-comentarios'),
				fx : 'scrollVert',
				rev: 1,
				after : function(currSlideElement, nextSlideElement, options, forwardFlag){
					$(nextSlideElement).imagesLoaded( function(){
						$('#comment-list-wrapper').css('height', parseInt($(nextSlideElement).css('height')) + 30);
					});
				}
			});			
		})

		$('#fake-input input[type="file"]').live('change', function(){
			var conteudo = $(this).val();
			if(conteudo != '')
				$('#fake-input .hover').html(conteudo.split('\\').pop());
			else
				$('#fake-input .hover').html(jsTraduz("SELECIONAR ARQUIVO"));
		});

		// Fazer verificação de campos obrigatórios
		$('#login-cadastro-comentarios', contexto).live('submit', function(){

			$('#input-redirect').val(window.location);

			if($('#cad-com-nome', contexto).val() == ''){
				alert(jsTraduz('Informe seu nome!'));
				return false;
			}
			if($('#cad-com-nomeas', contexto).val() == ''){
				alert(jsTraduz('Informe seu nome de assinatura!'));
				return false;
			}
			if($('#cad-com-email', contexto).val() == ''){
				alert(jsTraduz('Informe seu e-mail!'));
				return false;
			}
			if($('#cad-com-cidade', contexto).val() == ''){
				alert(jsTraduz('Informe sua cidade!'));
				return false;
			}
			if($('#cad-com-pais', contexto).val() == ''){
				alert(jsTraduz('Informe seu país!'));
				return false;
			}
			if($('#cad-com-senha', contexto).val() == ''){
				alert(jsTraduz('Informe sua senha!'));
				return false;
			}
			if($('#cad-com-confsenha', contexto).val() == ''){
				alert(jsTraduz('Informe sua confirmação de senha!'));
				return false;
			}
			if($('#cad-com-senha', contexto).val() != $('#cad-com-confsenha', contexto).val()){
				alert(jsTraduz('Suas senhas não conferem!'));
				return false;	
			}
		});

		$('#login-comentarios', contexto).live('submit', function(){

			var checkUser, checkPass;

			$('#input-redirect').val(window.location);
			jQuery.ajaxSetup({async:false, scriptCharset: "utf-8"});

			if($('#login-email', contexto).val() == ''){
				alert(jsTraduz('Informe seu e-mail de cadastro!'));
				return false;
			}

		    $.post(BASE+'/comentarios/checkUser', {user : $('#login-email', contexto).val()}, function(retorno){
		            checkUser = retorno;
		    });

		    if(checkUser != 1){
		    	alert(jsTraduz('E-mail não cadastrado!'));
		    	return false;
		    }

			if($('#login-senha', contexto).val() == ''){
				alert(jsTraduz('Informe sua senha!'));
				return false;
			}

			$.post(BASE+'/comentarios/checkPass', {user : $('#login-email', contexto).val(), pass : $('#login-senha', contexto).val()}, function(retorno){
		            checkPass = retorno;
		    });

		    if(checkPass != 1){
		    	alert(jsTraduz('Senha não confere!'));
		    	return false;
		    }
		});

		$('#form-comentario', contexto).live('submit', function(){
			$('#coment_id_noticia').val($('#id_noticia').val());
			$('#coment_area').val($('#val_area').val());
		});
		
		if (window.location.hash == 'logged' || window.location.hash == '#logged') {
			$('#btn-comente', contexto).trigger('click');
		};

		$('#esqueci-senha').live('click', function(e){
			e.preventDefault();

			$('#inner', contexto).addClass('hid');
			original = $('#inner', contexto).html();

			setTimeout( function(){
				$('#inner', contexto).load(BASE+'/comentarios/esqueci', function(){
					window.location.hash = "";
					$('#inner', contexto).removeClass('hid');
					$('#login-email').focus();
					$('#input-redirect').val(window.location);
				});
			}, 400);
		});

		$('#login-recuperacao').live('submit', function(e){
			e.preventDefault();
			
			var email_u = $('#login-recuperar').val();

			$('#inner', contexto).addClass('hid');
			setTimeout( function(){
				var msg = jsTraduz("<h2>RECUPERAÇÃO DE SENHA</h2><p>Uma nova senha foi enviada para o seu e-mail!</p><a href='#' id='botao-cancelar-post' title='VOLTAR'>VOLTAR</a>");
				$('#inner', contexto).html(msg);	
				$('#inner', contexto).removeClass('hid');
				$.post(BASE+'/comentarios/reenviar', { email : email_u }, function(){});				
			}, 200);			
		});

		$('#comentarios #inner #comment-list-wrapper').cycle({
			slideExpr: $('.lista-comentarios'),
			timeout : 0,
			next : $('#btn-mais-comentarios'),
			fx : 'scrollVert',
			rev: 1,
			after : function(currSlideElement, nextSlideElement, options, forwardFlag){
				$(nextSlideElement).imagesLoaded( function(){
					$('#comment-list-wrapper').css('height', parseInt($(nextSlideElement).css('height')) + 30);
				});
			}
		});

	}

});