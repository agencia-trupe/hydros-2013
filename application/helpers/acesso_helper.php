<?php

function acesso_privado($tipo_usuario = 'admin'){

    $CI =& get_instance();

    switch($tipo_usuario){
        case 'usuario':
            $var = 'logged_in_user';
            $red = 'home';
            break;
        case 'admin':
            $var = 'logged_in';
            $red = 'painel/home';
            break;
        default:
            $var = FALSE;
            $red = 'home';
    }

    if($var === FALSE || !$CI->session->userdata($var)){
        redirect($red);
        die();
    }
}

function verifica_acesso($tipo_usuario = 'usuario'){

    $CI =& get_instance();

    $var = ($tipo_usuario == 'usuario') ? 'logged_in' : 'logged_in_admin';

    return ($CI->session->userdata($var)) ? TRUE : FALSE;

}

function acesso_limitado($redirect = FALSE){

    $CI =& get_instance();

    $id_usuario = $CI->session->userdata('id');

    $query = $CI->db->get_where('usuarios', array('id' => $id_usuario))->result();

    $tipo = $query[0]->id_area_atuacao;

    ////////////////////////////////////////////////////////////
    // ARRAY COM OS TIPOS QUE NÃO TEM ACESSO COMPLETO AO SISTEMA
    ////////////////////////////////////////////////////////////
    $tipos_sem_acesso = array(6, 7);
    ////////////////////////////////////////////////////////////

    
    if(in_array($tipo, $tipos_sem_acesso)){
        if($redirect){
            redirect('home');
        }else{
            return true;
        }
    }
}

function ip(){
    
    if(isset($_SERVER["REMOTE_ADDR"]))
        return $_SERVER["REMOTE_ADDR"]; 
    elseif(isset($_SERVER["HTTP_X_FORWARDED_FOR"])) 
        return $_SERVER["HTTP_X_FORWARDED_FOR"];
    elseif(isset($_SERVER["HTTP_CLIENT_IP"])) 
        return $_SERVER["HTTP_CLIENT_IP"];
        
}

?>