<?php

function traduz($label, $ajax = FALSE){
    
    $CI =& get_instance();

    if($ajax)
        $CI->lang->load($CI->session->userdata('language').'_site', $CI->session->userdata('language'));
    
    $return = $CI->lang->line($label);
    
    if($return)
        return str_replace('$', '<br>', $return);
    else{
        switch(prefixo()){
            case 'en_':
                $linguagem = 'Ingles';
                break;
            case 'es_':
                $linguagem = 'Espanhol';
                break;
            case 'pt_':
                $linguagem = 'Portugues';
                break;
            default:
                $linguagem = 'Linguagem nao encontrada';
        }
        log_message('error',"Traducao nao encontrada. Ling : $linguagem - Indice : $label - View : " .$this->router->class);
        return "#".$label."#";
    }
}

/*
 * Função para adicionar o prefixo definido na sessão de acordo com a linguagem
 * ao nome da tabela
 */
function prefixo($arg = ''){
    $CI =& get_instance();
    return $CI->session->userdata('prefixo').$arg;
}
?>