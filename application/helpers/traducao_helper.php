<?php

function traduz($label, $ajax = FALSE){
    
    $CI =& get_instance();

    //if($ajax){
        if($CI->session->userdata('language') == 'pt' || $CI->session->userdata('language') == 'es' || $CI->session->userdata('language') == 'en'){
            $CI->lang->load($CI->session->userdata('language').'_site', $CI->session->userdata('language'));
        }else{
            $dns = $_SERVER['HTTP_HOST'];

            switch($dns){
                case 'preview.projetohydros.com':
                case 'www.projetohydros.com':
                case 'projetohydros.com':
                    $padrao = 'pt';
                    break;
                case 'preview.projecthydros.com':
                case 'www.projecthydros.com':
                case 'projecthydros.com':
                    $padrao = 'en';
                    break;
                case 'preview.proyectohydros.com':
                case 'www.proyectohydros.com':
                case 'proyectohydros.com':
                    $padrao = 'es';
                    break;
                default:
                    $padrao = 'pt';
            }
            $CI->lang->load($padrao.'_site', $padrao);
        }
    //}
    
    $return = $CI->lang->line($label);
    
    if($return)
        return $return;
    else
        return $label;
}

/*
 * Função para adicionar o prefixo definido na sessão de acordo com a linguagem
 */
function prefixo($arg){
    $CI =& get_instance();
    return $CI->session->userdata('prefixo').$arg;
}

function sufixo($arg){
    $CI =& get_instance();
    return $arg.'_'.$CI->session->userdata('language');
}

?>