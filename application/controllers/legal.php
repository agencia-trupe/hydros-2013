<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Legal extends MY_Frontcontroller {

	function __construct(){
		parent::__construct();
	}

	function index(){
		$this->load->view('legal');
	}

}
