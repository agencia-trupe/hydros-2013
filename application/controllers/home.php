<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MY_Frontcontroller {

    function __construct(){
   		parent::__construct();
        $this->load->model('banners_model', 'banners');
    }

    function index(){
        $data['slides'] = $this->banners->pegarTodos();
        $data['chamadas'] = $this->banners->pegarChamada();
        $data['noticia'] = $this->db->order_by('data', 'desc')->get($this->session->userdata('language').'_noticias')->result();

   		$this->load->view('home', $data);        
    }

    function cadastro(){

        $nome = $this->input->post('nome');
        $email = $this->input->post('email');
        $idioma = $this->input->post('idioma');

        $verifica_email = $this->db->get_where('cadastros', array('email' => $email))->num_rows();

        if($verifica_email > 0){
            $resposta = array(
                'codigo'  => 0,
                'mensagem' => traduz("Email já cadastrado!")
            );
        }else{

            $insert = $this->db->set('nome', $nome)
                               ->set('email', $email)
                               ->set('newsletter', 1)
                               ->set('idioma', $idioma)
                               ->set('data_cadastro', Date('Y-m-d'))
                               ->insert('cadastros');

            if($insert){
                $resposta = array(
                    'codigo'  => 1,
                    'mensagem' => traduz("Cadastro efetuado com sucesso!")
                );
            }else{
                $resposta = array(
                    'codigo'  => 0,
                    'mensagem' => traduz("Erro ao efetuar o cadastro!")
                );  
            }
        }

    	echo json_encode($resposta);
    }
}