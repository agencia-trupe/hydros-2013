<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @property CI_Loader $load
 * @property CI_Form_validation $form_validation
 * @property CI_Input $input
 * @property CI_Email $email
 * @property CI_DB_active_record $db
 * @property CI_DB_forge $dbforge
 */
class Arquivo extends CI_controller {

    function __construct() {
        parent::__construct();

        $this->load->helper('download');
    }

    function index($arquivo, $dir = 'arquivos', $default_filename = FALSE) {

        if(!$arquivo){
            $this->session->set_flashdata('download_failed', TRUE);
            if($this->session->userdata('redirect'))
                redirect($this->session->userdata('redirect'));
            else
                redirect('home');
        }

        if (strpos($arquivo, "_mp4") !== FALSE) {
            $arquivo = str_replace('_mp4', '.mp4', $arquivo);
            $dir = ($dir == 'video') ? '_'.$dir.'/' : '_'.$dir.'/video/';
        }elseif(strpos($arquivo, "aquavitae") !== FALSE){
            $arquivo = str_replace('_', '.', $arquivo);
            $dir = '_'.$dir.'/aquavitae/';
        }else {
            $arquivo = str_replace('_', '.', $arquivo);
            $dir = '_'.$dir.'/';
        }

        $nome = $arquivo;
        $filesize_definido = FALSE;

        if(file_exists($dir.$arquivo)){

            $arquivo = $dir.$arquivo;
            $extensao = end(explode('.', $arquivo));

            if($dir == '_video/' || $dir == '../_video/'){
                $file_name = traduz("video_institucional_hydros").".".$extensao;
            }elseif($extensao == 'zip'){
                $file_name = $nome;
            }else{
                if($nome == "8eaf685be4142e7a6158293849cdbfcf.pdf")
                    $file_name = "Hydros_Project_Booklet.pdf";
                elseif($nome == "9981aaec17bc72f8e827be3e7223302e.pdf")
                    $file_name = "Hydros_Project_Booklet_reduced.pdf";
                elseif($nome == "e923018417acad25aca400827c01758e.pdf")
                    $file_name = "Manual_Proyecto_Hydros.pdf";
                elseif($nome == "b5ab54edd0a7744d619ea70cd314ec2f.pdf")
                    $file_name = "Manual_Proyecto_Hydros_reducida.pdf";
                elseif($nome == "04a681f74954caa665f5c3f99735273f.pdf")
                    $file_name = "Cartilha_Projeto_Hydros.pdf";
                elseif($nome == "98487875daa6128de3ba03af669eaebf.pdf")
                    $file_name = "Cartilha_Projeto_Hydros_reduzida.pdf";
                else{
                    if($default_filename)
                        $file_name = $default_filename.".".$extensao;
                    else
                        $file_name = $nome;
                }
            }
            $file_path = base_url($arquivo);
            header("Content-Type: application/force-download");
            header('Content-Type: application/octet-stream');
            header("Content-Type: application/download");
            header("Content-Length : ".filesize($arquivo));
            header("Content-Disposition: attachment; filename=$file_name");
            //ob_clean();
            flush();
            //readfile($arquivo);

            // TESTE
            $fp = fopen($arquivo, "r");
            while (!feof($fp))
            {
                echo fread($fp, 65536);
                flush(); // this is essential for large downloads
            }
            fclose($fp);

        }else{
            $this->session->set_flashdata('download_failed', TRUE);
            if($this->session->userdata('redirect'))
                redirect($this->session->userdata('redirect'));
            else
                redirect('home');
        }
    }
}
?>
