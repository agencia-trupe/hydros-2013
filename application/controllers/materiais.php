<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Materiais extends MY_Frontcontroller {

	function __construct(){
		parent::__construct();

		$this->arr = $this->db->order_by('id', 'DESC')->get('galeria_imagens')->result();
        shuffle($this->arr);
	}

	function index(){
		redirect('materiais/videos');
	}

	function videos($slug = false){

		if(!$slug){

            $assunto_email = traduz("Conheça o Projeto Hydros! - Água em nós");
            $texto_email = traduz("Os vídeos “Água em nós”, composto por um filme mais completo e outros oito featurettes, são ferramentas de conscientização e sensibilização que mostram, de forma lúdica, a verdadeira relação que temos com a água em todos os seus aspectos. Portanto, use-os à vontade: publique no seu mural do Facebook e outras redes sociais ou faça o download para usá-los no ambiente de trabalho, com a família e amigos.".current_url());
            $texto_tweet = traduz("Conheça o Projeto Hydros! http://www.projetohydros.com - Vídeo Água em Nós!");
            $data['share']['email'] = "mailto:?subject=$assunto_email&body=$texto_email";
            $data['share']['tweet'] = "window.open('http://twitter.com/home?status=$texto_tweet','Twitter','toolbar=0,status=0,width=626,height=436'); return false;";
            $data['share']['tumblr'] = traduz("Conheça o Projeto Hydros! ".current_url()." - Vídeo Água em Nós!");

            $this->headervar['og']['title'] = "Projeto Hydros - Vídeo Água em Nós";
            $this->headervar['og']['description'] = "Os vídeos “Água em nós”, composto por um filme mais completo e outros oito featurettes, são ferramentas de conscientização e sensibilização que mostram, de forma lúdica, a verdadeira relação que temos com a água em todos os seus aspectos. Portanto, use-os à vontade: publique no seu mural do Facebook e outras redes sociais ou faça o download para usá-los no ambiente de trabalho, com a família e amigos."; 

            switch ($this->session->userdata('language')) {
                case 'es':
                    $data['frase_titulo'] = "El video “El agua en nosotros” es una herramienta de promoción de toma de conciencia y sensibilización que muestra, de forma lúdica, la verdadera relación que tenemos con el agua en todos sus aspectos.  Por lo tanto, úsalo como quieras: publica en tu página principal de Facebook y en otras redes sociales o haz el download para usarlo en tu trabajo, con tu familia y amigos.";
                    $data['link_video'] = "http://player.vimeo.com/video/46706692?byline=0&portrait=0&loop=1&wmode=transparent&nocache=";
                    $data['video1'] = array(
                        "slug" => "featurette_1",
                        "link" => "https://vimeo.com/62721452",
                        "id_embed" => "62721452",
                        "imagem" => "1_es.jpg",
                        "titulo" => traduz("Projeto Hydros")." - Qué es el agua",
                        "arquivo" => "Que_es_el_agua_mp4",
                        "email" => "mailto:?subject=".traduz("Projeto Hydros")." - Qué es el agua&body=".current_url()."/featurette_1",
                        "twitter" => "Qué es el agua - ".current_url()."/featurette_1",
                        "tumblr" => "Qué es el agua",
                        "fb_titulo" => "Qué es el agua",
                        "fb_description" => ""
                    );
                    $data['video2'] = array(
                        "slug" => "featurette_2",
                        "link" => "https://vimeo.com/62721451",
                        "id_embed" => "62721451",
                        "imagem" => "2_es.jpg",
                        "titulo" => traduz("Projeto Hydros")." - Sé parte del cambio",
                        "arquivo" => "Se_parte_del_cambio_mp4",
                        "email" => "mailto:?subject=".traduz("Projeto Hydros")." - Sé parte del cambio&body=".current_url()."/featurette_2",
                        "twitter" => "Sé parte del cambio - ".current_url()."/featurette_2",
                        "tumblr" => "Sé parte del cambio",
                        "fb_titulo" => "Sé parte del cambio",
                        "fb_description" => ""
                    );
                    $data['video3'] = array(
                        "slug" => "featurette_3",
                        "link" => "https://vimeo.com/62721450",
                        "id_embed" => "62721450",
                        "imagem" => "3_es.jpg",
                        "titulo" => traduz("Projeto Hydros")." - Las pequenas actitudes",
                        "arquivo" => "las_pequenas_actitudes_mp4",
                        "email" => "mailto:?subject=".traduz("Projeto Hydros")." - Las pequenas actitudes&body=".current_url()."/featurette_3",
                        "twitter" => "Las pequenas actitudes - ".current_url()."/featurette_3",
                        "tumblr" => "Las pequenas actitudes",
                        "fb_titulo" => "Las pequenas actitudes",
                        "fb_description" => ""
                    );
                    $data['video4'] = array(
                        "slug" => "featurette_4",
                        "link" => "https://vimeo.com/62721302",
                        "id_embed" => "62721302",
                        "imagem" => "4_es.jpg",
                        "titulo" => traduz("Projeto Hydros")." - Evolución de la naturaleza",
                        "arquivo" => "Evolucion_de_la_naturaleza_mp4",
                        "email" => "mailto:?subject=".traduz("Projeto Hydros")." - Evolución de la naturaleza&body=".current_url()."/featurette_4",
                        "twitter" => "Evolución de la naturaleza - ".current_url()."/featurette_4",
                        "tumblr" => "Evolución de la naturaleza",
                        "fb_titulo" => "Evolución de la naturaleza",
                        "fb_description" => ""
                    );
                    $data['video5'] = array(
                        "slug" => "featurette_5",
                        "link" => "https://vimeo.com/62721300",
                        "id_embed" => "62721300",
                        "imagem" => "5_es.jpg",
                        "titulo" => traduz("Projeto Hydros")." - El consumo humano de agua",
                        "arquivo" => "El_consumo_humano_de_agua_mp4",
                        "email" => "mailto:?subject=".traduz("Projeto Hydros")." - El consumo humano de agua&body=".current_url()."/featurette_5",
                        "twitter" => "El consumo humano de agua - ".current_url()."/featurette_5",
                        "tumblr" => "El consumo humano de agua",
                        "fb_titulo" => "El consumo humano de agua",
                        "fb_description" => ""
                    );
                    $data['video6'] = array(
                        "slug" => "featurette_6",
                        "link" => "https://vimeo.com/62721299",
                        "id_embed" => "62721299",
                        "imagem" => "6_es.jpg",
                        "titulo" => traduz("Projeto Hydros")." - El agua es vida",
                        "arquivo" => "El_agua_es_vida_mp4",
                        "email" => "mailto:?subject=".traduz("Projeto Hydros")." - El agua es vida&body=".current_url()."/featurette_6",
                        "twitter" => "El agua es vida - ".current_url()."/featurette_6",
                        "tumblr" => "El agua es vida",
                        "fb_titulo" => "El agua es vida",
                        "fb_description" => ""
                    );
                    $data['video7'] = array(
                        "slug" => "featurette_7",
                        "link" => "https://vimeo.com/62721298",
                        "id_embed" => "62721298",
                        "imagem" => "7_es.jpg",
                        "titulo" => traduz("Projeto Hydros")." - El agua de cada día",
                        "arquivo" => "El_agua_de_cada_dia_mp4",
                        "email" => "mailto:?subject=".traduz("Projeto Hydros")." - El agua de cada día&body=".current_url()."/featurette_7",
                        "twitter" => "El agua de cada día - ".current_url()."/featurette_7",
                        "tumblr" => "El agua de cada día",
                        "fb_titulo" => "El agua de cada día",
                        "fb_description" => ""
                    );
                    $data['video8'] = array(
                        "slug" => "featurette_8",
                        "link" => "https://vimeo.com/62721297",
                        "id_embed" => "62721297",
                        "imagem" => "8_es.jpg",
                        "titulo" => traduz("Projeto Hydros")." - Como usamos al agua",
                        "arquivo" => "Como_usamos_al_agua_mp4",
                        "email" => "mailto:?subject=".traduz("Projeto Hydros")." - Como usamos al agua&body=".current_url()."/featurette_8",
                        "twitter" => "Como usamos al agua - ".current_url()."/featurette_8",
                        "tumblr" => "Como usamos al agua",
                        "fb_titulo" => "Como usamos al agua",
                        "fb_description" => ""
                    );                    
                    break;
                case 'en':
                    $data['frase_titulo'] = "The “Water within ourselves” video is a tool for the promotion of awareness raise and sensitization that show, in a ludic manner, the real relationship we have with water in all possible aspects. Therefore you can use it as you wish: include it in your Facebook updates and spread it on other social networks, or download it to be used in your work, with your family and friends.";
                    $data['link_video'] = "http://player.vimeo.com/video/37958766?byline=0&portrait=0&loop=1&wmode=transparent&nocache=";
                    $data['video1'] = array(
                        "slug" => "featurette_1",
                        "link" => "https://vimeo.com/62721989",
                        "id_embed" => "62721989",
                        "imagem" => "1_en.jpg",
                        "titulo" => traduz("Projeto Hydros")." - What is Water",
                        "arquivo" => "What_is_Water_mp4",
                        "email" => "mailto:?subject=".traduz("Projeto Hydros")." - What is Water&body=".current_url()."/featurette_1",
                        "twitter" => "What is Water - ".current_url()."/featurette_1",
                        "tumblr" => "What is Water",
                        "fb_titulo" => "What is Water",
                        "fb_description" => ""
                    );
                    $data['video2'] = array(
                        "slug" => "featurette_2",
                        "link" => "https://vimeo.com/62721988",
                        "id_embed" => "62721988",
                        "imagem" => "2_en.jpg",
                        "titulo" => traduz("Projeto Hydros")." - Water is life",
                        "arquivo" => "Water_is_life_mp4",
                        "email" => "mailto:?subject=".traduz("Projeto Hydros")." - Water is life&body=".current_url()."/featurette_2",
                        "twitter" => "Water is life - ".current_url()."/featurette_2",
                        "tumblr" => "Water is life",
                        "fb_titulo" => "Water is life",
                        "fb_description" => ""
                    );
                    $data['video3'] = array(
                        "slug" => "featurette_3",
                        "link" => "https://vimeo.com/62721986",
                        "id_embed" => "62721986",
                        "imagem" => "3_en.jpg",
                        "titulo" => traduz("Projeto Hydros")." - The water on daily basis",
                        "arquivo" => "The_water_on_daily_basis_mp4",
                        "email" => "mailto:?subject=".traduz("Projeto Hydros")." - The water on daily basis&body=".current_url()."/featurette_3",
                        "twitter" => "The water on daily basis - ".current_url()."/featurette_3",
                        "tumblr" => "The water on daily basis",
                        "fb_titulo" => "The water on daily basis",
                        "fb_description" => ""
                    );
                    $data['video4'] = array(
                        "slug" => "featurette_4",
                        "link" => "https://vimeo.com/62721795",
                        "id_embed" => "62721795",
                        "imagem" => "4_en.jpg",
                        "titulo" => traduz("Projeto Hydros")." - Little steps",
                        "arquivo" => "Little_steps_mp4",
                        "email" => "mailto:?subject=".traduz("Projeto Hydros")." - Little steps&body=".current_url()."/featurette_4",
                        "twitter" => "Little steps - ".current_url()."/featurette_4",
                        "tumblr" => "Little steps",
                        "fb_titulo" => "Little steps",
                        "fb_description" => ""
                    );
                    $data['video5'] = array(
                        "slug" => "featurette_5",
                        "link" => "https://vimeo.com/62721794",
                        "id_embed" => "62721794",
                        "imagem" => "5_en.jpg",
                        "titulo" => traduz("Projeto Hydros")." - Human consuption of water",
                        "arquivo" => "Human_consuption_of_water_mp4",
                        "email" => "mailto:?subject=".traduz("Projeto Hydros")." - Human consuption of water&body=".current_url()."/featurette_5",
                        "twitter" => "Human consuption of water - ".current_url()."/featurette_5",
                        "tumblr" => "Human consuption of water",
                        "fb_titulo" => "Human consuption of water",
                        "fb_description" => ""
                    );
                    $data['video6'] = array(
                        "slug" => "featurette_6",
                        "link" => "https://vimeo.com/62721793",
                        "id_embed" => "62721793",
                        "imagem" => "6_en.jpg",
                        "titulo" => traduz("Projeto Hydros")." - How we use water",
                        "arquivo" => "How_we_use_water_mp4",
                        "email" => "mailto:?subject=".traduz("Projeto Hydros")." - How we use water&body=".current_url()."/featurette_6",
                        "twitter" => "How we use water - ".current_url()."/featurette_6",
                        "tumblr" => "How we use water",
                        "fb_titulo" => "How we use water",
                        "fb_description" => ""
                    );
                    $data['video7'] = array(
                        "slug" => "featurette_7",
                        "link" => "https://vimeo.com/62721792",
                        "id_embed" => "62721792",
                        "imagem" => "7_en.jpg",
                        "titulo" => traduz("Projeto Hydros")." - Evolution of the nature",
                        "arquivo" => "Evolution_of_the_nature_mp4",
                        "email" => "mailto:?subject=".traduz("Projeto Hydros")." - Evolution of the nature&body=".current_url()."/featurette_7",
                        "twitter" => "Evolution of the nature - ".current_url()."/featurette_7",
                        "tumblr" => "Evolution of the nature",
                        "fb_titulo" => "Evolution of the nature",
                        "fb_description" => ""
                    );
                    $data['video8'] = array(
                        "slug" => "featurette_8",
                        "link" => "https://vimeo.com/62721788",
                        "id_embed" => "62721788",
                        "imagem" => "8_en.jpg",
                        "titulo" => traduz("Projeto Hydros")." - Be part of change",
                        "arquivo" => "Be_part_of_change_mp4",
                        "email" => "mailto:?subject=".traduz("Projeto Hydros")." - Be part of change&body=".current_url()."/featurette_8",
                        "twitter" => "Be part of change - ".current_url()."/featurette_8",
                        "tumblr" => "Be part of change",
                        "fb_titulo" => "Be part of change",
                        "fb_description" => ""
                    );
                    break;
                case 'pt':
                    $data['frase_titulo'] = "O vídeo “Água em nós” é uma ferramenta de conscientização e sensibilização que mostra, de forma lúdica, a verdadeira relação que temos com a água em todos os seus aspectos. Portanto, use-o à vontade: publique no seu mural do Facebook e outras redes sociais ou faça o download para usá-lo no ambiente de trabalho, com a família e amigos.";
                    $data['link_video'] = "http://player.vimeo.com/video/37537397?byline=0&portrait=0&loop=1&wmode=transparent&nocache=";
                    $data['video1'] = array(
                        "slug" => "featurette_1",
                        "link" => "https://vimeo.com/62279892",
                        "id_embed" => "62279892",
                        "imagem" => "1_pt.jpg",
                        "titulo" => traduz("Projeto Hydros")." - Faça parte da mudança",
                        "arquivo" => "Faca_parte_da_mudanca_mp4",
                        "email" => "mailto:?subject=".traduz("Projeto Hydros")." - Faça parte da mudança&body=".current_url()."/featurette_1",
                        "twitter" => "Faça parte da mudança - ".current_url()."/featurette_1",
                        "tumblr" => "Faça parte da mudança",
                        "fb_titulo" => "Faça parte da mudança",
                        "fb_description" => ""
                    );
                    $data['video2'] = array(
                        "slug" => "featurette_2",
                        "link" => "https://vimeo.com/62279895",
                        "id_embed" => "62279895",
                        "imagem" => "2_pt.jpg",
                        "titulo" => traduz("Projeto Hydros")." - Água é vida",
                        "arquivo" => "Agua_e_vida_mp4",
                        "email" => "mailto:?subject=".traduz("Projeto Hydros")." - Água é vida&body=".current_url()."/featurette_2",
                        "twitter" => "Água é vida - ".current_url()."/featurette_2",
                        "tumblr" => "Água é vida",
                        "fb_titulo" => "Água é vida",
                        "fb_description" => ""
                    );
                    $data['video3'] = array(
                        "slug" => "featurette_3",
                        "link" => "https://vimeo.com/62279900",
                        "id_embed" => "62279900",
                        "imagem" => "3_pt.jpg",
                        "titulo" => traduz("Projeto Hydros")." - Como usamos a água",
                        "arquivo" => "Como_usamos_a_agua_mp4",
                        "email" => "mailto:?subject=".traduz("Projeto Hydros")." - Como usamos a água&body=".current_url()."/featurette_3",
                        "twitter" => "Como usamos a água - ".current_url()."/featurette_3",
                        "tumblr" => "Como usamos a água",
                        "fb_titulo" => "Como usamos a água",
                        "fb_description" => ""
                    );
                    $data['video4'] = array(
                        "slug" => "featurette_4",
                        "link" => "https://vimeo.com/62279902",
                        "id_embed" => "62279902",
                        "imagem" => "4_pt.jpg",
                        "titulo" => traduz("Projeto Hydros")." - Consumo humano de água",
                        "arquivo" => "Consumo_humano_de_agua_mp4",
                        "email" => "mailto:?subject=".traduz("Projeto Hydros")." - Consumo humano de água&body=".current_url()."/featurette_4",
                        "twitter" => "Consumo humano de água - ".current_url()."/featurette_4",
                        "tumblr" => "Consumo humano de água",
                        "fb_titulo" => "Consumo humano de água",
                        "fb_description" => ""
                    );
                    $data['video5'] = array(
                        "slug" => "featurette_5",
                        "link" => "https://vimeo.com/62279903",
                        "id_embed" => "62279903",
                        "imagem" => "5_pt.jpg",
                        "titulo" => traduz("Projeto Hydros")." - Evolução da natureza",
                        "arquivo" => "Evolucao_da_natureza_mp4",
                        "email" => "mailto:?subject=".traduz("Projeto Hydros")." - Evolução da natureza&body=".current_url()."/featurette_5",
                        "twitter" => "Evolução da natureza - ".current_url()."/featurette_5",
                        "tumblr" => "Evolução da natureza",
                        "fb_titulo" => "Evolução da natureza",
                        "fb_description" => ""
                    );
                    $data['video6'] = array(
                        "slug" => "featurette_6",
                        "link" => "https://vimeo.com/62280349",
                        "id_embed" => "62280349",
                        "imagem" => "6_pt.jpg",
                        "titulo" => traduz("Projeto Hydros")." - O que é água",
                        "arquivo" => "O_que_e_agua_mp4",
                        "email" => "mailto:?subject=".traduz("Projeto Hydros")." - O que é água&body=".current_url()."/featurette_6",
                        "twitter" => "O que é água - ".current_url()."/featurette_6",
                        "tumblr" => "O que é água",
                        "fb_titulo" => "O que é água",
                        "fb_description" => ""
                    );
                    $data['video7'] = array(
                        "slug" => "featurette_7",
                        "link" => "https://vimeo.com/62280572",
                        "id_embed" => "62280572",
                        "imagem" => "7_pt.jpg",
                        "titulo" => traduz("Projeto Hydros")." - Pequenas atitudes",
                        "arquivo" => "Pequenas_atitudes_mp4",
                        "email" => "mailto:?subject=".traduz("Projeto Hydros")." - Pequenas atitudes&body=".current_url()."/featurette_7",
                        "twitter" => "Pequenas atitudes - ".current_url()."/featurette_7",
                        "tumblr" => "Pequenas atitudes",
                        "fb_titulo" => "Pequenas atitudes",
                        "fb_description" => ""
                    );
                    $data['video8'] = array(
                        "slug" => "featurette_8",
                        "link" => "https://vimeo.com/62811973",
                        "id_embed" => "62811973",
                        "imagem" => "8_pt.jpg",
                        "titulo" => traduz("Projeto Hydros")." - A água no dia a dia",
                        "arquivo" => "A_agua_no_dia_a_dia_mp4",
                        "email" => "mailto:?subject=".traduz("Projeto Hydros")." - A água no dia a dia&body=".current_url()."/featurette_8",
                        "twitter" => "A água no dia a dia - ".current_url()."/featurette_8",
                        "tumblr" => "A água no dia a dia",
                        "fb_titulo" => "A água no dia a dia",
                        "fb_description" => ""
                    );
                    break;
            }

			$this->load->view('materiais/videos', $data);

		}else{

			$array_videos = array(
				'cocorico1',
				'cocorico2',
				'cocorico3',
				'cocorico4',
                'cocorico5',
                'featurette_1',
                'featurette_2',
                'featurette_3',
                'featurette_4',
                'featurette_5',
                'featurette_6',
                'featurette_7',
                'featurette_8'
			);

			if(!in_array($slug, $array_videos))
				redirect('materiais/videos');

            switch ($this->session->userdata('language')) {
                case 'es':
                    $data['frase_titulo'] = "El video “El agua en nosotros” es una herramienta de promoción de toma de conciencia y sensibilización que muestra, de forma lúdica, la verdadera relación que tenemos con el agua en todos sus aspectos.  Por lo tanto, úsalo como quieras: publica en tu página principal de Facebook y en otras redes sociales o haz el download para usarlo en tu trabajo, con tu familia y amigos.";
                    $data['link_video'] = "http://player.vimeo.com/video/46706692?byline=0&portrait=0&loop=1&wmode=transparent&nocache=";
                    $data['featurette_1'] = array(
                        "slug" => "featurette_1",
                        "link" => "https://vimeo.com/62721452",
                        "id_embed" => "62721452",
                        "imagem" => "1_es.jpg",
                        "titulo" => "Qué es el agua",
                        'subtitulo' => "",
                        "arquivo" => "Que_es_el_agua_mp4",
                        "email" => "mailto:?subject=".traduz("Projeto Hydros")." - Qué es el agua&body=".current_url()."/featurette_1",
                        "twitter" => "Qué es el agua - ".current_url()."/featurette_1",
                        "tumblr" => "Qué es el agua",
                        "fb_titulo" => "Qué es el agua",
                        "fb_description" => "",
                        "embed" => "<iframe src='http://player.vimeo.com/video/62721452?byline=0&portrait=0&loop=1&wmode=transparent&nocache=".time()."' width='640' height='360' frameborder='0' webkitAllowFullScreen allowFullScreen></iframe>",
                        'texto_email' => traduz("Os vídeos “Água em nós”, composto por um filme mais completo e outros oito featurettes, são ferramentas de conscientização e sensibilização que mostram, de forma lúdica, a verdadeira relação que temos com a água em todos os seus aspectos. Portanto, use-os à vontade: publique no seu mural do Facebook e outras redes sociais ou faça o download para usá-los no ambiente de trabalho, com a família e amigos.")."\n".current_url(),
                        'assunto_email' => traduz("Conheça o Projeto Hydros!") . " - Qué es el agua",
                        'texto_tweet' => traduz("Conheça o Projeto Hydros!") . " - Qué es el agua " . current_url(),
                        'titulo_pagina' => traduz("Conheça o Projeto Hydros!") . " - Qué es el agua",
                        'descricao_pagina' => traduz("Os vídeos “Água em nós”, composto por um filme mais completo e outros oito featurettes, são ferramentas de conscientização e sensibilização que mostram, de forma lúdica, a verdadeira relação que temos com a água em todos os seus aspectos. Portanto, use-os à vontade: publique no seu mural do Facebook e outras redes sociais ou faça o download para usá-los no ambiente de trabalho, com a família e amigos.")
                    );
                    $data['featurette_2'] = array(
                        "slug" => "featurette_2",
                        "link" => "https://vimeo.com/62721451",
                        "id_embed" => "62721451",
                        "imagem" => "2_es.jpg",
                        "titulo" => "Sé parte del cambio",
                        'subtitulo' => "",
                        "arquivo" => "Se_parte_del_cambio_mp4",
                        "email" => "mailto:?subject=".traduz("Projeto Hydros")." - Sé parte del cambio&body=".current_url(),
                        "twitter" => "Sé parte del cambio - ".current_url(),
                        "tumblr" => "Sé parte del cambio",
                        "fb_titulo" => "Sé parte del cambio",
                        "fb_description" => "",
                        "embed" => "<iframe src='http://player.vimeo.com/video/62721451?byline=0&portrait=0&loop=1&wmode=transparent&nocache=".time()."' width='640' height='360' frameborder='0' webkitAllowFullScreen allowFullScreen></iframe>",
                        'texto_email' => traduz("Os vídeos “Água em nós”, composto por um filme mais completo e outros oito featurettes, são ferramentas de conscientização e sensibilização que mostram, de forma lúdica, a verdadeira relação que temos com a água em todos os seus aspectos. Portanto, use-os à vontade: publique no seu mural do Facebook e outras redes sociais ou faça o download para usá-los no ambiente de trabalho, com a família e amigos.")."\n".current_url(),
                        'assunto_email' => traduz("Conheça o Projeto Hydros!") . " - Sé parte del cambio",
                        'texto_tweet' => traduz("Conheça o Projeto Hydros!") . " - Sé parte del cambio " . current_url(),
                        'titulo_pagina' => traduz("Conheça o Projeto Hydros!") . " - Sé parte del cambio",
                        'descricao_pagina' => traduz("Os vídeos “Água em nós”, composto por um filme mais completo e outros oito featurettes, são ferramentas de conscientização e sensibilização que mostram, de forma lúdica, a verdadeira relação que temos com a água em todos os seus aspectos. Portanto, use-os à vontade: publique no seu mural do Facebook e outras redes sociais ou faça o download para usá-los no ambiente de trabalho, com a família e amigos.")
                    );
                    $data['featurette_3'] = array(
                        "slug" => "featurette_3",
                        "link" => "https://vimeo.com/62721450",
                        "id_embed" => "62721450",
                        "imagem" => "3_es.jpg",
                        "titulo" => "Las pequenas actitudes",
                        'subtitulo' => "",
                        "arquivo" => "las_pequenas_actitudes_mp4",
                        "email" => "mailto:?subject=".traduz("Projeto Hydros")." - Las pequenas actitudes&body=".current_url(),
                        "twitter" => "Las pequenas actitudes - ".current_url(),
                        "tumblr" => "Las pequenas actitudes",
                        "fb_titulo" => "Las pequenas actitudes",
                        "fb_description" => "",
                        "embed" => "<iframe src='http://player.vimeo.com/video/62721450?byline=0&portrait=0&loop=1&wmode=transparent&nocache=".time()."' width='640' height='360' frameborder='0' webkitAllowFullScreen allowFullScreen></iframe>",
                        'texto_email' => traduz("Os vídeos “Água em nós”, composto por um filme mais completo e outros oito featurettes, são ferramentas de conscientização e sensibilização que mostram, de forma lúdica, a verdadeira relação que temos com a água em todos os seus aspectos. Portanto, use-os à vontade: publique no seu mural do Facebook e outras redes sociais ou faça o download para usá-los no ambiente de trabalho, com a família e amigos.")."\n".current_url(),
                        'assunto_email' => traduz("Conheça o Projeto Hydros!") . " - Las pequenas actitudes",
                        'texto_tweet' => traduz("Conheça o Projeto Hydros!") . " - Las pequenas actitudes " . current_url(),
                        'titulo_pagina' => traduz("Conheça o Projeto Hydros!") . " - Las pequenas actitudes",
                        'descricao_pagina' => traduz("Os vídeos “Água em nós”, composto por um filme mais completo e outros oito featurettes, são ferramentas de conscientização e sensibilização que mostram, de forma lúdica, a verdadeira relação que temos com a água em todos os seus aspectos. Portanto, use-os à vontade: publique no seu mural do Facebook e outras redes sociais ou faça o download para usá-los no ambiente de trabalho, com a família e amigos.")
                    );
                    $data['featurette_4'] = array(
                        "slug" => "featurette_4",
                        "link" => "https://vimeo.com/62721302",
                        "id_embed" => "62721302",
                        "imagem" => "4_es.jpg",
                        "titulo" => "Evolución de la naturaleza",
                        'subtitulo' => "",
                        "arquivo" => "Evolucion_de_la_naturaleza_mp4",
                        "email" => "mailto:?subject=".traduz("Projeto Hydros")." - Evolución de la naturaleza&body=".current_url(),
                        "twitter" => "Evolución de la naturaleza - ".current_url(),
                        "tumblr" => "Evolución de la naturaleza",
                        "fb_titulo" => "Evolución de la naturaleza",
                        "fb_description" => "",
                        "embed" => "<iframe src='http://player.vimeo.com/video/62721302?byline=0&portrait=0&loop=1&wmode=transparent&nocache=".time()."' width='640' height='360' frameborder='0' webkitAllowFullScreen allowFullScreen></iframe>",
                        'texto_email' => traduz("Os vídeos “Água em nós”, composto por um filme mais completo e outros oito featurettes, são ferramentas de conscientização e sensibilização que mostram, de forma lúdica, a verdadeira relação que temos com a água em todos os seus aspectos. Portanto, use-os à vontade: publique no seu mural do Facebook e outras redes sociais ou faça o download para usá-los no ambiente de trabalho, com a família e amigos.")."\n".current_url(),
                        'assunto_email' => traduz("Conheça o Projeto Hydros!") . " - Evolución de la naturaleza",
                        'texto_tweet' => traduz("Conheça o Projeto Hydros!") . " - Evolución de la naturaleza " . current_url(),
                        'titulo_pagina' => traduz("Conheça o Projeto Hydros!") . " - Evolución de la naturaleza",
                        'descricao_pagina' => traduz("Os vídeos “Água em nós”, composto por um filme mais completo e outros oito featurettes, são ferramentas de conscientização e sensibilização que mostram, de forma lúdica, a verdadeira relação que temos com a água em todos os seus aspectos. Portanto, use-os à vontade: publique no seu mural do Facebook e outras redes sociais ou faça o download para usá-los no ambiente de trabalho, com a família e amigos.")
                    );
                    $data['featurette_5'] = array(
                        "slug" => "featurette_5",
                        "link" => "https://vimeo.com/62721300",
                        "id_embed" => "62721300",
                        "imagem" => "5_es.jpg",
                        "titulo" => "El consumo humano de agua",
                        'subtitulo' => "",
                        "arquivo" => "El_consumo_humano_de_agua_mp4",
                        "email" => "mailto:?subject=".traduz("Projeto Hydros")." - El consumo humano de agua&body=".current_url(),
                        "twitter" => "El consumo humano de agua - ".current_url(),
                        "tumblr" => "El consumo humano de agua",
                        "fb_titulo" => "El consumo humano de agua",
                        "fb_description" => "",
                        "embed" => "<iframe src='http://player.vimeo.com/video/62721300?byline=0&portrait=0&loop=1&wmode=transparent&nocache=".time()."' width='640' height='360' frameborder='0' webkitAllowFullScreen allowFullScreen></iframe>",
                        'texto_email' => traduz("Os vídeos “Água em nós”, composto por um filme mais completo e outros oito featurettes, são ferramentas de conscientização e sensibilização que mostram, de forma lúdica, a verdadeira relação que temos com a água em todos os seus aspectos. Portanto, use-os à vontade: publique no seu mural do Facebook e outras redes sociais ou faça o download para usá-los no ambiente de trabalho, com a família e amigos.")."\n".current_url(),
                        'assunto_email' => traduz("Conheça o Projeto Hydros!") . " - El consumo humano de agua",
                        'texto_tweet' => traduz("Conheça o Projeto Hydros!") . " - El consumo humano de agua " . current_url(),
                        'titulo_pagina' => traduz("Conheça o Projeto Hydros!") . " - El consumo humano de agua",
                        'descricao_pagina' => traduz("Os vídeos “Água em nós”, composto por um filme mais completo e outros oito featurettes, são ferramentas de conscientização e sensibilização que mostram, de forma lúdica, a verdadeira relação que temos com a água em todos os seus aspectos. Portanto, use-os à vontade: publique no seu mural do Facebook e outras redes sociais ou faça o download para usá-los no ambiente de trabalho, com a família e amigos.")
                    );
                    $data['featurette_6'] = array(
                        "slug" => "featurette_6",
                        "link" => "https://vimeo.com/62721299",
                        "id_embed" => "62721299",
                        "imagem" => "6_es.jpg",
                        "titulo" => "El agua es vida",
                        'subtitulo' => "",
                        "arquivo" => "El_agua_es_vida_mp4",
                        "email" => "mailto:?subject=".traduz("Projeto Hydros")." - El agua es vida&body=".current_url(),
                        "twitter" => "El agua es vida - ".current_url(),
                        "tumblr" => "El agua es vida",
                        "fb_titulo" => "El agua es vida",
                        "fb_description" => "",
                        "embed" => "<iframe src='http://player.vimeo.com/video/62721299?byline=0&portrait=0&loop=1&wmode=transparent&nocache=".time()."' width='640' height='360' frameborder='0' webkitAllowFullScreen allowFullScreen></iframe>",
                        'texto_email' => traduz("Os vídeos “Água em nós”, composto por um filme mais completo e outros oito featurettes, são ferramentas de conscientização e sensibilização que mostram, de forma lúdica, a verdadeira relação que temos com a água em todos os seus aspectos. Portanto, use-os à vontade: publique no seu mural do Facebook e outras redes sociais ou faça o download para usá-los no ambiente de trabalho, com a família e amigos.")."\n".current_url(),
                        'assunto_email' => traduz("Conheça o Projeto Hydros!") . " - El agua es vida",
                        'texto_tweet' => traduz("Conheça o Projeto Hydros!") . " - El agua es vida " . current_url(),
                        'titulo_pagina' => traduz("Conheça o Projeto Hydros!") . " - El agua es vida",
                        'descricao_pagina' => traduz("Os vídeos “Água em nós”, composto por um filme mais completo e outros oito featurettes, são ferramentas de conscientização e sensibilização que mostram, de forma lúdica, a verdadeira relação que temos com a água em todos os seus aspectos. Portanto, use-os à vontade: publique no seu mural do Facebook e outras redes sociais ou faça o download para usá-los no ambiente de trabalho, com a família e amigos.")
                    );
                    $data['featurette_7'] = array(
                        "slug" => "featurette_7",
                        "link" => "https://vimeo.com/62721298",
                        "id_embed" => "62721298",
                        "imagem" => "7_es.jpg",
                        "titulo" => "El agua de cada día",
                        'subtitulo' => "",
                        "arquivo" => "El_agua_de_cada_dia_mp4",
                        "email" => "mailto:?subject=".traduz("Projeto Hydros")." - El agua de cada día&body=".current_url(),
                        "twitter" => "El agua de cada día - ".current_url(),
                        "tumblr" => "El agua de cada día",
                        "fb_titulo" => "El agua de cada día",
                        "fb_description" => "",
                        "embed" => "<iframe src='http://player.vimeo.com/video/62721298?byline=0&portrait=0&loop=1&wmode=transparent&nocache=".time()."' width='640' height='360' frameborder='0' webkitAllowFullScreen allowFullScreen></iframe>",
                        'texto_email' => traduz("Os vídeos “Água em nós”, composto por um filme mais completo e outros oito featurettes, são ferramentas de conscientização e sensibilização que mostram, de forma lúdica, a verdadeira relação que temos com a água em todos os seus aspectos. Portanto, use-os à vontade: publique no seu mural do Facebook e outras redes sociais ou faça o download para usá-los no ambiente de trabalho, com a família e amigos.")."\n".current_url(),
                        'assunto_email' => traduz("Conheça o Projeto Hydros!") . " - El agua de cada día",
                        'texto_tweet' => traduz("Conheça o Projeto Hydros!") . " - El agua de cada día " . current_url(),
                        'titulo_pagina' => traduz("Conheça o Projeto Hydros!") . " - El agua de cada día",
                        'descricao_pagina' => traduz("Os vídeos “Água em nós”, composto por um filme mais completo e outros oito featurettes, são ferramentas de conscientização e sensibilização que mostram, de forma lúdica, a verdadeira relação que temos com a água em todos os seus aspectos. Portanto, use-os à vontade: publique no seu mural do Facebook e outras redes sociais ou faça o download para usá-los no ambiente de trabalho, com a família e amigos.")
                    );
                    $data['featurette_8'] = array(
                        "slug" => "featurette_8",
                        "link" => "https://vimeo.com/62721297",
                        "id_embed" => "62721297",
                        "imagem" => "8_es.jpg",
                        "titulo" => "Como usamos al agua",
                        'subtitulo' => "",
                        "arquivo" => "Como_usamos_al_agua_mp4",
                        "email" => "mailto:?subject=".traduz("Projeto Hydros")." - Como usamos al agua&body=".current_url(),
                        "twitter" => "Como usamos al agua - ".current_url(),
                        "tumblr" => "Como usamos al agua",
                        "fb_titulo" => "Como usamos al agua",
                        "fb_description" => "",
                        "embed" => "<iframe src='http://player.vimeo.com/video/62721297?byline=0&portrait=0&loop=1&wmode=transparent&nocache=".time()."' width='640' height='360' frameborder='0' webkitAllowFullScreen allowFullScreen></iframe>",
                        'texto_email' => traduz("Os vídeos “Água em nós”, composto por um filme mais completo e outros oito featurettes, são ferramentas de conscientização e sensibilização que mostram, de forma lúdica, a verdadeira relação que temos com a água em todos os seus aspectos. Portanto, use-os à vontade: publique no seu mural do Facebook e outras redes sociais ou faça o download para usá-los no ambiente de trabalho, com a família e amigos.")."\n".current_url(),
                        'assunto_email' => traduz("Conheça o Projeto Hydros!") . " - Como usamos al agua",
                        'texto_tweet' => traduz("Conheça o Projeto Hydros!") . " - Como usamos al agua " . current_url(),
                        'titulo_pagina' => traduz("Conheça o Projeto Hydros!") . " - Como usamos al agua",
                        'descricao_pagina' => traduz("Os vídeos “Água em nós”, composto por um filme mais completo e outros oito featurettes, são ferramentas de conscientização e sensibilização que mostram, de forma lúdica, a verdadeira relação que temos com a água em todos os seus aspectos. Portanto, use-os à vontade: publique no seu mural do Facebook e outras redes sociais ou faça o download para usá-los no ambiente de trabalho, com a família e amigos.")
                    );
                    $data['cocorico1'] = FALSE;
                    $data['cocorico2'] = FALSE;
                    $data['cocorico3'] = FALSE;
                    $data['cocorico4'] = FALSE;
                    $data['cocorico5'] = FALSE;                 
                    break;
                case 'en':
                    $data['frase_titulo'] = "The “Water within ourselves” video is a tool for the promotion of awareness raise and sensitization that show, in a ludic manner, the real relationship we have with water in all possible aspects. Therefore you can use it as you wish: include it in your Facebook updates and spread it on other social networks, or download it to be used in your work, with your family and friends.";
                    $data['link_video'] = "http://player.vimeo.com/video/37958766?byline=0&portrait=0&loop=1&wmode=transparent&nocache=";
                    $data['featurette_1'] = array(
                        "slug" => "featurette_1",
                        "link" => "https://vimeo.com/62721989",
                        "id_embed" => "62721989",
                        "imagem" => "1_en.jpg",
                        "titulo" => "What is Water",
                        'subtitulo' => "",
                        "arquivo" => "What_is_Water_mp4",
                        "email" => "mailto:?subject=".traduz("Projeto Hydros")." - What is Water&body=".current_url(),
                        "twitter" => "What is Water - ".current_url(),
                        "tumblr" => "What is Water",
                        "fb_titulo" => "What is Water",
                        "fb_description" => "",
                        "embed" => "<iframe src='http://player.vimeo.com/video/62721989?byline=0&portrait=0&loop=1&wmode=transparent&nocache=".time()."' width='640' height='360' frameborder='0' webkitAllowFullScreen allowFullScreen></iframe>",
                        'texto_email' => traduz("Os vídeos “Água em nós”, composto por um filme mais completo e outros oito featurettes, são ferramentas de conscientização e sensibilização que mostram, de forma lúdica, a verdadeira relação que temos com a água em todos os seus aspectos. Portanto, use-os à vontade: publique no seu mural do Facebook e outras redes sociais ou faça o download para usá-los no ambiente de trabalho, com a família e amigos.")."\n".current_url(),
                        'assunto_email' => traduz("Conheça o Projeto Hydros!") . " - What is Water",
                        'texto_tweet' => traduz("Conheça o Projeto Hydros!") . " - What is Water " . current_url(),
                        'titulo_pagina' => traduz("Conheça o Projeto Hydros!") . " - What is Water",
                        'descricao_pagina' => traduz("Os vídeos “Água em nós”, composto por um filme mais completo e outros oito featurettes, são ferramentas de conscientização e sensibilização que mostram, de forma lúdica, a verdadeira relação que temos com a água em todos os seus aspectos. Portanto, use-os à vontade: publique no seu mural do Facebook e outras redes sociais ou faça o download para usá-los no ambiente de trabalho, com a família e amigos.")
                    );
                    $data['featurette_2'] = array(
                        "slug" => "featurette_2",
                        "link" => "https://vimeo.com/62721988",
                        "id_embed" => "62721988",
                        "imagem" => "2_en.jpg",
                        "titulo" => "Water is life",
                        'subtitulo' => "",
                        "arquivo" => "Water_is_life_mp4",
                        "email" => "mailto:?subject=".traduz("Projeto Hydros")." - Water is life&body=".current_url(),
                        "twitter" => "Water is life - ".current_url(),
                        "tumblr" => "Water is life",
                        "fb_titulo" => "Water is life",
                        "fb_description" => "",
                        "embed" => "<iframe src='http://player.vimeo.com/video/62721988?byline=0&portrait=0&loop=1&wmode=transparent&nocache=".time()."' width='640' height='360' frameborder='0' webkitAllowFullScreen allowFullScreen></iframe>",
                        'texto_email' => traduz("Os vídeos “Água em nós”, composto por um filme mais completo e outros oito featurettes, são ferramentas de conscientização e sensibilização que mostram, de forma lúdica, a verdadeira relação que temos com a água em todos os seus aspectos. Portanto, use-os à vontade: publique no seu mural do Facebook e outras redes sociais ou faça o download para usá-los no ambiente de trabalho, com a família e amigos.")."\n".current_url(),
                        'assunto_email' => traduz("Conheça o Projeto Hydros!") . " - Water is life",
                        'texto_tweet' => traduz("Conheça o Projeto Hydros!") . " - Water is life " . current_url(),
                        'titulo_pagina' => traduz("Conheça o Projeto Hydros!") . " - Water is life",
                        'descricao_pagina' => traduz("Os vídeos “Água em nós”, composto por um filme mais completo e outros oito featurettes, são ferramentas de conscientização e sensibilização que mostram, de forma lúdica, a verdadeira relação que temos com a água em todos os seus aspectos. Portanto, use-os à vontade: publique no seu mural do Facebook e outras redes sociais ou faça o download para usá-los no ambiente de trabalho, com a família e amigos.")
                    );
                    $data['featurette_3'] = array(
                        "slug" => "featurette_3",
                        "link" => "https://vimeo.com/62721986",
                        "id_embed" => "62721986",
                        "imagem" => "3_en.jpg",
                        "titulo" => "The water on daily basis",
                        'subtitulo' => "",
                        "arquivo" => "The_water_on_daily_basis_mp4",
                        "email" => "mailto:?subject=".traduz("Projeto Hydros")." - The water on daily basis&body=".current_url(),
                        "twitter" => "The water on daily basis - ".current_url(),
                        "tumblr" => "The water on daily basis",
                        "fb_titulo" => "The water on daily basis",
                        "fb_description" => "",
                        "embed" => "<iframe src='http://player.vimeo.com/video/62721986?byline=0&portrait=0&loop=1&wmode=transparent&nocache=".time()."' width='640' height='360' frameborder='0' webkitAllowFullScreen allowFullScreen></iframe>",
                        'texto_email' => traduz("Os vídeos “Água em nós”, composto por um filme mais completo e outros oito featurettes, são ferramentas de conscientização e sensibilização que mostram, de forma lúdica, a verdadeira relação que temos com a água em todos os seus aspectos. Portanto, use-os à vontade: publique no seu mural do Facebook e outras redes sociais ou faça o download para usá-los no ambiente de trabalho, com a família e amigos.")."\n".current_url(),
                        'assunto_email' => traduz("Conheça o Projeto Hydros!") . " - The water on daily basis",
                        'texto_tweet' => traduz("Conheça o Projeto Hydros!") . " - The water on daily basis " . current_url(),
                        'titulo_pagina' => traduz("Conheça o Projeto Hydros!") . " - The water on daily basis",
                        'descricao_pagina' => traduz("Os vídeos “Água em nós”, composto por um filme mais completo e outros oito featurettes, são ferramentas de conscientização e sensibilização que mostram, de forma lúdica, a verdadeira relação que temos com a água em todos os seus aspectos. Portanto, use-os à vontade: publique no seu mural do Facebook e outras redes sociais ou faça o download para usá-los no ambiente de trabalho, com a família e amigos.")
                    );
                    $data['featurette_4'] = array(
                        "slug" => "featurette_4",
                        "link" => "https://vimeo.com/62721795",
                        "id_embed" => "62721795",
                        "imagem" => "4_en.jpg",
                        "titulo" => "Little steps",
                        'subtitulo' => "",
                        "arquivo" => "Little_steps_mp4",
                        "email" => "mailto:?subject=".traduz("Projeto Hydros")." - Little steps&body=".current_url(),
                        "twitter" => "Little steps - ".current_url(),
                        "tumblr" => "Little steps",
                        "fb_titulo" => "Little steps",
                        "fb_description" => "",
                        "embed" => "<iframe src='http://player.vimeo.com/video/62721795?byline=0&portrait=0&loop=1&wmode=transparent&nocache=".time()."' width='640' height='360' frameborder='0' webkitAllowFullScreen allowFullScreen></iframe>",
                        'texto_email' => traduz("Os vídeos “Água em nós”, composto por um filme mais completo e outros oito featurettes, são ferramentas de conscientização e sensibilização que mostram, de forma lúdica, a verdadeira relação que temos com a água em todos os seus aspectos. Portanto, use-os à vontade: publique no seu mural do Facebook e outras redes sociais ou faça o download para usá-los no ambiente de trabalho, com a família e amigos.")."\n".current_url(),
                        'assunto_email' => traduz("Conheça o Projeto Hydros!") . " - Little steps",
                        'texto_tweet' => traduz("Conheça o Projeto Hydros!") . " - Little steps " . current_url(),
                        'titulo_pagina' => traduz("Conheça o Projeto Hydros!") . " - Little steps",
                        'descricao_pagina' => traduz("Os vídeos “Água em nós”, composto por um filme mais completo e outros oito featurettes, são ferramentas de conscientização e sensibilização que mostram, de forma lúdica, a verdadeira relação que temos com a água em todos os seus aspectos. Portanto, use-os à vontade: publique no seu mural do Facebook e outras redes sociais ou faça o download para usá-los no ambiente de trabalho, com a família e amigos.")
                    );
                    $data['featurette_5'] = array(
                        "slug" => "featurette_5",
                        "link" => "https://vimeo.com/62721794",
                        "id_embed" => "62721794",
                        "imagem" => "5_en.jpg",
                        "titulo" => "Human consuption of water",
                        'subtitulo' => "",
                        "arquivo" => "Human_consuption_of_water_mp4",
                        "email" => "mailto:?subject=".traduz("Projeto Hydros")." - Human consuption of water&body=".current_url(),
                        "twitter" => "Human consuption of water - ".current_url(),
                        "tumblr" => "Human consuption of water",
                        "fb_titulo" => "Human consuption of water",
                        "fb_description" => "",
                        "embed" => "<iframe src='http://player.vimeo.com/video/62721794?byline=0&portrait=0&loop=1&wmode=transparent&nocache=".time()."' width='640' height='360' frameborder='0' webkitAllowFullScreen allowFullScreen></iframe>",
                        'texto_email' => traduz("Os vídeos “Água em nós”, composto por um filme mais completo e outros oito featurettes, são ferramentas de conscientização e sensibilização que mostram, de forma lúdica, a verdadeira relação que temos com a água em todos os seus aspectos. Portanto, use-os à vontade: publique no seu mural do Facebook e outras redes sociais ou faça o download para usá-los no ambiente de trabalho, com a família e amigos.")."\n".current_url(),
                        'assunto_email' => traduz("Conheça o Projeto Hydros!") . " - Human consuption of water",
                        'texto_tweet' => traduz("Conheça o Projeto Hydros!") . " - Human consuption of water " . current_url(),
                        'titulo_pagina' => traduz("Conheça o Projeto Hydros!") . " - Human consuption of water",
                        'descricao_pagina' => traduz("Os vídeos “Água em nós”, composto por um filme mais completo e outros oito featurettes, são ferramentas de conscientização e sensibilização que mostram, de forma lúdica, a verdadeira relação que temos com a água em todos os seus aspectos. Portanto, use-os à vontade: publique no seu mural do Facebook e outras redes sociais ou faça o download para usá-los no ambiente de trabalho, com a família e amigos.")
                    );
                    $data['featurette_6'] = array(
                        "slug" => "featurette_6",
                        "link" => "https://vimeo.com/62721793",
                        "id_embed" => "62721793",
                        "imagem" => "6_en.jpg",
                        "titulo" => "How we use water",
                        'subtitulo' => "",
                        "arquivo" => "How_we_use_water_mp4",
                        "email" => "mailto:?subject=".traduz("Projeto Hydros")." - How we use water&body=".current_url(),
                        "twitter" => "How we use water - ".current_url(),
                        "tumblr" => "How we use water",
                        "fb_titulo" => "How we use water",
                        "fb_description" => "",
                        "embed" => "<iframe src='http://player.vimeo.com/video/62721793?byline=0&portrait=0&loop=1&wmode=transparent&nocache=".time()."' width='640' height='360' frameborder='0' webkitAllowFullScreen allowFullScreen></iframe>",
                        'texto_email' => traduz("Os vídeos “Água em nós”, composto por um filme mais completo e outros oito featurettes, são ferramentas de conscientização e sensibilização que mostram, de forma lúdica, a verdadeira relação que temos com a água em todos os seus aspectos. Portanto, use-os à vontade: publique no seu mural do Facebook e outras redes sociais ou faça o download para usá-los no ambiente de trabalho, com a família e amigos.")."\n".current_url(),
                        'assunto_email' => traduz("Conheça o Projeto Hydros!") . " - How we use water",
                        'texto_tweet' => traduz("Conheça o Projeto Hydros!") . " - How we use water " . current_url(),
                        'titulo_pagina' => traduz("Conheça o Projeto Hydros!") . " - How we use water",
                        'descricao_pagina' => traduz("Os vídeos “Água em nós”, composto por um filme mais completo e outros oito featurettes, são ferramentas de conscientização e sensibilização que mostram, de forma lúdica, a verdadeira relação que temos com a água em todos os seus aspectos. Portanto, use-os à vontade: publique no seu mural do Facebook e outras redes sociais ou faça o download para usá-los no ambiente de trabalho, com a família e amigos.")
                    );
                    $data['featurette_7'] = array(
                        "slug" => "featurette_7",
                        "link" => "https://vimeo.com/62721792",
                        "id_embed" => "62721792",
                        "imagem" => "7_en.jpg",
                        "titulo" => "Evolution of the nature",
                        'subtitulo' => "",
                        "arquivo" => "Evolution_of_the_nature_mp4",
                        "email" => "mailto:?subject=".traduz("Projeto Hydros")." - Evolution of the nature&body=".current_url(),
                        "twitter" => "Evolution of the nature - ".current_url(),
                        "tumblr" => "Evolution of the nature",
                        "fb_titulo" => "Evolution of the nature",
                        "fb_description" => "",
                        "embed" => "<iframe src='http://player.vimeo.com/video/62721792?byline=0&portrait=0&loop=1&wmode=transparent&nocache=".time()."' width='640' height='360' frameborder='0' webkitAllowFullScreen allowFullScreen></iframe>",
                        'texto_email' => traduz("Os vídeos “Água em nós”, composto por um filme mais completo e outros oito featurettes, são ferramentas de conscientização e sensibilização que mostram, de forma lúdica, a verdadeira relação que temos com a água em todos os seus aspectos. Portanto, use-os à vontade: publique no seu mural do Facebook e outras redes sociais ou faça o download para usá-los no ambiente de trabalho, com a família e amigos.")."\n".current_url(),
                        'assunto_email' => traduz("Conheça o Projeto Hydros!") . " - Evolution of the nature",
                        'texto_tweet' => traduz("Conheça o Projeto Hydros!") . " - Evolution of the nature " . current_url(),
                        'titulo_pagina' => traduz("Conheça o Projeto Hydros!") . " - Evolution of the nature",
                        'descricao_pagina' => traduz("Os vídeos “Água em nós”, composto por um filme mais completo e outros oito featurettes, são ferramentas de conscientização e sensibilização que mostram, de forma lúdica, a verdadeira relação que temos com a água em todos os seus aspectos. Portanto, use-os à vontade: publique no seu mural do Facebook e outras redes sociais ou faça o download para usá-los no ambiente de trabalho, com a família e amigos.")
                    );
                    $data['featurette_8'] = array(
                        "slug" => "featurette_8",
                        "link" => "https://vimeo.com/62721788",
                        "id_embed" => "62721788",
                        "imagem" => "8_en.jpg",
                        "titulo" => "Be part of change",
                        'subtitulo' => "",
                        "arquivo" => "Be_part_of_change_mp4",
                        "email" => "mailto:?subject=".traduz("Projeto Hydros")." - Be part of change&body=".current_url(),
                        "twitter" => "Be part of change - ".current_url(),
                        "tumblr" => "Be part of change",
                        "fb_titulo" => "Be part of change",
                        "fb_description" => "",
                        "embed" => "<iframe src='http://player.vimeo.com/video/62721788?byline=0&portrait=0&loop=1&wmode=transparent&nocache=".time()."' width='640' height='360' frameborder='0' webkitAllowFullScreen allowFullScreen></iframe>",
                        'texto_email' => traduz("Os vídeos “Água em nós”, composto por um filme mais completo e outros oito featurettes, são ferramentas de conscientização e sensibilização que mostram, de forma lúdica, a verdadeira relação que temos com a água em todos os seus aspectos. Portanto, use-os à vontade: publique no seu mural do Facebook e outras redes sociais ou faça o download para usá-los no ambiente de trabalho, com a família e amigos.")."\n".current_url(),
                        'assunto_email' => traduz("Conheça o Projeto Hydros!") . " - Be part of change",
                        'texto_tweet' => traduz("Conheça o Projeto Hydros!") . " - Be part of change " . current_url(),
                        'titulo_pagina' => traduz("Conheça o Projeto Hydros!") . " - Be part of change",
                        'descricao_pagina' => traduz("Os vídeos “Água em nós”, composto por um filme mais completo e outros oito featurettes, são ferramentas de conscientização e sensibilização que mostram, de forma lúdica, a verdadeira relação que temos com a água em todos os seus aspectos. Portanto, use-os à vontade: publique no seu mural do Facebook e outras redes sociais ou faça o download para usá-los no ambiente de trabalho, com a família e amigos.")
                    );
                    $data['cocorico1'] = FALSE;
                    $data['cocorico2'] = FALSE;
                    $data['cocorico3'] = FALSE;
                    $data['cocorico4'] = FALSE;
                    $data['cocorico5'] = FALSE;
                    break;
                case 'pt':
                    $data['frase_titulo'] = "O vídeo “Água em nós” é uma ferramenta de conscientização e sensibilização que mostra, de forma lúdica, a verdadeira relação que temos com a água em todos os seus aspectos. Portanto, use-o à vontade: publique no seu mural do Facebook e outras redes sociais ou faça o download para usá-lo no ambiente de trabalho, com a família e amigos.";
                    $data['link_video'] = "http://player.vimeo.com/video/37537397?byline=0&portrait=0&loop=1&wmode=transparent&nocache=";
                    $data['featurette_1'] = array(
                        "slug" => "featurette_1",
                        "link" => "https://vimeo.com/62279892",
                        "id_embed" => "62279892",
                        "imagem" => "1_pt.jpg",
                        "titulo" => "Faça parte da mudança",
                        'subtitulo' => "",
                        "arquivo" => "Faca_parte_da_mudanca_mp4",
                        "email" => "mailto:?subject=".traduz("Projeto Hydros")." - Faça parte da mudança&body=".current_url(),
                        "twitter" => "Faça parte da mudança - ".current_url(),
                        "tumblr" => "Faça parte da mudança",
                        "fb_titulo" => "Faça parte da mudança",
                        "fb_description" => "",
                        "embed" => "<iframe src='http://player.vimeo.com/video/62279892?byline=0&portrait=0&loop=1&wmode=transparent&nocache=".time()."' width='640' height='360' frameborder='0' webkitAllowFullScreen allowFullScreen></iframe>",
                        'texto_email' => traduz("Os vídeos “Água em nós”, composto por um filme mais completo e outros oito featurettes, são ferramentas de conscientização e sensibilização que mostram, de forma lúdica, a verdadeira relação que temos com a água em todos os seus aspectos. Portanto, use-os à vontade: publique no seu mural do Facebook e outras redes sociais ou faça o download para usá-los no ambiente de trabalho, com a família e amigos.")."\n".current_url(),
                        'assunto_email' => traduz("Conheça o Projeto Hydros!") . " - Faça parte da mudança",
                        'texto_tweet' => traduz("Conheça o Projeto Hydros!") . " - Faça parte da mudança " . current_url(),
                        'titulo_pagina' => traduz("Conheça o Projeto Hydros!") . " - Faça parte da mudança",
                        'descricao_pagina' => traduz("Os vídeos “Água em nós”, composto por um filme mais completo e outros oito featurettes, são ferramentas de conscientização e sensibilização que mostram, de forma lúdica, a verdadeira relação que temos com a água em todos os seus aspectos. Portanto, use-os à vontade: publique no seu mural do Facebook e outras redes sociais ou faça o download para usá-los no ambiente de trabalho, com a família e amigos.")
                    );
                    $data['featurette_2'] = array(
                        "slug" => "featurette_2",
                        "link" => "https://vimeo.com/62279895",
                        "id_embed" => "62279895",
                        "imagem" => "2_pt.jpg",
                        "titulo" => "Água é vida",
                        'subtitulo' => "",
                        "arquivo" => "Agua_e_vida_mp4",
                        "email" => "mailto:?subject=".traduz("Projeto Hydros")." - Água é vida&body=".current_url(),
                        "twitter" => "Água é vida - ".current_url(),
                        "tumblr" => "Água é vida",
                        "fb_titulo" => "Água é vida",
                        "fb_description" => "",
                        "embed" => "<iframe src='http://player.vimeo.com/video/62279895?byline=0&portrait=0&loop=1&wmode=transparent&nocache=".time()."' width='640' height='360' frameborder='0' webkitAllowFullScreen allowFullScreen></iframe>",
                        'texto_email' => traduz("Os vídeos “Água em nós”, composto por um filme mais completo e outros oito featurettes, são ferramentas de conscientização e sensibilização que mostram, de forma lúdica, a verdadeira relação que temos com a água em todos os seus aspectos. Portanto, use-os à vontade: publique no seu mural do Facebook e outras redes sociais ou faça o download para usá-los no ambiente de trabalho, com a família e amigos.")."\n".current_url(),
                        'assunto_email' => traduz("Conheça o Projeto Hydros!") . " - Água é vida",
                        'texto_tweet' => traduz("Conheça o Projeto Hydros!") . " - Água é vida " . current_url(),
                        'titulo_pagina' => traduz("Conheça o Projeto Hydros!") . " - Água é vida",
                        'descricao_pagina' => traduz("Os vídeos “Água em nós”, composto por um filme mais completo e outros oito featurettes, são ferramentas de conscientização e sensibilização que mostram, de forma lúdica, a verdadeira relação que temos com a água em todos os seus aspectos. Portanto, use-os à vontade: publique no seu mural do Facebook e outras redes sociais ou faça o download para usá-los no ambiente de trabalho, com a família e amigos.")
                    );
                    $data['featurette_3'] = array(
                        "slug" => "featurette_3",
                        "link" => "https://vimeo.com/62279900",
                        "id_embed" => "62279900",
                        "imagem" => "3_pt.jpg",
                        "titulo" => "Como usamos a água",
                        'subtitulo' => "",
                        "arquivo" => "Como_usamos_a_agua_mp4",
                        "email" => "mailto:?subject=".traduz("Projeto Hydros")." - Como usamos a água&body=".current_url(),
                        "twitter" => "Como usamos a água - ".current_url(),
                        "tumblr" => "Como usamos a água",
                        "fb_titulo" => "Como usamos a água",
                        "fb_description" => "",
                        "embed" => "<iframe src='http://player.vimeo.com/video/62279900?byline=0&portrait=0&loop=1&wmode=transparent&nocache=".time()."' width='640' height='360' frameborder='0' webkitAllowFullScreen allowFullScreen></iframe>",
                        'texto_email' => traduz("Os vídeos “Água em nós”, composto por um filme mais completo e outros oito featurettes, são ferramentas de conscientização e sensibilização que mostram, de forma lúdica, a verdadeira relação que temos com a água em todos os seus aspectos. Portanto, use-os à vontade: publique no seu mural do Facebook e outras redes sociais ou faça o download para usá-los no ambiente de trabalho, com a família e amigos.")."\n".current_url(),
                        'assunto_email' => traduz("Conheça o Projeto Hydros!") . " - Como usamos a água",
                        'texto_tweet' => traduz("Conheça o Projeto Hydros!") . " - Como usamos a água " . current_url(),
                        'titulo_pagina' => traduz("Conheça o Projeto Hydros!") . " - Como usamos a água",
                        'descricao_pagina' => traduz("Os vídeos “Água em nós”, composto por um filme mais completo e outros oito featurettes, são ferramentas de conscientização e sensibilização que mostram, de forma lúdica, a verdadeira relação que temos com a água em todos os seus aspectos. Portanto, use-os à vontade: publique no seu mural do Facebook e outras redes sociais ou faça o download para usá-los no ambiente de trabalho, com a família e amigos.")
                    );
                    $data['featurette_4'] = array(
                        "slug" => "featurette_4",
                        "link" => "https://vimeo.com/62279902",
                        "id_embed" => "62279902",
                        "imagem" => "4_pt.jpg",
                        "titulo" => "Consumo humano de água",
                        'subtitulo' => "",
                        "arquivo" => "Consumo_humano_de_agua_mp4",
                        "email" => "mailto:?subject=".traduz("Projeto Hydros")." - Consumo humano de água&body=".current_url(),
                        "twitter" => "Consumo humano de água - ".current_url(),
                        "tumblr" => "Consumo humano de água",
                        "fb_titulo" => "Consumo humano de água",
                        "fb_description" => "",
                        "embed" => "<iframe src='http://player.vimeo.com/video/62279902?byline=0&portrait=0&loop=1&wmode=transparent&nocache=".time()."' width='640' height='360' frameborder='0' webkitAllowFullScreen allowFullScreen></iframe>",
                        'texto_email' => traduz("Os vídeos “Água em nós”, composto por um filme mais completo e outros oito featurettes, são ferramentas de conscientização e sensibilização que mostram, de forma lúdica, a verdadeira relação que temos com a água em todos os seus aspectos. Portanto, use-os à vontade: publique no seu mural do Facebook e outras redes sociais ou faça o download para usá-los no ambiente de trabalho, com a família e amigos.")."\n".current_url(),
                        'assunto_email' => traduz("Conheça o Projeto Hydros!") . " - Consumo humano de água",
                        'texto_tweet' => traduz("Conheça o Projeto Hydros!") . " - Consumo humano de água " . current_url(),
                        'titulo_pagina' => traduz("Conheça o Projeto Hydros!") . " - Consumo humano de água",
                        'descricao_pagina' => traduz("Os vídeos “Água em nós”, composto por um filme mais completo e outros oito featurettes, são ferramentas de conscientização e sensibilização que mostram, de forma lúdica, a verdadeira relação que temos com a água em todos os seus aspectos. Portanto, use-os à vontade: publique no seu mural do Facebook e outras redes sociais ou faça o download para usá-los no ambiente de trabalho, com a família e amigos.")
                    );
                    $data['featurette_5'] = array(
                        "slug" => "featurette_5",
                        "link" => "https://vimeo.com/62279903",
                        "id_embed" => "62279903",
                        "imagem" => "5_pt.jpg",
                        "titulo" => "Evolução da natureza",
                        'subtitulo' => "",
                        "arquivo" => "Evolucao_da_natureza_mp4",
                        "email" => "mailto:?subject=".traduz("Projeto Hydros")." - Evolução da natureza&body=".current_url(),
                        "twitter" => "Evolução da natureza - ".current_url(),
                        "tumblr" => "Evolução da natureza",
                        "fb_titulo" => "Evolução da natureza",
                        "fb_description" => "",
                        "embed" => "<iframe src='http://player.vimeo.com/video/62279903?byline=0&portrait=0&loop=1&wmode=transparent&nocache=".time()."' width='640' height='360' frameborder='0' webkitAllowFullScreen allowFullScreen></iframe>",
                        'texto_email' => traduz("Os vídeos “Água em nós”, composto por um filme mais completo e outros oito featurettes, são ferramentas de conscientização e sensibilização que mostram, de forma lúdica, a verdadeira relação que temos com a água em todos os seus aspectos. Portanto, use-os à vontade: publique no seu mural do Facebook e outras redes sociais ou faça o download para usá-los no ambiente de trabalho, com a família e amigos.")."\n".current_url(),
                        'assunto_email' => traduz("Conheça o Projeto Hydros!") . " - Evolução da natureza",
                        'texto_tweet' => traduz("Conheça o Projeto Hydros!") . " - Evolução da natureza " . current_url(),
                        'titulo_pagina' => traduz("Conheça o Projeto Hydros!") . " - Evolução da natureza",
                        'descricao_pagina' => traduz("Os vídeos “Água em nós”, composto por um filme mais completo e outros oito featurettes, são ferramentas de conscientização e sensibilização que mostram, de forma lúdica, a verdadeira relação que temos com a água em todos os seus aspectos. Portanto, use-os à vontade: publique no seu mural do Facebook e outras redes sociais ou faça o download para usá-los no ambiente de trabalho, com a família e amigos.")
                    );
                    $data['featurette_6'] = array(
                        "slug" => "featurette_6",
                        "link" => "https://vimeo.com/62280349",
                        "id_embed" => "62280349",
                        "imagem" => "6_pt.jpg",
                        "titulo" => "O que é água",
                        'subtitulo' => "",
                        "arquivo" => "O_que_e_agua_mp4",
                        "email" => "mailto:?subject=".traduz("Projeto Hydros")." - O que é água&body=".current_url(),
                        "twitter" => "O que é água - ".current_url(),
                        "tumblr" => "O que é água",
                        "fb_titulo" => "O que é água",
                        "fb_description" => "",
                        "embed" => "<iframe src='http://player.vimeo.com/video/62280349?byline=0&portrait=0&loop=1&wmode=transparent&nocache=".time()."' width='640' height='360' frameborder='0' webkitAllowFullScreen allowFullScreen></iframe>",
                        'texto_email' => traduz("Os vídeos “Água em nós”, composto por um filme mais completo e outros oito featurettes, são ferramentas de conscientização e sensibilização que mostram, de forma lúdica, a verdadeira relação que temos com a água em todos os seus aspectos. Portanto, use-os à vontade: publique no seu mural do Facebook e outras redes sociais ou faça o download para usá-los no ambiente de trabalho, com a família e amigos.")."\n".current_url(),
                        'assunto_email' => traduz("Conheça o Projeto Hydros!") . " - O que é água",
                        'texto_tweet' => traduz("Conheça o Projeto Hydros!") . " - O que é água " . current_url(),
                        'titulo_pagina' => traduz("Conheça o Projeto Hydros!") . " - O que é água",
                        'descricao_pagina' => traduz("Os vídeos “Água em nós”, composto por um filme mais completo e outros oito featurettes, são ferramentas de conscientização e sensibilização que mostram, de forma lúdica, a verdadeira relação que temos com a água em todos os seus aspectos. Portanto, use-os à vontade: publique no seu mural do Facebook e outras redes sociais ou faça o download para usá-los no ambiente de trabalho, com a família e amigos.")
                    );
                    $data['featurette_7'] = array(
                        "slug" => "featurette_7",
                        "link" => "https://vimeo.com/62280572",
                        "id_embed" => "62280572",
                        "imagem" => "7_pt.jpg",
                        "titulo" => "Pequenas atitudes",
                        'subtitulo' => "",
                        "arquivo" => "Pequenas_atitudes_mp4",
                        "email" => "mailto:?subject=".traduz("Projeto Hydros")." - Pequenas atitudes&body=".current_url(),
                        "twitter" => "Pequenas atitudes - ".current_url(),
                        "tumblr" => "Pequenas atitudes",
                        "fb_titulo" => "Pequenas atitudes",
                        "fb_description" => "",
                        "embed" => "<iframe src='http://player.vimeo.com/video/62280572?byline=0&portrait=0&loop=1&wmode=transparent&nocache=".time()."' width='640' height='360' frameborder='0' webkitAllowFullScreen allowFullScreen></iframe>",
                        'texto_email' => traduz("Os vídeos “Água em nós”, composto por um filme mais completo e outros oito featurettes, são ferramentas de conscientização e sensibilização que mostram, de forma lúdica, a verdadeira relação que temos com a água em todos os seus aspectos. Portanto, use-os à vontade: publique no seu mural do Facebook e outras redes sociais ou faça o download para usá-los no ambiente de trabalho, com a família e amigos.")."\n".current_url(),
                        'assunto_email' => traduz("Conheça o Projeto Hydros!") . " - Pequenas atitudes",
                        'texto_tweet' => traduz("Conheça o Projeto Hydros!") . " - Pequenas atitudes " . current_url(),
                        'titulo_pagina' => traduz("Conheça o Projeto Hydros!") . " - Pequenas atitudes",
                        'descricao_pagina' => traduz("Os vídeos “Água em nós”, composto por um filme mais completo e outros oito featurettes, são ferramentas de conscientização e sensibilização que mostram, de forma lúdica, a verdadeira relação que temos com a água em todos os seus aspectos. Portanto, use-os à vontade: publique no seu mural do Facebook e outras redes sociais ou faça o download para usá-los no ambiente de trabalho, com a família e amigos.")
                    );
                    $data['featurette_8'] = array(
                        "slug" => "featurette_8",
                        "link" => "https://vimeo.com/62811973",
                        "id_embed" => "62811973",
                        "imagem" => "8_pt.jpg",
                        "titulo" => "A água no dia a dia",
                        'subtitulo' => "",
                        "arquivo" => "A_agua_no_dia_a_dia_mp4",
                        "email" => "mailto:?subject=".traduz("Projeto Hydros")." - A água no dia a dia&body=".current_url(),
                        "twitter" => "A água no dia a dia - ".current_url(),
                        "tumblr" => "A água no dia a dia",
                        "fb_titulo" => "A água no dia a dia",
                        "fb_description" => "",
                        "embed" => "<iframe src='http://player.vimeo.com/video/62811973?byline=0&portrait=0&loop=1&wmode=transparent&nocache=".time()."' width='640' height='360' frameborder='0' webkitAllowFullScreen allowFullScreen></iframe>",
                        'texto_email' => traduz("Os vídeos “Água em nós”, composto por um filme mais completo e outros oito featurettes, são ferramentas de conscientização e sensibilização que mostram, de forma lúdica, a verdadeira relação que temos com a água em todos os seus aspectos. Portanto, use-os à vontade: publique no seu mural do Facebook e outras redes sociais ou faça o download para usá-los no ambiente de trabalho, com a família e amigos.")."\n".current_url(),
                        'assunto_email' => traduz("Conheça o Projeto Hydros!") . " - A água no dia a dia",
                        'texto_tweet' => traduz("Conheça o Projeto Hydros!") . " - A água no dia a dia " . current_url(),
                        'titulo_pagina' => traduz("Conheça o Projeto Hydros!") . " - A água no dia a dia",
                        'descricao_pagina' => traduz("Os vídeos “Água em nós”, composto por um filme mais completo e outros oito featurettes, são ferramentas de conscientização e sensibilização que mostram, de forma lúdica, a verdadeira relação que temos com a água em todos os seus aspectos. Portanto, use-os à vontade: publique no seu mural do Facebook e outras redes sociais ou faça o download para usá-los no ambiente de trabalho, com a família e amigos.")
                    );
                    $data['cocorico1'] = array(
                        'arquivo' => FALSE,
                        'titulo' => traduz('Água'),
                        'subtitulo' => traduz('Filmete da Turma do Cocoricó'),
                        'embed' => "<iframe width='640' height='360' src='http://www.youtube.com/embed/InSMGSJ1ack?&wmode=transparent' frameborder='0' allowfullscreen></iframe>",
                        'assunto_email' => traduz("Conheça o Projeto Hydros! - Filmetes da Turma do Cocoricó"),
                        'texto_email' => traduz("Em 2012, o Projeto Hydros ganhou importantes aliados, por meio de uma iniciativa da Mexichem Brasil: a série “Pílulas de Cultura” estrelada pela Turma do Cocoricó. Veiculada na TV Cultura, a série foca o tema da água e é um ótimo exemplo de como o Projeto Hydros possui inúmeras possibilidades de interação. Reveja os vídeos.".current_url()),
                        'texto_tweet' => traduz("Filmetes da Turma do Cocoricó sobre o Projeto Hydros! ").current_url(),
                        'titulo_pagina' => 'Projeto Hydros - Filmetes da Turma do Cocoricó',
                        'descricao_pagina' => 'Em 2012, o Projeto Hydros ganhou importantes aliados, por meio de uma iniciativa da Mexichem Brasil: a série “Pílulas de Cultura” estrelada pela Turma do Cocoricó. Veiculada na TV Cultura, a série foca o tema da água e é um ótimo exemplo de como o Projeto Hydros possui inúmeras possibilidades de interação. Reveja os vídeos.'
                    );
                    $data['cocorico2'] = array(
                        'arquivo' => FALSE,
                        'titulo' => traduz('Não pode faltar água nesse mundo!'),
                        'subtitulo' => traduz('Filmete da Turma do Cocoricó'),
                        'embed' => "<iframe width='640' height='360' src='http://www.youtube.com/embed/SnmmMZ08duo?&wmode=transparent' frameborder='0' allowfullscreen></iframe>",
                        'assunto_email' => traduz("Conheça o Projeto Hydros! - Filmetes da Turma do Cocoricó"),
                        'texto_email' => traduz("Em 2012, o Projeto Hydros ganhou importantes aliados, por meio de uma iniciativa da Mexichem Brasil: a série “Pílulas de Cultura” estrelada pela Turma do Cocoricó. Veiculada na TV Cultura, a série foca o tema da água e é um ótimo exemplo de como o Projeto Hydros possui inúmeras possibilidades de interação. Reveja os vídeos.".current_url()),
                        'texto_tweet' => traduz("Filmetes da Turma do Cocoricó sobre o Projeto Hydros! ").current_url(),
                        'titulo_pagina' => 'Projeto Hydros - Filmetes da Turma do Cocoricó',
                        'descricao_pagina' => 'Em 2012, o Projeto Hydros ganhou importantes aliados, por meio de uma iniciativa da Mexichem Brasil: a série “Pílulas de Cultura” estrelada pela Turma do Cocoricó. Veiculada na TV Cultura, a série foca o tema da água e é um ótimo exemplo de como o Projeto Hydros possui inúmeras possibilidades de interação. Reveja os vídeos.'
                    );
                    $data['cocorico3'] = array(
                        'arquivo' => FALSE,
                        'titulo' => traduz('Entre nessa onda!'),
                        'subtitulo' => traduz('Filmete da Turma do Cocoricó'),
                        'embed' => "<iframe width='640' height='360' src='http://www.youtube.com/embed/bUfim1f1Uas?&wmode=transparent' frameborder='0' allowfullscreen></iframe>",
                        'assunto_email' => traduz("Conheça o Projeto Hydros! - Filmetes da Turma do Cocoricó"),
                        'texto_email' => traduz("Em 2012, o Projeto Hydros ganhou importantes aliados, por meio de uma iniciativa da Mexichem Brasil: a série “Pílulas de Cultura” estrelada pela Turma do Cocoricó. Veiculada na TV Cultura, a série foca o tema da água e é um ótimo exemplo de como o Projeto Hydros possui inúmeras possibilidades de interação. Reveja os vídeos.".current_url()),
                        'texto_tweet' => traduz("Filmetes da Turma do Cocoricó sobre o Projeto Hydros! ").current_url(),
                        'titulo_pagina' => 'Projeto Hydros - Filmetes da Turma do Cocoricó',
                        'descricao_pagina' => 'Em 2012, o Projeto Hydros ganhou importantes aliados, por meio de uma iniciativa da Mexichem Brasil: a série “Pílulas de Cultura” estrelada pela Turma do Cocoricó. Veiculada na TV Cultura, a série foca o tema da água e é um ótimo exemplo de como o Projeto Hydros possui inúmeras possibilidades de interação. Reveja os vídeos.'
                    );
                    $data['cocorico4'] = array(
                        'arquivo' => FALSE,
                        'titulo' => traduz('Água limpa é vida'),
                        'subtitulo' => traduz('Filmete da Turma do Cocoricó'),
                        'embed' => "<iframe width='640' height='360' src='http://www.youtube.com/embed/ev53svsXwHc?wmode=transparent' frameborder='0' allowfullscreen></iframe>",
                        'assunto_email' => traduz("Conheça o Projeto Hydros! - Filmetes da Turma do Cocoricó"),
                        'texto_email' => traduz("Em 2012, o Projeto Hydros ganhou importantes aliados, por meio de uma iniciativa da Mexichem Brasil: a série “Pílulas de Cultura” estrelada pela Turma do Cocoricó. Veiculada na TV Cultura, a série foca o tema da água e é um ótimo exemplo de como o Projeto Hydros possui inúmeras possibilidades de interação. Reveja os vídeos.".current_url()),
                        'texto_tweet' => traduz("Filmetes da Turma do Cocoricó sobre o Projeto Hydros! ").current_url(),
                        'titulo_pagina' => 'Projeto Hydros - Filmetes da Turma do Cocoricó',
                        'descricao_pagina' => 'Em 2012, o Projeto Hydros ganhou importantes aliados, por meio de uma iniciativa da Mexichem Brasil: a série “Pílulas de Cultura” estrelada pela Turma do Cocoricó. Veiculada na TV Cultura, a série foca o tema da água e é um ótimo exemplo de como o Projeto Hydros possui inúmeras possibilidades de interação. Reveja os vídeos.'
                    );
                    $data['cocorico5'] = array(
                        'arquivo' => FALSE,
                        'titulo' => traduz('Tudo é água'),
                        'subtitulo' => traduz('Filmete da Turma do Cocoricó'),
                        'embed' => "<iframe width='640' height='360' src='http://www.youtube.com/embed/yqziaH2YoKM?wmode=transparent' frameborder='0' allowfullscreen></iframe>",
                        'assunto_email' => traduz("Conheça o Projeto Hydros! - Filmetes da Turma do Cocoricó"),
                        'texto_email' => traduz("Em 2012, o Projeto Hydros ganhou importantes aliados, por meio de uma iniciativa da Mexichem Brasil: a série “Pílulas de Cultura” estrelada pela Turma do Cocoricó. Veiculada na TV Cultura, a série foca o tema da água e é um ótimo exemplo de como o Projeto Hydros possui inúmeras possibilidades de interação. Reveja os vídeos.".current_url()),
                        'texto_tweet' => traduz("Filmetes da Turma do Cocoricó sobre o Projeto Hydros! ").current_url(),
                        'titulo_pagina' => 'Projeto Hydros - Filmetes da Turma do Cocoricó',
                        'descricao_pagina' => 'Em 2012, o Projeto Hydros ganhou importantes aliados, por meio de uma iniciativa da Mexichem Brasil: a série “Pílulas de Cultura” estrelada pela Turma do Cocoricó. Veiculada na TV Cultura, a série foca o tema da água e é um ótimo exemplo de como o Projeto Hydros possui inúmeras possibilidades de interação. Reveja os vídeos.'
                    );           
                    break;
            }
    
            $data['video'] = $data[$slug];

            if(!$data['video'])
                redirect('materiais/videos');

            $data['share']['email'] = "mailto:?subject=".$data['video']['assunto_email']."&body=".$data['video']['texto_email'];
            $data['share']['tweet'] = "window.open('http://twitter.com/home?status=".$data['video']['texto_tweet']."','Twitter','toolbar=0,status=0,width=626,height=436'); return false;";
            $data['share']['tumblr'] = $data['video']['texto_tweet'];

            $this->headervar['og']['title'] = $data['video']['titulo_pagina'];
            $this->headervar['og']['description'] = $data['video']['descricao_pagina'];

			$this->load->view('materiais/videos_detalhe', $data);
		}
	}

	function galeria(){

		$assunto_email = traduz("Conheça a Galeria de Imagens do Projeto Hydros!");
		$texto_email = traduz("Um dos grandes destaques do Projeto Hydros são as fotografias que compõem os quatro livros da série, de autoria de artistas latino-americanos e europeus. São mais de 200 imagens para você utilizar. Aqui você encontra a opção de compartilhá-las ou baixá-las em todos os tamanhos. ".current_url());
		$texto_tweet = traduz("Conheça a Galeria de Imagens do Projeto Hydros! http://www.projetohydros.com - Vamos salvar a água do planeta!");
		$this->data['share']['email'] = "mailto:?subject=$assunto_email&body=$texto_email";
		$this->data['share']['tweet'] = "window.open('http://twitter.com/home?status=$texto_tweet','Twitter','toolbar=0,status=0,width=626,height=436'); return false;";
		$this->data['share']['tumblr'] = traduz("Conheça a Galeria de Imagens do Projeto Hydros! ".current_url()." - Vamos salvar a água do planeta!");

		$this->headervar['og']['title'] = traduz("Projeto Hydros - Galeria de Imagens");
		$this->headervar['og']['description'] = traduz("Um dos grandes destaques do Projeto Hydros são as fotografias que compõem os quatro livros da série, de autoria de artistas latino-americanos e europeus. São mais de 200 imagens para você utilizar. Aqui você encontra a opção de compartilhá-las ou baixá-las em todos os tamanhos. www.projetohydros.com");

		$this->data['galeria'] = $this->geraGaleria();
        $this->data['tamanho_galeria'] = $this->calculaLarguraGaleria();

		$this->load->view('materiais/galeria', $this->data);
	}

    function imagem($img = FALSE){
        $query = $this->db->get_where('galeria_imagens', array('id' => $img))->result();
        $imagem = $query[0];
        $imagem->nome_download = str_replace(' ', '_', $imagem->fotografo)."_".$imagem->id;

        $data['lista_imagens'] = $this->arr;

        if(!$img || !$imagem)
            redirect('galeria');

        foreach($data['lista_imagens'] as $k => $v){
            if($v->id == $imagem->id)
                $data['inicio'] = $k - 1;
        }

        $assunto_email = traduz("Conheça a Galeria de Imagens do Projeto Hydros!");
        $texto_email = traduz("Um dos grandes destaques do Projeto Hydros são as fotografias que compõem os quatro livros da série, de autoria de artistas latino-americanos e europeus. São mais de 200 imagens para você utilizar. Aqui você encontra a opção de compartilhá-las ou baixá-las em todos os tamanhos. ".current_url());
        $texto_tweet = traduz("Conheça a Galeria de Imagens do Projeto Hydros! http://www.projetohydros.com - Vamos salvar a água do planeta!");
        $data['share']['email'] = "mailto:?subject=$assunto_email&body=$texto_email";
        $data['share']['tweet'] = "window.open('http://twitter.com/home?status=$texto_tweet','Twitter','toolbar=0,status=0,width=626,height=436'); return false;";
        $data['share']['tumblr'] = traduz("Conheça a Galeria de Imagens do Projeto Hydros! ".current_url()." - Vamos salvar a água do planeta!");

        $this->headervar['og']['title'] = traduz("Projeto Hydros - Galeria de Imagens");
        $this->headervar['og']['description'] = traduz("Um dos grandes destaques do Projeto Hydros são as fotografias que compõem os quatro livros da série, de autoria de artistas latino-americanos e europeus. São mais de 200 imagens para você utilizar. Aqui você encontra a opção de compartilhá-las ou baixá-las em todos os tamanhos. www.projetohydros.com");

        $data['imagem'] = $imagem;
        $this->load->view('materiais/imagens_detalhes', $data);
    }

	function exposicao(){
        $assunto_email = traduz("Conheça as Exposições do Projeto Hydros!");
        $texto_email = traduz("Você pode utilizar de oito até 26 imagens dentre as mais de 200 extraídas da coleção de livros Hydros para criar sua própria exposição. Pense em um espaço disponível, como escolas, escritórios, estações do metrô, parques ou até mesmo museus. Disponha as fotos de uma forma que toda a mensagem embutida possa ser apreciada pelos visitantes.".current_url());
        $texto_tweet = traduz("Conheça as Exposições do Projeto Hydros! ".current_url()." - Vamos salvar a água do planeta!");
        $this->data['share']['email'] = "mailto:?subject=$assunto_email&body=$texto_email";
        $this->data['share']['tweet'] = "window.open('http://twitter.com/home?status=$texto_tweet','Twitter','toolbar=0,status=0,width=626,height=436'); return false;";
        $this->data['share']['tumblr'] = traduz("Conheça as Exposições do Projeto Hydros!");

        $this->headervar['og']['title'] = traduz("Projeto Hydros - Conheça as Exposições do Projeto Hydros!");
        $this->headervar['og']['description'] = traduz("Você pode utilizar de oito até 26 imagens dentre as mais de 200 extraídas da coleção de livros Hydros para criar sua própria exposição. Pense em um espaço disponível, como escolas, escritórios, estações do metrô, parques ou até mesmo museus. Disponha as fotos de uma forma que toda a mensagem embutida possa ser apreciada pelos visitantes.");
        $this->data['share']['description_tumblr'] = $this->headervar['og']['description'];

        $this->data['links'] = array(
            'pt' => array(
                26 => array(
                    'grande' => 'Grande_90x60cm_26imagens',
                    'media' => 'Media_60x40cm_26imagens',
                    'pequena' => 'Pequena_40x30cm_26imagens',
                    'gigantografia220x220' => 'Gigantografias_220x220cm_26imagens',
                    'gigantografia120x80' => 'Gigantografias_120x80cm_26imagens'
                ),
                19 => array(
                    'grande' => 'Grande_90x60cm_19imagens',
                    'media' => 'Media_60x40cm_19imagens',
                    'pequena' => 'Pequena_40x30cm_19imagens',
                    'gigantografia220x220' => 'Gigantografias_220x220cm_19imagens',
                    'gigantografia120x80' => 'Gigantografias_120x80cm_19imagens'
                ),
                14 => array(
                    'grande' => 'Grande_90x60cm_14imagens',
                    'media' => 'Media_60x40cm_14imagens',
                    'pequena' => 'Pequena_40x30cm_14imagens',
                    'gigantografia220x220' => 'Gigantografias_220x220cm_14imagens',
                    'gigantografia120x80' => 'Gigantografias_120x80cm_14imagens'
                ),
                8 => array(
                    'grande' => 'Grande_90x60cm_8imagens',
                    'media' => 'Media_60x40cm_8imagens',
                    'pequena' => 'Pequena_40x30cm_8imagens',
                    'gigantografia220x220' => 'Gigantografias_220x220cm_8imagens',
                    'gigantografia120x80' => 'Gigantografias_120x80cm_8imagens'
                )
            ),
            'es' => array(
                26 => array(
                    'grande' => 'Grande_90x60cm_26imagenes',
                    'media' => 'Mediana_60x40cm_26imagenes',
                    'pequena' => 'Pequena_40x30cm_26imagenes',
                    'gigantografia220x220' => 'Gigantografias_220x220cm_26imagenes',
                    'gigantografia120x80' => 'Gigantografias_120x80cm_26imagenes'
                ),
                19 => array(
                    'grande' => 'Grande_90x60cm_19imagenes',
                    'media' => 'Mediana_60x40cm_19imagenes',
                    'pequena' => 'Pequena_40x30cm_19imagenes',
                    'gigantografia220x220' => 'Gigantografias_220x220cm_19imagenes',
                    'gigantografia120x80' => 'Gigantografias_120x80cm_19imagenes'
                ),
                14 => array(
                    'grande' => 'Grande_90x60cm_14imagenes',
                    'media' => 'Mediana_60x40cm_14imagenes',
                    'pequena' => 'Pequena_40x30cm_14imagenes',
                    'gigantografia220x220' => 'Gigantografias_220x220cm_14imagenes',
                    'gigantografia120x80' => 'Gigantografias_120x80cm_14imagenes'
                ),
                8 => array(
                    'grande' => 'Grande_90x60cm_8imagenes',
                    'media' => 'Mediana_60x40cm_8imagenes',
                    'pequena' => 'Pequena_40x30cm_8imagenes',
                    'gigantografia220x220' => 'Gigantografias_220x220cm_8imagenes',
                    'gigantografia120x80' => 'Gigantografias_120x80cm_8imagenes'
                )
            ),
            'en' => array(
                26 => array(
                    'grande' => 'Large_354x236_26images',
                    'media' => 'Medium_236x157_26images',
                    'pequena' => 'Small_157x118_26images',
                    'gigantografia220x220' => 'Billboards_866x866_26images',
                    'gigantografia120x80' => 'Billboards_472x314_26images'
                ),
                19 => array(
                    'grande' => 'Large_354x236_19images',
                    'media' => 'Medium_236x157_19images',
                    'pequena' => 'Small_157x118_19images',
                    'gigantografia220x220' => 'Billboards_866x866_19images',
                    'gigantografia120x80' => 'Billboards_472x314_19images'
                ),
                14 => array(
                    'grande' => 'Large_354x236_14images',
                    'media' => 'Medium_236x157_14images',
                    'pequena' => 'Small_157x118_14images',
                    'gigantografia220x220' => 'Billboards_866x866_14images',
                    'gigantografia120x80' => 'Billboards_472x314_14images'
                ),
                8 => array(
                    'grande' => 'Large_354x236_8images',
                    'media' => 'Medium_236x157_8images',
                    'pequena' => 'Small_157x118_8images',
                    'gigantografia220x220' => 'Billboards_866x866_8images',
                    'gigantografia120x80' => 'Billboards_472x314_8images'
                )
            )
        );

		$this->load->view('materiais/exposicao', $this->data);
	}

    function downloadPDF($lang, $arquivo, $cartilha = FALSE){
        $this->layout = FALSE;

        $filename = $arquivo . ".pdf";

        $file_path = ($cartilha) ? "_arquivos/".$filename : "_arquivos/exposicoes/".$filename;

        if(file_exists($file_path)){
            header("Content-Type: application/force-download");
            header('Content-Type: application/octet-stream');
            header("Content-Type: application/download");
            header("Content-Length : ".filesize($file_path));
            header("Content-Disposition: attachment; filename=$filename");
            
            flush();
            
            $fp = fopen($file_path, "r"); 
            while (!feof($fp))
            {
                echo fread($fp, 65536); 
                flush();
            }  
            fclose($fp);
        }else{
            echo($file_path);
            if($this->session->userdata('redirect'))
                redirect($this->session->userdata('redirect'));
            else
                redirect('home');            
        }
    }

	function apoio(){
        $assunto_email = traduz("Conheça o Material de Apoio do Projeto Hydros!");
        $texto_email = traduz("Projetamos uma campanha composta por diversas peças, que você pode selecionar individualmente ou em conjunto por cartazes. Use sua imaginação para conscientizar e sensibilizar mais pessoas, junto ao Projeto Hydros. ".current_url());
        $texto_tweet = traduz("Conheça o Material de Apoio do Projeto Hydros! ".current_url()." - Vamos salvar a água do planeta!");
        $this->data['share']['email'] = "mailto:?subject=$assunto_email&body=$texto_email";
        $this->data['share']['tweet'] = "window.open('http://twitter.com/home?status=$texto_tweet','Twitter','toolbar=0,status=0,width=626,height=436'); return false;";
        $this->data['share']['tumblr'] = traduz("Conheça o Material de Apoio do Projeto Hydros! - Vamos salvar a água do planeta!");

        $this->headervar['og']['title'] = traduz("Projeto Hydros - Conheça o Material de Apoio do Projeto Hydros!");
        $this->headervar['og']['description'] = traduz("Projetamos uma campanha composta por diversas peças, que você pode selecionar individualmente ou em conjunto por cartazes. Use sua imaginação para conscientizar e sensibilizar mais pessoas, junto ao Projeto Hydros.");
        $this->data['share']['description_tumblr'] = $this->headervar['og']['description'];

		$this->load->view('materiais/apoio', $this->data);
	}

    function material($slug = false){

        if(!$slug)
            redirect('materiais/apoio');

        $assunto_email = traduz("Conheça o Projeto Hydros!");
        $texto_email = traduz("Texto texto texto .... <a href='http://www.projetohydros.com'>www.projetohydros.com</a>");
        $texto_tweet = traduz("Conheça o Projeto Hydros! http://www.projetohydros.com - Vamos salvar a água do planeta!");
        $this->data['share']['email'] = "mailto:?subject=$assunto_email&body=$texto_email";
        $this->data['share']['tweet'] = "window.open('http://twitter.com/home?status=$texto_tweet','Twitter','toolbar=0,status=0,width=626,height=436'); return false;";
        $this->data['share']['tumblr'] = traduz("Conheça o Projeto Hydros! http://www.projetohydros.com - Vamos salvar a água do planeta!");
        $this->data['marcar'] = $slug;

        $this->headervar['og']['title'] = traduz('Material de Apoio');
        $this->headervar['og']['description'] = traduz('Cada um de nós, Embaixadores da Água, pode também intervir em seus espaços cotidianos, como no escritório, no carro, nas portas e nos espelhos de nossos ambientes e, até mesmo, em nossa própria casa.');

        $bd = array(
            'pt' => array(
                'adesivo' => array('Mensagens_Adesivo_10x15cm_Portugues.jpg', 'Mensagens_Adesivo_20x13cm_2_Portugues.jpg', 'Mensagens_Adesivo_20x13cm_Portugues.jpg', 'Mensagens_Adesivo_40x25cm_2_Portugues.jpg', 'Mensagens_Adesivo_40x25cm_Portugues.jpg'),
                'banner' => array('Mensagens_Banner_2_Portugues.jpg', 'Mensagens_Banner_Portugues.jpg'),
                'cartaz' => array('Mensagens_Cartaz_2_Portugues.jpg', 'Mensagens_Cartaz_3_Portugues.jpg', 'Mensagens_Cartaz_4_Portugues.jpg'),
                'marcador' => array('Mensagens_Marcador_de_Livro_5x15cm_Portugues.jpg'),
                'mobile' => array('Mensagens_Mobile_40x40cm_2_Portugues.jpg', 'Mensagens_Mobile_40x40cm_Portugues.jpg', 'Mini_Campanha_Mobile_Portugues.jpg'),
                'bandeja' => array('Mensagens_Toalha_Bandeja_Portugues.jpg'),
                'wallpaper' => array('Mensagens_Wallpaper_2_Portugues.jpg', 'Mensagens_Wallpaper_Portugues.jpg'),
                'wobler' => array('Mensagens_Wobler_5x15cm_Portugues.jpg', 'Mensagens_Wobler_10x10cm_Portugues.jpg'),
                'bannersdigitais' => array('portugues1.png', 'portugues2.png')
            ),
            'es' => array(
                'adesivo' => array('Adhesivo_10x15cm.jpg', 'Adhesivo_20x13cm.jpg', 'Adhesivo_20x13cm_2.jpg', 'Adhesivo_40x25cm.jpg', 'Adhesivo_40x25cm_2.jpg'),
                'banner' => array('Banner_0,90x120m.jpg', 'Banner_0,90x120m_2.jpg'),
                'cartaz' => array('Afiche_A3_horizontal.jpg', 'Afiche_A3_vertical.jpg', 'Afiche_A3_vertical_2.jpg'),
                'marcador' => array('Marcador_de_libro_5x15cm.jpg'),
                'mobile' => array('Movil_40cm_diametro.jpg', 'Movil_40cm_diametro_2.jpg', 'Movil_Mini_Campanha_40cm_diametro.jpg'),
                'bandeja' => array('Mantel_para_bandeja_40x30cm.jpg'),
                'wallpaper' => array('Fondo_de_pantalla_1024x768pixels.jpg', 'Fondo_de_pantalla_1024x768pixels_2.jpg'),
                'wobler' => array('Marco_para_computadora_5x15cm.jpg', 'Marco_para_computadora_10x10cm.jpg'),
                'bannersdigitais' => array('espanhol1.png', 'espanhol2.png')
            ),
            'en' => array(
                'adesivo' => array('Sticker_10x15cm.jpg', 'Sticker_20x13cm.jpg', 'Sticker_20x13cm_2.jpg', 'Sticker_40x25cm.jpg' , 'Sticker_40x25cm_2.jpg'),
                'banner' => array('Banner_0,9x1,20m.jpg', 'Banner_0,9x1,20m_2.jpg'),
                'cartaz' => array('Poster_A3_horizontal.jpg', 'Poster_A3_vertical.jpg', 'Poster_A3_vertical_2.jpg'),
                'marcador' => array('Bookmark_5x15cm_English.jpg'),
                'mobile' => array('Mobile_40cm_diameter_English.jpg', 'Mobile_40cm_diameter_English_2.jpg', 'Mobile_Mini_Campanha_40cm_diameter_English.jpg'),
                'bandeja' => array('Tray_Towel_40x30cm.jpg'),
                'wallpaper' => array('Wallpaper_1024x768pixels.jpg', 'Wallpaper_1024x768pixels_2.jpg'),
                'wobler' => array('Computer_header_5x15cm.jpg', 'Computer_header_10x10cm.jpg'),
                'bannersdigitais' => array('ingles1.png', 'ingles2.png')
            )
        );
    
        switch ($slug) {
            case 'adesivo': $this->data['titulo'] = traduz("Adesivo"); break;
            case 'banner': $this->data['titulo'] = traduz("Banner"); break;
            case 'cartaz': $this->data['titulo'] = traduz("Cartaz"); break;
            case 'marcador': $this->data['titulo'] = traduz("Marcador de livro"); break;
            case 'mobile': $this->data['titulo'] = traduz("Móbile"); break;
            case 'bandeja': $this->data['titulo'] = traduz("Toalha de bandeja"); break;
            case 'wallpaper': $this->data['titulo'] = traduz("Wallpaper"); break;
            case 'wobler': $this->data['titulo'] = traduz("Wobler"); break;
            case 'bannersdigitais': $this->data['titulo'] = traduz("Banners Digitais"); break;
        }

        $this->data['dados'] = $bd[$this->session->userdata('language')][$slug];
        $this->data['slug_original'] = $slug;
        $this->load->view('materiais/apoio_detalhe', $this->data);   
    }

	function cartilha(){
        switch ($this->session->userdata('language')) {
            case 'en':
                $data['link_maior'] = "Cartilha_The_HydrosProject_atSchool_bigger";
                $data['link_menor'] = "HydrosProject_at_School_exhibit";
                break;
            case 'es':
                $data['link_maior'] = "Cartilha_ProyectoHydros_en_la_Escuela_Maior";
                $data['link_menor'] = "ProyectoHydros_en_la_escuela_Exposicion";
                break;
            default:
            case 'pt':
                $data['link_maior'] = "Cartilha_ProjetoHydros_na_Escola_Maior";
                $data['link_menor'] = "ProjetoHydros_na_escola_Exposicao";
                break;            
        }
		$this->load->view('materiais/cartilha', $data);
	}

    function geraGaleria(){
        
        $imgs = $this->arr;
        
        $retorno = '';
        $tipo_box = 1;
        $contador = 0;
        
        foreach($imgs as $k => $img){
            
            if($k == 0){
                $retorno = "<div class='box_grande'><div class='box_imagens_$tipo_box'>";
            }
            
            switch($tipo_box){
                case 1:
                    if($contador == 0)
                        $retorno .= "<a rel='galeria' href='".base_url('materiais/imagem/'.$img->id)."' class='modal'><img class='large_left' src='".base_url()."_imgs/galeria/thumbs/grd_$img->imagem'></a>";
                    else
                        $retorno .= "<a rel='galeria' href='".base_url('materiais/imagem/'.$img->id)."' class='modal'><img class='small' src='".base_url()."_imgs/galeria/thumbs/$img->imagem'></a>";
                    break;
                case 2:
                    if($contador == 6)
                        $retorno .= "<a rel='galeria' href='".base_url('materiais/imagem/'.$img->id)."' class='modal'><img class='small last' src='".base_url()."_imgs/galeria/thumbs/$img->imagem'></a>";
                    else
                        $retorno .= "<a rel='galeria' href='".base_url('materiais/imagem/'.$img->id)."' class='modal'><img class='small' src='".base_url()."_imgs/galeria/thumbs/$img->imagem'></a>";
                    break;
                case 3:
                    switch($contador){
                        case 0:
                            $retorno .= "<a rel='galeria' href='".base_url('materiais/imagem/'.$img->id)."' class='modal'><img class='small' src='".base_url()."_imgs/galeria/thumbs/$img->imagem'></a>";
                            break;
                        case 1:
                            $retorno .= "<a rel='galeria' href='".base_url('materiais/imagem/'.$img->id)."' class='modal'><img class='small' src='".base_url()."_imgs/galeria/thumbs/$img->imagem'></a><br/>";
                            break;
                        case 2:
                            $retorno .= "<a rel='galeria' href='".base_url('materiais/imagem/'.$img->id)."' class='modal'><img class='small' src='".base_url()."_imgs/galeria/thumbs/$img->imagem'></a>";
                            break;
                        case 3:
                            $retorno .= "<a rel='galeria' href='".base_url('materiais/imagem/'.$img->id)."' class='modal'><img class='small' src='".base_url()."_imgs/galeria/thumbs/$img->imagem'></a><br/>";
                            break;
                        case 4:
                            $retorno .= "<a rel='galeria' href='".base_url('materiais/imagem/'.$img->id)."' class='modal'><img class='small' src='".base_url()."_imgs/galeria/thumbs/$img->imagem'></a>";
                            break;
                        case 5:
                            $retorno .= "<a rel='galeria' href='".base_url('materiais/imagem/'.$img->id)."' class='modal'><img class='small' src='".base_url()."_imgs/galeria/thumbs/$img->imagem'></a>";
                            break;
                        case 6:
                            $retorno .= "<a rel='galeria' href='".base_url('materiais/imagem/'.$img->id)."' class='modal'><img class='large_right' src='".base_url()."_imgs/galeria/thumbs/grd_$img->imagem'></a>";
                            break;
                    }
                    break;
            }
            
            $contador++;
            
            if(($k + 1)%7 == 0){
                $tipo_box++;
                if($tipo_box > 3){
                    $tipo_box = 1;
                    $retorno .= "</div></div><div class='box_grande'><div class='box_imagens_$tipo_box'>";
                }else{
                    $retorno .= "</div><div class='box_imagens_$tipo_box'>";
                }
                $contador = 0;
            }
        }
        
        return $retorno.'</div></div>';
    }
    
    function calculaLarguraGaleria(){
        
        $imgs = $this->arr;
        
        $num = sizeof($imgs);
        
        $retorno = ceil($num / 21);
        return ($retorno * 780).'px';
    }
}

