<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Comentarios extends CI_Controller {

    function __construct(){
   		parent::__construct();
    }

    function comentar(){
        if ($this->session->userdata('front_logged_in')) {
            echo $this->load->view('comentarios/comentar');
        }else{
            echo $this->load->view('comentarios/logar');
        }
    }

    function formCadastro(){
        echo $this->load->view('comentarios/cadastrar');
    }

    function esqueci(){
        echo $this->load->view('comentarios/esqueci');   
    }

    function reenviar(){
        $email = $this->input->post('email');

        $nova_senha = $this->randomPassword();

        $emailconf['charset'] = 'utf-8';
        $emailconf['mailtype'] = 'html';
        
        $emailconf['protocol'] = 'smtp';        
        $emailconf['smtp_host'] = 'ssl://smtp.googlemail.com';
        $emailconf['smtp_user'] = 'noreply@projetohydros.com';
        $emailconf['smtp_pass'] = 'noreplyhydrostrupe';
        $emailconf['smtp_port'] = 465;
        
        $emailconf['crlf'] = "\r\n";
        $emailconf['newline'] = "\r\n";        
        
        $this->load->library('email');

        $this->email->initialize($emailconf);

        $u_email = $this->input->post('email');
        
        $from = 'noreply@projetohydros.com.br';
        $fromname = 'Projeto Hydros - Reenvio de Senha';
        $to = $u_email;
        $bc = FALSE;
        $bcc = 'bruno@trupe.net';

        if($this->db->get_where('cadastros_comentarios', array('email' => $u_email))->num_rows() > 0){
        
            $this->db->set('senha', md5($nova_senha))
                     ->where('email', $u_email)
                     ->update('cadastros_comentarios');
            

            $corpo_email = traduz("Sua nova senha para comentar no site do Projeto Hydros é : ").$nova_senha."<br><br>".traduz("Projeto Hydros");

            $assunto = 'Projeto Hydros - Reenvio de Senha';
            $email = "<html>
                        <head>                            
                        </head>
                        <body>
                        $corpo_email
                        </body>
                        </html>";

            $this->email->from($from, $fromname);
            $this->email->to($to);
            
            if($bc)
                $this->email->bc($bc);
            if($bcc)
                $this->email->bcc($bcc);
            
            $this->email->reply_to($u_email);

            $this->email->subject($assunto);
            $this->email->message($email);

            $this->session->set_flashdata('envio_status', TRUE);

            $this->email->send();       

        }
    }

    function randomPassword() {
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $pass = array(); //remember to declare $pass as an array
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, strlen($alphabet)-1); //use strlen instead of count
            $pass[$i] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }

    function clear(){
        $this->simpleloginfront->logout();
        echo "<pre>";
        print_r($this->session);
        echo "</pre>";
    }

    function cadastrar(){
        $nome = $this->input->post('nome_completo');
        $nomeass = $this->input->post('nome_assinatura');
        $email = $this->input->post('email');
        $cidade = $this->input->post('cidade');
        $pais = $this->input->post('pais');
        $senha = $this->input->post('senha');
        $imagem = $this->sobeImagem();
        $confirmar_senha = $this->input->post('confirmar_senha');

        $newsletter = $this->input->post('cadastra_newsletter');

        $redirect = $this->input->post('redirect');

        if($nome && $nomeass && $email && $cidade && $pais && $senha && ($senha == $confirmar_senha)){

            $check_cad_coment = $this->db->get_where('cadastros_comentarios', array('email' => $email))->num_rows();

            if(!$check_cad_coment){
                
                if($imagem)
                    $this->db->set('imagem', $imagem);

                $this->db->set('nome', $nome)
                         ->set('nome_assinatura', $nomeass)
                         ->set('email', $email)
                         ->set('cidade', $cidade)
                         ->set('pais', $pais)
                         ->set('senha', md5($senha))
                         ->set('data_cadastro', date('Y-m-d H:i:s'))
                         ->insert('cadastros_comentarios');

                $this->simpleloginfront->login($email, $senha);
            }

            $check_cad_news = $this->db->get_where('cadastros', array('email' => $email))->num_rows();

            if ($newsletter && !$check_cad_news) {
                $this->db->set('nome', $nome)
                         ->set('email', $email)
                         ->set('newsletter', 1)
                         ->set('idioma', $this->session->userdata('language'))
                         ->set('data_cadastro', date('Y-m-d H:i:s'))
                         ->insert('cadastros');
            }
        }
        redirect($redirect."#logged");
    }

    function checkUser(){
        $str = $this->input->post('user');
        echo $this->db->get_where('cadastros', array('email' => $str))->num_rows();
    }

    function checkPass(){
        $str = $this->input->post('user');
        $pwd = $this->input->post('pass');
        echo $this->db->get_where('cadastros_comentarios', array('email' => $str, 'senha' => md5($pwd)))->num_rows();
    }

    function logar(){
        $email = $this->input->post('login-comentarios');
        $senha = $this->input->post('senha-comentarios');
        $redirect = $this->input->post('redirect');

        $this->simpleloginfront->login($email, $senha);
        redirect($redirect."#logged");
    }

    function postar(){
        $comentario = $this->input->post('texto-comentario');
        $id_not = $this->input->post('coment_id');
        $redirect = $this->input->post('redirect');
        $area = $this->input->post('area_comentario');
        
        switch ($area) {
            case 'noticias':
                $tabela = prefixo("noticias_comentarios");
                break;
            case 'giro':
                $tabela = prefixo("giro_comentarios");
                break;
            case 'mapa':
                $tabela = prefixo("mapa_comentarios");
                break;            
        }

        $this->db->set('autor', $this->session->userdata('id'))
                 ->set('comentario', $comentario)
                 ->set('data', date('Y-m-d H:i:s'))
                 ->set('id_noticia', $id_not)
                 ->insert($tabela);

        redirect($redirect);
    }

    function sobeImagem($campo = 'userfile'){
        $this->load->library('upload');

        $original = array(
            'campo' => $campo,
            'dir' => '_imgs/cadastros/'
        );
        $campo = $original['campo'];

        $uploadconfig = array(
          'upload_path' => $original['dir'],
          'allowed_types' => 'jpg|png|gif',
          'max_size' => '2048',
          'max_width' => '1800',
          'max_height' => '1800');

        $this->upload->initialize($uploadconfig);

        if(isset($_FILES[$campo]) && $_FILES[$campo]['error'] != 4){
            if($this->upload->do_upload($campo)){
                $arquivo = $this->upload->data();
                $filename = url_title($arquivo['file_name'], 'underscore', true);
                rename($original['dir'].$arquivo['file_name'] , $original['dir'].$filename);

                $this->image_moo
                    ->load($original['dir'].$filename)
                    ->resize_crop(60,60)
                    ->save($original['dir'].$filename, TRUE);                    

                return $filename;
            }else{
                return false;
            }
        }else{
            return false;
        }       
    }   
}
