<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pegada extends MY_Frontcontroller {

	function __construct(){
		parent::__construct();
	}

	function index(){

		$assunto_email = traduz("PEGADA HYDROS");
		$texto_email = traduz("Já imaginou calcular a quantidade de água que você gasta no seu dia a dia, ao lavar as mãos ou escovar os dentes, por exemplo?")."\n".current_url();
		$texto_tweet = traduz("Calcule a quantidade de água que você gasta no seu dia a dia - Pegada Hydros").current_url();
		$this->data['share']['email'] = "mailto:?subject=$assunto_email&body=$texto_email";
		$this->data['share']['tweet'] = "window.open('http://twitter.com/home?status=$texto_tweet','Twitter','toolbar=0,status=0,width=626,height=436'); return false;";
		$this->data['share']['tumblr'] = traduz("Projeto Hydros - Pegada Hydros");

		$this->headervar['og']['title'] = traduz("Projeto Hydros - Pegada Hydros");
		$this->headervar['og']['description'] = traduz("Calcule a quantidade de água que você gasta no seu dia a dia - Pegada Hydros");
		$this->data['share']['description_tumblr'] = $this->headervar['og']['description'];

		$this->load->view('pegada', $this->data);
	}

}	