<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contato extends MY_Frontcontroller {

	function __construct(){
		parent::__construct();
	}

	function index(){
		$this->load->view('contato');
	}

	function enviar(){
        $emailconf['charset'] = 'utf-8';
        $emailconf['mailtype'] = 'html';
        
        
        $emailconf['protocol'] = 'smtp';        
        $emailconf['smtp_host'] = 'ssl://smtp.googlemail.com';
        $emailconf['smtp_user'] = 'noreply@projetohydros.com';
        $emailconf['smtp_pass'] = 'noreplyhydrostrupe';
        $emailconf['smtp_port'] = 465;
        
        $emailconf['crlf'] = "\r\n";
        $emailconf['newline'] = "\r\n";        
        
        $this->load->library('email');

        $this->email->initialize($emailconf);

        $u_nome = $this->input->post('nome');
        $u_email = $this->input->post('email');
        $u_tel = $this->input->post('telefone');
        $u_msg = $this->input->post('mensagem');
        $u_newsletter = $this->input->post('newsletter');

        $from = 'noreply@projetohydros.com.br';
        $fromname = 'Projeto Hydros - Contato';
        $to = 'contato@projetohydros.com.br';
        $bc = FALSE;
        $bcc = 'bruno@trupe.net';

        if($this->db->get_where('cadastros', array('email' => $u_email))->num_rows() == 0){
            $this->db->set('nome', $u_nome)
            		 ->set('email', $u_email)
            		 ->set('telefone', $u_tel)
            		 ->set('newsletter', $u_newsletter)
                     ->set('idioma', $this->session->userdata('language'))
                     ->set('data_cadastro', Date('Y-m-d'))
            		 ->insert('cadastros');
        }

        $assunto = 'Contato';
        $email = "<html>
                    <head>
                        <style type='text/css'>
                            .tit{
                                font-weight:bold;
                                font-size:14px;
                                color:#5DB8C9;
                                font-family:Arial;
                            }
                            .val{
                                color:#000;
                                font-size:12px;
                                font-family:Arial;
                            }
                        </style>
                    </head>
                    <body>
                    <span class='tit'>Nome :</span> <span class='val'>$u_nome</span><br />
                    <span class='tit'>E-mail :</span> <span class='val'>$u_email</span><br />
                    <span class='tit'>Telefone :</span> <span class='val'>$u_tel</span><br />
                    <span class='tit'>Mensagem :</span> <span class='val'>$u_msg</span><br />
                    </body>
                    </html>";

        $this->email->from($from, $fromname);
        $this->email->to($to);
        
        if($bc)
            $this->email->bc($bc);
        if($bcc)
            $this->email->bcc($bcc);
        
        $this->email->reply_to($u_email);

        $this->email->subject($assunto);
        $this->email->message($email);

        $this->session->set_flashdata('envio_status', TRUE);

        if (!$this->is_bot()) {
            $this->email->send();
        }

		redirect('contato/index', 'refresh');
	}

    private function is_bot(){
        return (isset($_POST['address']) && $this->input->post('address') && $_POST['address'] != "");
    }

}
