<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @property CI_Loader $load
 * @property CI_Form_validation $form_validation
 * @property CI_Input $input
 * @property CI_Email $email
 * @property CI_DB_active_record $db
 * @property CI_DB_forge $dbforge
 */
class Linguagem extends CI_controller {
    
    function __construct() {
        parent::__construct();
    }

    function index($ling = 'pt') {        
        if($ling == 'pt' || $ling == 'es' || $ling == 'en'){
            $this->session->set_userdata('language', $ling);
            $this->session->set_userdata('prefixo', $ling.'_');

            switch($ling){
                case 'pt':
                    $dns = (ENVIRONMENT == 'development') ? 'localhost/hydros2013/' : 'www.projetohydros.com/';
                    break;
                case 'es':
                    $dns = (ENVIRONMENT == 'development') ? 'localhost/hydros2013/' : 'www.proyectohydros.com/';
                    break;
                case 'en':
                    $dns = (ENVIRONMENT == 'development') ? 'localhost/hydros2013/' : 'www.projecthydros.com/';
                    break;
                default:
                    $dns = (ENVIRONMENT == 'development') ? 'localhost/hydros2013/' : 'www.projetohydros.com/';
            }

            if($this->session->userdata('redirect')){
                $redir = 'http://'.$dns.$this->session->userdata('redirect');
                redirect($redir);
            }else{
                redirect('http://'.$dns.'home');
            }
        }else{
            redirect('http://'.$dns.'home');
        }
    }
    
    public function jstraduz(){
        echo traduz($this->input->post('string'), TRUE);
    }
    
}

?>