<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax extends CI_Controller {

    function __construct(){
   		parent::__construct();
        
        if(!$this->input->is_ajax_request())
   		   redirect('home');
    }

    function pegarFotos(){
    	$ultimo = $this->input->post('ultimo');
		$porpagina = $this->input->post('porpagina');

		echo json_encode($this->db->order_by('data', 'desc')->get('pt_noticias', $porpagina, $ultimo)->result());
    }

}
