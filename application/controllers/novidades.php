<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Novidades extends MY_Frontcontroller {

	function __construct(){
		parent::__construct();

		$this->load->model('noticias_model', 'noticias');
		$this->load->model('giro_model', 'giro');
		$this->load->model('mapa_model', 'mapa');
	}

	function index(){
		redirect('novidades/recursos');
	}

	function recursos(){

		$this->data['flashlang'] = $this->session->userdata('language');

		$assunto_email = traduz("Mapa de recursos hídricos no mundo - Projeto Hydros");
		$texto_email = traduz("Saiba mais sobre os recursos hídricos disponíveis (água de superficie e subterrânea) e renováveis e como eles são usados em cada região (agricultura, doméstico e industrial), além da presença do Projeto Hydros em diversos países. \n".current_url());
		$texto_tweet = traduz("Mapa de recursos hídricos no mundo ".current_url()." - Vamos salvar a água do planeta!");
		$this->data['share']['email'] = "mailto:?subject=$assunto_email&body=$texto_email";
		$this->data['share']['tweet'] = "window.open('http://twitter.com/home?status=$texto_tweet','Twitter','toolbar=0,status=0,width=626,height=436'); return false;";
		$this->data['share']['tumblr'] = traduz("Mapa de recursos hídricos no mundo - Projeto Hydros");

		$this->headervar['og']['title'] = traduz("Mapa de recursos hídricos no mundo - Projeto Hydros");
		$this->headervar['og']['description'] = traduz("Saiba mais sobre os recursos hídricos disponíveis (água de superficie e subterrânea) e renováveis e como eles são usados em cada região (agricultura, doméstico e industrial), além da presença do Projeto Hydros em diversos países.");	
		$this->data['share']['description_tumblr'] = $this->headervar['og']['description'];

		// COMENTÁRIOS
		$this->data['comentarios']['lista'] = $this->mapa->pegarComentarios();
		$this->data['comentarios']['num_comentarios'] = $this->mapa->pegarComentarios(TRUE);
		$this->data['comentarios']['area'] = "noticias";

		$this->load->view('novidades/recursos', $this->data);
	}

	function noticias($pag = 0){

		$this->load->library('pagination');

		$pag_options = array(
			'base_url' => base_url("novidades/noticias/"),
			'per_page' => 15,
			'uri_segment' => 3,
			'next_link' => FALSE,
			'prev_link' => FALSE,
			'display_pages' => TRUE,
			'num_links' => 10,
			'first_link' => FALSE,
			'last_link' => FALSE,
			'total_rows' => $this->noticias->numeroResultados()
		);
		$this->pagination->initialize($pag_options);
		$this->data['paginacao'] = $this->pagination->create_links();
		$this->data['noticias'] = $this->noticias->pegarPaginado(false, $pag_options['per_page'], $pag);

		$assunto_email = traduz("Notícias do Projeto Hydros!");
		$texto_email = traduz("Acompanhe as últimas informações sobre a água no mundo. ".current_url());
		$texto_tweet = traduz("Acompanhe as últimas informações sobre a água no mundo. ".current_url());
		$this->data['share']['email'] = "mailto:?subject=$assunto_email&body=$texto_email";
		$this->data['share']['tweet'] = "window.open('http://twitter.com/home?status=$texto_tweet','Twitter','toolbar=0,status=0,width=626,height=436'); return false;";
		$this->data['share']['tumblr'] = traduz("Acompanhe as últimas informações sobre a água no mundo.");

		$this->headervar['og']['title'] = traduz("Projeto Hydros - Notícias da Água");
		$this->headervar['og']['description'] = traduz("Acompanhe as últimas informações sobre a água no mundo.");
		$this->data['share']['description_tumblr'] = $this->headervar['og']['description'];

		foreach ($this->data['noticias'] as $key => $value) {
			$value->data = formataHydros($value->data);
		}

		$this->load->view('novidades/noticias', $this->data);
	}

	function ler($slug){

		if(!$slug)
			redirect('novidades/noticias');

		$this->data['detalhe'] = $this->noticias->pegarPorSlug($slug);		
		$this->data['detalhe']->data = formataHydros($this->data['detalhe']->data);		

		// MÍDIAS SOCIAIS
		$assunto_email = $this->data['detalhe']->titulo;
		$texto_email = word_limiter(strip_tags($this->data['detalhe']->texto), 50)." ".current_url();
		$texto_tweet = $this->data['detalhe']->titulo . ' - ' . current_url();
		$this->data['share']['email'] = "mailto:?subject=$assunto_email&body=$texto_email";
		$this->data['share']['tweet'] = "window.open('http://twitter.com/home?status=$texto_tweet','Twitter','toolbar=0,status=0,width=626,height=436'); return false;";
		$this->data['share']['tumblr'] = $this->data['detalhe']->titulo;

		$this->headervar['og']['title'] = "Projeto Hydros - " . $assunto_email;
		$this->headervar['og']['description'] = $texto_email;
		$this->data['share']['description_tumblr'] = $this->headervar['og']['description'];

		// COMENTÁRIOS
		$this->data['comentarios']['lista'] = $this->noticias->pegarComentarios($this->data['detalhe']->id);
		$this->data['comentarios']['num_comentarios'] = $this->noticias->pegarComentarios($this->data['detalhe']->id, TRUE);
		$this->data['comentarios']['area'] = "noticias";

		$this->load->view('novidades/noticias_detalhes', $this->data);
	}

	function giro($pag = 0){
		$this->load->library('pagination');

		$pag_options = array(
			'base_url' => base_url("novidades/giro/"),
			'per_page' => 15,
			'uri_segment' => 3,
			'next_link' => FALSE,
			'prev_link' => FALSE,
			'display_pages' => TRUE,
			'num_links' => 10,
			'first_link' => FALSE,
			'last_link' => FALSE,
			'total_rows' => $this->giro->numeroResultados()
		);
		$this->pagination->initialize($pag_options);
		$this->data['paginacao'] = $this->pagination->create_links();
		$this->data['noticias'] = $this->giro->pegarPaginado(false, $pag_options['per_page'], $pag);

		$assunto_email = traduz("Giro pelo Projeto Hydros!");
		$texto_email = traduz("Saiba quais ações cada empresa do Grupo Empresarial Kaluz está desenvolvendo para implantar o Projeto Hydros entre seus principais públicos de interesse, como colaboradores, comunidade, escolas etc.".current_url());
		$texto_tweet = traduz("Giro pelo Projeto Hydros ".current_url());
		$this->data['share']['email'] = "mailto:?subject=$assunto_email&body=$texto_email";
		$this->data['share']['tweet'] = "window.open('http://twitter.com/home?status=$texto_tweet','Twitter','toolbar=0,status=0,width=626,height=436'); return false;";
		$this->data['share']['tumblr'] = traduz("Saiba quais ações cada empresa do Grupo Empresarial Kaluz está desenvolvendo para implantar o Projeto Hydros entre seus principais públicos de interesse, como colaboradores, comunidade, escolas etc.");

		$this->headervar['og']['title'] = traduz("Giro pelo Projeto Hydros");
		$this->headervar['og']['description'] = traduz("Saiba quais ações cada empresa do Grupo Empresarial Kaluz está desenvolvendo para implantar o Projeto Hydros entre seus principais públicos de interesse, como colaboradores, comunidade, escolas etc.");
		$this->data['share']['description_tumblr'] = $this->headervar['og']['description'];

		foreach ($this->data['noticias'] as $key => $value) {
			$value->data = formataHydros($value->data);
		}

		$this->load->view('novidades/giro', $this->data);		
	}

	function ler_giro($slug = false){

		if(!$slug)
			redirect('novidades/giro');

		$this->data['detalhe'] = $this->giro->pegarPorSlug($slug);
		$this->data['detalhe']->data = formataHydros($this->data['detalhe']->data);

		$assunto_email = $this->data['detalhe']->titulo;
		$texto_email = word_limiter(strip_tags($this->data['detalhe']->texto), 50)." ".current_url();
		$texto_tweet = $this->data['detalhe']->titulo . ' - ' . current_url();
		$this->data['share']['email'] = "mailto:?subject=$assunto_email&body=$texto_email";
		$this->data['share']['tweet'] = "window.open('http://twitter.com/home?status=$texto_tweet','Twitter','toolbar=0,status=0,width=626,height=436'); return false;";
		$this->data['share']['tumblr'] = $this->data['detalhe']->titulo;

		$this->headervar['og']['title'] = "Projeto Hydros - " . $assunto_email;
		$this->headervar['og']['description'] = $texto_email;
		$this->data['share']['description_tumblr'] = $this->headervar['og']['description'];

		// COMENTÁRIOS
		$this->data['comentarios']['lista'] = $this->giro->pegarComentarios($this->data['detalhe']->id);
		$this->data['comentarios']['num_comentarios'] = $this->giro->pegarComentarios($this->data['detalhe']->id, TRUE);
		$this->data['comentarios']['area'] = "giro";

		$this->load->view('novidades/giro_detalhes', $this->data);
	}
}
