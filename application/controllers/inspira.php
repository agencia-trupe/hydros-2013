<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Inspira extends MY_Frontcontroller {

	function __construct(){
		parent::__construct();
	}

	function index(){

		$assunto_email = traduz("HYDROS INSPIRA");
		$texto_email = traduz("Nesse espaço, você encontrará materiais feitos por pessoas que tiveram contato com o Projeto Hydros e ficaram sensibilizadas com a sua mensagem, de alguma forma. Portanto, se tiver algum produto ou manifestação artística que foi fruto do seu contato com o Projeto Hydros e que tenha atingido um determinado grupo de pessoas, como, por exemplo, um livro, vídeo, textos, música, exposição etc, envie para hydros@corecomunicacao.com.br. Nós iremos analisar o seu material e publicar aqui. O importante, realmente, é levar essa ideia adiante!")."\n".current_url();
		$texto_tweet = traduz("Materiais feitos por pessoas que tiveram contato com o Projeto Hydros - ").current_url();
		$this->data['share']['email'] = "mailto:?subject=$assunto_email&body=$texto_email";
		$this->data['share']['tweet'] = "window.open('http://twitter.com/home?status=$texto_tweet','Twitter','toolbar=0,status=0,width=626,height=436'); return false;";
		$this->data['share']['tumblr'] = traduz("Projeto Hydros - Hydros Inspira");

		$this->headervar['og']['title'] = traduz("Projeto Hydros - Hydros Inspira");
		$this->headervar['og']['description'] = traduz("Nesse espaço, você encontrará materiais feitos por pessoas que tiveram contato com o Projeto Hydros e ficaram sensibilizadas com a sua mensagem, de alguma forma.");
		$this->data['share']['description_tumblr'] = $this->headervar['og']['description'];

		$this->data['link_download_homemagua'] = "concurso-inspira.zip";

		$this->load->view('inspira/index', $this->data);
		$this->load->view('inspira/menu', $this->data);
	}

	function concursomexichem2012(){
		$this->load->view('inspira/concursomexichem2012', $this->data);
		$this->load->view('inspira/menu', $this->data);
	}

	function concursohomemeagua(){
		$this->load->view('inspira/concursohomemeagua', $this->data);
		$this->load->view('inspira/menu', $this->data);
	}

	function domdaagua(){
		$this->load->view('inspira/domdaagua', $this->data);
		$this->load->view('inspira/menu', $this->data);
	}

	function planeta(){
		$this->load->view('inspira/planeta', $this->data);
		$this->load->view('inspira/menu', $this->data);
	}

	function concursoargentina(){
		$this->load->view('inspira/concursoargentina', $this->data);
		$this->load->view('inspira/menu', $this->data);
	}

	function aquavitae(){
		$this->load->view('inspira/aquavitae', $this->data);
	}

	function concursocultural(){
		redirect('inspira/index');
		$this->load->view('inspira/concursocultural', $this->data);
	}
}