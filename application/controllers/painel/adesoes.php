<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Adesoes extends MY_Admincontroller {

   function __construct(){
   	parent::__construct();

   	$this->load->model('adesoes_model', 'adesoes');
   }

   function index($lang = 2, $pag = 0){
   	$this->load->library('pagination');

      $pag_options = array(
         'base_url' => base_url("painel/adesoes/index/$lang/"),
         'per_page' => 20,
         'uri_segment' => 5,
         'next_link' => "Próxima →",
         'next_tag_open' => "<li class='next'>",
         'next_tag_close' => '</li>',
         'prev_link' => "← Anterior",
         'prev_tag_open' => "<li class='prev'>",
         'prev_tag_close' => '</li>',
         'display_pages' => TRUE,
         'num_links' => 10,
         'first_link' => FALSE,
         'last_link' => FALSE,
         'num_tag_open' => '<li>',
         'num_tag_close' => '</li>',
         'cur_tag_open' => '<li><b>',
         'cur_tag_close' => '</b></li>',
         'total_rows' => $this->adesoes->numeroResultados($lang)
      );

      $this->pagination->initialize($pag_options);
      $data['paginacao'] = $this->pagination->create_links();

      $data['registros'] = $this->adesoes->pegarPaginado($lang, $pag_options['per_page'], $pag);

      switch ($lang) {
         case '1':
            $data['titulo'] = "Adesões de Pessoas";
            $data['unidade'] = "Adesão";
            $data['campo_1'] = "Nome";
            $data['campo_2'] = "País";
            break;         
         default:
         case '2':
            $data['titulo'] = "Adesões de Empresas"; 
            $data['unidade'] = "Adesão";
            $data['campo_1'] = "Nome da Empresa";
            $data['campo_2'] = "País";
            break;
      }
      

      if($this->session->flashdata('mostrarerro') === true)
         $data['mostrarerro'] = $this->session->flashdata('mostrarerro_mensagem');
      else
         $data['mostrarerro'] = false;
     
      if($this->session->flashdata('mostrarsucesso') === true)
         $data['mostrarsucesso'] = $this->session->flashdata('mostrarsucesso_mensagem');            
      else
         $data['mostrarsucesso'] = false;

      $data['lang'] = $lang;
   	$this->load->view('painel/adesoes/lista', $data);
   }

   function form($lang, $id = false){
        
      $data['registro'] = $this->adesoes->pegarPorId($lang, $id);
      
      if(!$data['registro'])
         redirect('painel/'.$this->router->class);        

      $data['titulo'] = $this->titulo;
      $data['unidade'] = $this->unidade;
      $data['lang'] = $lang;
      
      if ($lang == 1)
         $data['titulo_bread'] = "Detalhes de Adesão - Pessoa";
      else
         $data['titulo_bread'] = "Detalhes de Adesão - Empresa";
           
      
      $this->load->view('painel/'.$this->router->class.'/form', $data);
   }   

   function excluir($lang, $id){
	   if($this->adesoes->excluir($lang, $id)){
         $this->session->set_flashdata('mostrarsucesso', true);
         $this->session->set_flashdata('mostrarsucesso_mensagem', 'Adesão excluida com sucesso');
      }else{
         $this->session->set_flashdata('mostrarerro', true);
         $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao excluir adesão');
      }

      redirect('painel/adesoes/index/'.$lang, 'refresh');
   }

}