<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Giro extends MY_Admincontroller {

   function __construct(){
   	parent::__construct();

   	$this->load->model('giro_model', 'model');
   }

   function index($lang = 'pt', $pag = 0){
   	$this->load->library('pagination');

      $pag_options = array(
         'base_url' => base_url("painel/giro/index/$lang"),
         'per_page' => 20,
         'uri_segment' => 5,
         'next_link' => "Próxima →",
         'next_tag_open' => "<li class='next'>",
         'next_tag_close' => '</li>',
         'prev_link' => "← Anterior",
         'prev_tag_open' => "<li class='prev'>",
         'prev_tag_close' => '</li>',
         'display_pages' => TRUE,
         'num_links' => 10,
         'first_link' => FALSE,
         'last_link' => FALSE,
         'num_tag_open' => '<li>',
         'num_tag_close' => '</li>',
         'cur_tag_open' => '<li><b>',
         'cur_tag_close' => '</b></li>',
         'total_rows' => $this->model->numeroResultados($lang)
      );

      $this->pagination->initialize($pag_options);
      $data['paginacao'] = $this->pagination->create_links();

      $data['registros'] = $this->model->pegarPaginado($lang, $pag_options['per_page'], $pag);

      switch ($lang) {
         case 'es': $data['titulo'] = "Giro em Espanhol"; break;
         case 'en': $data['titulo'] = "Giro em Inglês"; break;
         default:
         case 'pt': $data['titulo'] = "Giro em Português"; break;
      }
      $data['unidade'] = "Item da seção Giro";
      $data['lang'] = $lang;
      
      if($this->session->flashdata('mostrarerro') === true)
         $data['mostrarerro'] = $this->session->flashdata('mostrarerro_mensagem');
      else
         $data['mostrarerro'] = false;
     
      if($this->session->flashdata('mostrarsucesso') === true)
         $data['mostrarsucesso'] = $this->session->flashdata('mostrarsucesso_mensagem');            
      else
         $data['mostrarsucesso'] = false;

   	$this->load->view('painel/giro/lista', $data);
   }

   function form($lang, $id = false){

      switch ($lang) {
         case 'pt': $lang_extenso = " (Português)"; $data['titulo_bread'] = " em Português"; break;
         case 'en': $lang_extenso = " (Inglês)"; $data['titulo_bread'] = " em Inglês"; break;
         case 'es': $lang_extenso = " (Espanhol)"; $data['titulo_bread'] = " em Espanhol"; break;
      }

      if($id){
         $data['titulo'] = 'Editar item da seção Giro'.$lang_extenso;
   	   $data['registro'] = $this->model->pegarPorId($lang, $id);
         if(!$data['registro'])
            redirect('painel/giro');
      }else{
         $data['titulo'] = 'Inserir item da seção Giro'.$lang_extenso;
         $data['registro'] = FALSE;
      }
      
      $data['lang'] = $lang;
      $data['unidade'] = "Item da seção Giro";
   	$this->load->view('painel/giro/form', $data);
   }

   function inserir($lang){
      if($this->model->inserir($lang)){
         $this->session->set_flashdata('mostrarsucesso', true);
         $this->session->set_flashdata('mostrarsucesso_mensagem', 'Item da seção Giro inserido com sucesso');
      }else{
         $this->session->set_flashdata('mostrarerro', true);
         $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao inserir Item da seção Giro');
      }

   	redirect('painel/giro/index/'.$lang, 'refresh');
   }

   function alterar($lang, $id){
      if($this->model->alterar($lang, $id)){
         $this->session->set_flashdata('mostrarsucesso', true);
         $this->session->set_flashdata('mostrarsucesso_mensagem', 'Item da seção Giro alterado com sucesso');
      }else{
         $this->session->set_flashdata('mostrarerro', true);
         $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao alterar Item da seção Giro');
      }     
   	redirect('painel/giro/index/'.$lang, 'refresh');
   }

   function excluir($lang, $id){
	   if($this->model->excluir($lang, $id)){
         $this->session->set_flashdata('mostrarsucesso', true);
         $this->session->set_flashdata('mostrarsucesso_mensagem', 'Item da seção Giro excluido com sucesso');
      }else{
         $this->session->set_flashdata('mostrarerro', true);
         $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao excluir Item da seção Giro');
      }

      redirect('painel/giro/index/'.$lang, 'refresh');
   }

}