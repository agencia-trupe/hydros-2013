<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Projeto extends MY_Frontcontroller {

	function __construct(){
		parent::__construct();

		$this->load->model('projeto_model', 'model');
	}

	function index(){
		redirect('projeto/contexto');
	}

	function contexto(){
		$this->load->view('projeto/contexto');
	}

	function apresentacao(){
		$this->load->view('projeto/apresentacao');
	}

	function desafios(){
		$this->load->view('projeto/desafios');
	}

	function apoio(){
		$data['empresas'] = $this->model->pegarApoio('empresas');
		$data['pessoas'] = $this->model->pegarApoio('pessoas');

		$this->headervar['og']['title'] = "Projeto Hydros - Apoio";
		$this->headervar['og']['description'] = "Seja você também um Embaixador da Água!";

		$this->load->view('projeto/apoio', $data);
	}

    function adesao(){
    	$this->layout = FALSE;
        $data['load_css'] = "adesao";
        $data['load_js'] = FALSE;

		$texto_tweet = traduz("Seja você também um Embaixador da Água! Vamos salvar a água do planeta! ".current_url());
		$vars['share']['email'] = "mailto:?subject=".traduz("Conheça o Projeto Hydros")."&body=".traduz("Seja você também um Embaixador da Água! Vamos salvar a água do planeta!" . current_url());
		$vars['share']['tweet'] = "window.open('http://twitter.com/home?status=$texto_tweet','Twitter','toolbar=0,status=0,width=626,height=436'); return false;";
		$vars['share']['tumblr'] = traduz("Seja você também um Embaixador da Água! Vamos salvar a água do planeta! ".current_url());	
		$vars['share']['description_tumblr'] = "Seja você também um Embaixador da Água! Vamos salvar a água do planeta!";

        echo $this->load->view('common/header', $data, TRUE).$this->load->view('projeto/adesao', $vars, TRUE);
    }

    function cadastraAdesaoEmpresa(){
    	$this->layout = FALSE;
        $data['load_css'] = "adesao";
        $data['load_js'] = FALSE;

		$texto_tweet = traduz("Seja você também um Embaixador da Água! Vamos salvar a água do planeta! ".current_url());
		$vars['share']['email'] = "mailto:?subject=".traduz("Conheça o Projeto Hydros")."&body=".traduz("Seja você também um Embaixador da Água! Vamos salvar a água do planeta!" . current_url());
		$vars['share']['tweet'] = "window.open('http://twitter.com/home?status=$texto_tweet','Twitter','toolbar=0,status=0,width=626,height=436'); return false;";
		$vars['share']['tumblr'] = traduz("Seja você também um Embaixador da Água! Vamos salvar a água do planeta! ".current_url());	
		$vars['share']['description_tumblr'] = "Seja você também um Embaixador da Água! Vamos salvar a água do planeta!";        

		$imagem = $this->sobeImagem();

    	$this->db->set('empresa', $this->input->post('cad-empresa'))
    			 ->set('contato_responsavel', $this->input->post('cad-responsavel'))
    			 ->set('email', $this->input->post('cad-email'))
    			 ->set('pais', $this->input->post('cad-pais'))
    			 ->set('website', $this->input->post('cad-website'))
    			 ->set('imagem', $imagem)
    			 ->set('data', date("Y-m-d H:i:s"))
    			 ->set('ip', ip())
    			 ->set('tipo', 2)
    			 ->insert('adesao');

        echo $this->load->view('common/header', $data, TRUE) . $this->load->view('projeto/adesao', $vars, TRUE);
    }

    function cadastraAdesaoPessoa(){
    	$this->db->set('nome', $this->input->post('cad-nome'))
    			 ->set('email', $this->input->post('cad-email'))
    			 ->set('pais', $this->input->post('cad-pais'))
    			 ->set('data', date("Y-m-d H:i:s"))
    			 ->set('ip', ip())
    			 ->set('tipo', 1)
    			 ->insert('adesao');
    }

	function participar(){
		$this->load->view('projeto/participar');
	}

    private function sobeImagem($campo = 'cad-imagem'){
        $uploadconfig = array(
                'upload_path'   => '_imgs/apoio/',
                'allowed_types' => 'gif|jpg|png|jpeg',
                'encrypt_name'  => TRUE,
                'overwrite'     => FALSE,
                'max_size'	=> '2000'
        );

        $this->load->library('upload', $uploadconfig);

        if($_FILES[$campo]['error'] != 4){
            if(!$this->upload->do_upload($campo)){
                die($this->upload->display_errors());
                return false;
            }else{

                $arquivo = $this->upload->data();
                $this->image_moo->load('_imgs/apoio/'.$arquivo['file_name'])
                                ->resize(161, 161,'#FFF')
                                ->save('_imgs/apoio/'.$arquivo['file_name'], TRUE);

                return $arquivo['file_name'];
            }
        }else{
            return false;
        }

    }

}