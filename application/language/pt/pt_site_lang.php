<?php

// ARQUIVO : application/views//home.php
$lang['Materiais para download'] = "";
$lang['Com os MATERIAIS DISPONÍVEIS você pode realizar ações concretas e se tornar um verdadeiro EMBAIXADOR DA ÁGUA!'] = "";
$lang['Vídeos'] = "";
$lang['vídeos'] = "";
$lang['Galeria de Imagens'] = "";
$lang['galeria de <br> imagens'] = "";
$lang['Exposição Hydros'] = "";
$lang['exposição <br> hydros'] = "";
$lang['Material de Apoio'] = "";
$lang['material de <br> apoio'] = "";
$lang['Saiba como usar o Projeto Hydros na sua escola'] = "";
$lang["O tutorial \“Como usar o Projeto Hydros na escola\” é o material ideal para que você, profissional da área da Educação, saiba como utilizar a cartilha Projeto Hydros na Escola e possa conscietizar os seus alunos sobre o tema água."] = "";
$lang['Siga-nos!'] = "";
$lang['projetoHydros'] = "";
$lang['FIQUE POR DENTRO'] = "";
$lang['Notícias da água'] = "";
$lang['Conceito criado pela Convenção de Ramsar para denominar o conjunto de áreas alagáveis, como lagos, manguezais e pântanos.'] = "";
$lang['leia mais'] = "";
$lang['Mapa de recursos hídricos no mundo'] = "";
$lang['Veja aqui dados sobre os recursos hídricos de cada país, além de descobrir como esses recursos são utilizados em cada região.'] = "";
$lang['Cadastre-se para receber novidades sobre o PROJETO HYDROS!'] = "";
$lang['nome'] = "";
$lang['e-mail'] = "";
$lang['ENVIAR'] = "";
$lang['Áreas úmidas'] = "";

// ARQUIVO : application/views//pegada.php
$lang['PEGADA HYDROS'] = "";
$lang['Já imaginou calcular a quantidade de água que você gasta no seu dia a dia, ao lavar as mãos ou escovar os dentes, por exemplo?'] = "";
$lang['Agora você pode descobrir a sua pegada hídrica com o aplicativo do Projeto Hydros, o Pegada Hydros. Basta ter um tablet ou smartphone que tenha App Store (iPhone) ou Google Play (Android) como plataforma de distribuição de aplicativo e fazer o download gratuito do App.'] = "";
$lang['Google Play'] = "";
$lang['iTunes'] = "";
$lang['Aguarde...'] = "";
$lang['Logo mais, o aplicativo Pegada Hydros se desdobrará em um game que irá ensinar, de forma divertida e interativa, as formas de captação, reuso e descarte de água. Além disso, ele se passará em algumas das cidades que representam os locais de atuação do Projeto Hydros: Cidade do México, São Paulo, Nova Iorque, Tóquio e Amsterdã. Em cada cidade, serão 5 fases, que vão ficando mais difíceis, de acordo com a evolução do usuário no jogo.'] = "";
$lang['Usando o sensor de gravidade dos smartphones e tablets, o jogador terá de direcionar o fluxo da água pelos canos, para os lugares da casa em que cada tipo de água deve ser utilizado - de chuveiros e pias a irrigação de hortas e abastecimento de reservatórios de água de reuso.'] = "";

// ARQUIVO : application/views//contato.php
$lang['Contato'] = "";
$lang['telefone'] = "";
$lang['mensagem'] = "";
$lang['Quer receber nossa newsletter com todas as novidades e informações sobre o Projeto Hydros?'] = "";
$lang['SIM'] = "";
$lang['NÃO'] = "";

// ARQUIVO : application/views/common/menu.php
$lang['Compartilhe e acompanhe o Projeto Hydros em todas as redes sociais!'] = "";
$lang["Conheça o Projeto Hydros"] = "";
$lang["O Projeto Hydros é uma iniciativa que tem o objetivo de disseminar a conscientização sobre temas relacionados à água e ao desperdício dos nossos recursos naturais. Se você quer fazer parte do nosso time e passar a ideia adiante, torne-se um Embaixador da Água! É super rápido e fácil, e você já estará fazendo a diferença com apenas um clique. .... www.projetohydros.com"] = "";
$lang[''] = "";
$lang['HOME'] = "";
$lang['PROJETO HYDROS'] = "";
$lang['MATERIAIS'] = "";
$lang['Contexto'] = "";
$lang['O que é o projeto?'] = "";
$lang['Quais são os desafios?'] = "";
$lang['Quem apoia?'] = "";
$lang['Como eu posso participar?'] = "";
$lang['Cartilha Projeto Hydros Na Escola'] = "";
$lang['CONTATO'] = "";
$lang['Mapa de Recursos Hídricos no Mundo'] = "";
$lang['Notícias da Água'] = "";
$lang['Giro pelo Projeto Hydros'] = "";

// ARQUIVO : application/views/common/footer.php
$lang['contexto'] = "";
$lang['o que é o projeto?'] = "";
$lang['quais são os desafios?'] = "";
$lang['quem apoia?'] = "";
$lang['como eu posso participar?'] = "";
$lang['galeria de imagens'] = "";
$lang['exposição hydros'] = "";
$lang['material de apoio'] = "";
$lang['Cartilha Projeto Hydros na Escola'] = "";
$lang['mapa de recursos hídricos no mundo'] = "";
$lang['notícias'] = "";
$lang['giro'] = "";
$lang['política de privacidade'] = "";
$lang['aviso legal'] = "";
$lang['Aviso Legal'] = "";
$lang['Projeto Hydros'] = "";
$lang['Todos os direitos reservados'] = "";
$lang['Coordenação:'] = "";
$lang['Criação de sites:'] = "";

// ARQUIVO : application/views/common/header.php
$lang['site_keywords'] = "Projeto Hydros, Água, Sustentabilidade, Consumo de Água, Consumo Consciente, Dia Mundial da Água, Consumo Consciente de Água, Planeta Sustentável";

// ARQUIVO : application/views/materiais/videos.php
$lang['A água em nós'] = "";
$lang['Os vídeos “Água em nós”, composto por um filme mais completo e outros oito featurettes, são ferramentas de conscientização e sensibilização que mostram, de forma lúdica, a verdadeira relação que temos com a água em todos os seus aspectos.  Portanto, use-os à vontade: publique no seu mural do Facebook e outras redes sociais ou faça o download para usá-los no ambiente de trabalho, com a família e amigos.'] = "";
$lang['Faça a diferença agindo diferente!'] = "";
$lang['Faça o download!'] = "";
$lang['Compartilhe!'] = "";
$lang['Compartilhe por e-mail!'] = "";
$lang['Compartilhe pelo facebook!'] = "";
$lang['Compartilhe no Tumblr'] = "";
$lang['Compartilhe no Tumblr!'] = "";
$lang['Advertência Legal de Utilização de Conteúdo'] = "";
$lang['O conteúdo do hotsite é de utilização livre, desde que observadas as regras a seguir estabelecidas:'] = "";
$lang['O objetivo do hotsite é exclusivamente informativo. O material exposto não pode ser utilizado para qualquer finalidade e/ou propósito contrário a esse fim, nem poderá ser copiado e/ou reproduzido sem a divulgação clara e expressa da fonte, creditando a autoria do texto, fotografia e/ou vídeo, produção etc., cujo nome se encontra indicado junto a cada material exposto. Exemplo: "www.projetohydros.com / (nome completo do autor). Todos os direitos reservados".'] = "";
$lang['A alteração de conteúdo e/ou distorções de qualquer natureza, como montagens de imagens, de voz ou outras, é expressamente proibida, em qualquer hipótese, podendo ser punida na forma da lei.'] = "";
$lang['É vedada, também, a veiculação do conteúdo deste hotsite, mesmo com indicação de autor, com a finalidade de lucro pessoal e/ou corporativo, ou com associação a religião, política, discriminação, pedofilia e/ou outras práticas ilícitas.'] = "";
$lang['A indicação de fonte efetuada por terceiro que utilizar os textos, imagens ou vídeos deste hotsite deverá conter, necessariamente, o endereço do domínio e o nome completo do autor do texto, fotografia ou vídeo correspondente.'] = "";
$lang['Personagens da Turma do Cocoricó estrelam filmetes do Projeto Hydros'] = "";
$lang['Em 2012, o Projeto Hydros ganhou importantes aliados, por meio de uma iniciativa da Mexichem Brasil: a série “Pílulas de Cultura” estrelada pela Turma do Cocoricó. Veiculada na TV Cultura, a série foca o tema da água e é um ótimo exemplo de como o Projeto Hydros possui inúmeras possibilidades de interação. Reveja os vídeos.'] = "";
$lang['Água'] = "";
$lang['Não pode faltar água nesse mundo'] = "";
$lang['Entre nessa onda!'] = "";
$lang['Água limpa é vida!'] = "";
$lang['Tudo é água'] = "";

// ARQUIVO : application/views/materiais/galeria.php
$lang['Um dos grandes destaques do Projeto Hydros são as fotografias que compõem os quatro livros da série, de autoria de artistas latino-americanos e europeus. São mais de 200 imagens para você utilizar. Aqui você encontra a opção de compartilhá-las ou baixá-las em todos os tamanhos.'] = "";
$lang['DOWNLOAD DA GALERIA <br>DE IMAGENS COMPLETA'] = "";
$lang['Você pode fazer o download da <br>galeria completa em alta resolução.'] = "";
$lang['clique para iniciar o download'] = "";
$lang['DICA'] = "";
$lang['Para que sua iniciativa tenha um resultado mais positivo e impactante, sugerimos a impressão do material selecionado em papel fotográfico e em alta resolução.'] = "";
$lang['Clique nas imagens para ampliar e ver detalhes:'] = "";

// ARQUIVO : application/views/materiais/exposicao.php
$lang['Você pode utilizar de 8 até 26 imagens, dentre as mais de 200 extraídas da coleção de livros Hydros, para criar sua própria exposição. Pense em um espaço disponível, como escolas, escritórios, estações do metrô, parques, ou mesmo museus. Disponha as fotos de uma forma que toda a mensagem embutida possa ser apreciada pelos visitantes.'] = "";
$lang['Selecione a exposição que deseja e faça o download!'] = "";
$lang['imagens'] = "";

// ARQUIVO : application/views/materiais/cartilha.php
$lang['Cartilha Projeto Hydros na Escola'] = "";
$lang['Pensando em sensibilizar e ensinar crianças nas escolas sobre a escassez da água, o Projeto Hydros criou a cartilha “Projeto Hydros na Escola”. Juntamente com as exposições e materiais de apoio (que podem ser utilizados em sala de aula), a cartilha foi criada para que suas atividades possam ser incluídas no planejamento escolar. Existem duas versões: uma mais completa e outra resumida, com ações mais pontuais.'] = "Pensando em sensibilizar e ensinar crianças nas escolas sobre a escassez da água, o Projeto Hydros criou a cartilha “Projeto Hydros na Escola”. Juntamente com as exposições e materiais de apoio (que podem ser utilizados em sala de aula), a cartilha foi criada para que suas atividades possam ser incluídas no planejamento escolar. Existem duas versões: uma completa e outra resumida, com ações mais pontuais.";

// ARQUIVO : application/views/materiais/videos_detalhe.php
$lang['Voltar'] = "";
$lang['VOLTAR'] = "";

// ARQUIVO : application/views/materiais/imagens_detalhes.php
$lang['Fotógrafo'] = "";
$lang['VOLTAR PARA GALERIA COMPLETA'] = "";

// ARQUIVO : application/views/materiais/apoio.php
$lang['Cada um de nós, Embaixadores da Água, pode também intervir em seus espaços cotidianos, como no escritório, no carro, nas portas e nos espelhos de nossos ambientes e, até mesmo, em nossa própria casa.'] = "";
$lang['Para isso, projetamos uma campanha composta por diversas peças, que você pode selecionar individualmente ou em conjunto, por cartazes. Use sua imaginação para conscientizar e sensibilizar mais pessoas sobre o Projeto Hydros.'] = "Para isso, projetamos uma campanha composta por diversas peças, que você pode selecionar individualmente ou em conjunto. Use sua imaginação para conscientizar e sensibilizar mais pessoas sobre o Projeto Hydros.";
$lang['Wallpaper'] = "";
$lang['ver'] = "";
$lang['Marcador de livro'] = "";
$lang['Testeira para computador'] = "";
$lang['Móbile'] = "";
$lang['Banner'] = "";
$lang['Toalha de bandeja'] = "";
$lang['Cartaz'] = "";
$lang['Adesivo'] = "";
$lang['Banner online'] = "";

// ARQUIVO : application/views/materiais/apoio_detalhe.php

// ARQUIVO : application/views/novidades/recursos.php
$lang['Saiba mais sobre os recursos hídricos disponíveis (água de superficie e subterrânea) e renováveis e como eles são usados em cada região (agricultura, doméstico e industrial), além da presença do Projeto Hydros em diversos países.'] = "";

// ARQUIVO : application/views/novidades/noticias.php
$lang['Acompanhe as últimas informações sobre a água no mundo.'] = "";

// ARQUIVO : application/views/novidades/giro.php
$lang['Saiba quais ações cada empresa do Grupo Empresarial Kaluz está desenvolvendo para implantar o Projeto Hydros entre seus principais públicos de interesse, como colaboradores, comunidade, escolas etc.'] = "";
$lang['VER MAIS'] = "";

// ARQUIVO : application/views/novidades/noticias_detalhes.php
$lang['voltar para a home de notícias'] = "";

// ARQUIVO : application/views/novidades/giro_detalhes.php
$lang['voltar para a home do Giro'] = "";

// ARQUIVO : application/views/projeto/contexto.php
$lang['Preservar a vida de todo o planeta é uma tarefa grande, que começa com ações muito simples'] = "";
$lang['O descaso do ser humano com a água está gerando preocupações alarmantes. De acordo com o Relatório Planeta Vivo 2010 da World Wildlife Fund (WWF), a Terra já ultrapassou 30% de sua capacidade de reposição dos recursos necessários para as nossas demandas. Porém, ainda podemos tentar reverter esse quadro, por meio de atitudes simples, em nosso dia a dia.'] = "";
$lang['Com a gestão adequada dos recursos hídricos, podemos amenizar o impacto das nossas ações na natureza e, assim, tentar garantir o fornecimento da quantidade e qualidade necessárias, para nós e para as próximas gerações. E você pode começar agora, integrando o grupo de “Embaixadores da Água” do Projeto Hydros.'] = "";

// ARQUIVO : application/views/projeto/apresentacao.php
$lang['Há tempos debatemos a forma como o homem se relaciona com a água'] = "";
$lang['Em 2008, a Fundação Kaluz e o Grupo Empresarial Kaluz, assim como as suas empresas Mexichem, Elementia e BX, lançaram uma série de quatro livros de fotografias tratando do tema água, com o objetivo de conscientizar e sensibilizar sobre a importância do uso correto desse elemento. As publicações chegaram às mãos de líderes nacionais, regionais e mundiais, que perceberam o potencial que o projeto trazia.'] = "";
$lang['Para ampliar a disseminação da preservação da água, nasceu o Portal Hydros. Interativo e trilíngue (espanhol, português e inglês), o site é o principal canal de comunicação entre o Projeto e seus Embaixadores. O portal tem inúmeras ferramentas que permitem a qualquer instituição ou cidadão comum, promover verdadeiras campanhas de sensibilização sobre a água.'] = "";
$lang['O <span class="txt-branco">Projeto Hydros</span> levanta a questão: <br> <span class="maior">\'Como me relaciono com a água?\'</span> <br> Trata-se de um grande chamado para que todos sejam “Embaixadores da água”, a <br> fim de disseminar o uso sustentável dos recursos hídricos.'] = "";
$lang['Em 2012, a ação foi lançada para todas as empresas do Grupo Kaluz, incentivando atitudes em todos os países nos quais atua, com foco na conscientização, preservação e sustentabilidade. No mesmo ano, o <strong>Projeto Hydros</strong> também chegou a escolas e comunidades próximas de suas unidades, abrindo um diálogo importante sobre o tema.'] = "";
$lang['O desafio para os próximos anos é disseminar, para o maior número possível de pessoas, essa conscientização de que os nossos recursos naturais estão acabando e precisamos fazer algo para reverter essa situação. A ideia é que o <strong>Projeto Hydros</strong> saia dos portões das empresas do Grupo Kaluz e ganhe o mundo.'] = "";

// ARQUIVO : application/views/projeto/desafios.php
$lang['Uma questão de <br> consciência e atitude'] = "";
$lang['Um dos desafios para a preservação da água potável do mundo é a conscientização. Por isso, o objetivo de <strong>Projeto Hydros</strong> é fazer com que os resultados já alcançados ao longo de 2012, se expandam ainda mais e envolvam cada vez mais pessoas. Prova desse esforço: 2013 é o Ano Internacional de Cooperação pela Água, da UNESCO.'] = "Um dos desafios para a preservação da água potável do mundo é a conscientização. Por isso, o objetivo do <strong>Projeto Hydros</strong> é fazer com que os resultados já alcançados ao longo de 2012, se expandam e envolvam cada vez mais pessoas. Prova desse esforço: 2013 é o Ano Internacional de Cooperação pela Água, da UNESCO.";
$lang['A ideia do <strong>Projeto Hydros</strong> é chamar a atenção da sociedade civil, empresas e estudantes em idade de formação escolar para as questões relacionadas à água e ao saneamento básico. E você pode colaborar, como um Embaixador da Água, abraçando a ideia do Projeto e incentivando todas as pessoas ao seu redor a debater o tema.'] = "";
$lang['Desafios para a preservação da água potável do mundo'] = "";
$lang['A <strong>UNESCO</strong> (Organização das Nações Unidas para a Educação, a Ciência e a Cultura) escolheu <strong>2013</strong> como o <strong>Ano Internacional de Cooperação pela Água</strong>. </p><p>O objetivo é promover uma maior interação entre nações e debater os desafios do manejo da água.</p><p> Segundo a ONU-Água, existe um aumento da demanda pelo acesso, alocação e serviços relacionados a esse bem natural. O ano vai destacar iniciativas de sucesso sobre cooperação pela água.'] = "";
$lang['Saiba mais em'] = "Saiba mais em <a href='http://www.unesco.org.br' title='Unesco'>www.unesco.org.br</a>";

// ARQUIVO : application/views/projeto/apoio.php
$lang['Seja você também um<br>Embaixador da Água!'] = "";
$lang['Simples mudanças de atitude podem fazer a diferença na preservação da água. Hoje já não é mais suficiente apenas fechar a torneira ao lavar a louça ou ao escovar os dentes, por exemplo. O grande desafio é a conscientização. Precisamos mostrar às pessoas que a escassez da água, especialmente a potável, aquela que consumimos, é um problema sério e que devemos tomar as atitudes corretas agora. Uma ação inicial é tornar-se um <strong>Embaixador da Água</strong> do <strong>Projeto Hydros</strong>. '] = "";
$lang['Esta é a lista de pessoas e empresas que decidiram fazer parte desta iniciativa. Seja uma delas e faça a sua parte!'] = "";
$lang['Empresas que apoiam o Projeto Hydros e agora são Embaixadoras da Água.'] = "";
$lang['voltar'] = "";
$lang['ver mais'] = "";
$lang['Seja um embaixador da água!'] = "";
$lang['PARTICIPE!'] = "";
$lang['Seja um'] = "";
$lang['Embaixador'] = "";
$lang['da Água!'] = "";
$lang['Pessoas que apoiam o Projeto Hydros e agora são Embaixadoras da Água.'] = "";

// ARQUIVO : application/views/projeto/participar.php
$lang['Para ser um Embaixador da Água'] = "";
$lang["Para ser um"] = "";
$lang["Embaixador da Água"] = "";
$lang['Para que você consiga utilizar todos os recursos disponíveis pelo <strong>Projeto Hydros</strong> da melhor forma possível, criamos dois tutoriais, com o passo a passo de como utilizar os materiais disponíveis neste site:'] = "";
$lang['Público Geral e Colaboradores'] = "";
$lang['um para o público em geral <br> e colaboradores do <br>Grupo Empresarial Kaluz'] = "";
$lang["um para o <br><span class='maior'>Público em geral</span> e<br> <strong>colaboradores do Grupo Empresarial Kaluz</strong>"] = "";
$lang["e outro para <br><span class='maior'>Profissionais da Área de Educação</span>"] = "";
$lang['clique para iniciar o<br> download'] = "";
$lang['Profissionais'] = "";
$lang['e outro para profissionais <br> da área de Educação'] = "";
$lang['Você está perto de tornar-se um <strong>Embaixador da Água</strong> e ter as ferramentas necessárias para sensibilizar e conscientizar as pessoas ao seu redor acerca de temas como a correta utilização e preservação da água.'] = "";
$lang['Participe do Projeto Hydros e faça a sua parte!'] = "";

// ARQUIVO : application/views/projeto/adesao.php
$lang['CADASTRO DE APOIO<br>AO PROJETO HYDROS'] = "";
$lang['Se você apoia o Projeto Hydros divulgue aqui<br> seu nome ou a marca de sua empresa.'] = "";
$lang['Cadastro de Pessoas'] = "";
$lang['cadastro de <br> <span class="maior">PESSOAS</span>'] = "";
$lang['Cadastro de Empresa'] = "";
$lang['cadastro de <br> <span class="maior">EMPRESAS</span>'] = "";
$lang['nome completo'] = "";
$lang['país'] = "";
$lang['CADASTRAR'] = "";
$lang['O Projeto Hydros não divulga publicamente os e-mails cadastrados nem fornece seus cadastros a terceiros. A solicitação do endereço de e-mail neste caso será usada apenas se for necessário confirmar alguma alteração. O Projeto Hydros se reserva o direito de excluir cadastros a seu critério.'] = "";
$lang['nome da empresa'] = "";
$lang['nome do<br> contato responsável'] = "";
$lang['website'] = "";
$lang['procurar imagem'] = "";
$lang['imagem da marca<br> da empresa'] = "";
$lang['Pronto, seu cadastro foi feito com sucesso.'] = "";
$lang['Você deu o primeiro passo para se tornar um Embaixador da Água.'] = "";
$lang['Quer saber mais? Acompanhe as novidades do Projeto Hydros sobre a água por meio de nossas redes sociais.'] = "";
$lang['Curta, compartilhe, siga e participe!'] = "";
$lang['A sua empresa deu o primeiro passo para se tornar uma Embaixadora da Água.'] = "";
$lang['Curta, compartilhe com seus colaboradores, siga e participe!'] = "";

$lang['GRANDES'] = "";
$lang['MÉDIAS'] = "";
$lang['PEQUENAS'] = "";
$lang['GIGANTOGRAFIAS'] = "";
$lang['GIGANTOGRAFIAS'] = "";

$lang['Wallpaper'] = "";
$lang['Marcador de livro'] = "";
$lang['Móbile'] = "";
$lang['Banner'] = "";
$lang['Banners'] = "";
$lang['Banners Digitais'] = "";
$lang['Toalha de bandeja'] = "";
$lang['Cartaz'] = "";
$lang['Adesivo'] = "";
$lang['Wobler'] = "Wobbler";

$lang['Email já cadastrado!'] = "";
$lang['Cadastro efetuado com sucesso!'] = "";
$lang['Erro ao efetuar o cadastro!'] = "";
$lang['Insira seu nome e um email válido!'] = "";
$lang['Obrigado por entrar em contato!<br>Responderemos assim que possível.'] = "";
$lang['video_institucional_hydros'] = "";
$lang["SELECIONAR ARQUIVO"] = "";
$lang["Informe seu nome!"] = "";
$lang["Informe seu nome de assinatura!"] = "";
$lang["Informe seu e-mail!"] = "";
$lang["Informe sua cidade!"] = "";
$lang["Informe seu país!"] = "";
$lang["Informe sua senha!"] = "";
$lang["Informe sua confirmação de senha!"] = "";
$lang["Suas senhas não conferem!"] = "";
$lang["Informe seu e-mail de cadastro!"] = "";
$lang["E-mail não cadastrado!"] = "";
$lang["Informe sua senha!"] = "";
$lang["Senha não confere!"] = "";

// Cadastro nos comentários
$lang["NOVO CADASTRO"] = "";
$lang["nome completo"] = "";
$lang["nome para assinar os comentários"] = "";
$lang["e-mail"] = "";
$lang["cidade"] = "";
$lang["país"] = "";
$lang["criar senha"] = "";
$lang["confirmar senha"] = "";
$lang["se desejar cadastre uma foto para o seu<br> perfil dentro do nosso site!<br>(Tamanho máximo de arquivo: 2Mb)"] = "";
$lang["SELECIONAR ARQUIVO"] = "";
$lang["Quer receber nossa newsletter com todas as novidades e informações sobre o Projeto Hydros?'"] = "";
$lang["SIM"] = "";
$lang["NÃO"] = "";
$lang["CADASTRAR"] = "";
$lang["(*) o endereço de e-mail será usado como login no sistema.<br> O Projeto Hydros não divulga nem fornece seus dados pessoais a terceiros. Leia nossa <a href='politica'>política de privacidade.</a>"] = "";

// Form de comentários
$lang["ENVIAR COMENTÁRIO"] = "";
$lang["Olá"] = "";
$lang["Deixe seu comentário abaixo"] = "";
$lang["ENVIAR"] = "";
$lang["CANCELAR | VOLTAR"] = "";

// Lista de comentários
$lang["COMENTE"] = "";
$lang["Ninguém comentou. Seja o primeiro!"] = "";
$lang["1 pessoa comentou"] = "";
$lang["pessoas comentaram"] = "";

// Login nos comentários
$lang["CONECTE-SE PARA COMENTAR"] = "";
$lang["Se você já é cadastrado, faça o login"] = "";
$lang["login (e-mail)"] = "";
$lang["senha"] = "";
$lang["ENTRAR"] = "";
$lang["esqueci a senha"] = "";
$lang["Se você ainda não é cadastrado,<br> cadastre-se agora!"] = "";
$lang["CADASTRAR-ME!"] = "";

$lang["O Projeto Hydros tenta mostrar o vínculo entre o planeta e o ser humano em relação à água, do qual ainda não estamos conscientes. Levantamos a questão: 'Como me relaciono com a água?'. Não sabemos. Está na hora de percebermos tudo o que fazemos com ela na construção da realidade de uma pessoa, de uma cidade, de um negócio ou de um país, e como cada uma dessas ações afeta o presente, o hoje, o agora."] = "";

$lang["EM BREVE"] = "";

$lang["PEGADA HYDROS"] = "";
$lang["Calcule a quantidade de água que você gasta no seu dia a dia - Pegada Hydros"] = "";
$lang["Projeto Hydros - Pegada Hydros"] = "";

$lang["<h2>RECUPERAÇÃO DE SENHA</h2><p>Uma nova senha foi enviada para o seu e-mail!</p><a href='#' id='botao-cancelar-post' title='VOLTAR'>VOLTAR</a>"] = "<h2>RECUPERAÇÃO DE SENHA</h2><p>Uma nova senha foi enviada para o seu e-mail!</p><a href='#' id='botao-cancelar-post' title='VOLTAR'>VOLTAR</a>";



$lang["HYDROS INSPIRA"] = "";
$lang["Nesse espaço, você encontrará materiais feitos por pessoas que tiveram contato com o Projeto Hydros e ficaram sensibilizadas com a sua mensagem, de alguma forma. Portanto, se tiver algum produto ou manifestação artística que foi fruto do seu contato com o Projeto Hydros e que tenha atingido um determinado grupo de pessoas, como, por exemplo, um livro, vídeo, textos, música, exposição etc, envie para hydros@corecomunicacao.com.br. Nós iremos analisar o seu material e publicar aqui. O importante, realmente, é levar essa ideia adiante!"] = "Nesse espaço, você encontrará materiais feitos por pessoas que tiveram contato com o Projeto Hydros e ficaram sensibilizadas com a sua mensagem, de alguma forma. Portanto, se tiver algum produto ou manifestação artística que foi fruto do seu contato com o Projeto Hydros e que tenha atingido um determinado grupo de pessoas, como, por exemplo, um livro, vídeo, textos, música, exposição etc, envie para <a href='mailto:hydros@corecomunicacao.com.br'>hydros@corecomunicacao.com.br</a>. Nós iremos analisar o seu material e publicar aqui. O importante, realmente, é levar essa ideia adiante!";
$lang["Materiais feitos por pessoas que tiveram contato com o Projeto Hydros - "] = "";
$lang["Projeto Hydros - Hydros Inspira"] = "";
$lang["Nesse espaço, você encontrará materiais feitos por pessoas que tiveram contato com o Projeto Hydros e ficaram sensibilizadas com a sua mensagem, de alguma forma."] = "";
$lang["O Dom da Água – Regina Moya"] = "";
$lang["A artista plástica e escritora mexicana Regina Moya foi sensibilizada pelo Projeto Hydros e, a partir dele, escreveu e ilustrou o livro infantil “O Dom da Água”, que você confere aqui na íntegra. Para conhecer um pouco mais sobre o trabalho da artista acesse <a href='http://www.reginamoya.com' target='_blank'>http://www.reginamoya.com</a>."] = "";

$lang['Concurso "O Homem e a água"'] = "";
$lang['O Grupo Empresarial Kaluz promoveu um concurso cultural de fotografias, em 2012, englobando todos os seus colaboradores ao redor do mundo, com o tema “O homem e a água”. Veja aqui as cinco imagens vencedoras que foram inspiradas também nas mensagens de sensibilização que o Projeto Hydros trouxe ao cotidiano dos profissionais.'] = "";
$lang['<strong>1º Lugar</strong> - María de los Ángeles Avilés Gamboa, da Mexichem, com o tema “Mar Morto, reviva!”'] = "";
$lang['<strong>2º Lugar</strong> - Emiel van den Boomen, da Wavin Holanda, com o tema “Banhando nas águas sagradas do templo Tirta Empul em Bali”'] = "";
$lang['<strong>3º Lugar</strong> - Liliana Echeverri Branch, da Mexichem Colombia S.A.S., com o tema “Os meninos do Pacífico são água”'] = "";
$lang['<strong>4º Lugar</strong> - Diego Alejandro David Vázquez, da Mexichem, com o tema “Com os pés em mais águas”'] = "";
$lang['<strong>5º Lugar</strong> - Filemón Peruyero Solís, da Mexichem TI, com o tema “Agua: meio de transporte e sustento diário”'] = "";
$lang["A Mexichem Brasil realizou um concurso com as escolas que receberam o Projeto Hydros e foram sensibilizadas por sua mensagem de preservação da água. Os jovens estudantes tinham que criar manifestações artísticas sozinhos ou em grupos. Aqui você confere os vencedores nacionais e o regionais."] = "";
$lang["Concurso Mexichem Brasil 2012"] = "";
$lang["Vencedores Nacionais"] = "";
$lang["Vencedores Regionais"] = "";
$lang["Alunos de 5ª a 8ª Série"] = "";

$lang['Game Hydros'] = "";
$lang['O Game Hydros ensina, de forma divertida e interativa, as formas de captação, reuso e descarte de água. Além disso, ele se passa em algumas das cidades que representam os locais de atuação do Projeto Hydros: Cidade do México, São Paulo, Nova Iorque, Tóquio e Amsterdã.  Em cada cidade, são 5 fases, que ficam mais difíceis, de acordo com a evolução do usuário no jogo.'] = "";
$lang['Usando o sensor de gravidade dos smartphones e tablets, o jogador direciona o fluxo da água pelos canos, para os lugares da casa em que cada tipo de água deve ser utilizado - de chuveiros e pias a irrigação de hortas e abastecimento de reservatórios de água de reuso.'] = "Usando o sensor de gravidade dos smartphones e tablets, o jogador direciona o fluxo da água pelos tubos, para os lugares da casa em que cada tipo de água deve ser utilizado - de chuveiros e pias a irrigação de hortas e abastecimento de reservatórios de água de reuso.";
$lang['A partir de agora, você também tem o destino da água do planeta em suas mãos!'] = "";

$lang["HYDROS E VOCÊ"] = "";
$lang["REVISTA AQUA VITAE &bull; A INSPIRAÇÃO HYDROS"] = "";
$lang['Revista Aqua Vitae'] = '';
$lang["A Aqua Vitae é uma revista criada pelo Grupo Empresarial Kaluz com o objetivo de debater ideias sobre a problemática do recurso hídrico.  Por isso, promoveu como uma de suas linhas de atuação, identificar especialistas mundiais sobre o tema, que encontram nas páginas da publicação a possibilidade de fortalecer com seus pensamentos, investigações e aportes, uma discussão séria e proativa acerca das diferentes facetas que o recurso hídrico tem para o desenvolvimento dos seres humanos, em harmonia com seu uso, manejo e conservação. Disponibilizamos as edições lançadas e que inspiraram a série fotográfica Hydros."] = "";

$lang['Quais as informações que recolhemos?'] = "";
$lang['Coletamos informações de você quando você se cadastra em nosso site ou subscreve a nossa newsletter.'] = "";
$lang['Ao encomendar ou registar no nosso site, conforme o caso, você pode ser solicitado a digitar o seu: nome, endereço de e-mail ou endereço postal. Você pode, entretanto, visitar nosso site de forma anônima.'] = "";
$lang['Como usamos as suas informações?'] = "";
$lang['Qualquer uma das informações que coletamos de você pode ser usado para enviar e-mails periódicos.'] = "";
$lang['O endereço de e-mail que você fornecer para processamento de pedidos, pode ser usado para enviar informações e atualizações referentes ao seu fim, além de receber a notícia ocasional da empresa, atualizações, produto relacionado ou informações de serviço, etc'] = "";
$lang['Como protegemos a sua informação?'] = "";
$lang['Implementamos uma série de medidas de segurança para manter a segurança de suas informações pessoais quando você entra, enviar ou acessar suas informações pessoais.'] = "";
$lang['Nós usamos cookies?'] = "";
$lang['Nós divulgamos qualquer informação a terceiros?'] = "";
$lang['Nós não vendemos, comercializamos ou transferimos a terceiros as suas informações pessoalmente identificáveis. Isso não inclui terceiros de confiança que nos auxiliam no funcionamento do nosso site, conduzindo nosso negócio, ou serviço para você, desde que as partes concordem em manter esta informação confidencial. Podemos também divulgar as suas informações quando acreditamos que é apropriado para cumprir a lei, fazer cumprir as nossas políticas de site, ou proteger nosso ou de outros direitos, propriedade ou segurança. No entanto, as informações do visitante não identificáveis ​​podem ser fornecidas a terceiros para marketing, publicidade, ou outros usos.'] = "";
$lang['Links de terceiros'] = "";
$lang['Nós não incluímos ou oferecemos produtos ou serviços de terceiros no nosso site. No entanto, buscamos proteger a integridade do nosso site e agradecemos qualquer feedback sobre o nosso site.'] = "";
$lang['Childrens Privacidade Online Protection Act Compliance'] = "";
$lang['Estamos em conformidade com os requisitos da COPPA (Childrens online Privacy Protection Act).'] = "";
$lang['Política apenas de Privacidade Online'] = "Política de Privacidade Online";
$lang['Esta política de privacidade online se aplica somente às informações coletadas através de nosso site e não a informações coletadas offline.'] = "";
$lang['Termos e Condições'] = "";
$lang['Por favor, visite o nosso <a href="legal" title="Aviso Legal">Aviso Legal</a>, seção que estabelece o uso, renúncias e limitações de responsabilidade que regem a utilização do conteúdo de nosso website em <a href="http://www.projetohydros.com" title="Projeto Hydros">www.projetohydros.com</a>'] = "";
$lang['Seu consentimento'] = "";
$lang['Ao utilizar nosso site, você concorda com a nossa política de privacidade online.'] = "";
$lang['Alterações à nossa Política de Privacidade'] = "";
$lang['Se nós decidirmos mudar nossa política de privacidade, vamos atualizar data de modificação da política de privacidade abaixo.'] = "";
$lang['Esta política foi modificada pela última vez em <strong>10/06/2013</strong>.'] = "";
$lang['Esta política está em conformidade da Trust Guard PCI compliance.'] = "Esta política está em conformidade da Trust Guard <a href='http://www.trust-guard.com/PCI-Compliance-s/65.htm' target='_blank'>PCI</a> compliance.";

$lang['Sim, nós usamos cookies para melhorar a experiência online do usuário do nosso website. Para quem não sabe, cookies são arquivos que armazenam dados de forma temporária no disco rígido do usuário, para que o website promova uma navegação mais conveniente. Os cookies podem evitar que uma mesma informação seja solicitada mais de uma vez ao mesmo usuário. No caso do nosso website utilizamos cookies com as seguintes finalidades:'] = "";
$lang['- manter o usuário logado após realizar o login para fazer comentários'] = "";
$lang['- armazenar qual a linguagem selecionada caso o usuário escolha a língua clicando nos botões do cabeçalho'] = "";
$lang['- armazenar qual foi a última página acessada sempre, para saber como redirecionar no caso de seleção de um novo idioma'] = "";

$lang['Concurso Cultural Água em 1 minuto'] = "";
$lang['Se a sua escola já recebeu o Projeto Hydros e a sua mensagem de conscientização e sensibilização sobre a água, está na hora de mostrar o que você aprendeu! Participe do Concurso Cultural “Água em 1 minuto” promovido pelo Projeto Hydros em parceria com o Grupo Empresarial Kaluz.'] = "";
$lang['Você encontra os materiais referentes ao concurso aqui:'] = "";
$lang['Regulamento'] = "";
$lang['Flyer'] = "";
$lang['Cartaz A3'] = "";
$lang['Cartaz A4'] = "";
$lang['Disponibilizamos abaixo todos os materiais do Projeto Hydros que podem, de alguma maneira, te inspirar para o concurso.'] = "";
$lang['Vídeo'] = "";
$lang['Se tiver alguma dúvida sobre o concurso, você pode nos contatar através do e-mail <a href="mailto:hydros@corecomunicacao.com.br">hydros@corecomunicacao.com.br</a> ou entrar em contato com o Embaixador da Água que entrou em contato com a sua escola.'] = "";

$lang['Música "Planeta" tem inspiração no Hydros'] = "";
$lang['A letra foi criada pela professora de Ciências da Escola Municipal Professora Lacy Luiza da Crus Flores, Alice Sitta Perera. Já a aluna Hillary do 9º ano ficou responsável por criar a melodia e cantar a música que fala nosso planeta, em especial sobre a água. A escola recebeu o Projeto Hydros em 2012 e resolveu ajudar de alguma maneira. O trabalho, que ficou maravilhoso, você confere aqui:'] = "";

$lang['Veja imagens das manifestações artísticas de pequenos embaixadores.'] = "";
$lang['Por meio de um concuso cultural na Argentina, pequenos Embaixadores da Água foram sensibilizados e inspirados pela mensagem que o Projeto Hydros traz às pessoas. Aqui, você pode ver os vencedores segurando suas verdadeiras obras de arte!'] = "";
?>