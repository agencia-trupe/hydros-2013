<?php

// ARQUIVO : application/views//home.php
$lang['Materiais para download'] = "Materials for download";
$lang['Com os MATERIAIS DISPONÍVEIS você pode realizar ações concretas e se tornar um verdadeiro EMBAIXADOR DA ÁGUA!'] = "With the MATERIALS AVAILABLE you can take concrete actions and become a real WATER AMBASSADOR!";
$lang['Vídeos'] = "Videos";
$lang['vídeos'] = "videos";
$lang['Galeria de Imagens'] = "Images gallery";
$lang['galeria de <br> imagens'] = "Images<br> gallery";
$lang['Exposição Hydros'] = "Hydros Exposition";
$lang['exposição <br> hydros'] = "Hydros <br> Exposition";
$lang['Material de Apoio'] = "Supporting material";
$lang['material de <br> apoio'] = "Supporting<br> material";
$lang['Saiba como usar o Projeto Hydros na sua escola'] = "Learn how to use the Hydros Project in your school";
$lang['O tutorial “Como usar o Projeto Hydros na escola” é o material ideal para que você, profissional da área da Educação, saiba como utilizar a cartilha Projeto Hydros na Escola e possa conscietizar os seus alunos sobre o tema água.'] = "The tutorial \"How to use the Hydros Project in school\" is the ideal material for you, professional of the Education area, to know  how to incorporate the Hydros Project manual in your activities and promote the awareness raise of your students about the water issue.";
$lang['Siga-nos!'] = "Follow us";
$lang['projetoHydros'] = "hydrosProject";
//$lang['FIQUE POR DENTRO'] = "KEEP YOURSELF UPDATED";
$lang['FIQUE POR DENTRO'] = "STAY ON";
$lang['Notícias da água'] = "Water News";
$lang['Conceito criado pela Convenção de Ramsar para denominar o conjunto de áreas alagáveis, como lagos, manguezais e pântanos.'] = "Concept created by Ramsar Convention to denote the set of flooded areas, such as lakes, swamps and marshes.";
$lang['leia mais'] = "learn more";
$lang['Mapa de recursos hídricos no mundo'] = "World Water Resources Map";
$lang['Veja aqui dados sobre os recursos hídricos de cada país, além de descobrir como esses recursos são utilizados em cada região.'] = "Find here information on the water resources in each country, and how they are used in each region.";
$lang['Cadastre-se para receber novidades sobre o PROJETO HYDROS!'] = "Register to receive updates about the Hydros Project!";
$lang['nome'] = "name";
$lang['e-mail'] = "e-mail";
$lang['ENVIAR'] = "SEND";
$lang['Áreas úmidas'] = "Wetlands";

// ARQUIVO : application/views//pegada.php
$lang['PEGADA HYDROS'] = "HYDROS FOOTPRINT";
$lang['Já imaginou calcular a quantidade de água que você gasta no seu dia a dia, ao lavar as mãos ou escovar os dentes, por exemplo?'] = "Imagine if you could calculate the quantity of water that you use in your day-to-day life to wash your hands or brush your teeth, for example!";
$lang['Agora você pode descobrir a sua pegada hídrica com o aplicativo do Projeto Hydros, o Pegada Hydros. Basta ter um tablet ou smartphone que tenha App Store (iPhone) ou Google Play (Android) como plataforma de distribuição de aplicativo e fazer o download gratuito do App.'] = "Now you can. Find out your water footprint with the Hydros Project application, the Hydros Footprint. If you have a tablet or smartphone, access the App Store or Google Play and free download it.";
$lang['Google Play'] = "Google Play";
$lang['iTunes'] = "iTunes";
$lang['Aguarde...'] = "Soon...";
$lang['Logo mais, o aplicativo Pegada Hydros se desdobrará em um game que irá ensinar, de forma divertida e interativa, as formas de captação, reuso e descarte de água. Além disso, ele se passará em algumas das cidades que representam os locais de atuação do Projeto Hydros: Cidade do México, São Paulo, Nova Iorque, Tóquio e Amsterdã. Em cada cidade, serão 5 fases, que vão ficando mais difíceis, de acordo com a evolução do usuário no jogo.'] = "But there is even more: soon this Hydros Footprint application will open out to a game which will teach in a very interactive, funny manner, the means for water collection, reuse and disposal. In addition, the game scenery is composed by some cities that represent the reach of the Hydros Project: Mexico City, São Paulo, New York, Tokyo and Amsterdam. In each one, there will be five phases that will become more difficult according to the user’s evolution.";
$lang['Usando o sensor de gravidade dos smartphones e tablets, o jogador terá de direcionar o fluxo da água pelos canos, para os lugares da casa em que cada tipo de água deve ser utilizado - de chuveiros e pias a irrigação de hortas e abastecimento de reservatórios de água de reuso.'] = "Using the gravity sensor of smartphones and tablets, the player will indicate the direction of the water flow through the piping and to the places of the house where each type of water should be used – from the showers and sinks to the garden irrigation and supply of reuse water tanks.";

// ARQUIVO : application/views//contato.php
$lang['Contato'] = "Contact";
$lang['telefone'] = "phone";
$lang['mensagem'] = "message";
$lang['Quer receber nossa newsletter com todas as novidades e informações sobre o Projeto Hydros?'] = "Do you want to receive our newsletter with all news and info about Hydros Project?";
$lang['SIM'] = "YES";
$lang['NÃO'] = "NO";

// ARQUIVO : application/views/common/menu.php
$lang['Compartilhe e acompanhe o Projeto Hydros em todas as redes sociais!'] = "Share and follow the Hydros Project on all channels!";
$lang["Conheça o Projeto Hydros"] = "Meet the Hydros Project";
$lang["O Projeto Hydros é uma iniciativa que tem o objetivo de disseminar a conscientização sobre temas relacionados à água e ao desperdício dos nossos recursos naturais. Se você quer fazer parte do nosso time e passar a ideia adiante, torne-se um Embaixador da Água! É super rápido e fácil, e você já estará fazendo a diferença com apenas um clique. .... www.projetohydros.com"] = "The Hydros Project is an initiative that has as goal to disseminate the awareness about themes related to water and wasting of our natural resources. If you want to be part of our team and carry the idea forward, become a Water Ambassador! It is super fast and easy, and you will already be doing the difference with just one click.  .... www.projecthydros.com";
$lang['HOME'] = "HOME";
$lang['PROJETO HYDROS'] = "HYDROS PROJECT";
$lang['MATERIAIS'] = "MATERIALS";
$lang['Contexto'] = "Context";
$lang['O que é o projeto?'] = "What is the project?";
$lang['Quais são os desafios?'] = "What are the challenges?";
$lang['Quem apoia?'] = "Who supports it?";
$lang['Como eu posso participar?'] = "How can I join?";
$lang['Cartilha Projeto Hydros Na Escola'] = "Hydros Project in School Booklet";
$lang['CONTATO'] = "CONTACT";
$lang['Mapa de Recursos Hídricos no Mundo'] = "Hydrous Resources Map in the world";
$lang['Notícias da Água'] = "News of the water";
$lang['Giro pelo Projeto Hydros'] = "Project Hydros Tour";

// ARQUIVO : application/views/common/footer.php
$lang['contexto'] = "context";
$lang['o que é o projeto?'] = "what is the project?";
$lang['quais são os desafios?'] = "what are the challenges?";
$lang['quem apoia?'] = "who supports it?";
$lang['como eu posso participar?'] = "how can I join?";
$lang['galeria de imagens'] = "images gallery";
$lang['exposição hydros'] = "hydros exposition";
$lang['material de apoio'] = "supporting material";
$lang['Cartilha Projeto Hydros na Escola'] = "Hydros Project in School Booklet";
$lang['mapa de recursos hídricos no mundo'] = "world water resources map";
$lang['notícias'] = "news";
$lang['giro'] = "tour";
$lang['política de privacidade'] = "privacy policy";
$lang['Política de Privacidade'] = "Privacy Policy";
$lang['aviso legal'] = "terms of use";
$lang['Aviso Legal'] = "Terms of Use";
$lang['Projeto Hydros'] = "Hydros Project";
$lang['Todos os direitos reservados'] = "All rights reserved";
$lang['Coordenação:'] = "Coordination:";
$lang['Criação de sites:'] = "Sites Creation";

// ARQUIVO : application/views/common/header.php
$lang['site_keywords'] = "Project Hydros, Water, sustainability, Water Consumption, Conscious Consumption, World Water Day, Conscious Water Consumption, Sustainable Planet";

// ARQUIVO : application/views/materiais/videos.php
$lang['A água em nós'] = "Water within ourselves";
$lang['Os vídeos “Água em nós”, composto por um filme mais completo e outros oito featurettes, são ferramentas de conscientização e sensibilização que mostram, de forma lúdica, a verdadeira relação que temos com a água em todos os seus aspectos.  Portanto, use-os à vontade: publique no seu mural do Facebook e outras redes sociais ou faça o download para usá-los no ambiente de trabalho, com a família e amigos.'] = "The “Water in us” videos, a more complete and eight other featurettes, are tools of awareness and sensitization that shows in a playful way, the true relationship we have with water in all its aspects. So use them at will: post on your Facebook wall and other social networks or download to use them at work, with family and friends.";
$lang['Faça a diferença agindo diferente!'] = "Make the difference by acting differently!";
$lang['Faça o download!'] = "Download!";
$lang['Compartilhe!'] = "Share!";
$lang['Compartilhe por e-mail!'] = "Share by e-mail!";
$lang['Compartilhe pelo facebook!'] = "Share on facebook!";
$lang['Compartilhe no Tumblr'] = "Share on Tumblr";
$lang['Compartilhe no Tumblr!'] = "Share on Tumblr!";
$lang['Advertência Legal de Utilização de Conteúdo'] = "Legal Advertence Use of Content";
$lang['O conteúdo do hotsite é de utilização livre, desde que observadas as regras a seguir estabelecidas:'] = "The use of the content of this hotsite is free, since the following rules are observed:";
$lang['O objetivo do hotsite é exclusivamente informativo. O material exposto não pode ser utilizado para qualquer finalidade e/ou propósito contrário a esse fim, nem poderá ser copiado e/ou reproduzido sem a divulgação clara e expressa da fonte, creditando a autoria do texto, fotografia e/ou vídeo, produção etc., cujo nome se encontra indicado junto a cada material exposto. Exemplo: "www.projetohydros.com / (nome completo do autor). Todos os direitos reservados".'] = "This hotsite has informational purposes only. Its contents/materials cannot be used for any contrary purpose, nor copied and/or reproduced without the express and clear disclosure of the source, crediting the authorship of the text, photo and/or video to their photographer, producer etc.., whose name is listed right beside, under or above each material. Example: \"www.projecthydros.com / (full name of the author.) All rights reserved.\"";
$lang['A alteração de conteúdo e/ou distorções de qualquer natureza, como montagens de imagens, de voz ou outras, é expressamente proibida, em qualquer hipótese, podendo ser punida na forma da lei.'] = "Changes of contents and/or distortions of any kind, such as compositions of images, voice or others, are expressly forbidden in any case, and may be punished according to the applicable law.";
$lang['É vedada, também, a veiculação do conteúdo deste hotsite, mesmo com indicação de autor, com a finalidade de lucro pessoal e/ou corporativo, ou com associação a religião, política, discriminação, pedofilia e/ou outras práticas ilícitas.'] = "It is also forbidden the placement of the contents of this hotsite, even with indication of their author, for the purpose of personal and/or corporate gains, or associated with religion, politics, discrimination, child abuse and/or other illicit practices.";
$lang['A indicação de fonte efetuada por terceiro que utilizar os textos, imagens ou vídeos deste hotsite deverá conter, necessariamente, o endereço do domínio e o nome completo do autor do texto, fotografia ou vídeo correspondente.'] = "The source reference made ​by a third party to use the texts, pictures or videos of this hotsite should necessarily contain the domain address and the full name of the author of the corresponding text, photograph or video.";

// Vídeos do cocoricó não aparecem em ES e EN

$lang['Personagens da Turma do Cocoricó estrelam filmetes do Projeto Hydros'] = "#";
$lang['Em 2012, o Projeto Hydros ganhou importantes aliados, por meio de uma iniciativa da Mexichem Brasil: a série “Pílulas de Cultura” estrelada pela Turma do Cocoricó. Veiculada na TV Cultura, a série foca o tema da água e é um ótimo exemplo de como o Projeto Hydros possui inúmeras possibilidades de interação. Reveja os vídeos.'] = "#";
$lang['Água'] = "#";
$lang['Não pode faltar água nesse mundo'] = "#";
$lang['Entre nessa onda!'] = "#";
$lang['Água limpa é vida!'] = "#";
$lang['Tudo é água'] = "#";

// ARQUIVO : application/views/materiais/galeria.php
$lang['Um dos grandes destaques do Projeto Hydros são as fotografias que compõem os quatro livros da série, de autoria de artistas latino-americanos e europeus. São mais de 200 imagens para você utilizar. Aqui você encontra a opção de compartilhá-las ou baixá-las em todos os tamanhos.'] = "One of Hydros Project highlights are the pictures that make up the four books of the series, written by Latin American and Europeans. There are over 200 pictures for you to use. Here you will find the option of sharing or downloading them in all sizes.";
$lang['DOWNLOAD DA GALERIA <br>DE IMAGENS COMPLETA'] = "FULL IMAGES <br>GALLERY DOWNLOAD";
$lang['Você pode fazer o download da <br>galeria completa em alta resolução.'] = "You can download the full gallery in high definition.";
$lang['clique para iniciar o download'] = "click to download";
$lang['DICA'] = "TIP";
$lang['Para que sua iniciativa tenha um resultado mais positivo e impactante, sugerimos a impressão do material selecionado em papel fotográfico e em alta resolução.'] = "For your initiative has a more positive outcome and impact, we suggest printing the selected material on photo paper and high resolution.";
$lang['Clique nas imagens para ampliar e ver detalhes:'] = "Click on the images to zoom and see details.";

// ARQUIVO : application/views/materiais/exposicao.php
$lang['Você pode utilizar de 8 até 26 imagens, dentre as mais de 200 extraídas da coleção de livros Hydros, para criar sua própria exposição. Pense em um espaço disponível, como escolas, escritórios, estações do metrô, parques, ou mesmo museus. Disponha as fotos de uma forma que toda a mensagem embutida possa ser apreciada pelos visitantes.'] = "You can use eight to 26 images among the more than 200 extracted from the Hydros books collection to create your own exposure. Think of a space such as schools, offices, subway stations, parks and even museums. Arrange the photos in a way that the entire embedded message can be enjoyed by visitors.";
$lang['Selecione a exposição que deseja e faça o download!'] = "Select the exposition you want and download it!";
$lang['imagens'] = "images";

// ARQUIVO : application/views/materiais/cartilha.php
$lang['Cartilha Projeto Hydros na Escola'] = "Hydros Project at School Booklet";
$lang['Pensando em sensibilizar e ensinar crianças nas escolas sobre a escassez da água, o Projeto Hydros criou a cartilha “Projeto Hydros na Escola”. Juntamente com as exposições e materiais de apoio (que podem ser utilizados em sala de aula), a cartilha foi criada para que suas atividades possam ser incluídas no planejamento escolar. Existem duas versões: uma mais completa e outra resumida, com ações mais pontuais.'] = "Thinking about awareness and teach children in school about the water scarcity, Hydros Project created the “Hydros Project at Scholl” booklet. Along with the exhibits and supporting materials (which can be used in the classroom), the booklet was created so its activities can be included on the school planning. There are two versions: a more complete e a short form one, with more specific actions.";

// ARQUIVO : application/views/materiais/videos_detalhe.php
$lang['Voltar'] = "Back";
$lang['VOLTAR'] = "BACK";

// ARQUIVO : application/views/materiais/imagens_detalhes.php
$lang['Fotógrafo'] = "Photographer";
$lang['VOLTAR PARA GALERIA COMPLETA'] = "BACK TO THE IMAGES GALLERY";

// ARQUIVO : application/views/materiais/apoio.php
$lang['Cada um de nós, Embaixadores da Água, pode também intervir em seus espaços cotidianos, como no escritório, no carro, nas portas e nos espelhos de nossos ambientes e, até mesmo, em nossa própria casa.'] = "Each of us, Water Ambassadors, can also intervene in our everyday spaces, such as the office, the car, the doors and the mirrors of our environments and even in our own house.";
$lang['Para isso, projetamos uma campanha composta por diversas peças, que você pode selecionar individualmente ou em conjunto, por cartazes. Use sua imaginação para conscientizar e sensibilizar mais pessoas sobre o Projeto Hydros.'] = "To help you, we planned a campaign composed of several parts, you can select individually or jointly. Use your imagination to awareness and sensitize more people, with the Project Hydros.";
$lang['Wallpaper'] = "Wallpaper";
$lang['ver'] = "see";
$lang['Marcador de livro'] = "Bookmark";
$lang['Testeira para computador'] = "Computer header";
$lang['Móbile'] = "Mobile";
$lang['Banner'] = "Banner";
$lang['Toalha de bandeja'] = "Tray Towel";
$lang['Cartaz'] = "Poster";
$lang['Adesivo'] = "Sticker";
$lang['Banner online'] = "Banner online";

// ARQUIVO : application/views/materiais/apoio_detalhe.php

// ARQUIVO : application/views/novidades/recursos.php
$lang['Saiba mais sobre os recursos hídricos disponíveis (água de superficie e subterrânea) e renováveis e como eles são usados em cada região (agricultura, doméstico e industrial), além da presença do Projeto Hydros em diversos países.'] = "Learn more about the available hydrous resources (surface water, groundwater and renewable water resources) e how they are used in each region (agriculture, domestic and industrial), and the presence of Hydros Project in several countries.";

// ARQUIVO : application/views/novidades/noticias.php
$lang['Acompanhe as últimas informações sobre a água no mundo.'] = "Keep up with the latest information on water in the world.";

// ARQUIVO : application/views/novidades/giro.php
$lang['Saiba quais ações cada empresa do Grupo Empresarial Kaluz está desenvolvendo para implantar o Projeto Hydros entre seus principais públicos de interesse, como colaboradores, comunidade, escolas etc.'] = "Learn what actions each company of Kaluz Group is developing to implement Hydros Project among its key stakeholders, such as employees, community, schools, etc.";
$lang['VER MAIS'] = "SEE MORE";

// ARQUIVO : application/views/novidades/noticias_detalhes.php
$lang['voltar para a home de notícias'] = "back to the news";

// ARQUIVO : application/views/novidades/giro_detalhes.php
$lang['voltar para a home do Giro'] = "back to the tour";

// ARQUIVO : application/views/projeto/contexto.php
$lang['Preservar a vida de todo o planeta é uma tarefa grande, que começa com ações muito simples'] = "Preserve all lives of the planet is a big task, which begins with very simple actions";
$lang['O descaso do ser humano com a água está gerando preocupações alarmantes. De acordo com o Relatório Planeta Vivo 2010 da World Wildlife Fund (WWF), a Terra já ultrapassou 30% de sua capacidade de reposição dos recursos necessários para as nossas demandas. Porém, ainda podemos tentar reverter esse quadro, por meio de atitudes simples, em nosso dia a dia.'] = "The neglect of human beings with the water is generating alarming concerns. According to 2010 Living Planet Report from World Wildlife Fund (WWF), the Earth has already exceeded 30% of its resources reposition capacity needed to our demands. However, we still can reverse this situation, through simple actions, daily.";
$lang['Com a gestão adequada dos recursos hídricos, podemos amenizar o impacto das nossas ações na natureza e, assim, tentar garantir o fornecimento da quantidade e qualidade necessárias, para nós e para as próximas gerações. E você pode começar agora, integrando o grupo de “Embaixadores da Água” do Projeto Hydros.'] = "With the adequate management of hydrous resources, we can soften the impact of our actions in the nature and, thereby, try to guarantee the supply of quantity and quality needed for us and the next generations. And you can start now, joining the Project Hydros “Water Ambassadors” group.";

// ARQUIVO : application/views/projeto/apresentacao.php
$lang['Há tempos debatemos a forma como o homem se relaciona com a água'] = "A long time we debate the way man relates himself with the water";
$lang['Em 2008, a Fundação Kaluz e o Grupo Empresarial Kaluz, assim como as suas empresas Mexichem, Elementia e BX, lançaram uma série de quatro livros de fotografias tratando do tema água, com o objetivo de conscientizar e sensibilizar sobre a importância do uso correto desse elemento. As publicações chegaram às mãos de líderes nacionais, regionais e mundiais, que perceberam o potencial que o projeto trazia.'] = "In 2008, the Kaluz Foundation and Kaluz Business Group, as its companies Mexichem, Elementia and BX, launched a series of four photography books dealing with the water theme, aiming to educate and raise awareness about the importance of proper use of this element. The publications got to the hands of national, regional and worldwide leaders, who realized the potential the project brought with it.";
$lang['Para ampliar a disseminação da preservação da água, nasceu o Portal Hydros. Interativo e trilíngue (espanhol, português e inglês), o site é o principal canal de comunicação entre o Projeto e seus Embaixadores. O portal tem inúmeras ferramentas que permitem a qualquer instituição ou cidadão comum, promover verdadeiras campanhas de sensibilização sobre a água.'] = "The Hydros Portal was born to expand the dissemination of the water preservation. Interactive and trilingual (English, Spanish and Portuguese), the site is the mainly communication channel between the Project and its Ambassadors. The portal has several tools that allow any institution or citizen to promote truly water sensitization campaigns.";
$lang['O <span class="txt-branco">Projeto Hydros</span> levanta a questão: <br> <span class="maior">\'Como me relaciono com a água?\'</span> <br> Trata-se de um grande chamado para que todos sejam “Embaixadores da água”, a <br> fim de disseminar o uso sustentável dos recursos hídricos.'] = 'The <span class="txt-branco">Hydros Project</span> brings the question: <br> <span class="maior">\'How do I relate to water?\'</span> <br> up. It is a big call for everybody to become “Water Ambassadors” in order to disseminate the sustainable use of the hydrous resources.';
$lang['Em 2012, a ação foi lançada para todas as empresas do Grupo Kaluz, incentivando atitudes em todos os países nos quais atua, com foco na conscientização, preservação e sustentabilidade. No mesmo ano, o <strong>Projeto Hydros</strong> também chegou a escolas e comunidades próximas de suas unidades, abrindo um diálogo importante sobre o tema.'] = "In 2012, the action was launched to all Kaluz Group’s companies, motivating attitudes in all countries which it operates, focusing on awareness, preservation, and sustainability. On the same year, the Hydros Project also got to schools and communities close to its units, opening a very important dialogue about the theme.";
$lang['O desafio para os próximos anos é disseminar, para o maior número possível de pessoas, essa conscientização de que os nossos recursos naturais estão acabando e precisamos fazer algo para reverter essa situação. A ideia é que o <strong>Projeto Hydros</strong> saia dos portões das empresas do Grupo Kaluz e ganhe o mundo.'] = "The challenge for the coming years is to spread to as many people as possible this awareness that our natural resources are running low and we need to do something to reverse this situation. The idea is that the Hydros Project exits gates of the Kaluz Group’s companies and wins the world.";

// ARQUIVO : application/views/projeto/desafios.php
$lang['Uma questão de <br> consciência e atitude'] = "A question of awareness and attitude";
$lang['Um dos desafios para a preservação da água potável do mundo é a conscientização. Por isso, o objetivo de <strong>Projeto Hydros</strong> é fazer com que os resultados já alcançados ao longo de 2012, se expandam ainda mais e envolvam cada vez mais pessoas. Prova desse esforço: 2013 é o Ano Internacional de Cooperação pela Água, da UNESCO.'] = "One of the challenges to preserve drinking water in the world is the awareness. Because of this, the Hydros Project goal is to expand even more the result reached in 2012 and to involve more people. Prove of this effort: 2013 is the UNESCO’s International Year of Water Cooperation.";
$lang['A ideia do <strong>Projeto Hydros</strong> é chamar a atenção da sociedade civil, empresas e estudantes em idade de formação escolar para as questões relacionadas à água e ao saneamento básico. E você pode colaborar, como um Embaixador da Água, abraçando a ideia do Projeto e incentivando todas as pessoas ao seu redor a debater o tema.'] = "The <strong>Hydros Project</strong> idea is to draw the civil society, companies and students attention to questions related to water and sanitation. And you can work as a Hydros Project, embracing the Project’s idea and encouraging everyone around you to discuss the issue.";
$lang['Desafios para a preservação da água potável do mundo'] = "Challenges for the preservation of the world's drinking water";
$lang['A <strong>UNESCO</strong> (Organização das Nações Unidas para a Educação, a Ciência e a Cultura) escolheu <strong>2013</strong> como o <strong>Ano Internacional de Cooperação pela Água</strong>. </p><p>O objetivo é promover uma maior interação entre nações e debater os desafios do manejo da água.</p><p> Segundo a ONU-Água, existe um aumento da demanda pelo acesso, alocação e serviços relacionados a esse bem natural. O ano vai destacar iniciativas de sucesso sobre cooperação pela água.'] = "<strong>UNESCO</strong> (United Nations Educational, Scientific and Cultural Organization) chose <strong>2013</strong> as the <strong>International Year of Water Cooperation</strong>.</p><p> The goal is to promote greater interaction between nations and discuss the challenges about water management.";
$lang['Saiba mais em'] = "Lear more at <a href='http://www.unesco.org.br' title='Unesco'>www.unesco.org.br</a>";

// ARQUIVO : application/views/projeto/apoio.php
$lang['Seja você também um<br>Embaixador da Água!'] = "Be a Water<br> Ambassador you too!";
$lang['Simples mudanças de atitude podem fazer a diferença na preservação da água. Hoje já não é mais suficiente apenas fechar a torneira ao lavar a louça ou ao escovar os dentes, por exemplo. O grande desafio é a conscientização. Precisamos mostrar às pessoas que a escassez da água, especialmente a potável, aquela que consumimos, é um problema sério e que devemos tomar as atitudes corretas agora. Uma ação inicial é tornar-se um <strong>Embaixador da Água</strong> do <strong>Projeto Hydros</strong>. '] = "Simple changes in attitude can make a difference in water preservation. Nowadays is not longer enough just turn off the tap while washing dishes or brushing your teeth, for example. The big challenge is awareness. We need to show everyone that scarcity of water, specially drinking, that we consume, is a serious problem and that we should take the right action now. An initial attitude is to become a <strong>Water Ambassador</strong> of <strong>Project Hydros</strong>.";
$lang['Esta é a lista de pessoas e empresas que decidiram fazer parte desta iniciativa. Seja uma delas e faça a sua parte!'] = "This is the list of people and companies who have decided to be part of this initiative. Be one of them and do you part!";
$lang['Empresas que apoiam o Projeto Hydros e agora são Embaixadoras da Água.'] = "Companies who support Hydros Project and now are Water Ambassadors.";
$lang['voltar'] = "back";
$lang['ver mais'] = "see more";
$lang['Seja um embaixador da água!'] = "Be a water ambassador!";
$lang['PARTICIPE!'] = "JOIN!";
$lang['Seja um'] = "Be a";
$lang['Embaixador'] = "Water";
$lang['da Água!'] = "Ambassador";
$lang['Pessoas que apoiam o Projeto Hydros e agora são Embaixadoras da Água.'] = "People who support Hydros Project and now are Water Ambassadors.";

// ARQUIVO : application/views/projeto/participar.php
$lang['Para ser um Embaixador da Água'] = "To be a Water Ambassador";
$lang["Para ser um"] = "To be a";
$lang["Embaixador da Água"] = "Water Ambassador";
$lang['Para que você consiga utilizar todos os recursos disponíveis pelo <strong>Projeto Hydros</strong> da melhor forma possível, criamos dois tutoriais, com o passo a passo de como utilizar os materiais disponíveis neste site:'] = "You are close to become a Water Ambassador and have the necessary tools to sensitize and aware people around you with themes as correct use and preservation of water.";
$lang['Público Geral e Colaboradores'] = "General Public and Employees";
$lang['um para o público em geral <br> e colaboradores do <br>Grupo Empresarial Kaluz'] = "one for the general public<br> and Kaluz Group’s <br>employees";
$lang["um para o <br><span class='maior'>Público em geral</span> e<br> <strong>colaboradores do Grupo Empresarial Kaluz</strong>"] = "one for the <br><span class='maior'>General public</span> y<br> <strong>and Kaluz Group’s employees</strong>";
$lang["e outro para <br><span class='maior'>Profissionais da Área de Educação</span>"] = "and one for the <br><span class='maior'>educational area professionals</span>";
$lang['clique para iniciar o<br> download'] = "click here to<br>download";
$lang['Profissionais'] = "Employees";
$lang['e outro para profissionais <br> da área de Educação'] = "and one for the <br>educational area professionals";
$lang['Você está perto de tornar-se um <strong>Embaixador da Água</strong> e ter as ferramentas necessárias para sensibilizar e conscientizar as pessoas ao seu redor acerca de temas como a correta utilização e preservação da água.'] = "You are close to become a <strong>Water Ambassador</strong> and have the necessary tools to sensitize and aware people around you with themes as correct use and preservation of water.";
$lang['Participe do Projeto Hydros e faça a sua parte!'] = "Join the Hydros Project and do your part!";

// ARQUIVO : application/views/projeto/adesao.php
$lang['CADASTRO DE APOIO<br>AO PROJETO HYDROS'] = "REGISTER OF HYDROS <br> PROJECT SUPPORT";
$lang['Se você apoia o Projeto Hydros divulgue aqui<br> seu nome ou a marca de sua empresa.'] = "If you support Hydros Project, publish here your name or your company’s brand.";
$lang['Cadastro de Pessoas'] = "PEOPLE";
$lang['cadastro de <br> <span class="maior">PESSOAS</span>'] = "<span=\"maior\">PEOPLE</span>";
$lang['Cadastro de Empresa'] = "COMPANY";
$lang['cadastro de <br> <span class="maior">EMPRESAS</span>'] = "<span=\"maior\">COMPANY</span>";
$lang['nome completo'] = "Full Name";
$lang['país'] = "Country";
$lang['CADASTRAR'] = "REGISTER";
$lang['O Projeto Hydros não divulga publicamente os e-mails cadastrados nem fornece seus cadastros a terceiros. A solicitação do endereço de e-mail neste caso será usada apenas se for necessário confirmar alguma alteração. O Projeto Hydros se reserva o direito de excluir cadastros a seu critério.'] = "Hydros Project does not publicly disclose registered e-mail or provide its records to third parties. The application of e-mail in this case will only be used if necessary to confirm any changes. Hydros Project reserves the right to delete entries at its discretion.";
$lang['nome da empresa'] = "Name of the <br> Company";
$lang['nome do<br> contato responsável'] = "Name of the <br>responsible contact";
$lang['website'] = "site";
$lang['procurar imagem'] = "search image";
$lang['imagem da marca<br> da empresa'] = "Brand's company logo";
$lang['Pronto, seu cadastro foi feito com sucesso.'] = "Done, your registration was successful.";
$lang['Você deu o primeiro passo para se tornar um Embaixador da Água.'] = "You gave the first step to become a Water Ambassador.";
$lang['Quer saber mais? Acompanhe as novidades do Projeto Hydros sobre a água por meio de nossas redes sociais.'] = "Want to learn more? Follow the Hydros Project news on our social medias.";
$lang['Curta, compartilhe, siga e participe!'] = "Like, share, follow and join!";
$lang['A sua empresa deu o primeiro passo para se tornar uma Embaixadora da Água.'] = "Your comapany gave the first step to become a Water Ambassador.";
$lang['Curta, compartilhe com seus colaboradores, siga e participe!'] = "Like, share, follow and join!";

$lang['GRANDES'] = "BIG";
$lang['MÉDIAS'] = "MEDIUM";
$lang['PEQUENAS'] = "SMALL";
$lang['GIGANTOGRAFIAS'] = "BILLBOARD";
$lang['GIGANTOGRAFIAS'] = "BILLBOARD";

$lang['Wallpaper'] = "Wallpaper";
$lang['Marcador de livro'] = "Bookmark";
$lang['Móbile'] = "Mobile";
$lang['Banner'] = "Banner";
$lang['Banners'] = "";
$lang['Banners Digitais'] = "Digital Banners";
$lang['Toalha de bandeja'] = "Tray Towel";
$lang['Cartaz'] = "Poster";
$lang['Adesivo'] = "Sticker";
$lang['Wobler'] = "Computer header";

// PARTE ENVIADA PARA SER TRADUZIDA

$lang['Email já cadastrado!'] = "E-mail registered!";
$lang['Cadastro efetuado com sucesso!'] = "Registration successfully done";
$lang['Erro ao efetuar o cadastro!'] = "Registration error!";
$lang['Insira seu nome e um email válido!'] = "Insert/introduce your name and validate email";
$lang['Obrigado por entrar em contato!<br>Responderemos assim que possível.'] = "Thank you for keeping in touch!<br>We will contact you as soon as possible";
$lang['video_institucional_hydros'] = "";

$lang["Informe seu nome!"] = "Inform your name!";
$lang["Informe seu nome de assinatura!"] = "Inform your signature/subscription";
$lang["Informe seu e-mail!"] = "Inform you e-mail!";
$lang["Informe sua cidade!"] = "Inform your city!";
$lang["Informe seu país!"] = "Inform your country!";
$lang["Informe sua senha!"] = "Inform your password!";
$lang["Informe sua confirmação de senha!"] = "Inform your password confirmation!";
$lang["Suas senhas não conferem!"] = "Your password is not correct";
$lang["Informe seu e-mail de cadastro!"] = "Inform your registred email";
$lang["E-mail não cadastrado!"] = "Email address is not registered ";
$lang["Informe sua senha!"] = "Inform your password!";
$lang["Senha não confere!"] = "Password is not correct";

// Cadastro nos comentários
$lang["NOVO CADASTRO"] = "NEW REGISTER";
$lang["nome completo"] = "Full name";
$lang["nome para assinar os comentários"] = "Name to sign the comments";
$lang["e-mail"] = "E-mail";
$lang["cidade"] = "City";
$lang["país"] = "Country";
$lang["criar senha"] = "Create password";
$lang["confirmar senha"] = "Validate password ";
$lang["se desejar cadastre uma foto para o seu<br> perfil dentro do nosso site!<br>(Tamanho máximo de arquivo: 2Mb)"] = "If you want to register photo<br> in your profile in our website<br>(Maximum file size: 2Mb)";
$lang["SELECIONAR ARQUIVO"] = "Select/choose file";
$lang["Quer receber nossa newsletter com todas as novidades e informações sobre o Projeto Hydros?'"] = "Do you want to receive our newsletter with all news and informations about Hydros Project?";
$lang["SIM"] = "YES";
$lang["NÃO"] = "NO";
$lang["CADASTRAR"] = "REGISTER";
$lang["(*) o endereço de e-mail será usado como login no sistema.<br> O Projeto Hydros não divulga nem fornece seus dados pessoais a terceiros. Leia nossa <a href='politica'>política de privacidade.</a>"] = "(*) e-mail will be used as login in the system.<br> Hydros Project does not publish your personal informations. <a href='politica'>Read our private regulation</a>.";

// Form de comentários
$lang["ENVIAR COMENTÁRIO"] = "SEND COMMENT";
$lang["Olá"] = "HI";
$lang["Deixe seu comentário abaixo"] = "Write your comment below";
$lang["ENVIAR"] = "SEND ";
$lang["CANCELAR | VOLTAR"] = "CANCEL | BACK";

// Lista de comentários
$lang["COMENTE!"] = "COMMENT!";
$lang["COMENTE"] = "COMMENT";
$lang["Ninguém comentou. Seja o primeiro!"] = "Nobody commented. Be the first!";
$lang["1 pessoa comentou"] = "One person commented";
$lang["pessoas comentaram"] = "people commented";

// Login nos comentários
$lang["CONECTE-SE PARA COMENTAR"] = "Connect to comment";
$lang["Se você já é cadastrado, faça o login"] = "If you are registered, please login";
$lang["login (e-mail)"] = "";
$lang["senha"] = "password";
$lang["ENTRAR"] = "ENTER";
$lang["esqueci a senha"] = "forgot my password";
$lang["Se você ainda não é cadastrado,<br> cadastre-se agora!"] = "If you are not registered yet,<br> do it now!";
$lang["CADASTRAR-ME!"] = "REGISTER";

$lang["O Projeto Hydros tenta mostrar o vínculo entre o planeta e o ser humano em relação à água, do qual ainda não estamos conscientes. Levantamos a questão: 'Como me relaciono com a água?'. Não sabemos. Está na hora de percebermos tudo o que fazemos com ela na construção da realidade de uma pessoa, de uma cidade, de um negócio ou de um país, e como cada uma dessas ações afeta o presente, o hoje, o agora."] = "The Hydros Project tries to show the bond between the planet and human beings in relation to water, of which we are still unaware. We ask the following: 'What is my relationship with water?' We do not know. It is time for us to realize everything that we do with it in building the reality of one person, one city, one business, or one country, and how each of these actions affects the present, today, now.";

$lang["EM BREVE"] = "SOON";

$lang["PEGADA HYDROS"] = "HYDROS FOOTPRINT";
$lang["Calcule a quantidade de água que você gasta no seu dia a dia - Pegada Hydros"] = "Calculate the quantity of water that you use in your day-to-day - Hydros Project";
$lang["Projeto Hydros - Pegada Hydros"] = "Hydros Project - Hydros Footprint";

$lang["<h2>RECUPERAÇÃO DE SENHA</h2><p>Uma nova senha foi enviada para o seu e-mail!</p><a href='#' id='botao-cancelar-post' title='VOLTAR'>VOLTAR</a>"] = "<h2>PASSWORD RECOVERY</h2><p>A new password has been sent to your e-mail!</p><a href='#' id='botao-cancelar-post' title='BACK'>BACK</a>";


$lang["HYDROS INSPIRA"] = "HYDROS INSPIRES";
$lang["Nesse espaço, você encontrará materiais feitos por pessoas que tiveram contato com o Projeto Hydros e ficaram sensibilizadas com a sua mensagem, de alguma forma. Portanto, se tiver algum produto ou manifestação artística que foi fruto do seu contato com o Projeto Hydros e que tenha atingido um determinado grupo de pessoas, como, por exemplo, um livro, vídeo, textos, música, exposição etc, envie para hydros@corecomunicacao.com.br. Nós iremos analisar o seu material e publicar aqui. O importante, realmente, é levar essa ideia adiante!"] = "Here you will find materials produced by people who have already known the Hydros Project and has been touched by its message somehow. So if you have any product of art work that had been influenced by the Hydros Project and had reached a certain number of people, like a book, video, text, song, exposition etc., send it to the e-mail <a href='mailto:hydros@corecomunicacao.com.br'>hydros@corecomunicacao.com.br</a>. We will analyze your material and it will have the chance to be published here. The most important is to carry this idea forward!";
$lang["Materiais feitos por pessoas que tiveram contato com o Projeto Hydros - "] = "Materials produced by people who have already known the Hydros Project and has been touched by its message somehow.";
$lang["Projeto Hydros - Hydros Inspira"] = "Hydros Project - Hydros Inspires";
$lang["Nesse espaço, você encontrará materiais feitos por pessoas que tiveram contato com o Projeto Hydros e ficaram sensibilizadas com a sua mensagem, de alguma forma."] = "Here you will find materials produced by people who have already known the Hydros Project and has been touched by its message somehow.";
$lang["O Dom da Água – Regina Moya"] = "The Gift of Water - Regina Moya";
$lang["A artista plástica e escritora mexicana Regina Moya foi sensibilizada pelo Projeto Hydros e, a partir dele, escreveu e ilustrou o livro infantil “O Dom da Água”, que você confere aqui na íntegra. Para conhecer um pouco mais sobre o trabalho da artista acesse <a href='http://www.reginamoya.com' target='_blank'>http://www.reginamoya.com</a>."] = "The Mexican writer and artist Regina Moya was sensitized by Hydros Project and, from it, wrote and illustrated the children's book \"The Gift of Water\", which you can check here the full content. To learn more about the artist's work visit <a href='http://www.reginamoya.com' target='_blank'>http://www.reginamoya.com</a>.";



$lang['Concurso "O Homem e a água"'] = "“Man and water” Contest";
$lang['O Grupo Empresarial Kaluz promoveu um concurso cultural de fotografias, em 2012, englobando todos os seus colaboradores ao redor do mundo, com o tema “O homem e a água”. Veja aqui as cinco imagens vencedoras que foram inspiradas também nas mensagens de sensibilização que o Projeto Hydros trouxe ao cotidiano dos profissionais.'] = "The Kaluz Business Group promoted a cultural contest of photographs in 2012, encompassing all its employees around the world, with the theme \"Man and water.\" Here are the five winning images that were inspired also in the awareness messages that Hydros Project has brought to everyday professionals.";
$lang['<strong>1º Lugar</strong> - María de los Ángeles Avilés Gamboa, da Mexichem, com o tema “Mar Morto, reviva!”'] = "<strong>1st Place</strong> - María de los Ángeles Avilés Gamboa, from Mexichem, with the theme \"Dead Sea, relive!\"";
$lang['<strong>2º Lugar</strong> - Emiel van den Boomen, da Wavin Holanda, com o tema “Banhando nas águas sagradas do templo Tirta Empul em Bali”'] = "<strong>2nd Place</strong> - Emiel van den Boomen, from Netherlands Wavin, with the theme \"Bathing in holy water at Tirta Empul temple in Bali\"";
$lang['<strong>3º Lugar</strong> - Liliana Echeverri Branch, da Mexichem Colombia S.A.S., com o tema “Os meninos do Pacífico são água”'] = "<strong>3rd Place</strong> - Liliana Echeverri Rama, from Colombia Mexichem SAS, with the theme \"The Pacific kids are water\"";
$lang['<strong>4º Lugar</strong> - Diego Alejandro David Vázquez, da Mexichem, com o tema “Com os pés em mais águas”'] = "<strong>4th Place</strong> - Diego Alejandro David Vázquez, from Mexichem, with the theme \"With the feet in more water\"";
$lang['<strong>5º Lugar</strong> - Filemón Peruyero Solís, da Mexichem TI, com o tema “Agua: meio de transporte e sustento diário”'] = "<strong>5th Place</strong> - Filemón Peruyero Solís, from IT Mexichem, with the theme \"Water: daily transportation and sustenance\"";
$lang["A Mexichem Brasil realizou um concurso com as escolas que receberam o Projeto Hydros e foram sensibilizadas por sua mensagem de preservação da água. Os jovens estudantes tinham que criar manifestações artísticas sozinhos ou em grupos. Aqui você confere os vencedores nacionais e o regionais."] = "Mexichem Brazil held a contest with the schools that received the Hydros Project and were sensitized by its message of water conservation. The young students had to create artistic manifestations alone or in groups. Here you can see the regional and national winners.";
$lang["Concurso Mexichem Brasil 2012"] = "Mexichem Brazil 2012 Contest";
$lang["Vencedores Nacionais"] = "National Winners";
$lang["Vencedores Regionais"] = "Regional Winners";
$lang["Alunos de 5ª a 8ª Série"] = "Students from middle school";

$lang['Game Hydros'] = "Hydros Game";
$lang['O Game Hydros ensina, de forma divertida e interativa, as formas de captação, reuso e descarte de água. Além disso, ele se passa em algumas das cidades que representam os locais de atuação do Projeto Hydros: Cidade do México, São Paulo, Nova Iorque, Tóquio e Amsterdã.  Em cada cidade, são 5 fases, que ficam mais difíceis, de acordo com a evolução do usuário no jogo.'] = "The Hydros Game teaches in a very interactive, funny manner, the means for water collection, reuse and disposal. In addition, the game scenery is composed by some cities that represent the reach of the Hydros Project: Mexico City, Sao Paulo, New York, Tokyo and Amsterdam. In each one, there are five phases that becomes more difficult according to the user’s evolution.";
$lang['Usando o sensor de gravidade dos smartphones e tablets, o jogador direciona o fluxo da água pelos canos, para os lugares da casa em que cada tipo de água deve ser utilizado - de chuveiros e pias a irrigação de hortas e abastecimento de reservatórios de água de reuso.'] = "Using the gravity sensor of smartphones and tablets, the player indicates the direction of the water flow through the piping and to the places of the house where each type of water should be used – from the showers and sinks to the garden irrigation and supply of reuse water tanks.";
$lang['A partir de agora, você também tem o destino da água do planeta em suas mãos!'] = " ";

$lang["HYDROS E VOCÊ"] = "HYDROS AND YOU";
$lang["REVISTA AQUA VITAE &bull; A INSPIRAÇÃO HYDROS"] = "AQUA VITAE MAGAZINE &bull; THE HYDROS INSPIRATION";
$lang['Revista Aqua Vitae'] = 'Aqua Vitae Magazine';
$lang["A Aqua Vitae é uma revista criada pelo Grupo Empresarial Kaluz com o objetivo de debater ideias sobre a problemática do recurso hídrico.  Por isso, promoveu como uma de suas linhas de atuação, identificar especialistas mundiais sobre o tema, que encontram nas páginas da publicação a possibilidade de fortalecer com seus pensamentos, investigações e aportes, uma discussão séria e proativa acerca das diferentes facetas que o recurso hídrico tem para o desenvolvimento dos seres humanos, em harmonia com seu uso, manejo e conservação. Disponibilizamos as edições lançadas e que inspiraram a série fotográfica Hydros."] = "The Aqua Vitae is a magazine created by Kaluz Business Group in order to discuss ideas about the problem of water resources. Therefore promoted as one of its business lines, identify global experts on the subject, which found on the pages of the publication the possibility of strengthening with their thoughts, research and contributions, a serious and proactive discussion about the different facets that the water resource has for the development of human beings in harmony with its use, management and conservation. We provide the editions launched and that inspired the photographic series Hydros.";

$lang['Quais as informações que recolhemos?'] = "What information do we collect? ";
$lang['Coletamos informações de você quando você se cadastra em nosso site ou subscreve a nossa newsletter.'] = "We collect information from you when you register on our site or subscribe to our newsletter. ";
$lang['Ao encomendar ou registar no nosso site, conforme o caso, você pode ser solicitado a digitar o seu: nome, endereço de e-mail ou endereço postal. Você pode, entretanto, visitar nosso site de forma anônima.'] = "When ordering or registering on our site, as appropriate, you may be asked to enter your: name, e-mail address or mailing address. You may, however, visit our site anonymously.";
$lang['Como usamos as suas informações?'] = "What do we use your information for? ";
$lang['Qualquer uma das informações que coletamos de você pode ser usado para enviar e-mails periódicos.'] = "Any of the information we collect from you may be used to send periodic emails.";
$lang['O endereço de e-mail que você fornecer para processamento de pedidos, pode ser usado para enviar informações e atualizações referentes ao seu fim, além de receber a notícia ocasional da empresa, atualizações, produto relacionado ou informações de serviço, etc'] = "The email address you provide for order processing, may be used to send you information and updates pertaining to your order, in addition to receiving occasional company news, updates, related product or service information, etc.";
$lang['Como protegemos a sua informação?'] = "How do we protect your information? ";
$lang['Implementamos uma série de medidas de segurança para manter a segurança de suas informações pessoais quando você entra, enviar ou acessar suas informações pessoais.'] = "We implement a variety of security measures to maintain the safety of your personal information when you enter, submit, or access your personal information. ";
$lang['Nós usamos cookies?'] = "Do we use cookies? ";
$lang['Nós divulgamos qualquer informação a terceiros?'] = "Do we disclose any information to outside parties? ";
$lang['Nós não vendemos, comercializamos ou transferimos a terceiros as suas informações pessoalmente identificáveis. Isso não inclui terceiros de confiança que nos auxiliam no funcionamento do nosso site, conduzindo nosso negócio, ou serviço para você, desde que as partes concordem em manter esta informação confidencial. Podemos também divulgar as suas informações quando acreditamos que é apropriado para cumprir a lei, fazer cumprir as nossas políticas de site, ou proteger nosso ou de outros direitos, propriedade ou segurança. No entanto, as informações do visitante não identificáveis ​​podem ser fornecidas a terceiros para marketing, publicidade, ou outros usos.'] = "We do not sell, trade, or otherwise transfer to outside parties your personally identifiable information. This does not include trusted third parties who assist us in operating our website, conducting our business, or servicing you, so long as those parties agree to keep this information confidential. We may also release your information when we believe release is appropriate to comply with the law, enforce our site policies, or protect ours or others rights, property, or safety. However, non-personally identifiable visitor information may be provided to other parties for marketing, advertising, or other uses.";
$lang['Links de terceiros'] = "Third party links ";
$lang['Nós não incluímos ou oferecemos produtos ou serviços de terceiros no nosso site. No entanto, buscamos proteger a integridade do nosso site e agradecemos qualquer feedback sobre o nosso site.'] = "We do not include or offer third party products or services on our website. Nonetheless, we seek to protect the integrity of our site and welcome any feedback about our site.";
$lang['Childrens Privacidade Online Protection Act Compliance'] = "Childrens Online Privacy Protection Act Compliance ";
$lang['Estamos em conformidade com os requisitos da COPPA (Childrens online Privacy Protection Act).'] = "We are in compliance with the requirements of COPPA (Childrens Online Privacy Protection Act), our policy is located at .";
$lang['Política apenas de Privacidade Online'] = "Online Privacy Policy Only";
$lang['Esta política de privacidade online se aplica somente às informações coletadas através de nosso site e não a informações coletadas offline.'] = "This online privacy policy applies only to information collected through our website and not to information collected offline.";
$lang['Termos e Condições'] = "Terms and Conditions ";
$lang['Por favor, visite o nosso <a href="legal" title="Aviso Legal">Aviso Legal</a>, seção que estabelece o uso, renúncias e limitações de responsabilidade que regem a utilização do conteúdo de nosso website em <a href="http://www.projetohydros.com" title="Projeto Hydros">www.projetohydros.com</a>'] = "Please also visit our <a href='legal' title='Aviso Legal'>Legal Warning</a> section establishing the use, disclaimers, and limitations of liability governing the use of our website at <a href='http://www.projecthydros.com' title='Project Hydros'>www.projecthydros.com</a>";
$lang['Seu consentimento'] = "Your Consent ";
$lang['Ao utilizar nosso site, você concorda com a nossa política de privacidade online.'] = "By using our site, you consent to our online privacy policy.";
$lang['Alterações à nossa Política de Privacidade'] = "Changes to our Privacy Policy ";
$lang['Se nós decidirmos mudar nossa política de privacidade, vamos atualizar data de modificação da política de privacidade abaixo.'] = "If we decide to change our privacy policy, we will update the Privacy Policy modification date below. ";
$lang['Esta política foi modificada pela última vez em <strong>10/06/2013</strong>.'] = "This policy was last modified on <strong>10/06/2013</strong>";
$lang['Esta política está em conformidade da Trust Guard PCI compliance.'] = "This policy is powered by Trust Guard <a href='http://www.trust-guard.com/PCI-Compliance-s/65.htm' target='_blank'>PCI</a> compliance.";

$lang['Sim, nós usamos cookies para melhorar a experiência online do usuário do nosso website. Para quem não sabe, cookies são arquivos que armazenam dados de forma temporária no disco rígido do usuário, para que o website promova uma navegação mais conveniente. Os cookies podem evitar que uma mesma informação seja solicitada mais de uma vez ao mesmo usuário. No caso do nosso website utilizamos cookies com as seguintes finalidades:'] = "Yes, we use cookies to enhance the user's online experience of our website. For those who do not know, cookies are files that store data temporarily in the user's hard disk so that the website promotes navigation more convenient. Cookies can ensure that the same information is requested more than once to the same user. In the case of our website use cookies for the following purposes:";
$lang['- manter o usuário logado após realizar o login para fazer comentários'] = "- Keep the user logged in after logging in to comment";
$lang['- armazenar qual a linguagem selecionada caso o usuário escolha a língua clicando nos botões do cabeçalho'] = "- To store the selected language if the user choose the language by clicking on the header";
$lang['- armazenar qual foi a última página acessada sempre, para saber como redirecionar no caso de seleção de um novo idioma'] = "- To store the last page accessed for how to redirect in case of selecting a new language";

$lang['Concurso Cultural Água em 1 minuto'] = "Water in 1 minute Cultural Contest";
$lang['Se a sua escola já recebeu o Projeto Hydros e a sua mensagem de conscientização e sensibilização sobre a água, está na hora de mostrar o que você aprendeu! Participe do Concurso Cultural “Água em 1 minuto” promovido pelo Projeto Hydros em parceria com o Grupo Empresarial Kaluz.'] = "If your school has already received Hydros Project and its awareness and sensitization message about water, it’s time to demonstrate what you have learned! Take part in the “Water in 1 minute” Cultural Contest, promoted by Hydros Project in partnership with the Grupo Empresarial Kaluz.";
$lang['Você encontra os materiais referentes ao concurso aqui:'] = "Here, you can find materials related to the competition:";
$lang['Regulamento'] = "Regulation";
$lang['Flyer'] = "";
$lang['Cartaz A3'] = "Poster A3";
$lang['Cartaz A4'] = "Poster A4";
$lang['Disponibilizamos abaixo todos os materiais do Projeto Hydros que podem, de alguma maneira, te inspirar para o concurso.'] = "Below, we make available all the Hydros Project’s materials which can, in some way, inspire you for the competition.";
$lang['Vídeo'] = "Video";
$lang['Se tiver alguma dúvida sobre o concurso, você pode nos contatar através do e-mail <a href="mailto:hydros@corecomunicacao.com.br">hydros@corecomunicacao.com.br</a> ou entrar em contato com o Embaixador da Água que entrou em contato com a sua escola.'] = "If you have any question about the competition, get in touch with us by this mail <a href='mailto:hydros@corecomunicacao.com.br'>hydros@corecomunicacao.com.br</a> or get in touch with the Water Ambassador who has entered in communication with your school.";

$lang['Música "Planeta" tem inspiração no Hydros'] = "The song \"Planet\" has been inspired in Hydros";
$lang['A letra foi criada pela professora de Ciências da Escola Municipal Professora Lacy Luiza da Crus Flores, Alice Sitta Perera. Já a aluna Hillary do 9º ano ficou responsável por criar a melodia e cantar a música que fala nosso planeta, em especial sobre a água. A escola recebeu o Projeto Hydros em 2012 e resolveu ajudar de alguma maneira. O trabalho, que ficou maravilhoso, você confere aqui:'] = "Lyrics were created by the teacher in Science Alice Sitta Perera, from the Municipal School \"Teacher Lacy Luiza da Cruz Flores\". On the other hand, the 9th grade student Hillary was the music’s author and the responsible for singing this song, which tells about our planet, especially about water. That school received the Hydros Project in 2012, and they decided to give a help, in any way. The work, which resulted wonderful, can be seen here:";

$lang['Veja imagens das manifestações artísticas de pequenos embaixadores.'] = "See the images about little ambassadors’ artistic expressions.";
$lang['Por meio de um concuso cultural na Argentina, pequenos Embaixadores da Água foram sensibilizados e inspirados pela mensagem que o Projeto Hydros traz às pessoas. Aqui, você pode ver os vencedores segurando suas verdadeiras obras de arte!'] = "By means of a cultural contest in Argentina, little water ambassadors have been sensitized and inspired by the message Hydros Project brings people. Here, you can look at the winners holding their true works of art!";
?>