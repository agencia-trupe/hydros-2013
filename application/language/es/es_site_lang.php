<?php

// ARQUIVO : application/views//home.php
$lang['Materiais para download'] = "Materiales para descargar";
$lang['Com os MATERIAIS DISPONÍVEIS você pode realizar ações concretas e se tornar um verdadeiro EMBAIXADOR DA ÁGUA!'] = "¡Con los MATERIALES DISPONIBLES puedes realizar acciones concretas y convertirte en un verdadero EMBAJADOR DEL AGUA!";
$lang['Vídeos'] = "Vídeos";
$lang['vídeos'] = "vídeos";
$lang['Galeria de Imagens'] = "Galería de imágenes";
$lang['galeria de <br> imagens'] = "Galería de<br> imágenes";
$lang['Exposição Hydros'] = "Exposición Hydros";
$lang['exposição <br> hydros'] = "Exposición <br> Hydros";
$lang['Material de Apoio'] = "Material de apoyo";
$lang['material de <br> apoio'] = "Material de <br> apoyo";
$lang['Saiba como usar o Projeto Hydros na sua escola'] = "Conoce la manera de usar el Proyecto Hydros en tu escuela";
$lang['O tutorial “Como usar o Projeto Hydros na escola” é o material ideal para que você, profissional da área da Educação, saiba como utilizar a cartilha Projeto Hydros na Escola e possa conscietizar os seus alunos sobre o tema água.'] = "El tutorial \"Cómo usar el Proyecto  Hydros en la escuela\" es el material ideal para que como profesional  del área de Educación, sepas cómo utilizar el manual Proyecto Hydros en tu escuela  y puedas promover la toma de conciencia  de tus alumnos acerca del tema del agua.";
$lang['Siga-nos!'] = "¡Síguenos!";
$lang['projetoHydros'] = "proyectoHydros";
$lang['FIQUE POR DENTRO'] = "¡MANTENTE AL TANTO!";
$lang['Notícias da água'] = "Noticias del agua";
$lang['Conceito criado pela Convenção de Ramsar para denominar o conjunto de áreas alagáveis, como lagos, manguezais e pântanos.'] = "Concepto creado por la Convención de Ramsar para denominar el conjunto de áreas inundables, como lagos, manglares y pantanos.";
$lang['leia mais'] = "lea más";
$lang['Mapa de recursos hídricos no mundo'] = "Mapa de recursos hídricos en el mundo";
$lang['Veja aqui dados sobre os recursos hídricos de cada país, além de descobrir como esses recursos são utilizados em cada região.'] = "Ve aquí datos sobre los recursos hídricos de cada país, además de descubrir la manera como esos recursos son utilizados en cada región.";
$lang['Cadastre-se para receber novidades sobre o PROJETO HYDROS!'] = "¡Regístrese para recibir novedades sobre el PROYECTO HYDROS!";
$lang['nome'] = "Nombre ";
$lang['e-mail'] = "Correo electrónico";
$lang['ENVIAR'] = "ENVIAR";
$lang['Áreas úmidas'] = "Áreas húmidas";

// ARQUIVO : application/views//pegada.php
$lang['PEGADA HYDROS'] = "HUELLA HYDROS";
$lang['Já imaginou calcular a quantidade de água que você gasta no seu dia a dia, ao lavar as mãos ou escovar os dentes, por exemplo?'] = "¡Imagínate calcular la cantidad de agua que utilizas en tu día a día, para lavarte las manos o cepillarte los dientes, por ejemplo!";
$lang['Agora você pode descobrir a sua pegada hídrica com o aplicativo do Projeto Hydros, o Pegada Hydros. Basta ter um tablet ou smartphone que tenha App Store (iPhone) ou Google Play (Android) como plataforma de distribuição de aplicativo e fazer o download gratuito do App.'] = "Ahora puedes descubrir tu huella hídrica con la aplicación del Proyecto Hydros, la Huella Hydros. Basta tener un tablet o smartphone con acceso a la App Store (iPhone) o al Google Play (Android) como plataforma de distribución de aplicaciones y hacer el download gratuito.";
$lang['Google Play'] = "Google Play";
$lang['iTunes'] = "iTunes";
$lang['Aguarde...'] = "Pronto...";
$lang['Logo mais, o aplicativo Pegada Hydros se desdobrará em um game que irá ensinar, de forma divertida e interativa, as formas de captação, reuso e descarte de água. Além disso, ele se passará em algumas das cidades que representam os locais de atuação do Projeto Hydros: Cidade do México, São Paulo, Nova Iorque, Tóquio e Amsterdã. Em cada cidade, serão 5 fases, que vão ficando mais difíceis, de acordo com a evolução do usuário no jogo.'] = "Y hay más: en breve, la aplicación de la Huella Hydros se desdoblará en un juego que enseñará, de manera divertida e interactiva, las formas de captación, reutilización y disposición del agua. Adicionalmente, el juego tiene como escenario ciudades representativas dela  actuación del Proyecto Hydros: Ciudad de México, São Paulo, Nueva York, Tokio y Amsterdam. En cada una de ellas, habrá cinco fases, que irán aumentando su dificultad, de acuerdo con la evolución del usuario en el juego.";
$lang['Usando o sensor de gravidade dos smartphones e tablets, o jogador terá de direcionar o fluxo da água pelos canos, para os lugares da casa em que cada tipo de água deve ser utilizado - de chuveiros e pias a irrigação de hortas e abastecimento de reservatórios de água de reuso.'] = "Usando el sistema de los smartphones y tablets, el jugador deberá indicar la dirección del flujo de agua por las tuberías, para los lugares de la casa en que cada tipo de agua debe ser utilizado - de duchas y pilas a la irrigación de huertas y abastecimiento de tanques de agua de reuso.";

// ARQUIVO : application/views//contato.php
$lang['Contato'] = "Contacto";
$lang['telefone'] = "teléfono";
$lang['mensagem'] = "mensaje:";
$lang['Quer receber nossa newsletter com todas as novidades e informações sobre o Projeto Hydros?'] = "¿Quieres recibir nuestra newsletter con todas las novedades e informaciones sobre el Proyecto Hydros?";
$lang['SIM'] = "SI";
$lang['NÃO'] = "NO";

// ARQUIVO : application/views/common/menu.php
$lang['Compartilhe e acompanhe o Projeto Hydros em todas as redes sociais!'] = "Acompaña las novedades de Hydros en nuestras redes sociales!";
$lang["Conheça o Projeto Hydros"] = "Conozca a los proyecto Hydros";
$lang["O Projeto Hydros é uma iniciativa que tem o objetivo de disseminar a conscientização sobre temas relacionados à água e ao desperdício dos nossos recursos naturais. Se você quer fazer parte do nosso time e passar a ideia adiante, torne-se um Embaixador da Água! É super rápido e fácil, e você já estará fazendo a diferença com apenas um clique. .... www.projetohydros.com"] = "El Proyecto Hydros es una iniciativa que tiene como objetivo difundir y crear conciencia acerca de los problemas relacionados con el agua y los residuos de nuestros recursos naturales. Si quieres ser parte de nuestro equipo y pasar la idea adelante, ¡tórnese un Embajador del Agua! Es rápido y fácil, y usted ya estará marcando la diferencia con solo un clic. .... www.proyectohydros.com";
$lang['HOME'] = "HOME";
$lang['PROJETO HYDROS'] = "PROYECTO HYDROS";
$lang['MATERIAIS'] = "MATERIALES";
$lang['Contexto'] = "Contexto";
$lang['O que é o projeto?'] = "¿Qué es el proyecto?";
$lang['Quais são os desafios?'] = "¿Cuáles son los retos?";
$lang['Quem apoia?'] = "¿Quién apoya?";
$lang['Como eu posso participar?'] = "¿Cómo yo puedo participar?";
$lang['Cartilha Projeto Hydros Na Escola'] = "manual Proyecto Hydros en la Escuela";
$lang['CONTATO'] = "CONTACTO";
$lang['Mapa de Recursos Hídricos no Mundo'] = "Mapa de los recursos hídricos en el mundo";
$lang['Notícias da Água'] = "Noticias del agua";
$lang['Giro pelo Projeto Hydros'] = "Giro por el Proyecto Hydros";

// ARQUIVO : application/views/common/footer.php
$lang['contexto'] = "contexto";
$lang['o que é o projeto?'] = "¿qué es el proyecto?";
$lang['quais são os desafios?'] = "¿cuáles son los desafíos?";
$lang['quem apoia?'] = "¿quién apoya?";
$lang['como eu posso participar?'] = "¿cómo puedo participar?";
$lang['galeria de imagens'] = "galería de imágenes";
$lang['exposição hydros'] = "exposición hydros";
$lang['material de apoio'] = "material de apoyo";
$lang['Cartilha Projeto Hydros na Escola'] = "Manual Proyecto Hydros en la Escuela";
$lang['mapa de recursos hídricos no mundo'] = "mapa de los recursos hídricos en el mundo";
$lang['notícias'] = "noticias del agua";
$lang['giro'] = "giro";
$lang['política de privacidade'] = "política de privacidad";
$lang['Política de Privacidade'] = "Política de privacidad";
$lang['aviso legal'] = "aviso legal";
$lang['Aviso Legal'] = "Aviso Legal";
$lang['Projeto Hydros'] = "Proyecto Hydros";
$lang['Todos os direitos reservados'] = "Todos los derechos reservados";
$lang['Coordenação:'] = "Coordinación";
$lang['Criação de sites:'] = "Creación de sitios";

// ARQUIVO : application/views/common/header.php
$lang['site_keywords'] = "Proyecto Hydros, Agua, Sostenibilidad, Consumo de agua, Consumo Consciente, Día Mundial del Agua, Consumo Consciente del gua, un planeta sostenible";

// ARQUIVO : application/views/materiais/videos.php
$lang['A água em nós'] = "El agua en nosotros";
$lang['Os vídeos “Água em nós”, composto por um filme mais completo e outros oito featurettes, são ferramentas de conscientização e sensibilização que mostram, de forma lúdica, a verdadeira relação que temos com a água em todos os seus aspectos.  Portanto, use-os à vontade: publique no seu mural do Facebook e outras redes sociais ou faça o download para usá-los no ambiente de trabalho, com a família e amigos.'] = "El video \"Agua en nosotros\" es una herramienta de concientización y sensibilidad que muestra, de una forma lúdica, la verdadera relación que tenemos con el agua en todos sus aspectos. Así, que lo puedes utilizar a tu antojo: publícalo en tu muro de Facebook y en otras redes sociales o descárgalo para usarlo en el trabajo, con la familia y los amigos.";
$lang['Faça a diferença agindo diferente!'] = "¡Haz la diferencia al actuar de manera diferente!";
$lang['Faça o download!'] = "Descargar!";
$lang['Compartilhe!'] = "Compártelo!";
$lang['Compartilhe por e-mail!'] = "Enviar por email!";
$lang['Compartilhe pelo facebook!'] = "Compartir en facebook!";
$lang['Compartilhe no Tumblr'] = "Compartir en Tumblr";
$lang['Compartilhe no Tumblr!'] = "Compartir en Tumblr!";
$lang['Advertência Legal de Utilização de Conteúdo'] = "Advertencia Legal de Utilización de Contenido";
$lang['O conteúdo do hotsite é de utilização livre, desde que observadas as regras a seguir estabelecidas:'] = "El contenido del hotsite es de libre utilización, a condición de que las reglas sean observadas y seguidas:";
$lang['O objetivo do hotsite é exclusivamente informativo. O material exposto não pode ser utilizado para qualquer finalidade e/ou propósito contrário a esse fim, nem poderá ser copiado e/ou reproduzido sem a divulgação clara e expressa da fonte, creditando a autoria do texto, fotografia e/ou vídeo, produção etc., cujo nome se encontra indicado junto a cada material exposto. Exemplo: "www.projetohydros.com / (nome completo do autor). Todos os direitos reservados".'] = "El objetivo del hotsite es sólo para fines informativos. El material expuesto no se puede utilizar para cualquier fin y/o propósito contrario, ni pondrá ser copiado y/o reproducido sin el consentimiento expreso y una indicación clara de la fuente, acreditando la autoría del texto, fotografía y/o vídeo, fotógrafo, productor, etc., cuyo nombre aparece al lado de cada material expuesto. Ejemplo: “www.proyectohydros.com / (nombre completo del autor). Todos los derechos reservados”.";
$lang['A alteração de conteúdo e/ou distorções de qualquer natureza, como montagens de imagens, de voz ou outras, é expressamente proibida, em qualquer hipótese, podendo ser punida na forma da lei.'] = "El cambio de contenido y/o distorsión de ningún tipo, como montajes de imágenes, de voz o de otro tipo, está expresamente prohibido, en cualquier caso,  puede ser castigado de acuerdo con la ley.";
$lang['É vedada, também, a veiculação do conteúdo deste hotsite, mesmo com indicação de autor, com a finalidade de lucro pessoal e/ou corporativo, ou com associação a religião, política, discriminação, pedofilia e/ou outras práticas ilícitas.'] = "Se prohíbe también la colocación de los contenidos de este hotsite, incluso con indicación del autor, con el fin de obtener beneficios personales y/o empresariales, o asociación con la religión, la política, la discriminación, el abuso o prácticas ilícitas.";
$lang['A indicação de fonte efetuada por terceiro que utilizar os textos, imagens ou vídeos deste hotsite deverá conter, necessariamente, o endereço do domínio e o nome completo do autor do texto, fotografia ou vídeo correspondente.'] = "La indicación de fuente hecha por un tercero para utilizar los textos, imágenes o videos de este hotsite debe contener, necesariamente, la dirección de dominio y el nombre completo del autor del texto, foto o video correspondiente.";

// Vídeos do cocoricó não aparecem em ES e EN
$lang['Em 2012, o Projeto Hydros ganhou importantes aliados, por meio de uma iniciativa da Mexichem Brasil: a série “Pílulas de Cultura” estrelada pela Turma do Cocoricó. Veiculada na TV Cultura, a série foca o tema da água e é um ótimo exemplo de como o Projeto Hydros possui inúmeras possibilidades de interação. Reveja os vídeos.'] = "#";
$lang['Personagens da Turma do Cocoricó estrelam filmetes do Projeto Hydros'] = "#";
$lang['Água'] = "#";
$lang['Não pode faltar água nesse mundo'] = "#";
$lang['Entre nessa onda!'] = "#";
$lang['Água limpa é vida!'] = "#";
$lang['Tudo é água'] = "#";

// ARQUIVO : application/views/materiais/galeria.php
$lang['Um dos grandes destaques do Projeto Hydros são as fotografias que compõem os quatro livros da série, de autoria de artistas latino-americanos e europeus. São mais de 200 imagens para você utilizar. Aqui você encontra a opção de compartilhá-las ou baixá-las em todos os tamanhos.'] = "Uno de los aspectos más destacados del Proyecto Hydros son las imágenes que componen los cuatro libros de la serie, escritos por artistas latinoamericanos y europeos. Hay más de 200 fotografías para su uso. Aquí se encuentra la opción de compartirlos o descargarlos en todos los tamaños.";
$lang['DOWNLOAD DA GALERIA <br>DE IMAGENS COMPLETA'] = "DESCARGAR LA GALERÍA<br> DE IMÁGENES COMPLETA";
$lang['Você pode fazer o download da <br>galeria completa em alta resolução.'] = "Puedes descargar la galería<br> completa en alta resolución.";
$lang['clique para iniciar o download'] = "click para iniciar la descarga";
$lang['DICA'] = "SUGERENCIA";
$lang['Para que sua iniciativa tenha um resultado mais positivo e impactante, sugerimos a impressão do material selecionado em papel fotográfico e em alta resolução.'] = "Para que tu iniciativa tenga un resultado más positivo e impactante, se sugiere imprimir el material seleccionado en papel fotográfico y en alta resolución.";
$lang['Clique nas imagens para ampliar e ver detalhes:'] = "Haz clic en las imágenes para ampliarlas y ver los detalles.";

// ARQUIVO : application/views/materiais/exposicao.php
$lang['Você pode utilizar de 8 até 26 imagens, dentre as mais de 200 extraídas da coleção de livros Hydros, para criar sua própria exposição. Pense em um espaço disponível, como escolas, escritórios, estações do metrô, parques, ou mesmo museus. Disponha as fotos de uma forma que toda a mensagem embutida possa ser apreciada pelos visitantes.'] = "Puedes utilizar desde ocho hasta 26 imágenes entre las más de 200 procedentes de la colección libros Hydros para crear tu propia exposición. Piensa en un espacio como escuelas, oficinas, estaciones de metro, parques e incluso museos. Organiza las fotos en una manera que todo el mensaje puede ser disfrutado por los visitantes.";
$lang['Selecione a exposição que deseja e faça o download!'] = "Selecciona la exposición que desees y descárgala!";
$lang['imagens'] = "imágenes";

// ARQUIVO : application/views/materiais/cartilha.php
$lang['Cartilha Projeto Hydros na Escola'] = "Cartilla Proyecto Hydros en la Escuela";
$lang['Pensando em sensibilizar e ensinar crianças nas escolas sobre a escassez da água, o Projeto Hydros criou a cartilha “Projeto Hydros na Escola”. Juntamente com as exposições e materiais de apoio (que podem ser utilizados em sala de aula), a cartilha foi criada para que suas atividades possam ser incluídas no planejamento escolar. Existem duas versões: uma mais completa e outra resumida, com ações mais pontuais.'] = "Pensando en la conciencia y en enseñar a los niños en las escuelas sobre la escasez del agua, el Proyecto Hydros creó el folleto \"Proyecto Hydros en la Escuela\". Junto con las exposiciones y los materiales de apoyo (que pueden ser utilizados en el aula), la cartilla fue creada para que sus actividades puedan ser incluidas en la planificación escolar. Hay dos versiones: una más completa y una forma corta, con acciones más específicas.";

// ARQUIVO : application/views/materiais/videos_detalhe.php
$lang['Voltar'] = "Volver";
$lang['VOLTAR'] = "VOLVER";

// ARQUIVO : application/views/materiais/imagens_detalhes.php
$lang['Fotógrafo'] = "Fotógrafo";
$lang['VOLTAR PARA GALERIA COMPLETA'] = "Volver a la galería";

// ARQUIVO : application/views/materiais/apoio.php
$lang['Cada um de nós, Embaixadores da Água, pode também intervir em seus espaços cotidianos, como no escritório, no carro, nas portas e nos espelhos de nossos ambientes e, até mesmo, em nossa própria casa.'] = "Cada uno de nosotros, Embajadores del Agua, también podemos intervenir en nuestros espacios cotidianos como la oficina, el automóvil, las puertas y los espejos de nuestro entorno e, incluso, en nuestra propia casa.";
$lang['Para isso, projetamos uma campanha composta por diversas peças, que você pode selecionar individualmente ou em conjunto, por cartazes. Use sua imaginação para conscientizar e sensibilizar mais pessoas sobre o Projeto Hydros.'] = "Por lo tanto, tenemos prevista una campaña compuesta de varias partes, que puedes seleccionar individualmente o en forma conjunta. Usa tu imaginación para  concientizar y sensibilizar a más personas, junto al Proyecto Hydros.";
$lang['Wallpaper'] = "Fondo de pantalla";
$lang['ver'] = "ver";
$lang['Marcador de livro'] = "Marcador de libro";
$lang['Testeira para computador'] = "Marco para computadora";
$lang['Móbile'] = "Móbil";
$lang['Banner'] = "Banner";
$lang['Toalha de bandeja'] = "Mantel para bandeja";
$lang['Cartaz'] = "Afiche";
$lang['Adesivo'] = "Adhesivo";
$lang['Banner online'] = "Banner online";

// ARQUIVO : application/views/materiais/apoio_detalhe.php

// ARQUIVO : application/views/novidades/recursos.php
$lang['Saiba mais sobre os recursos hídricos disponíveis (água de superficie e subterrânea) e renováveis e como eles são usados em cada região (agricultura, doméstico e industrial), além da presença do Projeto Hydros em diversos países.'] = "Ve aquí datos sobre los recursos hídricos de cada país, además  de descubrir la manera como esos recursos son utilizados en cada región.";

// ARQUIVO : application/views/novidades/noticias.php
$lang['Acompanhe as últimas informações sobre a água no mundo.'] = "Conoce la información más reciente sobre el agua en el mundo.";

// ARQUIVO : application/views/novidades/giro.php
$lang['Saiba quais ações cada empresa do Grupo Empresarial Kaluz está desenvolvendo para implantar o Projeto Hydros entre seus principais públicos de interesse, como colaboradores, comunidade, escolas etc.'] = "Aprende sobre las acciones que cada empresa del Grupo Empresarial Kaluz está desarrollando para poner en práctica el Proyecto Hydros entre sus principales partes interesadas, como los empleados, la comunidad, las escuelas, etc.";
$lang['VER MAIS'] = "VER MÁS";

// ARQUIVO : application/views/novidades/noticias_detalhes.php
$lang['voltar para a home de notícias'] = "volver a la página de noticias";

// ARQUIVO : application/views/novidades/giro_detalhes.php
$lang['voltar para a home do Giro'] = "volver a la página de Giro";

// ARQUIVO : application/views/projeto/contexto.php
$lang['Preservar a vida de todo o planeta é uma tarefa grande, que começa com ações muito simples'] = "Preservar la vida de todo el planeta es una tarea gigantesca, que empieza con acciones muy simples.";
$lang['O descaso do ser humano com a água está gerando preocupações alarmantes. De acordo com o Relatório Planeta Vivo 2010 da World Wildlife Fund (WWF), a Terra já ultrapassou 30% de sua capacidade de reposição dos recursos necessários para as nossas demandas. Porém, ainda podemos tentar reverter esse quadro, por meio de atitudes simples, em nosso dia a dia.'] = "El descuido del ser humano en relación al agua está generando preocupaciones alarmantes. De acuerdo con el Reporte Planeta Vivo 2010 de la World Wildlife Fund (WWF), la Tierra ya sobrepasó  un 30% de su capacidad de reposición de los recursos necesarios para nuestras demandas. Pero todavía podemos revertir este panorama por medio de actitudes simples, en nuestro día a día.";
$lang['Com a gestão adequada dos recursos hídricos, podemos amenizar o impacto das nossas ações na natureza e, assim, tentar garantir o fornecimento da quantidade e qualidade necessárias, para nós e para as próximas gerações. E você pode começar agora, integrando o grupo de “Embaixadores da Água” do Projeto Hydros.'] = "Con la gestión adecuada de los recursos hídricos, podemos reducir el impacto de nuestras acciones y así, intentar garantizar  el suministro necesario, tanto en calidad como cantidad,  para nosotros y para las próximas generaciones. Y tú puedes empezar ahora, integrándote al grupo de “Embajadores del Agua” del Proyecto Hydros.";

// ARQUIVO : application/views/projeto/apresentacao.php
$lang['Há tempos debatemos a forma como o homem se relaciona com a água'] = "Hace tiempo discutimos la manera cómo el ser humano se relaciona con el agua.";
$lang['Em 2008, a Fundação Kaluz e o Grupo Empresarial Kaluz, assim como as suas empresas Mexichem, Elementia e BX, lançaram uma série de quatro livros de fotografias tratando do tema água, com o objetivo de conscientizar e sensibilizar sobre a importância do uso correto desse elemento. As publicações chegaram às mãos de líderes nacionais, regionais e mundiais, que perceberam o potencial que o projeto trazia.'] = "En 2008, la Fundación Kaluz y el Grupo Empresarial Kaluz, así como sus empresas Mexichem, Elementia y Grupo Financiero BX+, lanzaron una serie de cuatro libros de fotografías sobre el tema del agua, con el objetivo de concientizar y sensibilizar acerca de la importancia del uso correcto de este elemento. Las publicaciones han llegado a las manos de líderes nacionales, regionales y mundiales, que se dieron cuenta del potencial del proyecto.";
$lang['Para ampliar a disseminação da preservação da água, nasceu o Portal Hydros. Interativo e trilíngue (espanhol, português e inglês), o site é o principal canal de comunicação entre o Projeto e seus Embaixadores. O portal tem inúmeras ferramentas que permitem a qualquer instituição ou cidadão comum, promover verdadeiras campanhas de sensibilização sobre a água.'] = "Para multiplicar la conciencia sobre la  preservación del agua, nació el Portal Hydros. Interactivo y trilingüe (español, portugués e inglés), el sitio es el principal canal de comunicación entre el Proyecto  y sus Embajadores. En el portal tienes múltiples herramientas que permiten a cualquier ciudadano o institución, promover verdaderas campañas de sensibilización sobre el agua.";
$lang['O <span class="txt-branco">Projeto Hydros</span> levanta a questão: <br> <span class="maior">\'Como me relaciono com a água?\'</span> <br> Trata-se de um grande chamado para que todos sejam “Embaixadores da água”, a <br> fim de disseminar o uso sustentável dos recursos hídricos.'] = 'El <span class="txt-branco">Proyecto Hydros</span> nos lleva a preguntar: <br><span class="maior">\"¿Cómo me relaciono con el agua?\"</span>. Esto es un gran llamado para que todos seamos “Embajadores del Agua”, a fin de comunicar el uso sustentable de los recursos hídricos.';

$lang['Em 2012, a ação foi lançada para todas as empresas do Grupo Kaluz, incentivando atitudes em todos os países nos quais atua, com foco na conscientização, preservação e sustentabilidade. No mesmo ano, o <strong>Projeto Hydros</strong> também chegou a escolas e comunidades próximas de suas unidades, abrindo um diálogo importante sobre o tema.'] = "En 2012, la acción fue lanzada en todas las empresas del Grupo Empresarial Kaluz, motivando actitudes positivas en personas de todos los países en los cuales actúa, enfocándose en la concientización, preservación y sustentabilidad del agua. Ese  mismo año, el Proyecto Hydros también llegó a las escuelas y comunidades próximas a sus unidades, abriendo un importante diálogo sobre el tema.";
$lang['O desafio para os próximos anos é disseminar, para o maior número possível de pessoas, essa conscientização de que os nossos recursos naturais estão acabando e precisamos fazer algo para reverter essa situação. A ideia é que o <strong>Projeto Hydros</strong> saia dos portões das empresas do Grupo Kaluz e ganhe o mundo.'] = "El reto para los próximos años es extender al mayor número posible de personas la conciencia de que nuestros recursos naturales se están acabando y necesitamos hacer algo para revertir esa situación. La idea es que el <strong>Proyecto Hydros</strong> salga de los muros de las empresas del Grupo Kaluz y gane el mundo.";

// ARQUIVO : application/views/projeto/desafios.php
$lang['Uma questão de <br> consciência e atitude'] = "Una cuestión de conciencia y actitud";
$lang['Um dos desafios para a preservação da água potável do mundo é a conscientização. Por isso, o objetivo de <strong>Projeto Hydros</strong> é fazer com que os resultados já alcançados ao longo de 2012, se expandam ainda mais e envolvam cada vez mais pessoas. Prova desse esforço: 2013 é o Ano Internacional de Cooperação pela Água, da UNESCO.'] = "Uno de los retos para alcanzar la preservación del agua potable en el mundo es la toma de conciencia. Por lo tanto, el objetivo del <strong>Proyecto Hydros</strong> es lograr que los resultados reales obtenidos a lo largo de 2012, se amplíen aún más al involucrar a un mayor número de personas. La prueba de este esfuerzo: 2013 es el Año Internacional de Cooperación para el Agua de la UNESCO.";
$lang['A ideia do <strong>Projeto Hydros</strong> é chamar a atenção da sociedade civil, empresas e estudantes em idade de formação escolar para as questões relacionadas à água e ao saneamento básico. E você pode colaborar, como um Embaixador da Água, abraçando a ideia do Projeto e incentivando todas as pessoas ao seu redor a debater o tema.'] = "La idea del <strong>Proyecto Hydros</strong> es llamar la atención de la sociedad civil, empresas y estudiantes, en edad de aprendizaje sobre temas relacionados con el agua y el saneamiento. Y tú puedes ayudar al convertirte en  un Embajador del Agua, abrazando la idea del Proyecto y animando a todos a tu alrededor para discutir el tema";
$lang['Desafios para a preservação da água potável do mundo'] = "Desafíos para la conservación del agua potable en el mundo";
$lang['A <strong>UNESCO</strong> (Organização das Nações Unidas para a Educação, a Ciência e a Cultura) escolheu <strong>2013</strong> como o <strong>Ano Internacional de Cooperação pela Água</strong>. </p><p>O objetivo é promover uma maior interação entre nações e debater os desafios do manejo da água.</p><p> Segundo a ONU-Água, existe um aumento da demanda pelo acesso, alocação e serviços relacionados a esse bem natural. O ano vai destacar iniciativas de sucesso sobre cooperação pela água.'] = "La <strong>UNESCO</strong> (Organización de las Naciones Unidas para la Educación, la Ciencia y la Cultura) eligió <strong>2013</strong> como el <strong>Año Internacional para la Cooperación para el Agua</strong>. El objetivo es promover una mayor interacción entre las naciones y discutir los retos de la gestión del agua. De acuerdo con la ONU-Agua, hay una demanda creciente de acceso, asignación y servicios relacionados con ese recurso natural.</p><p Durante el año se van a destacar iniciativas del suceso sobre cooperación por el agua.";
$lang['Saiba mais em'] = "Conoce más en : <a href='http://www.unesco.org/new/es/unesco' title='Unesco'>www.unesco.org/new/es/unesco</a>";;

// ARQUIVO : application/views/projeto/apoio.php
$lang['Seja você também um<br>Embaixador da Água!'] = "¡Conviértete tú también en un Embajador del Agua!";
$lang['Simples mudanças de atitude podem fazer a diferença na preservação da água. Hoje já não é mais suficiente apenas fechar a torneira ao lavar a louça ou ao escovar os dentes, por exemplo. O grande desafio é a conscientização. Precisamos mostrar às pessoas que a escassez da água, especialmente a potável, aquela que consumimos, é um problema sério e que devemos tomar as atitudes corretas agora. Uma ação inicial é tornar-se um <strong>Embaixador da Água</strong> do <strong>Projeto Hydros</strong>. '] = "Simples cambios de actitud pueden hacer la diferencia en la preservación del agua. Hoy ya no es suficiente solamente cerrar el grifo al lavar la loza o al cepillarse los dientes, por ejemplo. El gran reto es la toma de conciencia. Necesitamos enseñar a las personas que la escasez del agua, especialmente la potable, aquella que consumimos, es un problema serio y que debemos actuar de manera adecuada ahora. Una acción para lograrlo es convertirse en <strong>Embajador del Agua</strong> del <strong>Proyecto Hydros</strong>.";
$lang['Esta é a lista de pessoas e empresas que decidiram fazer parte desta iniciativa. Seja uma delas e faça a sua parte!'] = "Esta es la lista de personas y empresas que decidieron formar parte de esta iniciativa. ¡Conviértete en uno de ellos y a trabajar!";
$lang['Empresas que apoiam o Projeto Hydros e agora são Embaixadoras da Água.'] = "Empresas que apoyan el Proyecto Hydros y ya  son Embajadoras del Agua.";
$lang['voltar'] = "volver";
$lang['ver mais'] = "ver más";
$lang['Seja um embaixador da água!'] = "¡Yo soy un Embajador del Agua!";
$lang['PARTICIPE!'] = "¡PARTICIPA!";
$lang['Seja um'] = "¡SÉ UN";
$lang['Embaixador'] = "EMBAJADOR";
$lang['da Água!'] = "DEL AGUA!";
$lang['Pessoas que apoiam o Projeto Hydros e agora são Embaixadoras da Água.'] = "Personas que apoyan el Proyecto Hydros y ya son Embajadoras del Agua.";

// ARQUIVO : application/views/projeto/participar.php
$lang['Para ser um Embaixador da Água'] = "Para ser un Embajador del Agua";
$lang["Para ser um"] = "Para ser un";
$lang["Embaixador da Água"] = "Embajador del Agua";
$lang['Para que você consiga utilizar todos os recursos disponíveis pelo <strong>Projeto Hydros</strong> da melhor forma possível, criamos dois tutoriais, com o passo a passo de como utilizar os materiais disponíveis neste site:'] = "Para que puedas utilizar todos los recursos disponibles del Proyecto Hydros de la mejor manera posible, generamos dos tutoriales, con los cuales,  paso a paso, aprenderás a utilizar los materiales disponibles en este site:";
$lang['Público Geral e Colaboradores'] = "público en general y colaboradores";
$lang['um para o público em geral <br> e colaboradores do <br>Grupo Empresarial Kaluz'] = "uno es para el público en general<br> y empleados del<br> Grupo Empresarial Kaluz";
$lang["um para o <br><span class='maior'>Público em geral</span> e<br> <strong>colaboradores do Grupo Empresarial Kaluz</strong>"] = "uno para el <br><span class='maior'>Público en general</span> y<br> <strong>colaboradores del Grupo Empresarial Kaluz</strong>";
$lang["e outro para <br><span class='maior'>Profissionais da Área de Educação</span>"] = "y el otro, para <br><span class='maior'>maestros y profesores</span>";
$lang['clique para iniciar o<br> download'] = "para hacer download,<br> haz click aquí";
$lang['Profissionais'] = "Profesionales";
$lang['e outro para profissionais <br> da área de Educação'] = "y otro para profesionales del área de Educación";
$lang['Você está perto de tornar-se um <strong>Embaixador da Água</strong> e ter as ferramentas necessárias para sensibilizar e conscientizar as pessoas ao seu redor acerca de temas como a correta utilização e preservação da água.'] = "Estás a un paso de convertirte en un <strong>Embajador del Agua</strong> y contar con las herramientas necesarias para sensibilizar y concientizar a las personas cercanas con temas como la utilización correcta y preservación del agua.";
$lang['Participe do Projeto Hydros e faça a sua parte!'] = "¡Participa del Proyecto Hydros y haz tu parte!";

// ARQUIVO : application/views/projeto/adesao.php
$lang['CADASTRO DE APOIO<br>AO PROJETO HYDROS'] = "REGISTRO DE APOYO<br> AL PROYECTO HYDROS";
$lang['Se você apoia o Projeto Hydros divulgue aqui<br> seu nome ou a marca de sua empresa.'] = "Si tú apoyas al Proyecto Hydros, divulga<br> tu nombre o la marca de tu empresa.";
$lang['Cadastro de Pessoas'] = "PERSONAS";
$lang['cadastro de <br> <span class="maior">PESSOAS</span>'] = "cadastro de <br> <span class='maior'>PERSONAS</span>";
$lang['Cadastro de Empresa'] = "EMPRESAS";
$lang['cadastro de <br> <span class="maior">EMPRESAS</span>'] = "cadastro de <br> <span class='maior'>EMPRESAS</span>";
$lang['nome completo'] = "nombre completo";
$lang['país'] = "pais";
$lang['CADASTRAR'] = "REGISTRAR";
$lang['O Projeto Hydros não divulga publicamente os e-mails cadastrados nem fornece seus cadastros a terceiros. A solicitação do endereço de e-mail neste caso será usada apenas se for necessário confirmar alguma alteração. O Projeto Hydros se reserva o direito de excluir cadastros a seu critério.'] = "El Proyecto Hydros no divulga al público los e-mails registrados o comparte sus registros a terceros. La solicitud de dirección de correo electrónico sólo se utiliza si es necesario para confirmar algún cambio. El Proyecto Hydros se reserva el derecho a borrar entradas a su criterio.";
$lang['nome da empresa'] = "nombre de la empresa";
$lang['nome do<br> contato responsável'] = "Nombre del<br> contacto responsable";
$lang['website'] = "sitio";
$lang['procurar imagem'] = "búsqueda de imágenes";
$lang['imagem da marca<br> da empresa'] = "Imagen de la marca<br> de la empresa";
$lang['Pronto, seu cadastro foi feito com sucesso.'] = "¡Listo! Tu registro fue realizado con éxito.";
$lang['Você deu o primeiro passo para se tornar um Embaixador da Água.'] = "Ya diste el primer paso para convertirte en Embajador del Agua.";
$lang['Quer saber mais? Acompanhe as novidades do Projeto Hydros sobre a água por meio de nossas redes sociais.'] = "¿Quieres saber más? Acompaña las novedades del Proyecto Hydros acerca del agua por medio de nuestras redes sociales.";
$lang['Curta, compartilhe, siga e participe!'] = "¡Disfruta, comparte, avanza y participa!";
$lang['A sua empresa deu o primeiro passo para se tornar uma Embaixadora da Água.'] = "Tu empresa dio el primer paso para convertirse en Embajador del Agua.";
$lang['Curta, compartilhe com seus colaboradores, siga e participe!'] = "¡Dale me gusta, comparte con tus colaboradores, sigue y participa!";

$lang['GRANDES'] = "GRANDE";
$lang['MÉDIAS'] = "MEDIANA";
$lang['PEQUENAS'] = "PEQUEÑA";
$lang['GIGANTOGRAFIAS'] = "GIGANTOGRAFIAS";
$lang['GIGANTOGRAFIAS'] = "GIGANTOGRAFIAS";

$lang['Wallpaper'] = "Fondo de pantalla";
$lang['Marcador de livro'] = "Marcador de libro";
$lang['Móbile'] = "Móvil";
$lang['Banner'] = "Banner";
$lang['Banners'] = "";
$lang['Banners Digitais'] = "Banners digitales";
$lang['Toalha de bandeja'] = "Mantel para bandeja";
$lang['Cartaz'] = "Afiche";
$lang['Adesivo'] = "Adhesivo";
$lang['Wobler'] = "Marco para computadora";

// PARTE ENVIADA PARA SER TRADUZIDA

$lang['Email já cadastrado!'] = "E-mail registrado/cadastrado!";
$lang['Cadastro efetuado com sucesso!'] = "Registro efectuado com éxito!";
$lang['Erro ao efetuar o cadastro!'] = "Error ao efectuar el registro/cadastro!";
$lang['Insira seu nome e um email válido!'] = "Inserir su nombre y email valido ";
$lang['Obrigado por entrar em contato!<br>Responderemos assim que possível.'] = "Tu mensaje fue enviado correctamente.<br> Por favor, espera, entraremos en contacto contigo en breve.";
$lang['video_institucional_hydros'] = "";
$lang["SELECIONAR ARQUIVO"] = "SELECCIONAR ARCHIVO";
$lang["Informe seu nome!"] = "Informe su nombre!";
$lang["Informe seu nome de assinatura!"] = "Informe su nombre de firma!";
$lang["Informe seu e-mail!"] = "Informe su e-mail!";
$lang["Informe sua cidade!"] = "Informe su ciudad!";
$lang["Informe seu país!"] = "Informe su país!";
$lang["Informe sua senha!"] = "Informe su seña!";
$lang["Informe sua confirmação de senha!"] = "Informe su confirmacíon del seña!";
$lang["Suas senhas não conferem!"] = "Sus senãs no son validas!";
$lang["Informe seu e-mail de cadastro!"] = "Informe su e-mail de cadastro!";
$lang["E-mail não cadastrado!"] = "E-mail no es cadastrado!";
$lang["Informe sua senha!"] = "Informe su seña!";
$lang["Senha não confere!"] = "Senã incorrecta!";

// Cadastro nos comentários
$lang["NOVO CADASTRO"] = "NUEVO REGISTRO";
$lang["nome completo"] = "Nombre completo";
$lang["nome para assinar os comentários"] = "Nombre para firmar los comentarios";
$lang["e-mail"] = "Email";
$lang["cidade"] = "Ciudad";
$lang["país"] = "País";
$lang["criar senha"] = "Crear seña";
$lang["confirmar senha"] = "Confirmar seña";
$lang["se desejar cadastre uma foto para o seu<br> perfil dentro do nosso site!<br>(Tamanho máximo de arquivo: 2Mb)"] = "Si quieres registrar uma foto<br> para su perfil en nuestro site<br>(Tamaño máximo de archivo: 2Mb)";
$lang["SELECIONAR ARQUIVO"] = "SELECCIONAR ARCHIVO";
$lang["Quer receber nossa newsletter com todas as novidades e informações sobre o Projeto Hydros?'"] = "¿Quieres recibir nuestra newsletter con todas las noticias e información sobre el Proyecto Hydros?";
$lang["SIM"] = "SÍ";
$lang["NÃO"] = "NO";
$lang["CADASTRAR"] = "REGISTRAR";
$lang["(*) o endereço de e-mail será usado como login no sistema.<br> O Projeto Hydros não divulga nem fornece seus dados pessoais a terceiros. Leia nossa <a href='politica'>política de privacidade.</a>"] = "(*)El e-mail será utilizado como seña de acceso. Proyecto Hydros no revela ni proporciona sus datos personales. Lea nuestra <a href='politica'>política de privacidad</a>.";

// Form de comentários
$lang["ENVIAR COMENTÁRIO"] = "ENVIE SU COMENTARIO";
$lang["Olá"] = "Hola!";
$lang["Deixe seu comentário abaixo"] = "Deje su comentario";
$lang["ENVIAR"] = "ENVIAR";
$lang["CANCELAR | VOLTAR"] = "CANCELAR | VOLVER";

// Lista de comentários
$lang["COMENTE!"] = "COMENTE!";
$lang["COMENTE"] = "COMENTE";
$lang["Ninguém comentou. Seja o primeiro!"] = "Nadie comentó";
$lang["1 pessoa comentou"] = "Una persona comentó";
$lang["pessoas comentaram"] = "personas comentaron";

// Login nos comentários
$lang["CONECTE-SE PARA COMENTAR"] = "CONECT PARA COMENTAR";
$lang["Se você já é cadastrado, faça o login"] = "";
$lang["login (e-mail)"] = "";
$lang["senha"] = "seña";
$lang["ENTRAR"] = "ENTRAR";
$lang["esqueci a senha"] = "olvidó su seña";
$lang["Se você ainda não é cadastrado,<br> cadastre-se agora!"] = "Si no eres cadastrado,<br> haga ahora";
$lang["CADASTRAR-ME!"] = "CADASTRA-SE";

$lang["O Projeto Hydros tenta mostrar o vínculo entre o planeta e o ser humano em relação à água, do qual ainda não estamos conscientes. Levantamos a questão: 'Como me relaciono com a água?'. Não sabemos. Está na hora de percebermos tudo o que fazemos com ela na construção da realidade de uma pessoa, de uma cidade, de um negócio ou de um país, e como cada uma dessas ações afeta o presente, o hoje, o agora."] = "El Proyecto Hydros intenta mostrar el nexo planetario-humano todavía inconsciente del ser humano con el agua. Entonces nos preguntarnos: '¿Cómo me relaciono con el agua?' No lo sabemos. Es hora de darnos cuenta de todo lo que hacemos para construir la realidad – de un individuo, de una ciudad, de un negocio o de un país – en compañía del agua, y de cómo cada una de esas acciones influye en el momento presente, el hoy, el ahora.";

$lang["EM BREVE"] = "PRONTO";

$lang["PEGADA HYDROS"] = "HUELLA HYDROS";
$lang["Calcule a quantidade de água que você gasta no seu dia a dia - Pegada Hydros"] = "¡Calcule la cantidad de agua que utilizas en tu día a día! - Huella Hydros";
$lang["Projeto Hydros - Pegada Hydros"] = "Proyecto Hydros - Huella Hydros";

$lang["<h2>RECUPERAÇÃO DE SENHA</h2><p>Uma nova senha foi enviada para o seu e-mail!</p><a href='#' id='botao-cancelar-post' title='VOLTAR'>VOLTAR</a>"] = "<h2>Recuperación de la contraseña</h2><p>Una nueva contraseña ha sido enviada a su e-mail!</p><a href='#' id='botao-cancelar-post' title='VOLVER'>VOLVER</a>";


$lang["HYDROS INSPIRA"] = "HYDROS INSPIRA";
$lang["Nesse espaço, você encontrará materiais feitos por pessoas que tiveram contato com o Projeto Hydros e ficaram sensibilizadas com a sua mensagem, de alguma forma. Portanto, se tiver algum produto ou manifestação artística que foi fruto do seu contato com o Projeto Hydros e que tenha atingido um determinado grupo de pessoas, como, por exemplo, um livro, vídeo, textos, música, exposição etc, envie para hydros@corecomunicacao.com.br. Nós iremos analisar o seu material e publicar aqui. O importante, realmente, é levar essa ideia adiante!"] = "En este espacio, encontrarás materiales producidos por personas que tuvieron contacto con el Proyecto Hydros y se sensibilizaron ante su mensaje, de alguna manera. Por lo tanto, si tienes algún producto o manifestación artística que haya sido fruto de tu contacto con el Proyecto Hydros y que haya alcanzado un determinado grupo de personas, como, por ejemplo, un libro, video, textos, música, exposición etc, envíalo para <a href='mailto:hydros@corecomunicacao.com.br'>hydros@corecomunicacao.com.br</a>. Analisaremos tu material y podremos publicarlo aquí. Lo más importante, realmente, ¡es llevar esa idea adelante!";
$lang["Materiais feitos por pessoas que tiveram contato com o Projeto Hydros - "] = "Materiales producidos por personas que tuvieron contacto con el Proyecto Hydros y se sensibilizaron ante su mensaje, de alguna manera.";
$lang["Projeto Hydros - Hydros Inspira"] = "Proyecto Hydros - Hydros Inspira";
$lang["Nesse espaço, você encontrará materiais feitos por pessoas que tiveram contato com o Projeto Hydros e ficaram sensibilizadas com a sua mensagem, de alguma forma."] = "En este espacio, encontrarás materiales producidos por personas que tuvieron contacto con el Proyecto Hydros y se sensibilizaron ante su mensaje, de alguna manera.";
$lang["O Dom da Água – Regina Moya"] = "El Don del Agua – Regina Moya";
$lang["A artista plástica e escritora mexicana Regina Moya foi sensibilizada pelo Projeto Hydros e, a partir dele, escreveu e ilustrou o livro infantil “O Dom da Água”, que você confere aqui na íntegra. Para conhecer um pouco mais sobre o trabalho da artista acesse <a href='http://www.reginamoya.com' target='_blank'>http://www.reginamoya.com</a>."] = "La escritora y artista mexicana Regina Moya fue sensibilizada por del Proyecto Hydros y, de él, escribió e ilustró el libro para niños \"El Don del Agua\", que se puedes comprobar aquí completo. Para obtener más información acerca de la obra del artista visitas <a href='http://www.reginamoya.com' target='_blank'>http://www.reginamoya.com</a>.";


$lang['Concurso "O Homem e a água"'] = "Concurso “El hombre y el agua”";
$lang['O Grupo Empresarial Kaluz promoveu um concurso cultural de fotografias, em 2012, englobando todos os seus colaboradores ao redor do mundo, com o tema “O homem e a água”. Veja aqui as cinco imagens vencedoras que foram inspiradas também nas mensagens de sensibilização que o Projeto Hydros trouxe ao cotidiano dos profissionais.'] = "El Grupo Empresarial Kaluz promovió un concurso cultural de fotografías en 2012, abarcando a todos sus empleados en todo el mundo, con el tema \"El hombre y el agua.\" Aquí están las cinco imágenes ganadoras que se inspiró también en los mensajes de sensibilización que el Proyecto Hydros ha llevado al cotidiano de los profesionales.";
$lang['<strong>1º Lugar</strong> - María de los Ángeles Avilés Gamboa, da Mexichem, com o tema “Mar Morto, reviva!”'] = "<strong>1er Lugar</strong> - María de los Ángeles Avilés Gamboa, de Mexichem, con el lema \"¡Mar Muerto, revive!\"";
$lang['<strong>2º Lugar</strong> - Emiel van den Boomen, da Wavin Holanda, com o tema “Banhando nas águas sagradas do templo Tirta Empul em Bali”'] = "<strong>2º Lugar</strong> - Emiel van den Boomen, de Wavin Holanda, con el tema \"bañarse en las aguas sagradas del templo de Tirta Empul en Bali\"";
$lang['<strong>3º Lugar</strong> - Liliana Echeverri Branch, da Mexichem Colombia S.A.S., com o tema “Os meninos do Pacífico são água”'] = "<strong>3er Lugar</strong> - Liliana Echeverri Rama, de Mexichem Colombia SAS, con el tema \"Los niños del Pacífico son agua\"";
$lang['<strong>4º Lugar</strong> - Diego Alejandro David Vázquez, da Mexichem, com o tema “Com os pés em mais águas”'] = "<strong>4to Lugar</strong> - Diego Alejandro David Vázquez, Mexichem, con el lema \"Con los pies en más aguas\"";
$lang['<strong>5º Lugar</strong> - Filemón Peruyero Solís, da Mexichem TI, com o tema “Agua: meio de transporte e sustento diário”'] = "<strong>5to Lugar</strong> - Filemón Peruyero Solís, de Mexichem Servicios de TI, con el tema \"Agua: medio de transporte y sustento diario \"";
$lang["A Mexichem Brasil realizou um concurso com as escolas que receberam o Projeto Hydros e foram sensibilizadas por sua mensagem de preservação da água. Os jovens estudantes tinham que criar manifestações artísticas sozinhos ou em grupos. Aqui você confere os vencedores nacionais e o regionais."] = "Mexichem Brasil celebró un concurso con las escuelas que recibieron el Proyecto Hydros y se sensibilizaron por su mensaje de conservación del agua. Los jóvenes estudiantes tuvieron que crear manifestaciones artísticas solos o en grupos. Aquí puedes ver los ganadores regionales y nacionales.";
$lang["Concurso Mexichem Brasil 2012"] = "Concurso Mexichem Brasil 2012";
$lang["Vencedores Nacionais"] = "Ganadores Nacionales";
$lang["Vencedores Regionais"] = "Ganadores Regionales";
$lang["Alunos de 5ª a 8ª Série"] = "";

$lang['Game Hydros'] = "";
$lang['O Game Hydros ensina, de forma divertida e interativa, as formas de captação, reuso e descarte de água. Além disso, ele se passa em algumas das cidades que representam os locais de atuação do Projeto Hydros: Cidade do México, São Paulo, Nova Iorque, Tóquio e Amsterdã.  Em cada cidade, são 5 fases, que ficam mais difíceis, de acordo com a evolução do usuário no jogo.'] = "El Game Hydros enseña, de manera divertida e interactiva, las formas de captación, reutilización y disposición del agua. Adicionalmente, el juego tiene como escenario ciudades representativas de la actuación del Proyecto Hydros: Ciudad de México, Sao Paulo, Nueva York, Tokio y Ámsterdam. En cada una de ellas, ha cinco fases, que irán aumentando su dificultad, de acuerdo con la evolución del usuario en el juego.";
$lang['Usando o sensor de gravidade dos smartphones e tablets, o jogador direciona o fluxo da água pelos canos, para os lugares da casa em que cada tipo de água deve ser utilizado - de chuveiros e pias a irrigação de hortas e abastecimento de reservatórios de água de reuso.'] = "Usando el sistema de los smartphones y tabletas, el jugador debe indicar la dirección del flujo de agua por las tuberías, para los lugares de la casa en que cada tipo de agua debe ser utilizado - de duchas y pilas a la irrigación de huertas y abastecimiento de tanques de agua de reúso.";
$lang['A partir de agora, você também tem o destino da água do planeta em suas mãos!'] = " ";

$lang["HYDROS E VOCÊ"] = "HYDROS Y TÚ";
$lang["REVISTA AQUA VITAE &bull; A INSPIRAÇÃO HYDROS"] = "REVISTA AQUA VITAE &bull; LA INSPIRACIÓN HYDROS";
$lang['Revista Aqua Vitae'] = '';
$lang["A Aqua Vitae é uma revista criada pelo Grupo Empresarial Kaluz com o objetivo de debater ideias sobre a problemática do recurso hídrico.  Por isso, promoveu como uma de suas linhas de atuação, identificar especialistas mundiais sobre o tema, que encontram nas páginas da publicação a possibilidade de fortalecer com seus pensamentos, investigações e aportes, uma discussão séria e proativa acerca das diferentes facetas que o recurso hídrico tem para o desenvolvimento dos seres humanos, em harmonia com seu uso, manejo e conservação. Disponibilizamos as edições lançadas e que inspiraram a série fotográfica Hydros."] = "El Aqua Vitae es una revista creada por el Grupo Empresarial Kaluz para discutir ideas sobre el problema de los recursos hídricos. Por lo tanto, promovió como una de sus líneas de negocio, identificar expertos mundiales en la materia, que se encuentran en las páginas de la publicación la posibilidad de fortalecer con sus pensamientos, investigaciones y contribuciones, una discusión seria y proactiva sobre los diferentes aspectos que el recurso hídrico es para el desarrollo de los seres humanos en armonía con su uso, manejo y conservación. Proporcionamos las ediciones lanzadas y que inspiró las series fotográficas Hydros.";

$lang['Quais as informações que recolhemos?'] = "?Qué información recolectamos? ";
$lang['Coletamos informações de você quando você se cadastra em nosso site ou subscreve a nossa newsletter.'] = "Recolectamos su información desde el momento en que usted se registra en nuestro sitio o se subscribe a nuestro boletín. ";
$lang['Ao encomendar ou registar no nosso site, conforme o caso, você pode ser solicitado a digitar o seu: nome, endereço de e-mail ou endereço postal. Você pode, entretanto, visitar nosso site de forma anônima.'] = "Al solicitar o registrarse en nuestro sitio, cuando  sea apropiado, podrán solicitarle que introduzca: su nombre, dirección de e-mail o dirección postal. Sin embargo, usted puede visitar nuestro sitio anónimamente.";
$lang['Como usamos as suas informações?'] = "?Para qué utilizamos su información? ";
$lang['Qualquer uma das informações que coletamos de você pode ser usado para enviar e-mails periódicos.'] = "Cualquier información que recolectemos sobre usted puede ser usada para enviarle e-mails periódicos.";
$lang['O endereço de e-mail que você fornecer para processamento de pedidos, pode ser usado para enviar informações e atualizações referentes ao seu fim, além de receber a notícia ocasional da empresa, atualizações, produto relacionado ou informações de serviço, etc'] = "La dirección de e-mail que usted proporciona para procesamiento de la solicitud puede ser usada para enviarle información y actualizaciones relativas a su solicitud, además de recibir ocasionales noticias de la compañía, actualizaciones e información de productos y servicios relacionados etc.";
$lang['Como protegemos a sua informação?'] = "?Cómo se protege su información?
";
$lang['Implementamos uma série de medidas de segurança para manter a segurança de suas informações pessoais quando você entra, enviar ou acessar suas informações pessoais.'] = "Nosotros implementamos una variedad de medidas de seguridad para mantener la seguridad de su información personal, cuando usted introduce, presenta o accesa su información personal. ";
$lang['Nós usamos cookies?'] = "? Nosotros usamos cookies? ";
$lang['Nós divulgamos qualquer informação a terceiros?'] = "? Revelamos alguna información a personas externas? ";
$lang['Nós não vendemos, comercializamos ou transferimos a terceiros as suas informações pessoalmente identificáveis. Isso não inclui terceiros de confiança que nos auxiliam no funcionamento do nosso site, conduzindo nosso negócio, ou serviço para você, desde que as partes concordem em manter esta informação confidencial. Podemos também divulgar as suas informações quando acreditamos que é apropriado para cumprir a lei, fazer cumprir as nossas políticas de site, ou proteger nosso ou de outros direitos, propriedade ou segurança. No entanto, as informações do visitante não identificáveis ​​podem ser fornecidas a terceiros para marketing, publicidade, ou outros usos.'] = "No vendemos, comercializamos o transferimos, en ningún otro modo, a personas externas la información personalmente identificable. Esto no incluye a terceras partes confiables que nos asisten operando nuestra página web, conduciendo nuestros negocios, o sirviéndole a usted, siempre y cuando dichas terceras partes acuerden en mantener la confidencialidad de esa información. También podemos liberar su información cuando pensamos que liberarla es apropiado para cumplir con las leyes, reforzar nuestras políticas del sitio, o proteger nuestros derechos, nuestra propiedad o seguridad, o las de otros. Sin embargo, la información no personal ni identificable de los visitantes puede ser proporcionada a otras partes, para fines de mercadeo, publicidad u otros usos. ";
$lang['Links de terceiros'] = "Vínculos con terceras partes";
$lang['Nós não incluímos ou oferecemos produtos ou serviços de terceiros no nosso site. No entanto, buscamos proteger a integridade do nosso site e agradecemos qualquer feedback sobre o nosso site.'] = "Nosotros no incluimos ni ofrecemos productos o servicios de terceros en nuestra página web. No obstante, buscamos proteger la integridad de nuestro sitio y dar la bienvenida a cualquier retroalimentación relativa a nuestro sitio.";
$lang['Childrens Privacidade Online Protection Act Compliance'] = "Cumplimiento al Acto de Protección de la Privacidad en línea de los Niños ";
$lang['Estamos em conformidade com os requisitos da COPPA (Childrens online Privacy Protection Act).'] = "Damos cumplimiento a los requerimientos del COPPA (Acto de Protección de la Privacidad En Línea de los Niños), en las localidades donde se aplique nuestra política.";
$lang['Política apenas de Privacidade Online'] = "Política de Privacidad Sólo En Línea ";
$lang['Esta política de privacidade online se aplica somente às informações coletadas através de nosso site e não a informações coletadas offline.'] = "Esta política de privacidad sólo se aplica a la información recolectada a través de nuestra página web y no a la información recolectada por otros medios.";
$lang['Termos e Condições'] = "Términos y Condiciones";
$lang['Por favor, visite o nosso <a href="legal" title="Aviso Legal">Aviso Legal</a>, seção que estabelece o uso, renúncias e limitações de responsabilidade que regem a utilização do conteúdo de nosso website em <a href="http://www.projetohydros.com" title="Projeto Hydros">www.projetohydros.com</a>'] = "Favor de visitar también nuestra sección de <a href='legal' title='Aviso Legal'>Aviso Legal</a>, que establece el uso, renuncia y limitaciones de responsabilidad que regulan el uso de nuestra página web, en <a href='http://www.proyectohydros.com' title='Proyecto Hydros'>www.proyectohydros.com</a>";
$lang['Seu consentimento'] = "Su Consentimiento ";
$lang['Ao utilizar nosso site, você concorda com a nossa política de privacidade online.'] = "Al usar nuestro sitio, usted está de acuerdo con nuestra política de privacidad en línea.";
$lang['Alterações à nossa Política de Privacidade'] = "Cambios en nuestra Política de Privacidad ";
$lang['Se nós decidirmos mudar nossa política de privacidade, vamos atualizar data de modificação da política de privacidade abaixo.'] = "En caso de que decidamos cambiar nuestra política de privacidad, actualizaremos abajo la fecha de modificación de la Política de Privacidad.";
$lang['Esta política foi modificada pela última vez em <strong>10/06/2013</strong>.'] = "Esta política fue modificada por última vez el <strong>10/06/2013.</strong>";
$lang['Esta política está em conformidade da Trust Guard PCI compliance.'] = "Esta política se efectiva en cumplimiento a la Trust Guard <a href='http://www.trust-guard.com/PCI-Compliance-s/65.htm' target='_blank'>PCI.</a>";

$lang['Sim, nós usamos cookies para melhorar a experiência online do usuário do nosso website. Para quem não sabe, cookies são arquivos que armazenam dados de forma temporária no disco rígido do usuário, para que o website promova uma navegação mais conveniente. Os cookies podem evitar que uma mesma informação seja solicitada mais de uma vez ao mesmo usuário. No caso do nosso website utilizamos cookies com as seguintes finalidades:'] = "Sí, utilizamos cookies para mejorar la experiencia online del usuario de nuestro sitio. Para los que no saben, las cookies son ficheros que almacenan datos temporalmente en el disco duro del usuario para que el sitio promueve una navegación más cómoda. Los cookies pueden garantizar que la misma información que se solicita más de una vez al mismo usuario. En el caso de nuestros sitios utilizan cookies para los siguientes propósitos:";
$lang['- manter o usuário logado após realizar o login para fazer comentários'] = "- Mantenga el usuario conectado después de iniciar sesión para comentar";
$lang['- armazenar qual a linguagem selecionada caso o usuário escolha a língua clicando nos botões do cabeçalho'] = "- Que almacenan el idioma seleccionado si el usuario selecciona el idioma haciendo clic en la cabecera";
$lang['- armazenar qual foi a última página acessada sempre, para saber como redirecionar no caso de seleção de um novo idioma'] = "- Qué tienda fue la última página de acceso cuando, por la forma de reorientar en caso de seleccionar un nuevo idioma";

$lang['Concurso Cultural Água em 1 minuto'] = "Concurso Cultural Agua en 1 minuto";
$lang['Se a sua escola já recebeu o Projeto Hydros e a sua mensagem de conscientização e sensibilização sobre a água, está na hora de mostrar o que você aprendeu! Participe do Concurso Cultural “Água em 1 minuto” promovido pelo Projeto Hydros em parceria com o Grupo Empresarial Kaluz.'] = "Si tu escuela ha recibido el Proyecto Hydros y su mensaje de concientización y sensibilización sobre el agua, es el momento de mostrar lo que has aprendido! Participa en el Concurso Cultural “Agua en 1 minuto”, promovido por el Proyecto Hydros en asociación con el Grupo Empresarial Kaluz.";
$lang['Você encontra os materiais referentes ao concurso aqui:'] = "Aquí encontrarás los materiales referentes al concurso:";
$lang['Regulamento'] = "Reglamento";
$lang['Flyer'] = "Volante";
$lang['Cartaz A3'] = "Cartel A3";
$lang['Cartaz A4'] = "Cartel A4";
$lang['Disponibilizamos abaixo todos os materiais do Projeto Hydros que podem, de alguma maneira, te inspirar para o concurso.'] = "Abajo, ponemos en disponibilidad todos los materiales del Proyecto Hydros que pueden, en alguna manera, inspirarte para el concurso.";
$lang['Vídeo'] = "";
$lang['Se tiver alguma dúvida sobre o concurso, você pode nos contatar através do e-mail <a href="mailto:hydros@corecomunicacao.com.br">hydros@corecomunicacao.com.br</a> ou entrar em contato com o Embaixador da Água que entrou em contato com a sua escola.'] = "Si tienes alguna duda sobre el concurso, puedes contactarnos a través del e-mail <a href='mailto:hydros@corecomunicacao.com.br'>hydros@corecomunicacao.com.br</a> o contactar al Embajador del Agua que se ha comunicado con tu escuela.";

$lang['Música "Planeta" tem inspiração no Hydros'] = "La canción \"Planeta\" tiene inspiración en el Hydros";
$lang['A letra foi criada pela professora de Ciências da Escola Municipal Professora Lacy Luiza da Crus Flores, Alice Sitta Perera. Já a aluna Hillary do 9º ano ficou responsável por criar a melodia e cantar a música que fala nosso planeta, em especial sobre a água. A escola recebeu o Projeto Hydros em 2012 e resolveu ajudar de alguma maneira. O trabalho, que ficou maravilhoso, você confere aqui:'] = "La letra fue creada por la profesora de Ciencias de la Escuela Municipal \"Profesora Lacy Luiza da Cruz Flores\", Alice Sitta Perera. Por otra parte, la alumna Hillary, del 9º grado, fue la responsable de crear la melodía y cantar la canción, que habla de nuestro planeta, en especial sobre el agua. La escuela recibió el Proyecto Hydros en 2012, y decidió ayudar en alguna forma. El trabajo, que resultó maravilloso, puede ser visto aquí:";

$lang['Veja imagens das manifestações artísticas de pequenos embaixadores.'] = "Vea las imágenes de las manifestaciones artísticas de pequeños embajadores.";
$lang['Por meio de um concuso cultural na Argentina, pequenos Embaixadores da Água foram sensibilizados e inspirados pela mensagem que o Projeto Hydros traz às pessoas. Aqui, você pode ver os vencedores segurando suas verdadeiras obras de arte!'] = "Por medio de un concurso cultural en Argentina, pequeños Embajadores del agua han sido sensibilizados e inspirados por el mensaje que el Proyecto Hydros trae a las personas. Aquí, puedes ver a los vencedores exhibiendo sus verdaderas obras de arte!";
?>