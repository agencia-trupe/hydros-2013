<?php

/*
 *  Hook pre-controller para setar a linguagem como pt caso
 *  não esteja definido nenhum valor
 */
// function inicia_linguagem(){

//     if (!isset($_SERVER['HTTP_X_REQUESTED_WITH'])){
//         $CI =& get_instance();
        
//         if($CI->linguagem){
//             $padrao = 'pt';
    
//             if(!$CI->session->userdata('language'))
//                 $CI->session->set_userdata('language', $padrao);
    
//             // Define o prefixo da tabela a ser usada dependendo da linguagem
//             $CI->session->set_userdata('prefixo', $CI->session->userdata('language').'_');
    
//             $CI->lang->load($CI->session->userdata('language').'_site', $CI->session->userdata('language'));
//         }
//     }
// }
/*
 *  Hook post_controller_constructor para setar a linguagem como pt caso
 *  não esteja definido nenhum valor
 */
function inicia_linguagem(){

    if (!isset($_SERVER['HTTP_X_REQUESTED_WITH'])){
        $CI =& get_instance();

        $dns = $_SERVER['HTTP_HOST'];

        switch($dns){
            case 'preview.projetohydros.com':
            case 'www.projetohydros.com':
            case 'projetohydros.com':
                $padrao = 'pt';
                break;
            case 'preview.projecthydros.com':
            case 'www.projecthydros.com':
            case 'projecthydros.com':
                $padrao = 'en';
                break;
            case 'preview.proyectohydros.com':
            case 'www.proyectohydros.com':
            case 'proyectohydros.com':
                $padrao = 'es';
                break;
            default:
                $padrao = 'pt';
        }

        //if(!$CI->session->userdata('language'))
            $CI->session->set_userdata('language', $padrao);

        // Define o prefixo da tabela a ser usada dependendo da linguagem
        $CI->session->set_userdata('prefixo', $CI->session->userdata('language').'_');

        $CI->lang->load($CI->session->userdata('language').'_site', $CI->session->userdata('language'));

        switch($CI->session->userdata('language')){
            case 'pt':
                $CI->header['app_id'] = '324783017548156';
                $CI->data['app_id'] = '324783017548156';
                break;
            case 'en':
                $CI->header['app_id'] = '283483058370912';
                $CI->data['app_id'] = '283483058370912';
                break;
            case 'es':
                $CI->header['app_id'] = '258851117508206';
                $CI->data['app_id'] = '258851117508206';
                break;
            default:
                $CI->header['app_id'] = '324783017548156';
                $CI->data['app_id'] = '324783017548156';
        }
    }
}


/*
 *  Redirecionar de volta após requisição
 */
function grava_para_redirecionar(){

    if (!isset($_SERVER['HTTP_X_REQUESTED_WITH'])){
        $CI =& get_instance();

        @$CI->session->set_userdata('redirect', uri_string());
    }
}

?>