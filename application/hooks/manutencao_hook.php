<?php
function redirecionaManutencao(){
    $CORE =& get_instance();
    if($CORE->config->item('is_offline')){
        if(strpos(base_url(), 'preview') === FALSE){
            $CORE->load->view('manutencao/embreve');
            echo $CORE->output->get_output();
            exit();
        }
    }
}
?>
