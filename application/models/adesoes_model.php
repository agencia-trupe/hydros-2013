<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Adesoes_model extends MY_Model {

	function __construct(){
		parent::__construct();

		$this->tabela = "adesao";

		$this->dados = array();
        $this->dados_tratados = array();
	}

	function pegarTodos($lang = FALSE, $order_campo = 'id', $order = 'ASC'){
		return $this->db->order_by($order_campo, $order)->get_where($this->tabela, array('tipo' => $lang))->result();
	}

	function pegarChamada($lang = FALSE, $order_campo = 'id', $order = 'ASC'){
		return $this->db->order_by($order_campo, $order)->get_where($tabela, array('tipo' => $lang))->result();
	}

	function numeroResultados($lang = false){
		return $this->db->get_where($this->tabela, array('tipo' => $lang))->num_rows();
	}

	function pegarPaginado($lang = FALSE, $por_pagina, $inicio, $order_campo = 'id', $order = 'ASC'){
		return $this->db->order_by($order_campo, $order)->get_where($this->tabela, array('tipo' => $lang), $por_pagina, $inicio)->result();
	}

	function pegarPorId($lang, $id){
		$qry = $this->db->get_where($this->tabela, array('id' => $id))->result();
		if(isset($qry[0]))
			return $qry[0];
		else
			return FALSE;
	}	

	function inserir($lang){
		foreach($this->dados as $k => $v){
			if(array_key_exists($v, $this->dados_tratados))
				$this->db->set($v, $this->dados_tratados[$v]);
			elseif($this->input->post($v) !== FALSE)
				$this->db->set($v, $this->input->post($v));
		}
		return $this->db->insert($this->tabela);
	}

	function alterar($lang, $id){
		if($this->pegarPorId($lang, $id) !== FALSE){
			foreach($this->dados as $k => $v){
				if(array_key_exists($v, $this->dados_tratados) && $this->dados_tratados[$v] !== FALSE)
					$this->db->set($v, $this->dados_tratados[$v]);
				elseif($this->input->post($v) !== FALSE)
					$this->db->set($v, $this->input->post($v));
			}
			return $this->db->where('id', $id)->update($this->tabela);
		}
	}

	function excluir($lang, $id){
		if($this->pegarPorId($lang, $id) !== FALSE){
			return $this->db->where('id', $id)->delete($this->tabela);
		}
	}

	function sobeImagem($campo = 'userfile'){
		$this->load->library('upload');

		$original = array(
			'campo' => $campo,
			'dir' => '_imgs/home/'
		);
		$campo = $original['campo'];

		$uploadconfig = array(
		  'upload_path' => $original['dir'],
		  'allowed_types' => 'jpg|png|gif',
		  'max_size' => '0',
		  'max_width' => '553',
		  'min_width' => '551',
		  'max_height' => '553',
		  'min_height' => '551');

		$this->upload->initialize($uploadconfig);

		if(isset($_FILES[$campo]) && $_FILES[$campo]['error'] != 4){
		    if(!$this->upload->do_upload($campo)){
		    	die($this->upload->display_errors());
		    }else{
		        $arquivo = $this->upload->data();
		        $filename = url_title($arquivo['file_name'], 'underscore', true);
		        rename($original['dir'].$arquivo['file_name'] , $original['dir'].$filename);

	        	// $this->image_moo
	        	// 	->load($original['dir'].$filename)
	         	//  ->resize_crop(552, 552)
	         	//  ->save($original['dir'].$filename, TRUE);

		        return $filename;
		    }
		}else{
		    return false;
		}		
	}	
}
