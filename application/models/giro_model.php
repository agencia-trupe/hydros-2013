<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Giro_model extends MY_Model {

	function __construct(){
		parent::__construct();

		$this->dados = array('titulo', 'slug', 'data', 'imagem', 'olho', 'texto', 'pais');
        $this->dados_tratados = array(
        	'slug' => url_title($this->input->post('titulo'), '_', TRUE),
        	'data' => formataData($this->input->post('data'), 'br2mysql'),
        	'imagem' => $this->sobeImagem()
        );
	}

	function definirLinguagem(){
		if ($this->session->userdata('language') == 'pt') {
			$this->tabela = 'pt_giro';
			$this->tabela_comentarios = 'pt_giro_comentarios';
		}elseif ($this->session->userdata('language') == 'en') {
			$this->tabela = 'en_giro';
			$this->tabela_comentarios = 'en_giro_comentarios';
		}elseif ($this->session->userdata('language') == 'es') {
			$this->tabela = 'es_giro';
			$this->tabela_comentarios = 'es_giro_comentarios';
		}
	}

	function pegarPorSlug($slug){
		$this->definirLinguagem();
		$qry = $this->db->get_where($this->tabela, array('slug' => $slug))->result();
		if(isset($qry[0]) && $qry[0])
			return $qry[0];
		else
			return false;
	}		

	function pegarComentarios($id, $retornaContagem = FALSE){
		if ($retornaContagem) {
			return $this->db->get_where($this->tabela_comentarios, array('id_noticia' => $id))->num_rows();
		} else {
			$comentarios = $this->db->order_by('data', 'DESC')->get_where($this->tabela_comentarios, array('id_noticia' => $id))->result();
			foreach ($comentarios as $key => $value) {
				$value->autor = $this->pegarAutorComentario($value->autor);
			}
			return $comentarios;
		}
	}

	function pegarAutorComentario($id){
		$qry = $this->db->get_where("cadastros_comentarios", array('id' => $id))->result();
		if(isset($qry[0]) && $qry[0])
			return $qry[0];
		else
			return false;
	}	

	function numeroResultados($lang = false){
		if($lang)
			$this->tabela = $lang."_giro";
		else
			$this->definirLinguagem();

		return $this->db->get($this->tabela)->num_rows();
	}

	function pegarPaginado($lang = FALSE, $por_pagina, $inicio, $order_campo = 'id', $order = 'DESC'){
		if($lang)
			$this->tabela = $lang."_giro";
		else
			$this->definirLinguagem();

		return $this->db->order_by($order_campo, $order)->get($this->tabela, $por_pagina, $inicio)->result();
	}

	function pegarPorId($lang, $id){
		if($lang)
			$this->tabela = $lang."_giro";
		else
			$this->definirLinguagem();

		$qry = $this->db->get_where($this->tabela, array('id' => $id))->result();
		if(isset($qry[0]))
			return $qry[0];
		else
			return FALSE;
	}	

	function inserir($lang){
		if($lang)
			$this->tabela = $lang."_giro";
		else
			$this->definirLinguagem();

		$this->db->set('autor', $this->session->userdata('username'))
				 ->set('data_postagem', Date('Y-m-d'))
				 ->set('usuario_postagem', $this->session->userdata('username'))
				 ->set('data_ultima_alteracao', Date('Y-m-d'));


		foreach($this->dados as $k => $v){
			if(array_key_exists($v, $this->dados_tratados))
				$this->db->set($v, $this->dados_tratados[$v]);
			elseif($this->input->post($v) !== FALSE)
				$this->db->set($v, $this->input->post($v));
		}
		return $this->db->insert($this->tabela);
	}

	function alterar($lang, $id){
		if($lang)
			$this->tabela = $lang."_giro";
		else
			$this->definirLinguagem();

		$this->db->set('usuario_postagem', $this->session->userdata('username'))
				 ->set('data_ultima_alteracao', Date('Y-m-d'));

		$remover_imagem = $this->input->post('remover_imagem');

		if($this->pegarPorId($lang, $id) !== FALSE){
			foreach($this->dados as $k => $v){
				if(array_key_exists($v, $this->dados_tratados) && $this->dados_tratados[$v] !== FALSE)
					$this->db->set($v, $this->dados_tratados[$v]);
				elseif($this->input->post($v) !== FALSE)
					$this->db->set($v, $this->input->post($v));
			}
			if ($remover_imagem) {
				$this->db->set('imagem', '');
			}
			return $this->db->where('id', $id)->update($this->tabela);
		}
	}

	function excluir($lang, $id){
		if($lang)
			$this->tabela = $lang."_giro";
		else
			$this->definirLinguagem();

		if($this->pegarPorId($lang, $id) !== FALSE){
			return $this->db->where('id', $id)->delete($this->tabela);
		}
	}

	function sobeImagem($campo = 'userfile'){
		$this->load->library('upload');

		$original = array(
			'campo' => $campo,
			'dir' => '_imgs/giro/'
		);
		$campo = $original['campo'];

		$uploadconfig = array(
		  'upload_path' => $original['dir'],
		  'allowed_types' => 'jpg|png|gif',
		  'max_size' => '0',
		  'max_width' => '0',
		  'max_height' => '0');

		$this->upload->initialize($uploadconfig);

		if(isset($_FILES[$campo]) && $_FILES[$campo]['error'] != 4){
		    if(!$this->upload->do_upload($campo)){
		    	die($this->upload->display_errors());
		    }else{
		        $arquivo = $this->upload->data();
		        $filename = url_title($arquivo['file_name'], 'underscore', true);
		        rename($original['dir'].$arquivo['file_name'] , $original['dir'].$filename);

	        	$this->image_moo
	        		->load($original['dir'].$filename)
	                ->resize(598,1000)
	                ->resize_crop(598, 398)
	                ->save($original['dir'].$filename, TRUE)
	                ->resize(310, 310)
	                ->save($original['dir'].'thumbs/'.$filename, TRUE);

		        return $filename;
		    }
		}else{
		    return false;
		}		
	}	

}
