<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home_model extends MY_Model {

	function __construct(){
		parent::__construct();
	}

	function definirLinguagem(){
		if ($this->session->userdata('language') == 'pt') {
			$this->tabela_chamadas = 'pt_chamadas';
			$this->tabela_slides = 'pt_slides';
		}elseif ($this->session->userdata('language') == 'en') {
			$this->tabela_chamadas = 'en_chamadas';
			$this->tabela_slides = 'en_slides';
		}elseif ($this->session->userdata('language') == 'es') {
			$this->tabela_chamadas = 'es_chamadas';
			$this->tabela_slides = 'es_slides';
		}		
	}

	function pegarSlides(){
		$this->definirLinguagem();
		return $this->db->order_by('ordem', 'asc')->get($this->tabela_slides)->result();
	}
	
	function pegarChamadas(){
		$this->definirLinguagem();
		return $this->db->order_by('ordem', 'asc')->get($this->tabela_chamadas)->result();
	}

	function numeroResultados(){
		$this->definirLinguagem();
		return $this->db->get($this->tabela)->num_rows();
	}

}