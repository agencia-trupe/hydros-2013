<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Projeto_model extends CI_Model {

	function __construct(){
		parent::__construct();
	}

	function pegarApoio($tipo){
		$num_tipo = ($tipo == 'empresas') ? 2 : 1;
		$order = ($tipo == 'empresas') ? "empresa" : "nome";
		return $this->db->order_by($order, 'ASC')->get_where('adesao', array('tipo' => $num_tipo))->result();
	}

}