<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cadastros_model extends MY_Model {

	function __construct(){
		parent::__construct();

		$this->tabela = "cadastros";

        $this->dados = array('titulo', 'slug', 'data', 'imagem', 'olho', 'texto');
        $this->dados_tratados = array();
	}

	function numeroResultados($lang = false){
		if($lang == 'all')
			return $this->db->get($this->tabela)->num_rows();
		else
			return $this->db->get_where($this->tabela, array('idioma' => $lang))->num_rows();
	}

	function pegarPaginado($lang = FALSE, $por_pagina, $inicio, $order_campo = 'data_cadastro', $order = 'DESC'){
		if($lang == 'all')
			return $this->db->order_by($order_campo, $order)->get($this->tabela, $por_pagina, $inicio)->result();	
		else
			return $this->db->order_by($order_campo, $order)->get_where($this->tabela, array('idioma' => $lang), $por_pagina, $inicio)->result();
	}

	function pegarPorId($id){
		$qry = $this->db->get_where($this->tabela, array('id' => $id))->result();
		if(isset($qry[0]))
			return $qry[0];
		else
			return FALSE;
	}

	function excluir($id){
		if($this->pegarPorId($id) !== FALSE){
			return $this->db->where('id', $id)->delete($this->tabela);
		}
	}

}
