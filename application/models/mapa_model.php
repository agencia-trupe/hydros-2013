<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mapa_model extends CI_Model {

	function __construct(){
		parent::__construct();
	}

	function definirLinguagem(){
		if ($this->session->userdata('language') == 'pt') {
			$this->tabela_comentarios = 'pt_mapa_comentarios';
		}elseif ($this->session->userdata('language') == 'en') {
			$this->tabela_comentarios = 'en_mapa_comentarios';
		}elseif ($this->session->userdata('language') == 'es') {
			$this->tabela_comentarios = 'es_mapa_comentarios';
		}
	}

	function pegarComentarios($retornaContagem = FALSE){
		$this->definirLinguagem();

		if ($retornaContagem) {
			return $this->db->get($this->tabela_comentarios)->num_rows();
		} else {
			$comentarios = $this->db->order_by('data', 'DESC')->get($this->tabela_comentarios)->result();
			foreach ($comentarios as $key => $value) {
				$value->autor = $this->pegarAutorComentario($value->autor);
			}
			return $comentarios;
		}
	}

	function pegarAutorComentario($id){
		$this->definirLinguagem();
		$qry = $this->db->get_where("cadastros_comentarios", array('id' => $id))->result();
		if(isset($qry[0]) && $qry[0])
			return $qry[0];
		else
			return false;
	}
}
