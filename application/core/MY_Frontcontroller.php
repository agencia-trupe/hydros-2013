<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @property CI_Loader $load
 * @property CI_Form_validation $form_validation
 * @property CI_Input $input
 * @property CI_Email $email
 * @property CI_DB_active_record $db
 * @property CI_DB_forge $dbforge
 */
class MY_Frontcontroller extends CI_controller {

    var $headervar;
    var $footervar;
    var $menuvar;
    var $linguagem;
    var $layout;

    function __construct($css = '', $js = '') {
        parent::__construct();
        
        $this->headervar['load_css'] = $css;
        $this->headervar['load_js'] = $js;
        $this->linguagem = TRUE;
        $this->layout = TRUE;
        //$this->output->enable_profiler(TRUE);
    }
    
    function _output($output){

        switch ($this->session->userdata('language')) {
            case 'es': $this->footervar['analytics'] = "39804504-1"; break;
            case 'en': $this->footervar['analytics'] = "39786691-1"; break;
            default:
            case 'pt': $this->footervar['analytics'] = "39794265-1"; break;
        }

        if ($this->input->is_ajax_request() || $this->layout == FALSE) {
            echo $output;
        }else{
            echo $this->load->view('common/header', $this->headervar, TRUE).
                 $this->load->view('common/menu', $this->menuvar, TRUE).
                 $output.
                 $this->load->view('common/footer', $this->footervar, TRUE);
         }
    }

}
?>