<div class="centro container container-3">

	<div class="coluna esquerda">

		<h1><?=traduz('Material de Apoio')?></h1>

		<p class="maior">
			<?=traduz('Cada um de nós, Embaixadores da Água, pode também intervir em seus espaços cotidianos, como no escritório, no carro, nas portas e nos espelhos de nossos ambientes e, até mesmo, em nossa própria casa.')?>
		</p>
		<p class="maior">
			<?=traduz('Para isso, projetamos uma campanha composta por diversas peças, que você pode selecionar individualmente ou em conjunto, por cartazes. Use sua imaginação para conscientizar e sensibilizar mais pessoas sobre o Projeto Hydros.')?>
		</p>

	</div>

	<div class="coluna direita">


	</div>

	<a href="materiais/material/wallpaper" title="<?=traduz('Wallpaper')?>" class="link-apoio">
		<span class="texto-link"><?=traduz('Wallpaper')?></span>
		<img src="_imgs/layout/wallpaper.jpg">
		<span class="ver"><?=traduz('ver')?> &raquo;</span>
	</a>

	<a href="materiais/material/marcador" title="<?=traduz('Marcador de livro')?>" class="link-apoio">
		<span class="texto-link short"><?=traduz('Marcador de livro')?></span>
		<img src="_imgs/layout/marcador-de-livro.jpg">
		<span class="ver"><?=traduz('ver')?> &raquo;</span>
	</a>

	<a href="materiais/material/mobile" title="<?=traduz('Móbile')?>" class="link-apoio ultimo">
		<span class="texto-link"><?=traduz('Móbile')?></span>
		<img src="_imgs/layout/mobile.jpg">
		<span class="ver"><?=traduz('ver')?> &raquo;</span>
	</a>

	<a href="materiais/material/banner" title="<?=traduz('Banner')?>" class="link-apoio">
		<span class="texto-link"><?=traduz('Banner')?></span>
		<img src="_imgs/layout/banner.jpg">
		<span class="ver"><?=traduz('ver')?> &raquo;</span>
	</a>

	<a href="materiais/material/bandeja" title="<?=traduz('Toalha de bandeja')?>" class="link-apoio">
		<span class="texto-link"><?=traduz('Toalha de bandeja')?></span>
		<img src="_imgs/layout/toalha-de-bandeja.jpg">
		<span class="ver"><?=traduz('ver')?> &raquo;</span>
	</a>

	<a href="materiais/material/cartaz" title="<?=traduz('Cartaz')?>" class="link-apoio ultimo">
		<span class="texto-link"><?=traduz('Cartaz')?></span>
		<img src="_imgs/layout/cartaz.jpg">
		<span class="ver"><?=traduz('ver')?> &raquo;</span>
	</a>

	<a href="materiais/material/adesivo" title="<?=traduz('Adesivo')?>" class="link-apoio">
		<span class="texto-link"><?=traduz('Adesivo')?></span>
		<img src="_imgs/layout/adesivo.jpg">
		<span class="ver"><?=traduz('ver')?> &raquo;</span>
	</a>

	<a href="materiais/material/wobler" title="<?=traduz('Wobler')?>" class="link-apoio">
		<span class="texto-link short"><?=traduz('Wobler')?></span>
		<img src="_imgs/layout/wobler.jpg">
		<span class="ver"><?=traduz('ver')?> &raquo;</span>
	</a>

	<a href="materiais/material/bannersdigitais" title="<?=traduz('Banners Digitais')?>" class="link-apoio ultimo">
		<span class="texto-link short"><?=traduz('Banners Digitais')?></span>
		<img src="_imgs/layout/<?=prefixo('bannerthumb.png')?>">
		<span class="ver"><?=traduz('ver')?> &raquo;</span>
	</a>

	<div style="height:96px;">
		<div id="compartilhar-exposicoes" style="float:right; line-height:110%">
			<?=traduz('Compartilhe!')?><br>
			<ul>
				<li class="share-email primeiro">
					<a href="<?=$share['email']?>" title="<?=traduz('Compartilhe por e-mail!')?>">e-mail</a>
				</li>
				<li class="share-facebook">
					<a rel="nofollow" href="http://www.facebook.com/share.php" onclick="return fbs_click(this)" title="<?=traduz('Compartilhe pelo facebook!')?>">facebook</a>
				</li>
				<li class="share-twitter">
					<a rel="nofollow" href="" onclick="<?=$share['tweet']?>" title="<?=traduz('')?>">twitter</a>
				</li>
				<li class="share-tumblr">
					<a href="http://www.tumblr.com/share/link?url=<?php echo urlencode(current_url())?>&description=<?= urlencode($share['description_tumblr']) ?>&name=<?php echo urlencode($share['tumblr']) ?>" title="<?=traduz('Compartilhe no Tumblr!')?>">tumblr</a>
				</li>
			</ul>
		</div>
	</div>


	<div class="politica">
		<p class="primeiro">
			<strong><?=traduz('Advertência Legal de Utilização de Conteúdo')?></strong>			
			<?=traduz('O conteúdo do hotsite é de utilização livre, desde que observadas as regras a seguir estabelecidas:')?>
			1)&nbsp;<?=traduz('O objetivo do hotsite é exclusivamente informativo. O material exposto não pode ser utilizado para qualquer finalidade e/ou propósito contrário a esse fim, nem poderá ser copiado e/ou reproduzido sem a divulgação clara e expressa da fonte, creditando a autoria do texto, fotografia e/ou vídeo, produção etc., cujo nome se encontra indicado junto a cada material exposto. Exemplo: "www.projetohydros.com / (nome completo do autor). Todos os direitos reservados".')?>
			2)&nbsp;<?=traduz('A alteração de conteúdo e/ou distorções de qualquer natureza, como montagens de imagens, de voz ou outras, é expressamente proibida, em qualquer hipótese, podendo ser punida na forma da lei.')?>
			3)&nbsp;<?=traduz('É vedada, também, a veiculação do conteúdo deste hotsite, mesmo com indicação de autor, com a finalidade de lucro pessoal e/ou corporativo, ou com associação a religião, política, discriminação, pedofilia e/ou outras práticas ilícitas.')?>
			4)&nbsp;<?=traduz('A indicação de fonte efetuada por terceiro que utilizar os textos, imagens ou vídeos deste hotsite deverá conter, necessariamente, o endereço do domínio e o nome completo do autor do texto, fotografia ou vídeo correspondente.')?>
		</p>
	</div>	

</div>