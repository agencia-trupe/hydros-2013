<div class="centro container">

	<div class="coluna esquerda">
		<h1><?=traduz('Material de Apoio')?></h1>
		
		<h2 class="destaque">
			<?=$titulo?>
		</h2>

		<ul class="lista-apoio">
			<?php foreach ($dados as $key => $value): ?>
				<li>
					<img src="_imgs/material_apoio/thumbs/<?=$value?>">
					<div class="barra-share-large">
						<?php
						if($slug_original=='bannersdigitais')
							$link_download = str_replace('.png', '.zip', $value);
						elseif($slug_original != 'wallpaper')
							$link_download = str_replace('.jpg', '.pdf', $value);
						else
							$link_download = $value;
						?>
						<a href="_arquivos/apoio/<?=$link_download?>" target="_blank" title="<?=traduz('Faça o download!')?>" class="link-download"><?=traduz('Faça o download!')?></a>
						<div class="compartilhe"><?=traduz('Compartilhe!')?></div>
						<ul>
							<li class="share-email">
								<a href="<?=$share['email']?>" title="<?=traduz('Compartilhe por e-mail!')?>">e-mail</a>
							</li>
							<li class="share-facebook">
								<a rel="nofollow" href="http://www.facebook.com/share.php" onclick="return fbs_click(this)" title="<?=traduz('Compartilhe pelo facebook!')?>">facebook</a>
							</li>
							<li class="share-twitter">
								<a rel="nofollow" href="" onclick="<?=$share['tweet']?>" title="<?=traduz('')?>">twitter</a>
							</li>
							<li class="share-tumblr">
								<a href="http://www.tumblr.com/share/video?embed=<?php echo urlencode("<iframe src='http://player.vimeo.com/video/37537397?byline=0&portrait=0&loop=1&wmode=transparent&nocache=".time()."' width='640' height='360' frameborder='0' webkitAllowFullScreen allowFullScreen></iframe>") ?>&caption=<?php echo urlencode($share['tumblr']) ?>" title="<?=traduz('Compartilhe no Tumblr!')?>">tumblr</a>
							</li>
						</ul>
					</div>
				</li>
			<?php endforeach ?>
		</ul>

	</div>


	<div class="coluna direita">

		<ul class="sub-lateral-apoio">
			<li><a href="materiais/material/wallpaper" <?if($marcar=='wallpaper')echo"class='ativo'"?> title="<?=traduz('Wallpaper')?>"><?=traduz('Wallpaper')?> <span class="ver"><?=traduz('ver')?> &raquo;</span></a></li>
			<li><a href="materiais/material/marcador" <?if($marcar=='marcador')echo"class='ativo'"?> title="<?=traduz('Marcador de livro')?>"><?=traduz('Marcador de livro')?> <span class="ver"><?=traduz('ver')?> &raquo;</span></a></li>
			<li><a href="materiais/material/mobile" <?if($marcar=='mobile')echo"class='ativo'"?> title="<?=traduz('Móbile')?>"><?=traduz('Móbile')?> <span class="ver"><?=traduz('ver')?> &raquo;</span></a></li>
			<li><a href="materiais/material/banner" <?if($marcar=='banner')echo"class='ativo'"?> title="<?=traduz('Banner')?>"><?=traduz('Banner')?> <span class="ver"><?=traduz('ver')?> &raquo;</span></a></li>
			<li><a href="materiais/material/bandeja" <?if($marcar=='bandeja')echo"class='ativo'"?> title="<?=traduz('Toalha de bandeja')?>"><?=traduz('Toalha de bandeja')?> <span class="ver"><?=traduz('ver')?> &raquo;</span></a></li>
			<li><a href="materiais/material/cartaz" <?if($marcar=='cartaz')echo"class='ativo'"?> title="<?=traduz('Cartaz')?>"><?=traduz('Cartaz')?> <span class="ver"><?=traduz('ver')?> &raquo;</span></a></li>
			<li><a href="materiais/material/adesivo" <?if($marcar=='adesivo')echo"class='ativo'"?> title="<?=traduz('Adesivo')?>"><?=traduz('Adesivo')?> <span class="ver"><?=traduz('ver')?> &raquo;</span></a></li>
			<li><a href="materiais/material/wobler" <?if($marcar=='wobler')echo"class='ativo'"?> title="<?=traduz('Wobler')?>"><?=traduz('Wobler')?> <span class="ver"><?=traduz('ver')?> &raquo;</span></a></li>
			<li><a href="materiais/material/bannersdigitais" <?if($marcar=='bannersdigitais')echo"class='ativo'"?> title="<?=traduz('Banners Digitais')?>"><?=traduz('Banners Digitais')?> <span class="ver"><?=traduz('ver')?> &raquo;</span></a></li>
		</ul>

	</div>

	<div class="politica" style="margin-top:55px;">
		<p class="primeiro">
			<strong><?=traduz('Advertência Legal de Utilização de Conteúdo')?></strong>			
			<?=traduz('O conteúdo do hotsite é de utilização livre, desde que observadas as regras a seguir estabelecidas:')?>
			1)&nbsp;<?=traduz('O objetivo do hotsite é exclusivamente informativo. O material exposto não pode ser utilizado para qualquer finalidade e/ou propósito contrário a esse fim, nem poderá ser copiado e/ou reproduzido sem a divulgação clara e expressa da fonte, creditando a autoria do texto, fotografia e/ou vídeo, produção etc., cujo nome se encontra indicado junto a cada material exposto. Exemplo: "www.projetohydros.com / (nome completo do autor). Todos os direitos reservados".')?>
			2)&nbsp;<?=traduz('A alteração de conteúdo e/ou distorções de qualquer natureza, como montagens de imagens, de voz ou outras, é expressamente proibida, em qualquer hipótese, podendo ser punida na forma da lei.')?>
			3)&nbsp;<?=traduz('É vedada, também, a veiculação do conteúdo deste hotsite, mesmo com indicação de autor, com a finalidade de lucro pessoal e/ou corporativo, ou com associação a religião, política, discriminação, pedofilia e/ou outras práticas ilícitas.')?>
			4)&nbsp;<?=traduz('A indicação de fonte efetuada por terceiro que utilizar os textos, imagens ou vídeos deste hotsite deverá conter, necessariamente, o endereço do domínio e o nome completo do autor do texto, fotografia ou vídeo correspondente.')?>
		</p>
	</div>		

</div>