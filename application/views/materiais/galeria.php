<div class="centro">
	<h1><?=traduz('Galeria de Imagens')?></h1>
	<p class="maior">
		<?=traduz('Um dos grandes destaques do Projeto Hydros são as fotografias que compõem os quatro livros da série, de autoria de artistas latino-americanos e europeus. São mais de 200 imagens para você utilizar. Aqui você encontra a opção de compartilhá-las ou baixá-las em todos os tamanhos.')?>
	</p>
</div>

<div id="galeria" class="borda">

	<div class="centro slide-container">
		<div class="slider" id="slider1"></div>
	</div>

    <div class="viewport">
        <div class="galeria-imagens">
            <div class="imagens" style="width:<?=$tamanho_galeria?>">
                <?=$galeria?>
            </div>
        </div>
    </div>
    
    <div class="centro slide-container">
		<div class="slider" id="slider2"></div>
	</div>

</div>

<div class="centro container container-3">

	<div class="coluna">

		<a href="arquivo/galeria-hydros-completa_zip" title="<?=traduz('DOWNLOAD DA GALERIA <br>DE IMAGENS COMPLETA')?>" class="link-roxo">
			<h3><?=traduz('DOWNLOAD DA GALERIA <br>DE IMAGENS COMPLETA')?></h3>
			<h4><?=traduz('Você pode fazer o download da <br>galeria completa em alta resolução.')?></h4>
			<img src="_imgs/layout/caixa_download.png" alt="botão de download">
			<span class="verde"><?=traduz('clique para iniciar o download')?></span>
		</a>

	</div>

	<div class="coluna meio larga">

		<div id="dica">
			<h2><?=traduz('DICA')?></h2>
			<p>
				<?=traduz('Para que sua iniciativa tenha um resultado mais positivo e impactante, sugerimos a impressão do material selecionado em papel fotográfico e em alta resolução.')?>
			</p>
	
			<div id="compartilhar-galeria">
				<h5><?=traduz('Compartilhe!')?></h5>
				<ul>
					<li class="share-email primeiro">
						<a href="<?=$share['email']?>" title="<?=traduz('Compartilhe por e-mail!')?>">e-mail</a>
					</li>
					<li class="share-facebook">
						<a rel="nofollow" href="http://www.facebook.com/share.php" onclick="return fbs_click(this)" title="<?=traduz('Compartilhe pelo facebook!')?>">facebook</a>
					</li>
					<li class="share-twitter">
						<a rel="nofollow" href="" onclick="<?=$share['tweet']?>" title="<?=traduz('')?>">twitter</a>
					</li>
					<li class="share-tumblr">
						<a href="http://www.tumblr.com/share/link?url=<?php echo urlencode(current_url())?>&name=<?=$share['tweet']?>&description=<?php echo urlencode($share['tumblr']) ?>" title="<?=traduz('Compartilhe no Tumblr!')?>">tumblr</a>
					</li>
				</ul>			
			</div>	

		</div>
	</div>

		<div class="politica">
			<p class="primeiro">
				<strong><?=traduz('Advertência Legal de Utilização de Conteúdo')?></strong>			
				<?=traduz('O conteúdo do hotsite é de utilização livre, desde que observadas as regras a seguir estabelecidas:')?>
				1)&nbsp;<?=traduz('O objetivo do hotsite é exclusivamente informativo. O material exposto não pode ser utilizado para qualquer finalidade e/ou propósito contrário a esse fim, nem poderá ser copiado e/ou reproduzido sem a divulgação clara e expressa da fonte, creditando a autoria do texto, fotografia e/ou vídeo, produção etc., cujo nome se encontra indicado junto a cada material exposto. Exemplo: "www.projetohydros.com / (nome completo do autor). Todos os direitos reservados".')?>
				2)&nbsp;<?=traduz('A alteração de conteúdo e/ou distorções de qualquer natureza, como montagens de imagens, de voz ou outras, é expressamente proibida, em qualquer hipótese, podendo ser punida na forma da lei.')?>
				3)&nbsp;<?=traduz('É vedada, também, a veiculação do conteúdo deste hotsite, mesmo com indicação de autor, com a finalidade de lucro pessoal e/ou corporativo, ou com associação a religião, política, discriminação, pedofilia e/ou outras práticas ilícitas.')?>
				4)&nbsp;<?=traduz('A indicação de fonte efetuada por terceiro que utilizar os textos, imagens ou vídeos deste hotsite deverá conter, necessariamente, o endereço do domínio e o nome completo do autor do texto, fotografia ou vídeo correspondente.')?>
			</p>
		</div>	    
</div>

