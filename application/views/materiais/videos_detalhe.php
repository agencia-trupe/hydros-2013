<div class="centro container">

	<div class="video-detalhe" style="min-height:470px;">
		<h1><?=$video['titulo']?></h1>
		<h2><?=$video['subtitulo']?></h2>

		<?=$video['embed']?>
		<div class="barra-share-large">
			<?php if ($video['arquivo']): ?>
				<a href="arquivo/<?=$video['arquivo']?>" title="<?=traduz('Faça o download!')?>" class="link-download"><?=traduz('Faça o download!')?></a>	
			<?php endif ?>
			<div class="compartilhe"><?=traduz('Compartilhe!')?></div>
			<ul>
				<li class="share-email">
					<a href="<?=$share['email']?>" title="<?=traduz('Compartilhe por e-mail!')?>">e-mail</a>
				</li>
				<li class="share-facebook">
					<a rel="nofollow" href="http://www.facebook.com/share.php" onclick="return fbs_click(this)" title="<?=traduz('Compartilhe pelo facebook!')?>">facebook</a>
				</li>
				<li class="share-twitter">
					<a rel="nofollow" href="" onclick="<?=$share['tweet']?>" title="<?=traduz('Compartilhe no Tumblr')?>">twitter</a>
				</li>
				<li class="share-tumblr">
					<a href="http://www.tumblr.com/share/video?embed=<?php echo urlencode($video['embed']) ?>&caption=<?php echo urlencode($share['tumblr']) ?>" title="<?=traduz('Compartilhe no Tumblr!')?>">tumblr</a>
				</li>
			</ul>
		</div>

	</div>

</div>

<div class="bordat" id="botao-voltar">

	<div class="centro">

		<a href="materiais/videos" title="<?=traduz('Voltar')?>" id="link-voltar">&laquo; <?=traduz('VOLTAR')?></a>	

		<div class="politica" style="margin-top:20px;">
			<p class="primeiro">
				<strong><?=traduz('Advertência Legal de Utilização de Conteúdo')?></strong>			
				<?=traduz('O conteúdo do hotsite é de utilização livre, desde que observadas as regras a seguir estabelecidas:')?>
				1)&nbsp;<?=traduz('O objetivo do hotsite é exclusivamente informativo. O material exposto não pode ser utilizado para qualquer finalidade e/ou propósito contrário a esse fim, nem poderá ser copiado e/ou reproduzido sem a divulgação clara e expressa da fonte, creditando a autoria do texto, fotografia e/ou vídeo, produção etc., cujo nome se encontra indicado junto a cada material exposto. Exemplo: "www.projetohydros.com / (nome completo do autor). Todos os direitos reservados".')?>
				2)&nbsp;<?=traduz('A alteração de conteúdo e/ou distorções de qualquer natureza, como montagens de imagens, de voz ou outras, é expressamente proibida, em qualquer hipótese, podendo ser punida na forma da lei.')?>
				3)&nbsp;<?=traduz('É vedada, também, a veiculação do conteúdo deste hotsite, mesmo com indicação de autor, com a finalidade de lucro pessoal e/ou corporativo, ou com associação a religião, política, discriminação, pedofilia e/ou outras práticas ilícitas.')?>
				4)&nbsp;<?=traduz('A indicação de fonte efetuada por terceiro que utilizar os textos, imagens ou vídeos deste hotsite deverá conter, necessariamente, o endereço do domínio e o nome completo do autor do texto, fotografia ou vídeo correspondente.')?>
			</p>
		</div>	    

	</div>

</div>