<div class="centro container">

	<div class="coluna esquerda">
		<h1><?=traduz('A água em nós')?></h1>

		<p>
			<?=traduz('Os vídeos “Água em nós”, composto por um filme mais completo e outros oito featurettes, são ferramentas de conscientização e sensibilização que mostram, de forma lúdica, a verdadeira relação que temos com a água em todos os seus aspectos.  Portanto, use-os à vontade: publique no seu mural do Facebook e outras redes sociais ou faça o download para usá-los no ambiente de trabalho, com a família e amigos.')?>
		</p>
		<h4><?=traduz('Faça a diferença agindo diferente!')?></h4>
	</div>


	<div class="coluna direita">

		<iframe src="<?=$link_video?><?=time()?>" width="640" height="360" frameborder="0" webkitAllowFullScreen allowFullScreen></iframe>
		<div class="barra-share-large">
			<a href="arquivo/HYDROSPT_mp4/video" title="<?=traduz('Faça o download!')?>" class="link-download"><?=traduz('Faça o download!')?></a>
			<div class="compartilhe"><?=traduz('Compartilhe!')?></div>
			<ul>
				<li class="share-email">
					<a href="<?=$share['email']?>" title="<?=traduz('Compartilhe por e-mail!')?>">e-mail</a>
				</li>
				<li class="share-facebook">
					<a rel="nofollow" href="http://www.facebook.com/share.php" onclick="return fbs_click(this)" title="<?=traduz('Compartilhe pelo facebook!')?>">facebook</a>
				</li>
				<li class="share-twitter">
					<a rel="nofollow" href="" onclick="<?=$share['tweet']?>" title="<?=traduz('Compartilhe no Tumblr')?>">twitter</a>
				</li>
				<li class="share-tumblr">
					<a href="http://www.tumblr.com/share/video?embed=<?php echo urlencode("<iframe src='http://player.vimeo.com/video/37537397?byline=0&portrait=0&loop=1&wmode=transparent&nocache=".time()."' width='640' height='360' frameborder='0' webkitAllowFullScreen allowFullScreen></iframe>") ?>&caption=<?php echo urlencode($share['tumblr']) ?>" title="<?=traduz('Compartilhe no Tumblr!')?>">tumblr</a>
				</li>
			</ul>
		</div>

	</div>

	<div id="links-videos">

		<div class="link-video-holder">
			<a href="materiais/videos/<?=$video1['slug']?>" title="" class="link-detalhe">
				<div class="titulo-video"><?=$video1['fb_titulo']?></div>
				<div class="play-overlay"></div>
				<img src="_imgs/videos/thumbs/<?=$video1['imagem']?>">
			</a>
			<div class="barra-share-small">
				<a href="arquivo/<?=$video1['arquivo']?>" title="<?=traduz('Faça o download!')?>" class="link-download"></a>
				<ul>
					<li class="share-email">
						<a href="<?=$video1['email']?>" title="<?=traduz('Compartilhe por e-mail!')?>">e-mail</a>
					</li>
					<li class="share-facebook">
						<a rel="nofollow" href="http://www.facebook.com/share.php" data-fb-title="<?=$video1['fb_titulo']?>" data-fb-description="<?=$video1['fb_description']?>" onclick="return fbs_click(this)" title="<?=traduz('Compartilhe pelo facebook!')?>">facebook</a>
					</li>
					<li class="share-twitter">
						<a rel="nofollow" href="" onclick="window.open('http://twitter.com/home?status=<?=$video1['twitter']?>','Twitter','toolbar=0,status=0,width=626,height=436'); return false;" title="<?=traduz('Compartilhe no Twitter')?>">twitter</a>
					</li>
					<li class="share-tumblr">
						<a href="http://www.tumblr.com/share/video?embed=<?php echo urlencode("<iframe src='http://player.vimeo.com/video/".$video1['id_embed']."?byline=0&portrait=0&loop=1&wmode=transparent&nocache=".time()."' width='640' height='360' frameborder='0' webkitAllowFullScreen allowFullScreen></iframe>") ?>&caption=<?php echo urlencode($share['tumblr']) ?>" title="<?=traduz('Compartilhe no Tumblr!')?>">tumblr</a>
					</li>
				</ul>
			</div>
		</div>

		<div class="link-video-holder">
			<a href="materiais/videos/<?=$video2['slug']?>" title="" class="link-detalhe">
				<div class="titulo-video"><?=$video2['fb_titulo']?></div>
				<div class="play-overlay"></div>
				<img src="_imgs/videos/thumbs/<?=$video2['imagem']?>">
			</a>
			<div class="barra-share-small">
				<a href="arquivo/<?=$video2['arquivo']?>" title="<?=traduz('Faça o download!')?>" class="link-download"></a>
				<ul>
					<li class="share-email">
						<a href="<?=$video2['email']?>" title="<?=traduz('Compartilhe por e-mail!')?>">e-mail</a>
					</li>
					<li class="share-facebook">
						<a rel="nofollow" href="http://www.facebook.com/share.php" data-fb-title="<?=$video2['fb_titulo']?>" data-fb-description="<?=$video2['fb_description']?>" onclick="return fbs_click(this)" title="<?=traduz('Compartilhe pelo facebook!')?>">facebook</a>
					</li>
					<li class="share-twitter">
						<a rel="nofollow" href="" onclick="window.open('http://twitter.com/home?status=<?=$video2['twitter']?>','Twitter','toolbar=0,status=0,width=626,height=436'); return false;" title="<?=traduz('Compartilhe no Twitter')?>">twitter</a>
					</li>
					<li class="share-tumblr">
						<a href="http://www.tumblr.com/share/video?embed=<?php echo urlencode("<iframe src='http://player.vimeo.com/video/".$video2['id_embed']."?byline=0&portrait=0&loop=1&wmode=transparent&nocache=".time()."' width='640' height='360' frameborder='0' webkitAllowFullScreen allowFullScreen></iframe>") ?>&caption=<?php echo urlencode($share['tumblr']) ?>" title="<?=traduz('Compartilhe no Tumblr!')?>">tumblr</a>
					</li>
				</ul>
			</div>
		</div>

		<div class="link-video-holder">
			<a href="materiais/videos/<?=$video3['slug']?>" title="" class="link-detalhe">
				<div class="titulo-video"><?=$video3['fb_titulo']?></div>
				<div class="play-overlay"></div>
				<img src="_imgs/videos/thumbs/<?=$video3['imagem']?>">
			</a>
			<div class="barra-share-small">
				<a href="arquivo/<?=$video3['arquivo']?>" title="<?=traduz('Faça o download!')?>" class="link-download"></a>
				<ul>
					<li class="share-email">
						<a href="<?=$video3['email']?>" title="<?=traduz('Compartilhe por e-mail!')?>">e-mail</a>
					</li>
					<li class="share-facebook">
						<a rel="nofollow" href="http://www.facebook.com/share.php" data-fb-title="<?=$video3['fb_titulo']?>" data-fb-description="<?=$video3['fb_description']?>" onclick="return fbs_click(this)" title="<?=traduz('Compartilhe pelo facebook!')?>">facebook</a>
					</li>
					<li class="share-twitter">
						<a rel="nofollow" href="" onclick="window.open('http://twitter.com/home?status=<?=$video3['twitter']?>','Twitter','toolbar=0,status=0,width=626,height=436'); return false;" title="<?=traduz('Compartilhe no Twitter')?>">twitter</a>
					</li>
					<li class="share-tumblr">
						<a href="http://www.tumblr.com/share/video?embed=<?php echo urlencode("<iframe src='http://player.vimeo.com/video/".$video3['id_embed']."?byline=0&portrait=0&loop=1&wmode=transparent&nocache=".time()."' width='640' height='360' frameborder='0' webkitAllowFullScreen allowFullScreen></iframe>") ?>&caption=<?php echo urlencode($share['tumblr']) ?>" title="<?=traduz('Compartilhe no Tumblr!')?>">tumblr</a>
					</li>
				</ul>
			</div>
		</div>

		<div class="link-video-holder ultimo">
			<a href="materiais/videos/<?=$video4['slug']?>" title="" class="link-detalhe">
				<div class="titulo-video"><?=$video4['fb_titulo']?></div>
				<div class="play-overlay"></div>
				<img src="_imgs/videos/thumbs/<?=$video4['imagem']?>">
			</a>
			<div class="barra-share-small">
				<a href="arquivo/<?=$video4['arquivo']?>" title="<?=traduz('Faça o download!')?>" class="link-download"></a>
				<ul>
					<li class="share-email">
						<a href="<?=$video4['email']?>" title="<?=traduz('Compartilhe por e-mail!')?>">e-mail</a>
					</li>
					<li class="share-facebook">
						<a rel="nofollow" href="http://www.facebook.com/share.php" data-fb-title="<?=$video4['fb_titulo']?>" data-fb-description="<?=$video4['fb_description']?>" onclick="return fbs_click(this)" title="<?=traduz('Compartilhe pelo facebook!')?>">facebook</a>
					</li>
					<li class="share-twitter">
						<a rel="nofollow" href="" onclick="window.open('http://twitter.com/home?status=<?=$video4['twitter']?>','Twitter','toolbar=0,status=0,width=626,height=436'); return false;" title="<?=traduz('Compartilhe no Twitter')?>">twitter</a>
					</li>
					<li class="share-tumblr">
						<a href="http://www.tumblr.com/share/video?embed=<?php echo urlencode("<iframe src='http://player.vimeo.com/video/".$video4['id_embed']."?byline=0&portrait=0&loop=1&wmode=transparent&nocache=".time()."' width='640' height='360' frameborder='0' webkitAllowFullScreen allowFullScreen></iframe>") ?>&caption=<?php echo urlencode($share['tumblr']) ?>" title="<?=traduz('Compartilhe no Tumblr!')?>">tumblr</a>
					</li>
				</ul>
			</div>
		</div>

		<div class="link-video-holder">
			<a href="materiais/videos/<?=$video5['slug']?>" title="" class="link-detalhe">
				<div class="titulo-video"><?=$video5['fb_titulo']?></div>
				<div class="play-overlay"></div>
				<img src="_imgs/videos/thumbs/<?=$video5['imagem']?>">
			</a>
			<div class="barra-share-small">
				<a href="arquivo/<?=$video5['arquivo']?>" title="<?=traduz('Faça o download!')?>" class="link-download"></a>
				<ul>
					<li class="share-email">
						<a href="<?=$video5['email']?>" title="<?=traduz('Compartilhe por e-mail!')?>">e-mail</a>
					</li>
					<li class="share-facebook">
						<a rel="nofollow" href="http://www.facebook.com/share.php" data-fb-title="<?=$video5['fb_titulo']?>" data-fb-description="<?=$video5['fb_description']?>" onclick="return fbs_click(this)" title="<?=traduz('Compartilhe pelo facebook!')?>">facebook</a>
					</li>
					<li class="share-twitter">
						<a rel="nofollow" href="" onclick="window.open('http://twitter.com/home?status=<?=$video5['twitter']?>','Twitter','toolbar=0,status=0,width=626,height=436'); return false;" title="<?=traduz('Compartilhe no Twitter')?>">twitter</a>
					</li>
					<li class="share-tumblr">
						<a href="http://www.tumblr.com/share/video?embed=<?php echo urlencode("<iframe src='http://player.vimeo.com/video/".$video5['id_embed']."?byline=0&portrait=0&loop=1&wmode=transparent&nocache=".time()."' width='640' height='360' frameborder='0' webkitAllowFullScreen allowFullScreen></iframe>") ?>&caption=<?php echo urlencode($share['tumblr']) ?>" title="<?=traduz('Compartilhe no Tumblr!')?>">tumblr</a>
					</li>
				</ul>
			</div>
		</div>

		<div class="link-video-holder">
			<a href="materiais/videos/<?=$video6['slug']?>" title="" class="link-detalhe">
				<div class="titulo-video"><?=$video6['fb_titulo']?></div>
				<div class="play-overlay"></div>
				<img src="_imgs/videos/thumbs/<?=$video6['imagem']?>">
			</a>
			<div class="barra-share-small">
				<a href="arquivo/<?=$video6['arquivo']?>" title="<?=traduz('Faça o download!')?>" class="link-download"></a>
				<ul>
					<li class="share-email">
						<a href="<?=$video6['email']?>" title="<?=traduz('Compartilhe por e-mail!')?>">e-mail</a>
					</li>
					<li class="share-facebook">
						<a rel="nofollow" href="http://www.facebook.com/share.php" data-fb-title="<?=$video6['fb_titulo']?>" data-fb-description="<?=$video6['fb_description']?>" onclick="return fbs_click(this)" title="<?=traduz('Compartilhe pelo facebook!')?>">facebook</a>
					</li>
					<li class="share-twitter">
						<a rel="nofollow" href="" onclick="window.open('http://twitter.com/home?status=<?=$video6['twitter']?>','Twitter','toolbar=0,status=0,width=626,height=436'); return false;" title="<?=traduz('Compartilhe no Twitter')?>">twitter</a>
					</li>
					<li class="share-tumblr">
						<a href="http://www.tumblr.com/share/video?embed=<?php echo urlencode("<iframe src='http://player.vimeo.com/video/".$video6['id_embed']."?byline=0&portrait=0&loop=1&wmode=transparent&nocache=".time()."' width='640' height='360' frameborder='0' webkitAllowFullScreen allowFullScreen></iframe>") ?>&caption=<?php echo urlencode($share['tumblr']) ?>" title="<?=traduz('Compartilhe no Tumblr!')?>">tumblr</a>
					</li>
				</ul>
			</div>
		</div>

		<div class="link-video-holder">
			<a href="materiais/videos/<?=$video7['slug']?>" title="" class="link-detalhe">
				<div class="titulo-video"><?=$video7['fb_titulo']?></div>
				<div class="play-overlay"></div>
				<img src="_imgs/videos/thumbs/<?=$video7['imagem']?>">
			</a>
			<div class="barra-share-small">
				<a href="arquivo/<?=$video7['arquivo']?>" title="<?=traduz('Faça o download!')?>" class="link-download"></a>
				<ul>
					<li class="share-email">
						<a href="<?=$video7['email']?>" title="<?=traduz('Compartilhe por e-mail!')?>">e-mail</a>
					</li>
					<li class="share-facebook">
						<a rel="nofollow" href="http://www.facebook.com/share.php" data-fb-title="<?=$video7['fb_titulo']?>" data-fb-description="<?=$video7['fb_description']?>" onclick="return fbs_click(this)" title="<?=traduz('Compartilhe pelo facebook!')?>">facebook</a>
					</li>
					<li class="share-twitter">
						<a rel="nofollow" href="" onclick="window.open('http://twitter.com/home?status=<?=$video7['twitter']?>','Twitter','toolbar=0,status=0,width=626,height=436'); return false;" title="<?=traduz('Compartilhe no Twitter')?>">twitter</a>
					</li>
					<li class="share-tumblr">
						<a href="http://www.tumblr.com/share/video?embed=<?php echo urlencode("<iframe src='http://player.vimeo.com/video/".$video7['id_embed']."?byline=0&portrait=0&loop=1&wmode=transparent&nocache=".time()."' width='640' height='360' frameborder='0' webkitAllowFullScreen allowFullScreen></iframe>") ?>&caption=<?php echo urlencode($share['tumblr']) ?>" title="<?=traduz('Compartilhe no Tumblr!')?>">tumblr</a>
					</li>
				</ul>
			</div>
		</div>

		<div class="link-video-holder ultimo">
			<a href="materiais/videos/<?=$video8['slug']?>" title="" class="link-detalhe">
				<div class="titulo-video"><?=$video8['fb_titulo']?></div>
				<div class="play-overlay"></div>
				<img src="_imgs/videos/thumbs/<?=$video8['imagem']?>">
			</a>
			<div class="barra-share-small">
				<a href="arquivo/<?=$video8['arquivo']?>" title="<?=traduz('Faça o download!')?>" class="link-download"></a>
				<ul>
					<li class="share-email">
						<a href="<?=$video8['email']?>" title="<?=traduz('Compartilhe por e-mail!')?>">e-mail</a>
					</li>
					<li class="share-facebook">
						<a rel="nofollow" href="http://www.facebook.com/share.php" data-fb-title="<?=$video8['fb_titulo']?>" data-fb-description="<?=$video8['fb_description']?>" onclick="return fbs_click(this)" title="<?=traduz('Compartilhe pelo facebook!')?>">facebook</a>
					</li>
					<li class="share-twitter">
						<a rel="nofollow" href="" onclick="window.open('http://twitter.com/home?status=<?=$video8['twitter']?>','Twitter','toolbar=0,status=0,width=626,height=436'); return false;" title="<?=traduz('Compartilhe no Twitter')?>">twitter</a>
					</li>
					<li class="share-tumblr">
						<a href="http://www.tumblr.com/share/video?embed=<?php echo urlencode("<iframe src='http://player.vimeo.com/video/".$video8['id_embed']."?byline=0&portrait=0&loop=1&wmode=transparent&nocache=".time()."' width='640' height='360' frameborder='0' webkitAllowFullScreen allowFullScreen></iframe>") ?>&caption=<?php echo urlencode($share['tumblr']) ?>" title="<?=traduz('Compartilhe no Tumblr!')?>">tumblr</a>
					</li>
				</ul>
			</div>
		</div>

	</div>
</div>

<div class="bordat">
	<div class="centro container">

		<div id="videos-cocorico" <?if($this->session->userdata('language') != 'pt')echo"style='height:0; overflow:hidden;'"?>>

			<div id="texto-cocorico">
				<h2><?=traduz('Personagens da Turma do Cocoricó estrelam filmetes do Projeto Hydros')?></h2>
				<p>
					<?=traduz('Em 2012, o Projeto Hydros ganhou importantes aliados, por meio de uma iniciativa da Mexichem Brasil: a série “Pílulas de Cultura” estrelada pela Turma do Cocoricó. Veiculada na TV Cultura, a série foca o tema da água e é um ótimo exemplo de como o Projeto Hydros possui inúmeras possibilidades de interação. Reveja os vídeos.')?>
				</p>
			</div>

			<div class="link-video-holder">
				<a href="materiais/videos/cocorico1" title="" class="link-detalhe">
					<div class="titulo-video"><?=traduz('Água')?></div>
					<div class="play-overlay azul"></div>
					<img src="_imgs/videos/thumb-cocorico-1.png">
				</a>
				<div class="barra-share-small">
<!-- 					<a href="" title="<?=traduz('Faça o download!')?>" class="link-download"></a> -->
					<ul>
						<li class="share-email">
							<a href="mailto:?subject=Conheça o Projeto Hydros! - Filmetes da Turma do Cocoricó&body=Em 2012, o Projeto Hydros ganhou importantes aliados, por meio de uma iniciativa da Mexichem Brasil: a série “Pílulas de Cultura” estrelada pela Turma do Cocoricó. Veiculada na TV Cultura, a série foca o tema da água e é um ótimo exemplo de como o Projeto Hydros possui inúmeras possibilidades de interação. Reveja os vídeos." title="Compartilhe por e-mail!">e-mail</a>
						</li>
						<li class="share-facebook">
							<a rel="nofollow" href="http://www.facebook.com/share.php" onclick="return fbs_click(this)" title="Compartilhe pelo facebook!">facebook</a>
						</li>
						<li class="share-twitter">
							<a rel="nofollow" href="" onclick="window.open('http://twitter.com/home?status=Filmetes da Turma do Cocoricó sobre o Projeto Hydros! http://www.projetohydros.com/hydros2013/materiais/videos/cocorico1','Twitter','toolbar=0,status=0,width=626,height=436'); return false;" title="Compartilhe no Tumblr">twitter</a>
						</li>
						<li class="share-tumblr">
							<a href="http://www.tumblr.com/share/video?embed=%3Ciframe+width%3D%27640%27+height%3D%27360%27+src%3D%27http%3A%2F%2Fwww.youtube.com%2Fembed%2FInSMGSJ1ack%3F%26wmode%3Dtransparent%27+frameborder%3D%270%27+allowfullscreen%3E%3C%2Fiframe%3E&caption=Filmetes+da+Turma+do+Cocoric%C3%B3+sobre+o+Projeto+Hydros%21+http%3A%2F%2Fwww.projetohydros.com%2Fhydros2013%2Fmateriais%2Fvideos%2Fcocorico1" title="Compartilhe no Tumblr!">tumblr</a>
						</li>
					</ul>
				</div>
			</div>

			<div class="link-video-holder">
				<a href="materiais/videos/cocorico2" title="" class="link-detalhe">
					<div class="titulo-video"><?=traduz('Não pode faltar água nesse mundo')?></div>
					<div class="play-overlay azul"></div>
					<img src="_imgs/videos/thumb-cocorico-2.png">
				</a>
				<div class="barra-share-small">
					<!-- <a href="" title="<?=traduz('Faça o download!')?>" class="link-download"></a> -->
					<ul>
						<li class="share-email">
							<a href="mailto:?subject=Conheça o Projeto Hydros! - Filmetes da Turma do Cocoricó&body=Em 2012, o Projeto Hydros ganhou importantes aliados, por meio de uma iniciativa da Mexichem Brasil: a série “Pílulas de Cultura” estrelada pela Turma do Cocoricó. Veiculada na TV Cultura, a série foca o tema da água e é um ótimo exemplo de como o Projeto Hydros possui inúmeras possibilidades de interação. Reveja os vídeos." title="Compartilhe por e-mail!">e-mail</a>
						</li>
						<li class="share-facebook">
							<a rel="nofollow" href="http://www.facebook.com/share.php" onclick="return fbs_click(this)" title="Compartilhe pelo facebook!">facebook</a>
						</li>
						<li class="share-twitter">
							<a rel="nofollow" href="" onclick="window.open('http://twitter.com/home?status=Filmetes da Turma do Cocoricó sobre o Projeto Hydros! http://www.projetohydros.com/hydros2013/materiais/videos/cocorico2','Twitter','toolbar=0,status=0,width=626,height=436'); return false;" title="Compartilhe no Tumblr">twitter</a>
						</li>
						<li class="share-tumblr">
							<a href="http://www.tumblr.com/share/video?embed=%3Ciframe+width%3D%27640%27+height%3D%27360%27+src%3D%27http%3A%2F%2Fwww.youtube.com%2Fembed%2FSnmmMZ08duo%3F%26wmode%3Dtransparent%27+frameborder%3D%270%27+allowfullscreen%3E%3C%2Fiframe%3E&caption=Filmetes+da+Turma+do+Cocoric%C3%B3+sobre+o+Projeto+Hydros%21+http%3A%2F%2Fwww.projetohydros.com%2Fhydros2013%2Fmateriais%2Fvideos%2Fcocorico2" title="Compartilhe no Tumblr!">tumblr</a>
						</li>
					</ul>
				</div>
			</div>

			<div class="link-video-holder">
				<a href="materiais/videos/cocorico3" title="" class="link-detalhe">
					<div class="titulo-video"><?=traduz('Entre nessa onda!')?></div>
					<div class="play-overlay azul"></div>
					<img src="_imgs/videos/thumb-cocorico-3.png">
				</a>
				<div class="barra-share-small">
					<!-- <a href="" title="<?=traduz('Faça o download!')?>" class="link-download"></a> -->
					<ul>
						<li class="share-email">
							<a href="mailto:?subject=Conheça o Projeto Hydros! - Filmetes da Turma do Cocoricó&body=Em 2012, o Projeto Hydros ganhou importantes aliados, por meio de uma iniciativa da Mexichem Brasil: a série “Pílulas de Cultura” estrelada pela Turma do Cocoricó. Veiculada na TV Cultura, a série foca o tema da água e é um ótimo exemplo de como o Projeto Hydros possui inúmeras possibilidades de interação. Reveja os vídeos." title="Compartilhe por e-mail!">e-mail</a>
						</li>
						<li class="share-facebook">
							<a rel="nofollow" href="http://www.facebook.com/share.php" onclick="return fbs_click(this)" title="Compartilhe pelo facebook!">facebook</a>
						</li>
						<li class="share-twitter">
							<a rel="nofollow" href="" onclick="window.open('http://twitter.com/home?status=Filmetes da Turma do Cocoricó sobre o Projeto Hydros! http://www.projetohydros.com/hydros2013/materiais/videos/cocorico3','Twitter','toolbar=0,status=0,width=626,height=436'); return false;" title="Compartilhe no Tumblr">twitter</a>
						</li>
						<li class="share-tumblr">
							<a href="http://www.tumblr.com/share/video?embed=%3Ciframe+width%3D%27640%27+height%3D%27360%27+src%3D%27http%3A%2F%2Fwww.youtube.com%2Fembed%2FbUfim1f1Uas%3F%26wmode%3Dtransparent%27+frameborder%3D%270%27+allowfullscreen%3E%3C%2Fiframe%3E&caption=Filmetes+da+Turma+do+Cocoric%C3%B3+sobre+o+Projeto+Hydros%21+http%3A%2F%2Fwww.projetohydros.com%2Fhydros2013%2Fmateriais%2Fvideos%2Fcocorico3" title="Compartilhe no Tumblr!">tumblr</a>
						</li>
					</ul>
				</div>
			</div>

			<div class="link-video-holder ultimo">
				<a href="materiais/videos/cocorico4" title="" class="link-detalhe">
					<div class="titulo-video"><?=traduz('Água limpa é vida!')?></div>
					<div class="play-overlay azul"></div>
					<img src="_imgs/videos/thumb-cocorico-4.png">
				</a>
				<div class="barra-share-small">
					<!-- <a href="" title="<?=traduz('Faça o download!')?>" class="link-download"></a> -->
					<ul>
						<li class="share-email">
							<a href="mailto:?subject=Conheça o Projeto Hydros! - Filmetes da Turma do Cocoricó&body=Em 2012, o Projeto Hydros ganhou importantes aliados, por meio de uma iniciativa da Mexichem Brasil: a série “Pílulas de Cultura” estrelada pela Turma do Cocoricó. Veiculada na TV Cultura, a série foca o tema da água e é um ótimo exemplo de como o Projeto Hydros possui inúmeras possibilidades de interação. Reveja os vídeos." title="Compartilhe por e-mail!">e-mail</a>
						</li>
						<li class="share-facebook">
							<a rel="nofollow" href="http://www.facebook.com/share.php" onclick="return fbs_click(this)" title="Compartilhe pelo facebook!">facebook</a>
						</li>
						<li class="share-twitter">
							<a rel="nofollow" href="" onclick="window.open('http://twitter.com/home?status=Filmetes da Turma do Cocoricó sobre o Projeto Hydros! http://www.projetohydros.com/hydros2013/materiais/videos/cocorico4','Twitter','toolbar=0,status=0,width=626,height=436'); return false;" title="Compartilhe no Tumblr">twitter</a>
						</li>
						<li class="share-tumblr">
							<a href="http://www.tumblr.com/share/video?embed=%3Ciframe+width%3D%27640%27+height%3D%27360%27+src%3D%27http%3A%2F%2Fwww.youtube.com%2Fembed%2Fev53svsXwHc%3Fwmode%3Dtransparent%27+frameborder%3D%270%27+allowfullscreen%3E%3C%2Fiframe%3E&caption=Filmetes+da+Turma+do+Cocoric%C3%B3+sobre+o+Projeto+Hydros%21+http%3A%2F%2Fwww.projetohydros.com%2Fhydros2013%2Fmateriais%2Fvideos%2Fcocorico4" title="Compartilhe no Tumblr!">tumblr</a>
						</li>
					</ul>
				</div>
			</div>

			<div class="link-video-holder">
				<a href="materiais/videos/cocorico5" title="" class="link-detalhe">
					<div class="titulo-video"><?=traduz('Tudo é água')?></div>
					<div class="play-overlay azul"></div>
					<img src="_imgs/videos/thumb-cocorico-5.png">
				</a>
				<div class="barra-share-small">
					<!-- <a href="" title="<?=traduz('Faça o download!')?>" class="link-download"></a> -->
					<ul>
						<li class="share-email">
							<a href="mailto:?subject=Conheça o Projeto Hydros! - Filmetes da Turma do Cocoricó&body=Em 2012, o Projeto Hydros ganhou importantes aliados, por meio de uma iniciativa da Mexichem Brasil: a série “Pílulas de Cultura” estrelada pela Turma do Cocoricó. Veiculada na TV Cultura, a série foca o tema da água e é um ótimo exemplo de como o Projeto Hydros possui inúmeras possibilidades de interação. Reveja os vídeos." title="Compartilhe por e-mail!">e-mail</a>
						</li>
						<li class="share-facebook">
							<a rel="nofollow" href="http://www.facebook.com/share.php" onclick="return fbs_click(this)" title="Compartilhe pelo facebook!">facebook</a>
						</li>
						<li class="share-twitter">
							<a rel="nofollow" href="" onclick="window.open('http://twitter.com/home?status=Filmetes da Turma do Cocoricó sobre o Projeto Hydros! http://www.projetohydros.com/hydros2013/materiais/videos/cocorico5','Twitter','toolbar=0,status=0,width=626,height=436'); return false;" title="Compartilhe no Tumblr">twitter</a>
						</li>
						<li class="share-tumblr">
							<a href="http://www.tumblr.com/share/video?embed=%3Ciframe+width%3D%27640%27+height%3D%27360%27+src%3D%27http%3A%2F%2Fwww.youtube.com%2Fembed%2FyqziaH2YoKM%3Fwmode%3Dtransparent%27+frameborder%3D%270%27+allowfullscreen%3E%3C%2Fiframe%3E&caption=Filmetes+da+Turma+do+Cocoric%C3%B3+sobre+o+Projeto+Hydros%21+http%3A%2F%2Fwww.projetohydros.com%2Fhydros2013%2Fmateriais%2Fvideos%2Fcocorico5" title="Compartilhe no Tumblr!">tumblr</a>
						</li>
					</ul>
				</div>
			</div>				

		</div>


		<div class="politica">
			<p class="primeiro">
				<strong><?=traduz('Advertência Legal de Utilização de Conteúdo')?></strong>			
				<?=traduz('O conteúdo do hotsite é de utilização livre, desde que observadas as regras a seguir estabelecidas:')?>
				1)&nbsp;<?=traduz('O objetivo do hotsite é exclusivamente informativo. O material exposto não pode ser utilizado para qualquer finalidade e/ou propósito contrário a esse fim, nem poderá ser copiado e/ou reproduzido sem a divulgação clara e expressa da fonte, creditando a autoria do texto, fotografia e/ou vídeo, produção etc., cujo nome se encontra indicado junto a cada material exposto. Exemplo: "www.projetohydros.com / (nome completo do autor). Todos os direitos reservados".')?>
				2)&nbsp;<?=traduz('A alteração de conteúdo e/ou distorções de qualquer natureza, como montagens de imagens, de voz ou outras, é expressamente proibida, em qualquer hipótese, podendo ser punida na forma da lei.')?>
				3)&nbsp;<?=traduz('É vedada, também, a veiculação do conteúdo deste hotsite, mesmo com indicação de autor, com a finalidade de lucro pessoal e/ou corporativo, ou com associação a religião, política, discriminação, pedofilia e/ou outras práticas ilícitas.')?>
				4)&nbsp;<?=traduz('A indicação de fonte efetuada por terceiro que utilizar os textos, imagens ou vídeos deste hotsite deverá conter, necessariamente, o endereço do domínio e o nome completo do autor do texto, fotografia ou vídeo correspondente.')?>
			</p>
		</div>	    	

	</div>
</div>