<div class="centro container apoio">

	<div class="coluna esquerda">
		<h1><?=traduz('Exposição Hydros')?></h1>
		<p class="maior">
			<?=traduz('Você pode utilizar de 8 até 26 imagens, dentre as mais de 200 extraídas da coleção de livros Hydros, para criar sua própria exposição. Pense em um espaço disponível, como escolas, escritórios, estações do metrô, parques, ou mesmo museus. Disponha as fotos de uma forma que toda a mensagem embutida possa ser apreciada pelos visitantes.')?>
		</p>
	</div>

	<div class="coluna direita" style="padding-bottom:45px;">
		<div class="circulos">
			<img id="dica-img" src="_imgs/layout/img-exposicoes.png">
			<div id="dica">
				<h2><?=traduz('DICA')?></h2>
				<p>
					<?=traduz('Para que sua iniciativa tenha um resultado mais positivo e impactante, sugerimos a impressão do material selecionado em papel fotográfico e em alta resolução.')?>
				</p>
			</div>
		</div>
	</div>

</div>

<div class="bordat" id="lista-exposicoes">
	
	<div class="centro">

		<h1><?=traduz('Selecione a exposição que deseja e faça o download!')?></h1>

		<div class="coluna">
			<h2>26 <?=traduz('imagens')?></h2>
			<img src="_imgs/layout/thumb-expo1.jpg">
			<a href="materiais/downloadPDF/<?=$this->session->userdata('language').'/'.$links[$this->session->userdata('language')]['26']['grande']?>" title="<?=traduz('GRANDES')?>">
				<span class="maior"><?=traduz('GRANDES')?></span>
				90 x 60 cm
			</a>
			<a href="materiais/downloadPDF/<?=$this->session->userdata('language').'/'.$links[$this->session->userdata('language')]['26']['media']?>" title="<?=traduz('MÉDIAS')?>">
				<span class="maior"><?=traduz('MÉDIAS')?></span>
				60 x 40 cm
			</a>
			<a href="materiais/downloadPDF/<?=$this->session->userdata('language').'/'.$links[$this->session->userdata('language')]['26']['pequena']?>" title="<?=traduz('PEQUENAS')?>">
				<span class="maior"><?=traduz('PEQUENAS')?></span>
				40 x 30 cm
			</a>
			<a href="materiais/downloadPDF/<?=$this->session->userdata('language').'/'.$links[$this->session->userdata('language')]['26']['gigantografia220x220']?>" title="<?=traduz('GIGANTOGRAFIAS')?>">
				<span class="maior"><?=traduz('GIGANTOGRAFIAS')?></span>
				220 x 220 cm
			</a>
			<a href="materiais/downloadPDF/<?=$this->session->userdata('language').'/'.$links[$this->session->userdata('language')]['26']['gigantografia120x80']?>" title="<?=traduz('GIGANTOGRAFIAS')?>">
				<span class="maior"><?=traduz('GIGANTOGRAFIAS')?></span>
				120 x 80 cm
			</a>		
		</div>

		<div class="coluna">
			<h2>19 <?=traduz('imagens')?></h2>
			<img src="_imgs/layout/thumb-expo2.jpg">
			<a href="materiais/downloadPDF/<?=$this->session->userdata('language').'/'.$links[$this->session->userdata('language')]['19']['grande']?>" title="<?=traduz('GRANDES')?>">
				<span class="maior"><?=traduz('GRANDES')?></span>
				90 x 60 cm
			</a>
			<a href="materiais/downloadPDF/<?=$this->session->userdata('language').'/'.$links[$this->session->userdata('language')]['19']['media']?>" title="<?=traduz('MÉDIAS')?>">
				<span class="maior"><?=traduz('MÉDIAS')?></span>
				60 x 40 cm
			</a>
			<a href="materiais/downloadPDF/<?=$this->session->userdata('language').'/'.$links[$this->session->userdata('language')]['19']['pequena']?>" title="<?=traduz('PEQUENAS')?>">
				<span class="maior"><?=traduz('PEQUENAS')?></span>
				40 x 30 cm
			</a>
			<a href="materiais/downloadPDF/<?=$this->session->userdata('language').'/'.$links[$this->session->userdata('language')]['19']['gigantografia220x220']?>" title="<?=traduz('GIGANTOGRAFIAS')?>">
				<span class="maior"><?=traduz('GIGANTOGRAFIAS')?></span>
				220 x 220 cm
			</a>
			<a href="materiais/downloadPDF/<?=$this->session->userdata('language').'/'.$links[$this->session->userdata('language')]['19']['gigantografia120x80']?>" title="<?=traduz('GIGANTOGRAFIAS')?>">
				<span class="maior"><?=traduz('GIGANTOGRAFIAS')?></span>
				120 x 80 cm
			</a>	
		</div>

		<div class="coluna">
			<h2>14 <?=traduz('imagens')?></h2>
			<img src="_imgs/layout/thumb-expo3.jpg">
			<a href="materiais/downloadPDF/<?=$this->session->userdata('language').'/'.$links[$this->session->userdata('language')]['14']['grande']?>" title="<?=traduz('GRANDES')?>">
				<span class="maior"><?=traduz('GRANDES')?></span>
				90 x 60 cm
			</a>
			<a href="materiais/downloadPDF/<?=$this->session->userdata('language').'/'.$links[$this->session->userdata('language')]['14']['media']?>" title="<?=traduz('MÉDIAS')?>">
				<span class="maior"><?=traduz('MÉDIAS')?></span>
				60 x 40 cm
			</a>
			<a href="materiais/downloadPDF/<?=$this->session->userdata('language').'/'.$links[$this->session->userdata('language')]['14']['pequena']?>" title="<?=traduz('PEQUENAS')?>">
				<span class="maior"><?=traduz('PEQUENAS')?></span>
				40 x 30 cm
			</a>
			<a href="materiais/downloadPDF/<?=$this->session->userdata('language').'/'.$links[$this->session->userdata('language')]['14']['gigantografia220x220']?>" title="<?=traduz('GIGANTOGRAFIAS')?>">
				<span class="maior"><?=traduz('GIGANTOGRAFIAS')?></span>
				220 x 220 cm
			</a>
			<a href="materiais/downloadPDF/<?=$this->session->userdata('language').'/'.$links[$this->session->userdata('language')]['14']['gigantografia120x80']?>" title="<?=traduz('GIGANTOGRAFIAS')?>">
				<span class="maior"><?=traduz('GIGANTOGRAFIAS')?></span>
				120 x 80 cm
			</a>		
		</div>

		<div class="coluna ultima">
			<h2>8 <?=traduz('imagens')?></h2>
			<img src="_imgs/layout/thumb-expo4.jpg">
			<a href="materiais/downloadPDF/<?=$this->session->userdata('language').'/'.$links[$this->session->userdata('language')]['8']['grande']?>" title="<?=traduz('GRANDES')?>">
				<span class="maior"><?=traduz('GRANDES')?></span>
				90 x 60 cm
			</a>
			<a href="materiais/downloadPDF/<?=$this->session->userdata('language').'/'.$links[$this->session->userdata('language')]['8']['media']?>" title="<?=traduz('MÉDIAS')?>">
				<span class="maior"><?=traduz('MÉDIAS')?></span>
				60 x 40 cm
			</a>
			<a href="materiais/downloadPDF/<?=$this->session->userdata('language').'/'.$links[$this->session->userdata('language')]['8']['pequena']?>" title="<?=traduz('PEQUENAS')?>">
				<span class="maior"><?=traduz('PEQUENAS')?></span>
				40 x 30 cm
			</a>
			<a href="materiais/downloadPDF/<?=$this->session->userdata('language').'/'.$links[$this->session->userdata('language')]['8']['gigantografia220x220']?>" title="<?=traduz('GIGANTOGRAFIAS')?>">
				<span class="maior"><?=traduz('GIGANTOGRAFIAS')?></span>
				220 x 220 cm
			</a>
			<a href="materiais/downloadPDF/<?=$this->session->userdata('language').'/'.$links[$this->session->userdata('language')]['8']['gigantografia120x80']?>" title="<?=traduz('GIGANTOGRAFIAS')?>">
				<span class="maior"><?=traduz('GIGANTOGRAFIAS')?></span>
				120 x 80 cm
			</a>		
		</div>


		<div style="padding-top:22px; height:96px;">
			<div id="compartilhar-exposicoes" style="float:right; line-height:110%">
				<?=traduz('Compartilhe!')?><br>
				<ul>
					<li class="share-email primeiro">
						<a href="<?=$share['email']?>" title="<?=traduz('Compartilhe por e-mail!')?>">e-mail</a>
					</li>
					<li class="share-facebook">
						<a rel="nofollow" href="http://www.facebook.com/share.php" onclick="return fbs_click(this)" title="<?=traduz('Compartilhe pelo facebook!')?>">facebook</a>
					</li>
					<li class="share-twitter">
						<a rel="nofollow" href="" onclick="<?=$share['tweet']?>" title="<?=traduz('')?>">twitter</a>
					</li>
					<li class="share-tumblr">
						<a href="http://www.tumblr.com/share/link?url=<?php echo urlencode(current_url())?>&description=<?= urlencode($share['description_tumblr']) ?>&name=<?php echo urlencode($share['tumblr']) ?>" title="<?=traduz('Compartilhe no Tumblr!')?>">tumblr</a>
					</li>
				</ul>
			</div>
		</div>


		<div class="politica">
			<p class="primeiro">
				<strong><?=traduz('Advertência Legal de Utilização de Conteúdo')?></strong>			
				<?=traduz('O conteúdo do hotsite é de utilização livre, desde que observadas as regras a seguir estabelecidas:')?>
				1)&nbsp;<?=traduz('O objetivo do hotsite é exclusivamente informativo. O material exposto não pode ser utilizado para qualquer finalidade e/ou propósito contrário a esse fim, nem poderá ser copiado e/ou reproduzido sem a divulgação clara e expressa da fonte, creditando a autoria do texto, fotografia e/ou vídeo, produção etc., cujo nome se encontra indicado junto a cada material exposto. Exemplo: "www.projetohydros.com / (nome completo do autor). Todos os direitos reservados".')?>
				2)&nbsp;<?=traduz('A alteração de conteúdo e/ou distorções de qualquer natureza, como montagens de imagens, de voz ou outras, é expressamente proibida, em qualquer hipótese, podendo ser punida na forma da lei.')?>
				3)&nbsp;<?=traduz('É vedada, também, a veiculação do conteúdo deste hotsite, mesmo com indicação de autor, com a finalidade de lucro pessoal e/ou corporativo, ou com associação a religião, política, discriminação, pedofilia e/ou outras práticas ilícitas.')?>
				4)&nbsp;<?=traduz('A indicação de fonte efetuada por terceiro que utilizar os textos, imagens ou vídeos deste hotsite deverá conter, necessariamente, o endereço do domínio e o nome completo do autor do texto, fotografia ou vídeo correspondente.')?>
			</p>
		</div>		

	</div>

</div>