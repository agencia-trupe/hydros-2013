<div class="centro container fundoimg">

	<div class="margemextra">

		<h1><?=traduz('Cartilha Projeto Hydros na Escola')?></h1>

		<p class="maior">
			<?=traduz('Pensando em sensibilizar e ensinar crianças nas escolas sobre a escassez da água, o Projeto Hydros criou a cartilha “Projeto Hydros na Escola”. Juntamente com as exposições e materiais de apoio (que podem ser utilizados em sala de aula), a cartilha foi criada para que suas atividades possam ser incluídas no planejamento escolar. Existem duas versões: uma mais completa e outra resumida, com ações mais pontuais.')?>
		</p>

		<div class="inline" style="margin:0 40px 40px 0;">
			<img src="_imgs/layout/<?=prefixo("cartilha_maior")?>.jpg" id="ampliada">
			<div class="barra-share-large">
				<a href="materiais/downloadPDF/<?=$this->session->userdata('language')?>/<?=$link_maior?>/1" title="<?=traduz('Faça o download!')?>" class="link-download"><?=traduz('Faça o download!')?></a>
				<div class="compartilhe"><?=traduz('Compartilhe!')?></div>
				<ul>
					<li class="share-email"><a href="" title="<?=traduz('')?>">e-mail</a></li>
					<li class="share-facebook"><a href="" title="<?=traduz('')?>">facebook</a></li>
					<li class="share-twitter"><a href="" title="<?=traduz('')?>">twitter</a></li>
					<li class="share-tumblr"><a href="" title="<?=traduz('')?>">tumblr</a></li>
				</ul>
			</div>
		</div>

		<div class="inline">
			<img src="_imgs/layout/<?=prefixo("cartilha_menor")?>.jpg" id="ampliada">
			<div class="barra-share-large">
				<a href="materiais/downloadPDF/<?=$this->session->userdata('language')?>/<?=$link_menor?>/1" title="<?=traduz('Faça o download!')?>" class="link-download"><?=traduz('Faça o download!')?></a>
				<div class="compartilhe"><?=traduz('Compartilhe!')?></div>
				<ul>
					<li class="share-email"><a href="" title="<?=traduz('')?>">e-mail</a></li>
					<li class="share-facebook"><a href="" title="<?=traduz('')?>">facebook</a></li>
					<li class="share-twitter"><a href="" title="<?=traduz('')?>">twitter</a></li>
					<li class="share-tumblr"><a href="" title="<?=traduz('')?>">tumblr</a></li>
				</ul>
			</div>
		</div>

	</div>

	<div class="politica">
		<p class="primeiro">
			<strong><?=traduz('Advertência Legal de Utilização de Conteúdo')?></strong>			
			<?=traduz('O conteúdo do hotsite é de utilização livre, desde que observadas as regras a seguir estabelecidas:')?>
			1)&nbsp;<?=traduz('O objetivo do hotsite é exclusivamente informativo. O material exposto não pode ser utilizado para qualquer finalidade e/ou propósito contrário a esse fim, nem poderá ser copiado e/ou reproduzido sem a divulgação clara e expressa da fonte, creditando a autoria do texto, fotografia e/ou vídeo, produção etc., cujo nome se encontra indicado junto a cada material exposto. Exemplo: "www.projetohydros.com / (nome completo do autor). Todos os direitos reservados".')?>
			2)&nbsp;<?=traduz('A alteração de conteúdo e/ou distorções de qualquer natureza, como montagens de imagens, de voz ou outras, é expressamente proibida, em qualquer hipótese, podendo ser punida na forma da lei.')?>
			3)&nbsp;<?=traduz('É vedada, também, a veiculação do conteúdo deste hotsite, mesmo com indicação de autor, com a finalidade de lucro pessoal e/ou corporativo, ou com associação a religião, política, discriminação, pedofilia e/ou outras práticas ilícitas.')?>
			4)&nbsp;<?=traduz('A indicação de fonte efetuada por terceiro que utilizar os textos, imagens ou vídeos deste hotsite deverá conter, necessariamente, o endereço do domínio e o nome completo do autor do texto, fotografia ou vídeo correspondente.')?>
		</p>
	</div>	

</div>