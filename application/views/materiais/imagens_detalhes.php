<div class="centro container detalheimg">

	<h1><?=traduz('Galeria de Imagens')?></h1>

	<img src="_imgs/galeria/<?=$imagem->imagem?>" id="ampliada">
	<div class="barra-share-large">
		<a href="arquivo/<?=str_replace('.', '_', $imagem->arquivo_alta)?>/arquivos/<?=$imagem->nome_download?>" target="_blank" title="<?=traduz('Faça o download!')?>" class="link-download"><?=traduz('Faça o download!')?></a>
		<div class="compartilhe"><?=traduz('Compartilhe!')?></div>
		<ul>
			<li class="share-email">
				<a href="<?=$share['email']?>" title="<?=traduz('Compartilhe por e-mail!')?>">e-mail</a>
			</li>
			<li class="share-facebook">
				<a rel="nofollow" href="http://www.facebook.com/share.php" onclick="return fbs_click(this)" title="<?=traduz('Compartilhe pelo facebook!')?>">facebook</a>
			</li>
			<li class="share-twitter">
				<a rel="nofollow" href="" onclick="<?=$share['tweet']?>" title="<?=traduz('Compartilhe no Tumblr')?>">twitter</a>
			</li>
			<li class="share-tumblr">
				<a href="http://www.tumblr.com/share/photo?source=<?php echo urlencode(current_url()) ?>&caption=<?php echo urlencode($share['tweet']) ?>" title="<?=traduz('Compartilhe no Tumblr!')?>">tumblr</a>
			</li>
		</ul>
	</div>		

	<div id="fotografo">
		<?=traduz('Fotógrafo')?>: <?=$imagem->fotografo?>
	</div>

</div>

<div class="bordat">
	<div class="centro" id="navegar-thumbs">

        <div class="navegacao-thumbs">
            <div id="nav-esq" class="buttons prev"></div>
            <div class="viewport">
                <ul class="overview">
                    <?foreach($lista_imagens as $li_img):?>
                        <li><a href='<?=base_url('materiais/imagem/'.$li_img->id)?>'><img class='thumb-nav ativo' id='<?=$li_img->id?>' src='<?=base_url('_imgs/galeria/thumbs/'.$li_img->imagem)?>'></a></li>
                    <?endforeach;?>
                </ul>
            </div>
            <div id="nav-dir" class="buttons next"></div>
        </div>

        <a href="materiais/galeria" class="voltar-grande">
        	&laquo; <?=traduz('VOLTAR PARA GALERIA COMPLETA')?>
        </a>

		<div class="politica" style="margin-top:57px;">
			<p class="primeiro">
				<strong><?=traduz('Advertência Legal de Utilização de Conteúdo')?></strong>			
				<?=traduz('O conteúdo do hotsite é de utilização livre, desde que observadas as regras a seguir estabelecidas:')?>
				1)&nbsp;<?=traduz('O objetivo do hotsite é exclusivamente informativo. O material exposto não pode ser utilizado para qualquer finalidade e/ou propósito contrário a esse fim, nem poderá ser copiado e/ou reproduzido sem a divulgação clara e expressa da fonte, creditando a autoria do texto, fotografia e/ou vídeo, produção etc., cujo nome se encontra indicado junto a cada material exposto. Exemplo: "www.projetohydros.com / (nome completo do autor). Todos os direitos reservados".')?>
				2)&nbsp;<?=traduz('A alteração de conteúdo e/ou distorções de qualquer natureza, como montagens de imagens, de voz ou outras, é expressamente proibida, em qualquer hipótese, podendo ser punida na forma da lei.')?>
				3)&nbsp;<?=traduz('É vedada, também, a veiculação do conteúdo deste hotsite, mesmo com indicação de autor, com a finalidade de lucro pessoal e/ou corporativo, ou com associação a religião, política, discriminação, pedofilia e/ou outras práticas ilícitas.')?>
				4)&nbsp;<?=traduz('A indicação de fonte efetuada por terceiro que utilizar os textos, imagens ou vídeos deste hotsite deverá conter, necessariamente, o endereço do domínio e o nome completo do autor do texto, fotografia ou vídeo correspondente.')?>
			</p>
		</div>	      

	</div>
</div>

<script type="text/javascript">
$('document').ready( function(){
    $('.navegacao-thumbs').tinycarousel({
        controls : true,
        display: 1,
        start : '<?=$inicio?>',
        axis : 'x'
    });      
});
</script>