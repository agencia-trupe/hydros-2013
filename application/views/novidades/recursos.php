<div class="centro">

	<h1><?=traduz('Mapa de recursos hídricos no mundo')?></h1>

	<p class="maior">
		<?=traduz('Saiba mais sobre os recursos hídricos disponíveis (água de superficie e subterrânea) e renováveis e como eles são usados em cada região (agricultura, doméstico e industrial), além da presença do Projeto Hydros em diversos países.')?>
	</p>

	<img src="_imgs/layout/aspas-gigantes.png" id="aspas">

	<div id="flashContent">
		<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="980" height="674" id="Hydros" align="middle">
			<param name="movie" value="hydrosLoader.swf?language=<?php echo $flashlang;?>" />
			<param name="quality" value="high" />
			<param name="bgcolor" value="#ffffff" />
			<param name="play" value="true" />
			<param name="loop" value="true" />
			<param name="wmode" value="window" />
			<param name="scale" value="showall" />
			<param name="menu" value="true" />
			<param name="devicefont" value="false" />
			<param name="salign" value="" />
			<param name="allowScriptAccess" value="sameDomain" />
			<embed src="hydrosLoader.swf?language=<?php echo $flashlang;?>" quality="high" bgcolor="#ffffff" width="980" height="674" name="widget" align="middle" allowScriptAccess="sameDomain" allowFullScreen="false" type="application/x-shockwave-flash" pluginspage="http://www.adobe.com/go/getflashplayer" />
			<!--[if !IE]>
			<object type="application/x-shockwave-flash" data="hydrosLoader.swf?language=<?php echo $flashlang;?>" width="980" height="674">
				<param name="movie" value="hydrosLoader.swf?language=<?php echo $flashlang;?>" />
				<param name="quality" value="high" />
				<param name="bgcolor" value="#ffffff" />
				<param name="play" value="true" />
				<param name="loop" value="true" />
				<param name="wmode" value="window" />
				<param name="scale" value="showall" />
				<param name="menu" value="true" />
				<param name="devicefont" value="false" />
				<param name="salign" value="" />
				<param name="allowScriptAccess" value="sameDomain" />
				<embed src="hydrosLoader.swf?language=<?php echo $flashlang;?>" quality="high" bgcolor="#ffffff" width="980" height="674" name="widget" align="middle" allowScriptAccess="sameDomain" allowFullScreen="false" type="application/x-shockwave-flash" pluginspage="http://www.adobe.com/go/getflashplayer" />
			<!--<![endif]
				<a href="http://www.adobe.com/go/getflash">
					<img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" />
				</a>
			<!--[if !IE]>
			</object>
			<!--<![endif]-->
		</object>
	</div>
	<div class="barra-share-large" class="fullwidth">
		<div class="compartilhe"><?=traduz('Compartilhe!')?></div>
			<ul>
				<li class="share-email">
					<a href="<?=$share['email']?>" title="<?=traduz('Compartilhe por e-mail!')?>">e-mail</a>
				</li>
				<li class="share-facebook">
					<a rel="nofollow" href="http://www.facebook.com/share.php" onclick="return fbs_click(this)" title="<?=traduz('Compartilhe pelo facebook!')?>">facebook</a>
				</li>
				<li class="share-twitter">
					<a rel="nofollow" href="" onclick="<?=$share['tweet']?>" title="<?=traduz('Compartilhe no Tumblr')?>">twitter</a>
				</li>
				<li class="share-tumblr">
					<a href="http://www.tumblr.com/share/link?url=<?php echo urlencode(current_url())?>&description=<?= urlencode($share['description_tumblr']) ?>&name=<?php echo urlencode($share['tumblr']) ?>" title="<?=traduz('Compartilhe no Tumblr!')?>">tumblr</a>
				</li>
			</ul>
	</div>

	<div style="margin:30px 0;">
		<input type="hidden" id="id_noticia" value="1">
		<input type="hidden" id="val_area" value="mapa">
		<?=$this->load->view('comentarios/listar', $comentarios)?>
	</div>	

</div>