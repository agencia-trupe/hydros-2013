<div class="centro">

	<h1><?=traduz('Giro pelo Projeto Hydros')?></h1>

	<div class="caixa-voltar">
		<a href="novidades/giro" title="<?=traduz('voltar para a home do Giro')?>">
			&laquo; <?=traduz('voltar para a home do Giro')?>
		</a>
	</div>

	<div class="container">

		<div class="coluna detalhe">

			<div class="light"><?=$detalhe->data.' | '.$detalhe->autor?></div>

			<?php if ($detalhe->imagem): ?>
				<div class="titulo">
					<img src="_imgs/giro/<?=$detalhe->imagem?>">
					<h2><?=$detalhe->titulo?></h2>
				</div>
			<?else:?>
				<h2 class="titulosemimagem"><?=$detalhe->titulo?></h2>
			<?endif;?>

			
			<?php if ($detalhe->olho): ?>
				<div class="olho">
					<?=$detalhe->olho?>
				</div>	
			<?php endif ?>

			<div class="texto">
				<?=$detalhe->texto?>
			</div>

			<div class="borda">
				<div class="caixa-voltar semborda">
					<a href="novidades/giro" title="<?=traduz('voltar para a home do Giro')?>">
						&laquo; <?=traduz('voltar para a home do Giro')?>
					</a>
				</div>
				<div id="compartilhar-noticias" class="detalhe">
					<?=traduz('Compartilhe!')?>
					<ul>
						<li class="share-email">
							<a href="<?=$share['email']?>" title="<?=traduz('Compartilhe por e-mail!')?>">e-mail</a>
						</li>
						<li class="share-facebook">
							<a rel="nofollow" href="http://www.facebook.com/share.php" onclick="return fbs_click(this)" title="<?=traduz('Compartilhe pelo facebook!')?>">facebook</a>
						</li>
						<li class="share-twitter">
							<a rel="nofollow" href="" onclick="<?=$share['tweet']?>" title="<?=traduz('Compartilhe no Tumblr')?>">twitter</a>
						</li>
						<li class="share-tumblr">
							<a href="http://www.tumblr.com/share/link?url=<?php echo urlencode(current_url())?>&description=<?= urlencode($share['description_tumblr']) ?>&name=<?php echo urlencode($share['tumblr']) ?>" title="<?=traduz('Compartilhe no Tumblr!')?>">tumblr</a>
						</li>
					</ul>
				</div>
			</div>

		</div>

		<div class="coluna lateral">
			<input type="hidden" id="id_noticia" value="<?=$detalhe->id?>">
			<input type="hidden" id="val_area" value="giro">
			<?=$this->load->view('comentarios/listar', $comentarios)?>
		</div>

	</div>

</div>