<div class="centro container">

	<div class="coluna lista-noticias">

		<h1><?=traduz('Giro pelo Projeto Hydros')?></h1>
		<h2><?=traduz('Saiba quais ações cada empresa do Grupo Empresarial Kaluz está desenvolvendo para implantar o Projeto Hydros entre seus principais públicos de interesse, como colaboradores, comunidade, escolas etc.')?></h2>

		<?php if ($noticias): ?>
			
			<ul class="list-not">
				<?php foreach ($noticias as $key => $value): ?>
					
						<li>
							<a href="novidades/ler_giro/<?=$value->slug?>" title="<?=$value->titulo?>">
								<div class="data"><?=$value->data?></div>
								<?php if ($value->imagem): ?>
									<img src="_imgs/giro/thumbs/<?=$value->imagem?>">	
								<?php endif ?>
								<div class="texto">
									<h3><?=$value->titulo?></h3>
									<?=$value->olho?>
								</div>
							</a>
						</li>				
				<?php endforeach ?>			
			</ul>

			<?php if ($paginacao): ?>
				<div id="paginacao">
					<?=$paginacao?>
				</div>
			<?php endif ?>

		<?php else: ?>

			<h2 style="margin-top:60px;">Nenhum Item Cadastrado</h2>
			
		<?php endif ?>

	</div>

	<div class="coluna lateral">
		<img src="_imgs/layout/img-noticias.png" style="margin-bottom:45px;">

		<div id="compartilhar-noticias">
			<?=traduz('Compartilhe!')?>
			<ul>
				<li class="share-email">
					<a href="<?=$share['email']?>" title="<?=traduz('Compartilhe por e-mail!')?>">e-mail</a>
				</li>
				<li class="share-facebook">
					<a rel="nofollow" href="http://www.facebook.com/share.php" onclick="return fbs_click(this)" title="<?=traduz('Compartilhe pelo facebook!')?>">facebook</a>
				</li>
				<li class="share-twitter">
					<a rel="nofollow" href="" onclick="<?=$share['tweet']?>" title="<?=traduz('Compartilhe no Tumblr')?>">twitter</a>
				</li>
				<li class="share-tumblr">
					<a href="http://www.tumblr.com/share/link?url=<?php echo urlencode(current_url())?>&description=<?= urlencode($share['description_tumblr']) ?>&name=<?php echo urlencode($share['tumblr']) ?>" title="<?=traduz('Compartilhe no Tumblr!')?>">tumblr</a>
				</li>
			</ul>
		</div>
	</div>

</div>