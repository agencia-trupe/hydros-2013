<h2><?=traduz('ENVIAR COMENTÁRIO')?></h2>

<p>
	<?=traduz('Olá')?> <?=$this->session->userdata('nome_assinatura')?>!<br>
	<span class="menor"><?=traduz('Deixe seu comentário abaixo')?>:</span>
</p>

<form method="post" id="form-comentario" action="comentarios/postar">

	<textarea name="texto-comentario" id="input-comentario" required></textarea>

	<input type="hidden" name="redirect" id="input-redirect">

	<input type="hidden" name="coment_id" id="coment_id_noticia">

	<input type="hidden" name="area_comentario" id="coment_area">

	<input type="submit" value="<?=traduz('ENVIAR')?>">

	<a href="#" id="botao-cancelar-post" title="<?=traduz('CANCELAR | VOLTAR')?>"><?=traduz('CANCELAR | VOLTAR')?></a>

</form>