<h2><?=traduz('NOVO CADASTRO')?></h2>

<form id="login-cadastro-comentarios" action="comentarios/cadastrar" method="post" enctype="multipart/form-data">

	<label>
		<?=traduz('nome completo')?>
		<input type="text" name="nome_completo" required id="cad-com-nome">
	</label>

	<label>
		<?=traduz('nome para assinar os comentários')?>
		<input type="text" name="nome_assinatura" required id="cad-com-nomeas">
	</label>

	<label>
		<?=traduz('e-mail')?>*
		<input type="email" name="email" required id="cad-com-email">
	</label>

	<label>
		<?=traduz('cidade')?>
		<input type="text" name="cidade" required id="cad-com-cidade">
	</label>

	<label>
		<?=traduz('país')?>
		<input type="text" name="pais" required id="cad-com-pais">
	</label>

	<label>
		<?=traduz('criar senha')?>
		<input type="password" name="senha" required id="cad-com-senha">
	</label>

	<label>
		<?=traduz('confirmar senha')?>
		<input type="password" name="confirmar_senha" required id="cad-com-confsenha">
	</label>

	<label>
		<?=traduz('se desejar cadastre uma foto para o seu<br> perfil dentro do nosso site!<br>(Tamanho máximo de arquivo: 2Mb)')?>
		<div id="fake-input">
			<input type="file" name="userfile">
			<div class="hover"><?=traduz('SELECIONAR ARQUIVO')?></div>
		</div>
	</label>

	<div class="checkboxes">
		<?=traduz('Quer receber nossa newsletter com todas as novidades e informações sobre o Projeto Hydros?')?>
		<label><input type="radio" name="cadastra_newsletter" value="1" checked><?=traduz('SIM')?></label>
		<label><input type="radio" name="cadastra_newsletter" value="0"><?=traduz('NÃO')?></label>
	</div>

	<input type="hidden" name="redirect" id="input-redirect">

	<input type="submit" value="<?=traduz('CADASTRAR')?>">

	<p class="small">
		<?=traduz('(*) o endereço de e-mail será usado como login no sistema.<br> O Projeto Hydros não divulga nem fornece seus dados pessoais a terceiros. Leia nossa <a href="politica">política de privacidade.</a>')?>
	</p>

</form>