<h2><?=traduz('CONECTE-SE PARA COMENTAR')?></h2>

<p><?=traduz('Se você já é cadastrado, faça o login')?>:</p>

<form id="login-comentarios" action="comentarios/logar" method="post">

	<input type="email" name="login-comentarios" placeholder="<?=traduz('login (e-mail)')?>" required id="login-email">

	<input type="password" name="senha-comentarios" placeholder="<?=traduz('senha')?>" required id="login-senha">

	<input type="hidden" name="redirect" id="input-redirect">

	<input type="submit" value="<?=traduz('ENTRAR')?>">

	<a class="esqueci-senha" id="esqueci-senha" href="#">&raquo; <?=traduz('esqueci a senha')?> &laquo;</a>

</form>

<p>
	<?=traduz('Se você ainda não é cadastrado,<br> cadastre-se agora!')?>
</p>

<a href="#" title="<?=traduz('CADASTRAR-ME!')?>" id="btn-cadastre" class="botao-roxo fonte-verde"><?=traduz('CADASTRAR-ME!')?></a>