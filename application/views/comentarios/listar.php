<div id="comentarios" <?if($area=='mapa')echo"class='large'"?>>

	<div id="inner">

		<a href="" title="<?=traduz('COMENTE!')?>" id="btn-comente" class="botao-comente botao-roxo"><?=traduz('COMENTE!')?></a>

		<p>
			<?php if ($num_comentarios == 0): ?>
				<?=traduz('Ninguém comentou. Seja o primeiro!')?>
			<?php elseif($num_comentarios == 1): ?>
				<?=traduz('1 pessoa comentou')?>:
			<?php else: ?>
				<?=$num_comentarios?>&nbsp;<?=traduz('pessoas comentaram')?>:
			<?php endif ?>
		</p>
		

		<?php if ($lista): ?>

			<div id="comment-list-wrapper">
				<ul class="lista-comentarios">

					<?php foreach ($lista as $key => $value): ?>					
						<li>

							<p>
								<?php if ($value->autor && $value->autor->imagem): ?>
									<img src="_imgs/cadastros/<?=$value->autor->imagem?>">
								<?php endif ?>
								<?=$value->comentario?>
							</p>

							<p class="assinatura">
								<?php if ($value->autor): ?>
									<?=$value->autor->nome?> &bull; <?=$value->autor->cidade?>, <?=$value->autor->pais?>
								<?php endif ?>
								<strong><?=formataTimestamp($value->data)?></strong>
							</p>

						</li>

						<?php if ($key%4 == 0 && $key > 0): ?>
							</ul><ul class="lista-comentarios">
						<?php endif ?>

					<?php endforeach ?>

				</ul>
			</div>

			<a href="#" id="btn-mais-comentarios" title="<?=traduz('ver mais comentários')?>"><?=traduz('ver mais comentários')?></a>

		<?php endif ?>

	</div>
	
</div>