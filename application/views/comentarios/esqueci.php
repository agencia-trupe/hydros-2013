<h2><?=traduz('CONECTE-SE PARA COMENTAR')?></h2>

<p><?=traduz('Informe seu e-mail para receber uma nova senha')?>:</p>

<form id="login-recuperacao" action="comentarios/reenviar" method="post">

	<input type="email" name="login-comentarios" placeholder="<?=traduz('login (e-mail)')?>" required id="login-recuperar">

	<input type="hidden" name="redirect" id="input-redirect">

	<input type="submit" value="<?=traduz('REENVIAR')?>">

</form>