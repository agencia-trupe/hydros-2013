<div class="container top">

	<ul class="breadcrumb">
    	<li>
      		<a href="painel/noticias/index/<?=$lang?>">Notícias <?=$titulo_bread?></a> <span class="divider">/</span>
    	</li>
    	<li class="active">
      		<a href="painel/noticias/form/<?=$lang?>"><?=$titulo?></a>
    	</li>
  </ul>

  <?if(isset($mostrarsucesso) && $mostrarsucesso):?>
    <div class="alert alert-block alert-success fade in" data-dismiss="alert"><?=$mostrarsucesso?></div>
  <?elseif(isset($mostrarerro) && $mostrarerro):?>
    <div class="alert alert-block alert-error fade in" data-dismiss="alert"><?=$mostrarerro?></div>
  <?endif;?>

  <div class="page-header users-header">
    <h2>
      <?=$titulo?>
    </h2>
  </div>  

<?if ($registro): ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/alterar/'.$lang.'/'.$registro->id)?>" enctype="multipart/form-data">

		<div id="dialog"></div>
		
		<label>Título da Notícia
		<input type="text" name="titulo" required autofocus value="<?=$registro->titulo?>"></label>

		<label>Data
		<input type="text" name="data" class="datepicker" required value="<?=formataData($registro->data, 'mysql2br')?>"></label>

		Imagem
		<?php if ($registro->imagem): ?>
			<br><img src="_imgs/noticias/<?=$registro->imagem?>"><br>
			<label><input type="checkbox" name="remover_imagem" value="1"> Remover Imagem</label>
		<?php endif ?>
		<label><input type="file" name="userfile"></label>

		<label>Olho
		<textarea name="olho" class="pequeno basico"><?=$registro->olho?></textarea></label>

		<label>Texto
		<textarea name="texto" class="medio basico"><?=$registro->texto?></textarea></label>

		<input type="hidden" name="lang" value="<?=$lang?>">

		<div class="form-actions">
        	<button class="btn btn-primary" type="submit">Salvar</button>
        	<button class="btn btn-voltar" type="reset">Voltar</button>
      	</div>
	</form>	
	
<?else: ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/inserir/'.$lang)?>" enctype="multipart/form-data">

		<div id="dialog"></div>
		
		<label>Título da Notícia
		<input type="text" name="titulo" required autofocus></label>

		<label>Data
		<input type="text" name="data" class="datepicker" required></label>

		<label>Imagem
		<input type="file" name="userfile"></label>		

		<label>Olho
		<textarea name="olho" class="pequeno basico"></textarea></label>

		<label>Texto
		<textarea name="texto" class="medio basico"></textarea></label>

		<input type="hidden" name="lang" value="<?=$lang?>">

		<div class="form-actions">
        	<button class="btn btn-primary" type="submit">Inserir</button>
        	<button class="btn btn-voltar" type="reset">Voltar</button>
      	</div>
	</form>

<?endif ?>