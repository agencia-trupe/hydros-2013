<div class="container top">

  <?if(isset($mostrarsucesso) && $mostrarsucesso):?>
    <div class="alert alert-block alert-success fade in" data-dismiss="alert"><?=$mostrarsucesso?></div>
  <?elseif(isset($mostrarerro) && $mostrarerro):?>
    <div class="alert alert-block alert-error fade in" data-dismiss="alert"><?=$mostrarerro?></div>
  <?endif;?>

  <div class="page-header users-header">
    <h2>
      <?=$titulo?>
    </h2>
  </div>  

  <div class="row">
    <div class="span12 columns">

      <?php if ($registros): ?>

        <table class="table table-striped table-bordered table-condensed">

          <thead>
            <tr>
              <th class="yellow header headerSortDown"><?=$campo_1?></th>
              <th class="header"><?=$campo_2?></th>
              <th class="red header">Ações</th>
            </tr>
          </thead>

          <tbody>
            <?php foreach ($registros as $key => $value): ?>
              
                <tr class="tr-row" id="row_<?=$value->id?>">
                  <?php if ($lang == '1'): ?>
                    <td class="nowrap"><?=$value->nome?></td>                    
                  <?php else: ?>
                    <td class="nowrap"><?=$value->empresa?></td>                    
                  <?php endif ?>                  
                  <td><?=$value->pais?></td>
                  <td class="crud-actions">
                    <a href="painel/<?=$this->router->class?>/form/<?=$lang?>/<?=$value->id?>" class="btn btn-primary">ver</a>
                    <a href="painel/<?=$this->router->class?>/excluir/<?=$lang?>/<?=$value->id?>" class="btn btn-danger btn-delete">excluir</a>
                  </td>
                </tr>

            <?php endforeach ?>
          </tbody>

        </table>

        <?php if ($paginacao): ?>          
          <div class="pagination">
            <ul>        
              <?=$paginacao?>
            </ul>
          </div>
        <?php endif ?>

      <?php else:?>

        <h2>Nenhuma Adesão Cadastrada</h2>

      <?php endif ?>

    </div>
  </div>