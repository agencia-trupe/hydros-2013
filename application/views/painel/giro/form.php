<div class="container top">

	<ul class="breadcrumb">
    	<li>
      		<a href="painel/giro/index/<?=$lang?>">Giro <?=$titulo_bread?></a> <span class="divider">/</span>
    	</li>
    	<li class="active">
      		<a href="painel/giro/form/<?=$lang?>"><?=$titulo?></a>
    	</li>
  </ul>

  <?if(isset($mostrarsucesso) && $mostrarsucesso):?>
    <div class="alert alert-block alert-success fade in" data-dismiss="alert"><?=$mostrarsucesso?></div>
  <?elseif(isset($mostrarerro) && $mostrarerro):?>
    <div class="alert alert-block alert-error fade in" data-dismiss="alert"><?=$mostrarerro?></div>
  <?endif;?>

  <div class="page-header users-header">
    <h2>
      <?=$titulo?>
    </h2>
  </div>  

<?if ($registro): ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/alterar/'.$lang.'/'.$registro->id)?>" enctype="multipart/form-data">

		<div id="dialog"></div>
		
		<label>Título
		<input type="text" name="titulo" required autofocus value="<?=$registro->titulo?>"></label>

		<label>País
			<select name="pais" required>
				<option value="" <?php if($registro->pais=='')echo" selected"?>>selecione um país</option>
				<optgroup label="Mais Comuns">
					<option value="Brasil" <?php if($registro->pais=='Brasil')echo" selected"?>>Brasil</option>
					<option value="EUA" <?php if($registro->pais=='EUA')echo" selected"?>>EUA</option>
					<option value="México" <?php if($registro->pais=='México')echo" selected"?>>México</option>
				</optgroup>
				<optgroup label="Demais">
					<option value="África do Sul" <?php if($registro->pais=='África do Sul')echo" selected"?>>África do Sul</option>
					<option value="Albânia" <?php if($registro->pais=='Albânia')echo" selected"?>>Albânia</option>
					<option value="Alemanha" <?php if($registro->pais=='Alemanha')echo" selected"?>>Alemanha</option>
					<option value="Andorra" <?php if($registro->pais=='Andorra')echo" selected"?>>Andorra</option>
					<option value="Angola" <?php if($registro->pais=='Angola')echo" selected"?>>Angola</option>
					<option value="Anguilla" <?php if($registro->pais=='Anguilla')echo" selected"?>>Anguilla</option>
					<option value="Antigua" <?php if($registro->pais=='Antigua')echo" selected"?>>Antigua</option>
					<option value="Arábia Saudita" <?php if($registro->pais=='Arábia Saudita')echo" selected"?>>Arábia Saudita</option>
					<option value="Argentina" <?php if($registro->pais=='Argentina')echo" selected"?>>Argentina</option>
					<option value="Armênia" <?php if($registro->pais=='Armênia')echo" selected"?>>Armênia</option>
					<option value="Aruba" <?php if($registro->pais=='Aruba')echo" selected"?>>Aruba</option>
					<option value="Austrália" <?php if($registro->pais=='Austrália')echo" selected"?>>Austrália</option>
					<option value="Áustria" <?php if($registro->pais=='Áustria')echo" selected"?>>Áustria</option>
					<option value="Azerbaijão" <?php if($registro->pais=='Azerbaijão')echo" selected"?>>Azerbaijão</option>
					<option value="Bahamas" <?php if($registro->pais=='Bahamas')echo" selected"?>>Bahamas</option>
					<option value="Bahrein" <?php if($registro->pais=='Bahrein')echo" selected"?>>Bahrein</option>
					<option value="Bangladesh" <?php if($registro->pais=='Bangladesh')echo" selected"?>>Bangladesh</option>
					<option value="Barbados" <?php if($registro->pais=='Barbados')echo" selected"?>>Barbados</option>
					<option value="Bélgica" <?php if($registro->pais=='Bélgica')echo" selected"?>>Bélgica</option>
					<option value="Benin" <?php if($registro->pais=='Benin')echo" selected"?>>Benin</option>
					<option value="Bermudas" <?php if($registro->pais=='Bermudas')echo" selected"?>>Bermudas</option>
					<option value="Botsuana" <?php if($registro->pais=='Botsuana')echo" selected"?>>Botsuana</option>				
					<option value="Brunei" <?php if($registro->pais=='Brunei')echo" selected"?>>Brunei</option>
					<option value="Bulgária" <?php if($registro->pais=='Bulgária')echo" selected"?>>Bulgária</option>
					<option value="Burkina Fasso" <?php if($registro->pais=='Burkina Fasso')echo" selected"?>>Burkina Fasso</option>
					<option value="botão" <?php if($registro->pais=='botão')echo" selected"?>>botão</option>
					<option value="Cabo Verde" <?php if($registro->pais=='Cabo Verde')echo" selected"?>>Cabo Verde</option>
					<option value="Camarões" <?php if($registro->pais=='Camarões')echo" selected"?>>Camarões</option>
					<option value="Camboja" <?php if($registro->pais=='Camboja')echo" selected"?>>Camboja</option>
					<option value="Canadá" <?php if($registro->pais=='Canadá')echo" selected"?>>Canadá</option>
					<option value="Cazaquistão" <?php if($registro->pais=='Cazaquistão')echo" selected"?>>Cazaquistão</option>
					<option value="Chade" <?php if($registro->pais=='Chade')echo" selected"?>>Chade</option>
					<option value="Chile" <?php if($registro->pais=='Chile')echo" selected"?>>Chile</option>
					<option value="China" <?php if($registro->pais=='China')echo" selected"?>>China</option>
					<option value="Cidade do Vaticano" <?php if($registro->pais=='Cidade do Vaticano')echo" selected"?>>Cidade do Vaticano</option>
					<option value="Colômbia" <?php if($registro->pais=='Colômbia')echo" selected"?>>Colômbia</option>
					<option value="Congo" <?php if($registro->pais=='Congo')echo" selected"?>>Congo</option>
					<option value="Coréia do Sul" <?php if($registro->pais=='Coréia do Sul')echo" selected"?>>Coréia do Sul</option>
					<option value="Costa do Marfim" <?php if($registro->pais=='Costa do Marfim')echo" selected"?>>Costa do Marfim</option>
					<option value="Costa Rica" <?php if($registro->pais=='Costa Rica')echo" selected"?>>Costa Rica</option>
					<option value="Croácia" <?php if($registro->pais=='Croácia')echo" selected"?>>Croácia</option>
					<option value="Dinamarca" <?php if($registro->pais=='Dinamarca')echo" selected"?>>Dinamarca</option>
					<option value="Djibuti" <?php if($registro->pais=='Djibuti')echo" selected"?>>Djibuti</option>
					<option value="Dominica" <?php if($registro->pais=='Dominica')echo" selected"?>>Dominica</option>				
					<option value="Egito" <?php if($registro->pais=='Egito')echo" selected"?>>Egito</option>
					<option value="El Salvador" <?php if($registro->pais=='El Salvador')echo" selected"?>>El Salvador</option>
					<option value="Emirados Árabes" <?php if($registro->pais=='Emirados Árabes')echo" selected"?>>Emirados Árabes</option>
					<option value="Equador" <?php if($registro->pais=='Equador')echo" selected"?>>Equador</option>
					<option value="Eritréia" <?php if($registro->pais=='Eritréia')echo" selected"?>>Eritréia</option>
					<option value="Escócia" <?php if($registro->pais=='Escócia')echo" selected"?>>Escócia</option>
					<option value="Eslováquia" <?php if($registro->pais=='Eslováquia')echo" selected"?>>Eslováquia</option>
					<option value="Eslovênia" <?php if($registro->pais=='Eslovênia')echo" selected"?>>Eslovênia</option>
					<option value="Espanha" <?php if($registro->pais=='Espanha')echo" selected"?>>Espanha</option>
					<option value="Estônia" <?php if($registro->pais=='Estônia')echo" selected"?>>Estônia</option>
					<option value="Etiópia" <?php if($registro->pais=='Etiópia')echo" selected"?>>Etiópia</option>
					<option value="Fiji" <?php if($registro->pais=='Fiji')echo" selected"?>>Fiji</option>
					<option value="Filipinas" <?php if($registro->pais=='Filipinas')echo" selected"?>>Filipinas</option>
					<option value="Finlândia" <?php if($registro->pais=='Finlândia')echo" selected"?>>Finlândia</option>
					<option value="França" <?php if($registro->pais=='França')echo" selected"?>>França</option>
					<option value="Gabão" <?php if($registro->pais=='Gabão')echo" selected"?>>Gabão</option>
					<option value="Gâmbia" <?php if($registro->pais=='Gâmbia')echo" selected"?>>Gâmbia</option>
					<option value="Gana" <?php if($registro->pais=='Gana')echo" selected"?>>Gana</option>
					<option value="Geórgia" <?php if($registro->pais=='Geórgia')echo" selected"?>>Geórgia</option>
					<option value="Gibraltar" <?php if($registro->pais=='Gibraltar')echo" selected"?>>Gibraltar</option>
					<option value="Granada" <?php if($registro->pais=='Granada')echo" selected"?>>Granada</option>
					<option value="Grécia" <?php if($registro->pais=='Grécia')echo" selected"?>>Grécia</option>
					<option value="Guadalupe" <?php if($registro->pais=='Guadalupe')echo" selected"?>>Guadalupe</option>
					<option value="Guam" <?php if($registro->pais=='Guam')echo" selected"?>>Guam</option>
					<option value="Guatemala" <?php if($registro->pais=='Guatemala')echo" selected"?>>Guatemala</option>
					<option value="Guiana" <?php if($registro->pais=='Guiana')echo" selected"?>>Guiana</option>
					<option value="Guiana Francesa" <?php if($registro->pais=='Guiana Francesa')echo" selected"?>>Guiana Francesa</option>
					<option value="Guiné-bissau" <?php if($registro->pais=='Guiné-bissau')echo" selected"?>>Guiné-bissau</option>
					<option value="Haiti" <?php if($registro->pais=='Haiti')echo" selected"?>>Haiti</option>
					<option value="Holanda" <?php if($registro->pais=='Holanda')echo" selected"?>>Holanda</option>
					<option value="Honduras" <?php if($registro->pais=='Honduras')echo" selected"?>>Honduras</option>
					<option value="Hong Kong" <?php if($registro->pais=='Hong Kong')echo" selected"?>>Hong Kong</option>
					<option value="Hungria" <?php if($registro->pais=='Hungria')echo" selected"?>>Hungria</option>
					<option value="Iêmen" <?php if($registro->pais=='Iêmen')echo" selected"?>>Iêmen</option>
					<option value="Ilhas Cayman" <?php if($registro->pais=='Ilhas Cayman')echo" selected"?>>Ilhas Cayman</option>
					<option value="Ilhas Cook" <?php if($registro->pais=='Ilhas Cook')echo" selected"?>>Ilhas Cook</option>
					<option value="Ilhas Curaçao" <?php if($registro->pais=='Ilhas Curaçao')echo" selected"?>>Ilhas Curaçao</option>
					<option value="Ilhas Marshall" <?php if($registro->pais=='Ilhas Marshall')echo" selected"?>>Ilhas Marshall</option>
					<option value="Ilhas Turks & Caicos" <?php if($registro->pais=='Ilhas Turks & Caicos')echo" selected"?>>Ilhas Turks & Caicos</option>
					<option value="Ilhas Virgens (brit.)" <?php if($registro->pais=='Ilhas Virgens (brit.)')echo" selected"?>>Ilhas Virgens (brit.)</option>
					<option value="Ilhas Virgens(amer.)" <?php if($registro->pais=='Ilhas Virgens(amer.)')echo" selected"?>>Ilhas Virgens(amer.)</option>
					<option value="Ilhas Wallis e Futuna" <?php if($registro->pais=='Ilhas Wallis e Futuna')echo" selected"?>>Ilhas Wallis e Futuna</option>
					<option value="Índia" <?php if($registro->pais=='Índia')echo" selected"?>>Índia</option>
					<option value="Indonésia" <?php if($registro->pais=='Indonésia')echo" selected"?>>Indonésia</option>
					<option value="Inglaterra" <?php if($registro->pais=='Inglaterra')echo" selected"?>>Inglaterra</option>
					<option value="Irlanda" <?php if($registro->pais=='Irlanda')echo" selected"?>>Irlanda</option>
					<option value="Islândia" <?php if($registro->pais=='Islândia')echo" selected"?>>Islândia</option>
					<option value="Israel" <?php if($registro->pais=='Israel')echo" selected"?>>Israel</option>
					<option value="Itália" <?php if($registro->pais=='Itália')echo" selected"?>>Itália</option>
					<option value="Jamaica" <?php if($registro->pais=='Jamaica')echo" selected"?>>Jamaica</option>
					<option value="Japão" <?php if($registro->pais=='Japão')echo" selected"?>>Japão</option>
					<option value="Jordânia" <?php if($registro->pais=='Jordânia')echo" selected"?>>Jordânia</option>
					<option value="Kuwait" <?php if($registro->pais=='Kuwait')echo" selected"?>>Kuwait</option>
					<option value="Latvia" <?php if($registro->pais=='Latvia')echo" selected"?>>Latvia</option>
					<option value="Líbano" <?php if($registro->pais=='Líbano')echo" selected"?>>Líbano</option>
					<option value="Liechtenstein" <?php if($registro->pais=='Liechtenstein')echo" selected"?>>Liechtenstein</option>
					<option value="Lituânia" <?php if($registro->pais=='Lituânia')echo" selected"?>>Lituânia</option>
					<option value="Luxemburgo" <?php if($registro->pais=='Luxemburgo')echo" selected"?>>Luxemburgo</option>
					<option value="Macau" <?php if($registro->pais=='Macau')echo" selected"?>>Macau</option>
					<option value="Macedônia" <?php if($registro->pais=='Macedônia')echo" selected"?>>Macedônia</option>
					<option value="Madagascar" <?php if($registro->pais=='Madagascar')echo" selected"?>>Madagascar</option>
					<option value="Malásia" <?php if($registro->pais=='Malásia')echo" selected"?>>Malásia</option>
					<option value="Malaui" <?php if($registro->pais=='Malaui')echo" selected"?>>Malaui</option>
					<option value="Mali" <?php if($registro->pais=='Mali')echo" selected"?>>Mali</option>
					<option value="Malta" <?php if($registro->pais=='Malta')echo" selected"?>>Malta</option>
					<option value="Marrocos" <?php if($registro->pais=='Marrocos')echo" selected"?>>Marrocos</option>
					<option value="Martinica" <?php if($registro->pais=='Martinica')echo" selected"?>>Martinica</option>
					<option value="Mauritânia" <?php if($registro->pais=='Mauritânia')echo" selected"?>>Mauritânia</option>
					<option value="Mauritius" <?php if($registro->pais=='Mauritius')echo" selected"?>>Mauritius</option>				
					<option value="Moldova" <?php if($registro->pais=='Moldova')echo" selected"?>>Moldova</option>
					<option value="Mônaco" <?php if($registro->pais=='Mônaco')echo" selected"?>>Mônaco</option>
					<option value="Montserrat" <?php if($registro->pais=='Montserrat')echo" selected"?>>Montserrat</option>
					<option value="Nepal" <?php if($registro->pais=='Nepal')echo" selected"?>>Nepal</option>
					<option value="Nicarágua" <?php if($registro->pais=='Nicarágua')echo" selected"?>>Nicarágua</option>
					<option value="Niger" <?php if($registro->pais=='Niger')echo" selected"?>>Niger</option>
					<option value="Nigéria" <?php if($registro->pais=='Nigéria')echo" selected"?>>Nigéria</option>
					<option value="Noruega" <?php if($registro->pais=='Noruega')echo" selected"?>>Noruega</option>
					<option value="Nova Caledônia" <?php if($registro->pais=='Nova Caledônia')echo" selected"?>>Nova Caledônia</option>
					<option value="Nova Zelândia" <?php if($registro->pais=='Nova Zelândia')echo" selected"?>>Nova Zelândia</option>
					<option value="Omã" <?php if($registro->pais=='Omã')echo" selected"?>>Omã</option>
					<option value="Palau" <?php if($registro->pais=='Palau')echo" selected"?>>Palau</option>
					<option value="Panamá" <?php if($registro->pais=='Panamá')echo" selected"?>>Panamá</option>
					<option value="Papua-nova Guiné" <?php if($registro->pais=='Papua-nova Guiné')echo" selected"?>>Papua-nova Guiné</option>
					<option value="Paquistão" <?php if($registro->pais=='Paquistão')echo" selected"?>>Paquistão</option>
					<option value="Peru" <?php if($registro->pais=='Peru')echo" selected"?>>Peru</option>
					<option value="Polinésia Francesa" <?php if($registro->pais=='Polinésia Francesa')echo" selected"?>>Polinésia Francesa</option>
					<option value="Polônia" <?php if($registro->pais=='Polônia')echo" selected"?>>Polônia</option>
					<option value="Porto Rico" <?php if($registro->pais=='Porto Rico')echo" selected"?>>Porto Rico</option>
					<option value="Portugal" <?php if($registro->pais=='Portugal')echo" selected"?>>Portugal</option>
					<option value="Qatar" <?php if($registro->pais=='Qatar')echo" selected"?>>Qatar</option>
					<option value="Quênia" <?php if($registro->pais=='Quênia')echo" selected"?>>Quênia</option>
					<option value="Rep. Dominicana" <?php if($registro->pais=='Rep. Dominicana')echo" selected"?>>Rep. Dominicana</option>
					<option value="Rep. Tcheca" <?php if($registro->pais=='Rep. Tcheca')echo" selected"?>>Rep. Tcheca</option>
					<option value="Reunion" <?php if($registro->pais=='Reunion')echo" selected"?>>Reunion</option>
					<option value="Romênia" <?php if($registro->pais=='Romênia')echo" selected"?>>Romênia</option>
					<option value="Ruanda" <?php if($registro->pais=='Ruanda')echo" selected"?>>Ruanda</option>
					<option value="Rússia" <?php if($registro->pais=='Rússia')echo" selected"?>>Rússia</option>
					<option value="Saipan" <?php if($registro->pais=='Saipan')echo" selected"?>>Saipan</option>
					<option value="Samoa Americana" <?php if($registro->pais=='Samoa Americana')echo" selected"?>>Samoa Americana</option>
					<option value="Senegal" <?php if($registro->pais=='Senegal')echo" selected"?>>Senegal</option>
					<option value="Serra Leone" <?php if($registro->pais=='Serra Leone')echo" selected"?>>Serra Leone</option>
					<option value="Seychelles" <?php if($registro->pais=='Seychelles')echo" selected"?>>Seychelles</option>
					<option value="Singapura" <?php if($registro->pais=='Singapura')echo" selected"?>>Singapura</option>
					<option value="Síria" <?php if($registro->pais=='Síria')echo" selected"?>>Síria</option>
					<option value="Sri Lanka" <?php if($registro->pais=='Sri Lanka')echo" selected"?>>Sri Lanka</option>
					<option value="St. Kitts & Nevis" <?php if($registro->pais=='St. Kitts & Nevis')echo" selected"?>>St. Kitts & Nevis</option>
					<option value="St. Lúcia" <?php if($registro->pais=='St. Lúcia')echo" selected"?>>St. Lúcia</option>
					<option value="St. Vincent" <?php if($registro->pais=='St. Vincent')echo" selected"?>>St. Vincent</option>
					<option value="Sudão" <?php if($registro->pais=='Sudão')echo" selected"?>>Sudão</option>
					<option value="Suécia" <?php if($registro->pais=='Suécia')echo" selected"?>>Suécia</option>
					<option value="Suiça" <?php if($registro->pais=='Suiça')echo" selected"?>>Suiça</option>
					<option value="Suriname" <?php if($registro->pais=='Suriname')echo" selected"?>>Suriname</option>
					<option value="Tailândia" <?php if($registro->pais=='Tailândia')echo" selected"?>>Tailândia</option>
					<option value="Taiwan" <?php if($registro->pais=='Taiwan')echo" selected"?>>Taiwan</option>
					<option value="Tanzânia" <?php if($registro->pais=='Tanzânia')echo" selected"?>>Tanzânia</option>
					<option value="Togo" <?php if($registro->pais=='Togo')echo" selected"?>>Togo</option>
					<option value="Trinidad & Tobago" <?php if($registro->pais=='Trinidad & Tobago')echo" selected"?>>Trinidad & Tobago</option>
					<option value="Tunísia" <?php if($registro->pais=='Tunísia')echo" selected"?>>Tunísia</option>
					<option value="Turquia" <?php if($registro->pais=='Turquia')echo" selected"?>>Turquia</option>
					<option value="Ucrânia" <?php if($registro->pais=='Ucrânia')echo" selected"?>>Ucrânia</option>
					<option value="Uganda" <?php if($registro->pais=='Uganda')echo" selected"?>>Uganda</option>
					<option value="Uruguai" <?php if($registro->pais=='Uruguai')echo" selected"?>>Uruguai</option>
					<option value="Venezuela" <?php if($registro->pais=='Venezuela')echo" selected"?>>Venezuela</option>
					<option value="Vietnã" <?php if($registro->pais=='Vietnã')echo" selected"?>>Vietnã</option>
					<option value="Zaire" <?php if($registro->pais=='Zaire')echo" selected"?>>Zaire</option>
					<option value="Zâmbia" <?php if($registro->pais=='Zâmbia')echo" selected"?>>Zâmbia</option>
					<option value="Zimbábue" <?php if($registro->pais=='Zimbábue')echo" selected"?>>Zimbábue</option>
				</optgroup>
			</select></label>	

		<label>Data
		<input type="text" name="data" class="datepicker" required value="<?=formataData($registro->data, 'mysql2br')?>"></label>

		Imagem
		<?php if ($registro->imagem): ?>
			<br><img src="_imgs/giro/<?=$registro->imagem?>"><br>
			<label><input type="checkbox" name="remover_imagem" value="1"> Remover Imagem</label>
		<?php endif ?>
		<label><input type="file" name="userfile"></label>

		<label>Olho
		<textarea name="olho" class="pequeno basico"><?=$registro->olho?></textarea></label>

		<label>Texto
		<textarea name="texto" class="medio basico"><?=$registro->texto?></textarea></label>

		<input type="hidden" name="lang" value="<?=$lang?>">

		<div class="form-actions">
        	<button class="btn btn-primary" type="submit">Salvar</button>
        	<button class="btn btn-voltar" type="reset">Voltar</button>
      	</div>
	</form>	
	
<?else: ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/inserir/'.$lang)?>" enctype="multipart/form-data">

		<div id="dialog"></div>
		
		<label>Título
		<input type="text" name="titulo" required autofocus></label>

		<label>País
			<select name="pais" required>
				<option value="">selecione um país</option>
				<optgroup label="Mais Comuns">
					<option value="Brasil">Brasil</option>
					<option value="EUA">EUA</option>
					<option value="México">México</option>
				</optgroup>
				<optgroup label="Demais">
					<option value="África do Sul">África do Sul</option>
					<option value="Albânia">Albânia</option>
					<option value="Alemanha">Alemanha</option>
					<option value="Andorra">Andorra</option>
					<option value="Angola">Angola</option>
					<option value="Anguilla">Anguilla</option>
					<option value="Antigua">Antigua</option>
					<option value="Arábia Saudita">Arábia Saudita</option>
					<option value="Argentina">Argentina</option>
					<option value="Armênia">Armênia</option>
					<option value="Aruba">Aruba</option>
					<option value="Austrália">Austrália</option>
					<option value="Áustria">Áustria</option>
					<option value="Azerbaijão">Azerbaijão</option>
					<option value="Bahamas">Bahamas</option>
					<option value="Bahrein">Bahrein</option>
					<option value="Bangladesh">Bangladesh</option>
					<option value="Barbados">Barbados</option>
					<option value="Bélgica">Bélgica</option>
					<option value="Benin">Benin</option>
					<option value="Bermudas">Bermudas</option>
					<option value="Botsuana">Botsuana</option>				
					<option value="Brunei">Brunei</option>
					<option value="Bulgária">Bulgária</option>
					<option value="Burkina Fasso">Burkina Fasso</option>
					<option value="botão">botão</option>
					<option value="Cabo Verde">Cabo Verde</option>
					<option value="Camarões">Camarões</option>
					<option value="Camboja">Camboja</option>
					<option value="Canadá">Canadá</option>
					<option value="Cazaquistão">Cazaquistão</option>
					<option value="Chade">Chade</option>
					<option value="Chile">Chile</option>
					<option value="China">China</option>
					<option value="Cidade do Vaticano">Cidade do Vaticano</option>
					<option value="Colômbia">Colômbia</option>
					<option value="Congo">Congo</option>
					<option value="Coréia do Sul">Coréia do Sul</option>
					<option value="Costa do Marfim">Costa do Marfim</option>
					<option value="Costa Rica">Costa Rica</option>
					<option value="Croácia">Croácia</option>
					<option value="Dinamarca">Dinamarca</option>
					<option value="Djibuti">Djibuti</option>
					<option value="Dominica">Dominica</option>				
					<option value="Egito">Egito</option>
					<option value="El Salvador">El Salvador</option>
					<option value="Emirados Árabes">Emirados Árabes</option>
					<option value="Equador">Equador</option>
					<option value="Eritréia">Eritréia</option>
					<option value="Escócia">Escócia</option>
					<option value="Eslováquia">Eslováquia</option>
					<option value="Eslovênia">Eslovênia</option>
					<option value="Espanha">Espanha</option>
					<option value="Estônia">Estônia</option>
					<option value="Etiópia">Etiópia</option>
					<option value="Fiji">Fiji</option>
					<option value="Filipinas">Filipinas</option>
					<option value="Finlândia">Finlândia</option>
					<option value="França">França</option>
					<option value="Gabão">Gabão</option>
					<option value="Gâmbia">Gâmbia</option>
					<option value="Gana">Gana</option>
					<option value="Geórgia">Geórgia</option>
					<option value="Gibraltar">Gibraltar</option>
					<option value="Granada">Granada</option>
					<option value="Grécia">Grécia</option>
					<option value="Guadalupe">Guadalupe</option>
					<option value="Guam">Guam</option>
					<option value="Guatemala">Guatemala</option>
					<option value="Guiana">Guiana</option>
					<option value="Guiana Francesa">Guiana Francesa</option>
					<option value="Guiné-bissau">Guiné-bissau</option>
					<option value="Haiti">Haiti</option>
					<option value="Holanda">Holanda</option>
					<option value="Honduras">Honduras</option>
					<option value="Hong Kong">Hong Kong</option>
					<option value="Hungria">Hungria</option>
					<option value="Iêmen">Iêmen</option>
					<option value="Ilhas Cayman">Ilhas Cayman</option>
					<option value="Ilhas Cook">Ilhas Cook</option>
					<option value="Ilhas Curaçao">Ilhas Curaçao</option>
					<option value="Ilhas Marshall">Ilhas Marshall</option>
					<option value="Ilhas Turks & Caicos">Ilhas Turks & Caicos</option>
					<option value="Ilhas Virgens (brit.)">Ilhas Virgens (brit.)</option>
					<option value="Ilhas Virgens(amer.)">Ilhas Virgens(amer.)</option>
					<option value="Ilhas Wallis e Futuna">Ilhas Wallis e Futuna</option>
					<option value="Índia">Índia</option>
					<option value="Indonésia">Indonésia</option>
					<option value="Inglaterra">Inglaterra</option>
					<option value="Irlanda">Irlanda</option>
					<option value="Islândia">Islândia</option>
					<option value="Israel">Israel</option>
					<option value="Itália">Itália</option>
					<option value="Jamaica">Jamaica</option>
					<option value="Japão">Japão</option>
					<option value="Jordânia">Jordânia</option>
					<option value="Kuwait">Kuwait</option>
					<option value="Latvia">Latvia</option>
					<option value="Líbano">Líbano</option>
					<option value="Liechtenstein">Liechtenstein</option>
					<option value="Lituânia">Lituânia</option>
					<option value="Luxemburgo">Luxemburgo</option>
					<option value="Macau">Macau</option>
					<option value="Macedônia">Macedônia</option>
					<option value="Madagascar">Madagascar</option>
					<option value="Malásia">Malásia</option>
					<option value="Malaui">Malaui</option>
					<option value="Mali">Mali</option>
					<option value="Malta">Malta</option>
					<option value="Marrocos">Marrocos</option>
					<option value="Martinica">Martinica</option>
					<option value="Mauritânia">Mauritânia</option>
					<option value="Mauritius">Mauritius</option>				
					<option value="Moldova">Moldova</option>
					<option value="Mônaco">Mônaco</option>
					<option value="Montserrat">Montserrat</option>
					<option value="Nepal">Nepal</option>
					<option value="Nicarágua">Nicarágua</option>
					<option value="Niger">Niger</option>
					<option value="Nigéria">Nigéria</option>
					<option value="Noruega">Noruega</option>
					<option value="Nova Caledônia">Nova Caledônia</option>
					<option value="Nova Zelândia">Nova Zelândia</option>
					<option value="Omã">Omã</option>
					<option value="Palau">Palau</option>
					<option value="Panamá">Panamá</option>
					<option value="Papua-nova Guiné">Papua-nova Guiné</option>
					<option value="Paquistão">Paquistão</option>
					<option value="Peru">Peru</option>
					<option value="Polinésia Francesa">Polinésia Francesa</option>
					<option value="Polônia">Polônia</option>
					<option value="Porto Rico">Porto Rico</option>
					<option value="Portugal">Portugal</option>
					<option value="Qatar">Qatar</option>
					<option value="Quênia">Quênia</option>
					<option value="Rep. Dominicana">Rep. Dominicana</option>
					<option value="Rep. Tcheca">Rep. Tcheca</option>
					<option value="Reunion">Reunion</option>
					<option value="Romênia">Romênia</option>
					<option value="Ruanda">Ruanda</option>
					<option value="Rússia">Rússia</option>
					<option value="Saipan">Saipan</option>
					<option value="Samoa Americana">Samoa Americana</option>
					<option value="Senegal">Senegal</option>
					<option value="Serra Leone">Serra Leone</option>
					<option value="Seychelles">Seychelles</option>
					<option value="Singapura">Singapura</option>
					<option value="Síria">Síria</option>
					<option value="Sri Lanka">Sri Lanka</option>
					<option value="St. Kitts & Nevis">St. Kitts & Nevis</option>
					<option value="St. Lúcia">St. Lúcia</option>
					<option value="St. Vincent">St. Vincent</option>
					<option value="Sudão">Sudão</option>
					<option value="Suécia">Suécia</option>
					<option value="Suiça">Suiça</option>
					<option value="Suriname">Suriname</option>
					<option value="Tailândia">Tailândia</option>
					<option value="Taiwan">Taiwan</option>
					<option value="Tanzânia">Tanzânia</option>
					<option value="Togo">Togo</option>
					<option value="Trinidad & Tobago">Trinidad & Tobago</option>
					<option value="Tunísia">Tunísia</option>
					<option value="Turquia">Turquia</option>
					<option value="Ucrânia">Ucrânia</option>
					<option value="Uganda">Uganda</option>
					<option value="Uruguai">Uruguai</option>
					<option value="Venezuela">Venezuela</option>
					<option value="Vietnã">Vietnã</option>
					<option value="Zaire">Zaire</option>
					<option value="Zâmbia">Zâmbia</option>
					<option value="Zimbábue">Zimbábue</option>
				</optgroup>
			</select></label>

		<label>Data
		<input type="text" name="data" class="datepicker" required></label>

		<label>Imagem
		<input type="file" name="userfile"></label>		

		<label>Olho
		<textarea name="olho" class="pequeno basico"></textarea></label>

		<label>Texto
		<textarea name="texto" class="medio basico"></textarea></label>

		<input type="hidden" name="lang" value="<?=$lang?>">

		<div class="form-actions">
        	<button class="btn btn-primary" type="submit">Inserir</button>
        	<button class="btn btn-voltar" type="reset">Voltar</button>
      	</div>
	</form>

<?endif ?>