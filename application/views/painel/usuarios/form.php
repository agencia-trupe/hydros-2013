<div class="container top">

  <?if(isset($mostrarsucesso) && $mostrarsucesso):?>
    <div class="alert alert-block alert-success fade in" data-dismiss="alert"><?=$mostrarsucesso?></div>
  <?elseif(isset($mostrarerro) && $mostrarerro):?>
    <div class="alert alert-block alert-error fade in" data-dismiss="alert"><?=$mostrarerro?></div>
  <?endif;?>

  <div class="page-header users-header">
    <h2>
      <?=$titulo?>
    </h2>
  </div>  

<?if ($registro): ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/alterar/'.$registro->id)?>">

		<div id="dialog"></div>
		
		<label>Usuário<br>
		<input type="text" name="username" required autofocus value="<?=$registro->username?>"></label>

		<label>E-mail<br>
		<input type="email" name="email" value="<?=$registro->email?>"></label>

		<label>Senha<br>
		<input type="password" name="password" id="senha"></label>

		<label>Confirmar Senha<br>
		<input type="password" id="conf-senha"></label>

		<div class="form-actions">
        	<button class="btn btn-primary" type="submit">Salvar</button>
        	<button class="btn btn-voltar" type="reset">Voltar</button>
      	</div>
	</form>

	<script defer>
		$('document').ready( function(){
			$('form').submit( function(){
				if($('#senha').val() != ''){
					if($('#senha').val() != $('#conf-senha').val()){
						bootbox.alert('As senhas informadas não conferem.');
						$('#senha').focus();
						return false;
					}
				}
			});
		});
	</script>	
	
<?else: ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/inserir')?>">

		<div id="dialog"></div>
		
		<label>Usuário<br>
		<input type="text" name="username" required autofocus></label>

		<label>E-mail<br>
		<input type="email" name="email"></label>

		<label>Senha<br>
		<input type="password" name="password" id="senha" required></label>

		<label>Confirmar Senha<br>
		<input type="password" id="conf-senha" required></label>

		<div class="form-actions">
        	<button class="btn btn-primary" type="submit">Inserir</button>
        	<button class="btn btn-voltar" type="reset">Voltar</button>
      	</div>
	</form>
	
	<script defer>
		$('document').ready( function(){
			$('form').submit( function(){
				if($('#senha').val() == '' || $('#conf-senha').val() == ''){
					bootbox.alert('Informe a senha e a confirmação de senha corretamente.');
					$('#senha').focus();
					return false;
				}
				if($('#senha').val() != $('#conf-senha').val()){
					bootbox.alert('As senhas informadas não conferem.');
					$('#senha').focus();
					return false;
				}
			});
		});
	</script>

<?endif ?>