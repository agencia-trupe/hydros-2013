<div class="navbar navbar-fixed-top">

  <div class="navbar-inner">

    <div class="container">

      <a href="painel/home" class="brand">Projeto Hydros</a>

      <ul class="nav">

        <li <?if($this->router->class=='home')echo" class='active'"?>><a href="painel/home">Início</a></li>

        <li class="dropdown  <?if($this->router->class=='banners')echo"active"?>">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Banners <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="painel/banners/index/pt">Banners em Português</a></li>
            <li><a href="painel/banners/index/en">Banners em Inglês</a></li>
            <li><a href="painel/banners/index/es">Banners em Espanhol</a></li>
          </ul>
        </li>

        <li class="dropdown  <?if($this->router->class=='giro')echo"active"?>">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Giro <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="painel/giro/index/pt">Giro em Português</a></li>
            <li><a href="painel/giro/index/en">Giro em Inglês</a></li>
            <li><a href="painel/giro/index/es">Giro em Espanhol</a></li>
          </ul>
        </li>

        <li class="dropdown <?if($this->router->class=='noticias')echo"active"?>">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Notícias <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="painel/noticias/index/pt">Notícias em Português</a></li>
            <li><a href="painel/noticias/index/en">Notícias em Inglês</a></li>
            <li><a href="painel/noticias/index/es">Notícias em Espanhol</a></li>            
          </ul>
        </li>

        <li class="dropdown <?if($this->router->class=='comentarios' && $this->router->method=='index' && $this->uri->segment(5) == 'mapa')echo"active"?>">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Mapa de Recursos Hídricos <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="painel/comentarios/index/pt/mapa/0">Comentários em Português</a></li>
            <li><a href="painel/comentarios/index/en/mapa/0">Comentários em Inglês</a></li>
            <li><a href="painel/comentarios/index/es/mapa/0">Comentários em Espanhol</a></li>            
          </ul>
        </li>        

        <li class="dropdown <?if($this->router->class=='cadastros')echo"active"?>">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Newsletter <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="painel/cadastros/index/pt">Cadastros em Português</a></li>
            <li><a href="painel/cadastros/index/en">Cadastros em Inglês</a></li>
            <li><a href="painel/cadastros/index/es">Cadastros em Espanhol</a></li>
            <li><a href="painel/cadastros/index/all">Todos os Cadastros</a></li>
          </ul>
        </li>

        <li class="dropdown <?if($this->router->class=='adesoes')echo"active"?>">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Adesões <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="painel/adesoes/index/2">Empresas</a></li>
            <li><a href="painel/adesoes/index/1">Pessoas</a></li>            
          </ul>
        </li>        

        <li class="dropdown <?if($this->router->class=='usuarios')echo"active"?>">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Sistema <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="painel/usuarios">Usuários</a></li>
            <li><a href="painel/home/logout">Logout</a></li>
          </ul>
        </li>

      </ul>

    </div>
  </div>
</div>