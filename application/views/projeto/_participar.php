<div class="centro container ultalteracao">

	<div class="topo">

		<div class="destaque">
			<?=traduz('Participe do Projeto Hydros e faça a sua parte!')?>
		</div>

		<div class="titulo">
			<h1><?=traduz('Para ser um')?></h1>
			<h2><?=traduz('Embaixador da Água')?></h2>

			<p>
				<?=traduz('Você está perto de tornar-se um <strong>Embaixador da Água</strong> e ter as ferramentas necessárias para sensibilizar e conscientizar as pessoas ao seu redor acerca de temas como a correta utilização e preservação da água.')?>
			</p>
		</div>

	</div>
	
	<div class="meio">
		<p><?=traduz('Para que você consiga utilizar todos os recursos disponíveis pelo <strong>Projeto Hydros</strong> da melhor forma possível, criamos dois tutoriais, com o passo a passo de como utilizar os materiais disponíveis neste site:')?></p>
	</div>

	
	<div class="container links-container">
		<a href="" onclick="javascript: return false;" title="<?=traduz('Público Geral e Colaboradores')?>" class="link-roxo primeiro">
			<p><?=traduz("um para o <br><span class='maior'>Público em geral</span> e<br> <strong>colaboradores do Grupo Empresarial Kaluz</strong>")?></p>
			<?php
			switch ($this->session->userdata('language')) {
				case 'en':
					echo "<img src='capa-tutorial-home-en.jpg'>";
					break;
				case 'es':
					echo "<img src='_imgs/layout/capa-tutorial-home-es.jpg'>";
					break;				
				default:
				case 'pt':
				echo "<img src='_imgs/layout/capa-tutorial-home-pt.jpg'>";
					break;
			}
			?>
			<span class="verde"><?=traduz('clique para iniciar o<br> download')?></span>
		</a>
		<a href="" onclick="javascript: return false;" title="<?=traduz('Profissionais')?>" class="link-roxo">
			<p><?=traduz("e outro para <br><span class='maior'>Profissionais da Área de Educação</span>")?></p>
			<img src="_imgs/layout/capa-tutorial-comoparticipar.jpg">
			<span class="verde"><?=traduz('clique para iniciar o<br> download')?></span>
		</a>
	</div>

</div>

