<div class="centro container">

	<div class="coluna esquerda">

		<h1><?=traduz('Há tempos debatemos a forma como o homem se relaciona com a água')?></h1>

		<p>
			<?=traduz('Em 2008, a Fundação Kaluz e o Grupo Empresarial Kaluz, assim como as suas empresas Mexichem, Elementia e BX, lançaram uma série de quatro livros de fotografias tratando do tema água, com o objetivo de conscientizar e sensibilizar sobre a importância do uso correto desse elemento. As publicações chegaram às mãos de líderes nacionais, regionais e mundiais, que perceberam o potencial que o projeto trazia.')?>
		</p>

		<p>
			<?=traduz('Para ampliar a disseminação da preservação da água, nasceu o Portal Hydros. Interativo e trilíngue (espanhol, português e inglês), o site é o principal canal de comunicação entre o Projeto e seus Embaixadores. O portal tem inúmeras ferramentas que permitem a qualquer instituição ou cidadão comum, promover verdadeiras campanhas de sensibilização sobre a água.')?>
		</p>

		<p class="quote">
			<?=traduz('O <span class="txt-branco">Projeto Hydros</span> levanta a questão: <br> <span class="maior">\'Como me relaciono com a água?\'</span> <br> Trata-se de um grande chamado para que todos sejam “Embaixadores da água”, a <br> fim de disseminar o uso sustentável dos recursos hídricos.')?>
		</p>

		<p>
			<?=traduz('Em 2012, a ação foi lançada para todas as empresas do Grupo Kaluz, incentivando atitudes em todos os países nos quais atua, com foco na conscientização, preservação e sustentabilidade. No mesmo ano, o <strong>Projeto Hydros</strong> também chegou a escolas e comunidades próximas de suas unidades, abrindo um diálogo importante sobre o tema.')?>
		</p>

		<p>
			<?=traduz('O desafio para os próximos anos é disseminar, para o maior número possível de pessoas, essa conscientização de que os nossos recursos naturais estão acabando e precisamos fazer algo para reverter essa situação. A ideia é que o <strong>Projeto Hydros</strong> saia dos portões das empresas do Grupo Kaluz e ganhe o mundo.')?>
		</p>

	</div>

	<div class="coluna direita">
		<img src="_imgs/layout/imagem_projeto.png" alt="<?=traduz('')?>">
	</div>

</div>