<div class="centro container ultalteracao">

	<div class="topo">

		<div class="destaque">
			<?=traduz('Participe do Projeto Hydros e faça a sua parte!')?>
		</div>

		<div class="titulo">
			<h1><?=traduz('Para ser um')?></h1>
			<h2><?=traduz('Embaixador da Água')?></h2>

			<p>
				<?=traduz('Você está perto de tornar-se um <strong>Embaixador da Água</strong> e ter as ferramentas necessárias para sensibilizar e conscientizar as pessoas ao seu redor acerca de temas como a correta utilização e preservação da água.')?>
			</p>
		</div>

	</div>
	
	<div class="meio">
		<p><?=traduz('Para que você consiga utilizar todos os recursos disponíveis pelo <strong>Projeto Hydros</strong> da melhor forma possível, criamos dois tutoriais, com o passo a passo de como utilizar os materiais disponíveis neste site:')?></p>
	</div>
<?php
switch ($this->session->userdata('language')) {
	case 'en':
		$link1 = "_materiais/step_by_step_portal_hydros.pdf";
		$img1 = "_imgs/layout/passoapasso-en.jpg";
		$link2 = "_materiais/education_professional.pdf";
		$img2 = "_imgs/layout/capa-tutorial-comoparticipar-en.jpg";
		break;
	case 'es':
		$link1 = "_materiais/paso_a_paso_portal_hydros.pdf";
		$img1 = "_imgs/layout/passoapasso-es.jpg";
		$link2 = "_materiais/profesional_del_area_de_educacion.pdf";
		$img2 = "_imgs/layout/capa-tutorial-comoparticipar-es.jpg";
		break;
	default:
	case 'pt':
		$link1 = "_materiais/passo_a_passo_portal_hydros.pdf";
		$img1 = "_imgs/layout/passoapasso-pt.jpg";
		$link2 = "_materiais/profissional_da_educacao.pdf";
		$img2 = "_imgs/layout/capa-tutorial-comoparticipar-pt.jpg";
		break;
}
?>
	

	<div class="container links-container">
		<a href="<?=$link1?>" target="_blank" title="<?=traduz('Público Geral e Colaboradores')?>" class="link-roxo primeiro">
			<p><?=traduz("um para o <br><span class='maior'>Público em geral</span> e<br> <strong>colaboradores do Grupo Empresarial Kaluz</strong>")?></p>
			<img src="<?=$img1?>">
			<span class="verde"><?=traduz('clique para iniciar o<br> download')?></span>
		</a>
		<a href="<?=$link2?>" target="_blank" title="<?=traduz('Profissionais')?>" class="link-roxo">
			<p><?=traduz("e outro para <br><span class='maior'>Profissionais da Área de Educação</span>")?></p>
			<img src="<?=$img2?>">
			<span class="verde"><?=traduz('clique para iniciar o<br> download')?></span>
		</a>
	</div>

</div>

