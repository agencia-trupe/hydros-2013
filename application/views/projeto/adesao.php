	<div id="adesao">

		<div id="testeira">
			<h1><?=traduz('CADASTRO DE APOIO<br>AO PROJETO HYDROS')?></h1>
			<h2><?=traduz('Se você apoia o Projeto Hydros divulgue aqui<br> seu nome ou a marca de sua empresa.')?></h2>

			<div id="box-seja">
				<div class="frase1"><?=traduz('Seja um')?></div>
				<div class="frase2"><?=traduz('Embaixador')?></div>
				<div class="frase3"><?=traduz('da Água!')?></div>
			</div>
		</div>

		<div id="botoes" <?php if($this->router->method=='cadastraAdesaoEmpresa')echo" class='hid'"?>>
			<a href="#" class="margem" title="<?=traduz('Cadastro de Pessoas')?>" id="cadastro-pessoas"><?=traduz('cadastro de <br> <span class="maior">PESSOAS</span>')?></a>
			<a href="#" title="<?=traduz('Cadastro de Empresa')?>" id="cadastro-empresas"><?=traduz('cadastro de <br> <span class="maior">EMPRESAS</span>')?></a>
		</div>

		<div id="form-pessoas" class="hid">
			<h3><?=traduz('cadastro de <br> <span class="maior">PESSOAS</span>')?></h3>

			<form id="form-cad-pessoas" method="post" action="">
				<label><?=traduz('nome completo')?> <input type="text" name="cad-nome" id="cad-nome" required></label>
				<label><?=traduz('e-mail')?> <input type="email" name="cad-email" id="cad-email" required></label>
				<label><?=traduz('país')?> <input type="text" name="cad-pais" id="cad-pais" required></label>
				<div class="submit-placeholder">
					<input type="submit" value="<?=traduz('CADASTRAR')?>">
				</div>
			</form>
			
			<p class="disclaimer">
				<?=traduz('O Projeto Hydros não divulga publicamente os e-mails cadastrados nem fornece seus cadastros a terceiros. A solicitação do endereço de e-mail neste caso será usada apenas se for necessário confirmar alguma alteração. O Projeto Hydros se reserva o direito de excluir cadastros a seu critério.')?>
			</p>
		</div>

		<div id="form-empresas" class="hid">
			<h3><?=traduz('cadastro de <br> <span class="maior">EMPRESAS</span>')?></h3>

			<form id="form-cad-empresas" method="post" action="projeto/cadastraAdesaoEmpresa" enctype="multipart/form-data">
				<label><?=traduz('nome da empresa')?> <input type="text" name="cad-empresa" id="cad-empresa" required></label>
				<label><input type="text" name="cad-responsavel" id="cad-responsavel" required><div class="duaslinhas"><?=traduz('nome do<br> contato responsável')?></div></label>
				<label><?=traduz('e-mail')?> <input type="email" name="cad-email" id="cademp-email" required></label>
				<label><?=traduz('país')?> <input type="text" name="cad-pais" id="cademp-pais" required></label>
				<label><?=traduz('website')?> <input type="text" name="cad-website" id="cad-website" required></label>
				<label class="special">
					<div id="fake-input"><?=traduz('procurar imagem')?> &raquo;</div>
					<input type="file" name="cad-imagem" id="cad-imagem" required>
					<div class="duaslinhas"><?=traduz('imagem da marca<br> da empresa')?></div>
				</label>
				<div class="submit-placeholder">
					<input type="submit" value="<?=traduz('CADASTRAR')?>">
				</div>
			</form>
			
			<p class="disclaimer emp">
				<?=traduz('O Projeto Hydros não divulga publicamente os e-mails cadastrados nem fornece seus cadastros a terceiros. A solicitação do endereço de e-mail neste caso será usada apenas se for necessário confirmar alguma alteração. O Projeto Hydros se reserva o direito de excluir cadastros a seu critério.')?>
			</p>
		</div>

		<div id="agradecimento-pessoas" class="agradecimento hid">
			<p>
				<?=traduz('Pronto, seu cadastro foi feito com sucesso.')?>
			</p>
			<p class="maior">
				<?=traduz('Você deu o primeiro passo para se tornar um Embaixador da Água.')?>
			</p>
			<p>
				<?=traduz('Quer saber mais? Acompanhe as novidades do Projeto Hydros sobre a água por meio de nossas redes sociais.')?> 
			</p>
			<p>
				<?=traduz('Curta, compartilhe, siga e participe!')?>
			</p>

			<ul>
				<li class="share-email">
					<a href="<?=$share['email']?>" title="<?=traduz('Compartilhe por e-mail!')?>">e-mail</a>
				</li>
				<li class="share-facebook">
					<a rel="nofollow" href="http://www.facebook.com/share.php" onclick="return fbs_click(this)" title="<?=traduz('Compartilhe pelo facebook!')?>">facebook</a>
				</li>
				<li class="share-twitter">
					<a rel="nofollow" href="" onclick="<?=$share['tweet']?>" title="<?=traduz('Compartilhe no Tumblr')?>">twitter</a>
				</li>
				<li class="share-tumblr">
					<a href="http://www.tumblr.com/share/link?url=<?php echo urlencode(current_url())?>&description=<?= urlencode($share['description_tumblr']) ?>&name=<?php echo urlencode($share['tumblr']) ?>" title="<?=traduz('Compartilhe no Tumblr!')?>">tumblr</a>
				</li>
			</ul>

		</div>

		<div id="agradecimento-empresas" class="agradecimento  <?php if($this->router->method!='cadastraAdesaoEmpresa')echo" hid"?>">
			<p>
				<?=traduz('Pronto, seu cadastro foi feito com sucesso.')?> 
			</p>
			<p class="maior">
				<?=traduz('A sua empresa deu o primeiro passo para se tornar uma Embaixadora da Água.')?> 
			</p>
			<p>
				<?=traduz('Quer saber mais? Acompanhe as novidades do Projeto Hydros sobre a água por meio de nossas redes sociais.')?> 
			</p>
			<p>
				<?=traduz('Curta, compartilhe com seus colaboradores, siga e participe!')?>
			</p>

			<ul>
				<li class="share-email">
					<a href="<?=$share['email']?>" title="<?=traduz('Compartilhe por e-mail!')?>">e-mail</a>
				</li>
				<li class="share-facebook">
					<a rel="nofollow" href="http://www.facebook.com/share.php" onclick="return fbs_click(this)" title="<?=traduz('Compartilhe pelo facebook!')?>">facebook</a>
				</li>
				<li class="share-twitter">
					<a rel="nofollow" href="" onclick="<?=$share['tweet']?>" title="<?=traduz('Compartilhe no Tumblr')?>">twitter</a>
				</li>
				<li class="share-tumblr">
					<a href="http://www.tumblr.com/share/link?url=<?php echo urlencode(current_url())?>&description=<?= urlencode($share['description_tumblr']) ?>&name=<?php echo urlencode($share['tumblr']) ?>" title="<?=traduz('Compartilhe no Tumblr!')?>">tumblr</a>
				</li>
			</ul>
		</div>

	</div>

	<script defer>
	$('document').ready( function(){

		$('#cadastro-pessoas').click( function(e){
			e.preventDefault();

			$('#botoes').slideUp('normal', function(){
				$('#form-pessoas').slideDown('normal');
				$('#adesao').css('height', '440px');
				parent.$("#fancybox-content").height('470px');
			})
		});

		$('#cadastro-empresas').click( function(e){
			e.preventDefault();

			$('#botoes').slideUp('normal', function(){
				$('#form-empresas').slideDown('normal');
				$('#adesao').css('height', '540px');
				parent.$("#fancybox-content").height('587px');
			})
		});

		$('#form-pessoas').submit( function(e){
			e.preventDefault();
			var nome = $('#cad-nome').val();
			var email = $('#cad-email').val();
			var pais = $('#cad-pais').val();

			// FALTOU TRADUZIR OS ALERTAS
			
			if(!nome){
				alert('Informe seu nome!');
				return false;
			}
			if(!email){
				alert('Informe seu e-mail!');
				return false;
			}
			if(!pais){
				alert('Informe seu país!');
				return false;
			}

			$.post(BASE+'projeto/cadastraAdesaoPessoa',
				{
					'cad-nome' : nome,
					'cad-email' : email,
					'cad-pais' : pais
				},
				function(retorno){
					$('#form-pessoas').slideUp('normal', function(){
						$('#agradecimento-pessoas').slideDown('normal');
						$('#adesao').css('height', '480px');
						parent.$("#fancybox-content").height('500px');
					})
				});
		});

		$('#form-empresas').submit( function(e){
			//e.preventDefault();
			var empresa = $('#cad-empresa').val();
			var responsavel = $('#cad-responsavel').val();
			var email = $('#cademp-email').val();
			var pais = $('#cademp-pais').val();
			var imagem = $('#cad-imagem').val();
			var website = $('#cad-website').val();

			// FALTOU TRADUZIR OS ALERTAS

			if(!empresa){
				alert('Informe o nome da Empresa!');
				return false;
			}
			if(!responsavel){
				alert('Informe o nome do Responśavel!');
				return false;
			}
			if(!email){
				alert('Informe seu e-mail!');
				return false;
			}
			if(!pais){
				alert('Informe o Páis da Empresa!');
				return false;
			}
			if(!imagem){
				alert('Envie a marca da sua empresa!');
				return false;
			}			

			parent.$("#fancybox-content").height('500px');
		});

		var guarda_texto = '';

		$('#cad-imagem').live('change', function(){
			var conteudo = $(this).val();
			
			if(guarda_texto == '')
				guarda_texto = $('#fake-input').html();

			if(conteudo != '')
				$('#fake-input').html(conteudo.split('\\').pop());
			else
				$('#fake-input').html(guarda_texto);
		});

	});

	</script>

	</body>
</html>