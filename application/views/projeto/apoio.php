<div class="centro container">

	<div class="coluna esquerda">

		<h1><?=traduz('Seja você também um<br>Embaixador da Água!')?></h1>

		<p>
			<?=traduz('Simples mudanças de atitude podem fazer a diferença na preservação da água. Hoje já não é mais suficiente apenas fechar a torneira ao lavar a louça ou ao escovar os dentes, por exemplo. O grande desafio é a conscientização. Precisamos mostrar às pessoas que a escassez da água, especialmente a potável, aquela que consumimos, é um problema sério e que devemos tomar as atitudes corretas agora. Uma ação inicial é tornar-se um <strong>Embaixador da Água</strong> do <strong>Projeto Hydros</strong>. ')?>
		</p>

		<p>
			<?=traduz('Esta é a lista de pessoas e empresas que decidiram fazer parte desta iniciativa. Seja uma delas e faça a sua parte!')?>
		</p>

		<div id="separador"></div>

		<?php if ($empresas): ?>

			<h2><?=traduz('Empresas que apoiam o Projeto Hydros e agora são Embaixadoras da Água.')?></h2>

			<div id="empresas">
				<ul class="lista-empresas">
					<?php foreach ($empresas as $key => $value): ?>
						<?php if ($value->website): ?>
							<li <?if(($key+1)%3==0 && $key > 0)echo" class='semmargem'"?>><a href="<?=prep_url($value->website)?>" title="<?=$value->empresa?>" target="_blank"><img src="_imgs/apoio/<?=$value->imagem?>" alt="<?=$value->empresa?>"></a></li>
						<?php else: ?>
							<li <?if(($key+1)%3==0 && $key > 0)echo" class='semmargem'"?>><img src="_imgs/apoio/<?=$value->imagem?>" alt="<?=$value->empresa?>"></li>
						<?php endif ?>
						<?php if (($key+1)%18==0 && $key > 0 && $key < sizeof($empresas)): ?>
							</ul><ul class="lista-empresas" style="display:none;">
						<?php endif ?>
					<?php endforeach ?>
				</ul>
			</div>

			<div id="paginacao-empresas">
				<a href="#" title="<?=traduz('voltar')?>" id="emp-nav-prev">&laquo; <?=traduz('voltar')?></a>
				<a href="#" title="<?=traduz('ver mais')?>" id="emp-nav-next"><?=traduz('ver mais')?> &raquo;</a>
			</div>

		<?php endif ?>

	</div>

	<div class="coluna direita">

		<a href="ajax/adesao" title="<?=traduz('Seja um embaixador da água!')?>" id="lanca-modal-adesao">
			<div id="box-participe"><?=traduz('PARTICIPE!')?></div>

			<div id="box-seja">
				<div class="frase1"><?=traduz('Seja um')?></div>
				<div class="frase2"><?=traduz('Embaixador')?></div>
				<div class="frase3"><?=traduz('da Água!')?></div>
			</div>
		</a>

		<h2><?=traduz('Pessoas que apoiam o Projeto Hydros e agora são Embaixadoras da Água.')?></h2>

		<?php if ($pessoas): ?>
			
			<div id="pessoas">
				<div class="animate">
					<ul class="lista-pessoas">
						<?php foreach ($pessoas as $key => $value): ?>
							<li>
								<div class="nome"><?=$value->nome?></div><div class="pais"><?=$value->pais?></div>
							</li>
							<?php if (($key+1)%30==0 && $key > 0 && $key < sizeof($pessoas)): ?>
								</ul><ul class="lista-pessoas" style="display:none;">
							<?php endif ?>
						<?php endforeach ?>
					</ul>
				</div>
				<div id="paginacao-pessoas">
					<a href="#" title="<?=traduz('voltar')?>" id="pess-nav-prev">&laquo; <?=traduz('voltar')?></a>
					<a href="#" title="<?=traduz('ver mais')?>" id="pess-nav-next"><?=traduz('ver mais')?> &raquo;</a>
				</div>
			</div>

		<?php endif ?>

	</div>

</div>