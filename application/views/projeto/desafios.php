<div class="centro container">

	<div class="coluna esquerda">

		<h1><?=traduz('Uma questão de <br> consciência e atitude')?></h1>

		<p>
			<?=traduz('Um dos desafios para a preservação da água potável do mundo é a conscientização. Por isso, o objetivo de <strong>Projeto Hydros</strong> é fazer com que os resultados já alcançados ao longo de 2012, se expandam ainda mais e envolvam cada vez mais pessoas. Prova desse esforço: 2013 é o Ano Internacional de Cooperação pela Água, da UNESCO.')?>
		</p>

		<p>
			<?=traduz('A ideia do <strong>Projeto Hydros</strong> é chamar a atenção da sociedade civil, empresas e estudantes em idade de formação escolar para as questões relacionadas à água e ao saneamento básico. E você pode colaborar, como um Embaixador da Água, abraçando a ideia do Projeto e incentivando todas as pessoas ao seu redor a debater o tema.')?>
		</p>

		<img style="margin-top:48px;margin-right:30px; float:right;" src="_imgs/layout/imagem1_desafios.png" alt="<?=traduz('Desafios para a preservação da água potável do mundo')?>">

	</div>

	<div class="coluna direita">
		<div id="caixa-azul">
			<div>
				<p>
					<?=traduz('A <strong>UNESCO</strong> (Organização das Nações Unidas para a Educação, a Ciência e a Cultura) escolheu <strong>2013</strong> como o <strong>Ano Internacional de Cooperação pela Água</strong>. </p><p>O objetivo é promover uma maior interação entre nações e debater os desafios do manejo da água.</p><p> Segundo a ONU-Água, existe um aumento da demanda pelo acesso, alocação e serviços relacionados a esse bem natural. O ano vai destacar iniciativas de sucesso sobre cooperação pela água.')?>
				</p>
				<p style="margin:0;">
					<?=traduz('Saiba mais em')?>
				</p>
			</div>
		</div>
	</div>

</div>