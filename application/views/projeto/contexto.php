<div class="centro container">

	<div class="coluna esquerda">

		<h1><?=traduz('Preservar a vida de todo o planeta é uma tarefa grande, que começa com ações muito simples')?></h1>

		<p>
			<?=traduz('O descaso do ser humano com a água está gerando preocupações alarmantes. De acordo com o Relatório Planeta Vivo 2010 da World Wildlife Fund (WWF), a Terra já ultrapassou 30% de sua capacidade de reposição dos recursos necessários para as nossas demandas. Porém, ainda podemos tentar reverter esse quadro, por meio de atitudes simples, em nosso dia a dia.')?>
		</p>

		<p>
			<?=traduz('Com a gestão adequada dos recursos hídricos, podemos amenizar o impacto das nossas ações na natureza e, assim, tentar garantir o fornecimento da quantidade e qualidade necessárias, para nós e para as próximas gerações. E você pode começar agora, integrando o grupo de “Embaixadores da Água” do Projeto Hydros.')?>
		</p>

	</div>

	<div class="coluna direita">
		<img src="_imgs/layout/imagem_contexto.png" alt="<?=traduz('')?>">
	</div>

</div>