<div class="centro basico">

	<h1><?=traduz('Política de Privacidade')?></h1>

	<div class="politica">
		<p class="strong">
			<?=traduz('Quais as informações que recolhemos?')?>
		</p>
		<p>
			<?=traduz('Coletamos informações de você quando você se cadastra em nosso site ou subscreve a nossa newsletter.')?>
		</p>
		<p>
			<?=traduz('Ao encomendar ou registar no nosso site, conforme o caso, você pode ser solicitado a digitar o seu: nome, endereço de e-mail ou endereço postal. Você pode, entretanto, visitar nosso site de forma anônima.')?>
		</p>
		<p class="strong">
			<?=traduz('Como usamos as suas informações?')?>
		</p>
		<p>
			<?=traduz('Qualquer uma das informações que coletamos de você pode ser usado para enviar e-mails periódicos.')?>
		</p>
		<p>
			<?=traduz('O endereço de e-mail que você fornecer para processamento de pedidos, pode ser usado para enviar informações e atualizações referentes ao seu fim, além de receber a notícia ocasional da empresa, atualizações, produto relacionado ou informações de serviço, etc')?>
		</p>
		<p class="strong">
			<?=traduz('Como protegemos a sua informação?')?>
		</p>
		<p>
			<?=traduz('Implementamos uma série de medidas de segurança para manter a segurança de suas informações pessoais quando você entra, enviar ou acessar suas informações pessoais.')?>
		</p>
		<p class="strong">
			<?=traduz('Nós usamos cookies?')?>
		</p>
		<p>
			<?=traduz('Sim, nós usamos cookies para melhorar a experiência online do usuário do nosso website. Para quem não sabe, cookies são arquivos que armazenam dados de forma temporária no disco rígido do usuário, para que o website promova uma navegação mais conveniente. Os cookies podem evitar que uma mesma informação seja solicitada mais de uma vez ao mesmo usuário. No caso do nosso website utilizamos cookies com as seguintes finalidades:')?><br>
			<?=traduz('- manter o usuário logado após realizar o login para fazer comentários')?><br>
			<?=traduz('- armazenar qual a linguagem selecionada caso o usuário escolha a língua clicando nos botões do cabeçalho')?><br>
			<?=traduz('- armazenar qual foi a última página acessada sempre, para saber como redirecionar no caso de seleção de um novo idioma')?>
		</p>
		<p class="strong">
			<?=traduz('Nós divulgamos qualquer informação a terceiros?')?>
		</p>
		<p>
			<?=traduz('Nós não vendemos, comercializamos ou transferimos a terceiros as suas informações pessoalmente identificáveis. Isso não inclui terceiros de confiança que nos auxiliam no funcionamento do nosso site, conduzindo nosso negócio, ou serviço para você, desde que as partes concordem em manter esta informação confidencial. Podemos também divulgar as suas informações quando acreditamos que é apropriado para cumprir a lei, fazer cumprir as nossas políticas de site, ou proteger nosso ou de outros direitos, propriedade ou segurança. No entanto, as informações do visitante não identificáveis ​​podem ser fornecidas a terceiros para marketing, publicidade, ou outros usos.')?>
		</p>
		<p class="strong">
			<?=traduz('Links de terceiros')?>
		</p>
		<p>
			<?=traduz('Nós não incluímos ou oferecemos produtos ou serviços de terceiros no nosso site. No entanto, buscamos proteger a integridade do nosso site e agradecemos qualquer feedback sobre o nosso site.')?>
		</p>
		<p class="strong">
			<?=traduz('Política apenas de Privacidade Online')?>
		</p>
		<p>
			<?=traduz('Esta política de privacidade online se aplica somente às informações coletadas através de nosso site e não a informações coletadas offline.')?>
		</p>
		<p class="strong">
			<?=traduz('Termos e Condições')?>
		</p>
		<p>
			<?=traduz('Por favor, visite o nosso <a href="legal" title="Aviso Legal">Aviso Legal</a>, seção que estabelece o uso, renúncias e limitações de responsabilidade que regem a utilização do conteúdo de nosso website em <a href="http://www.projetohydros.com" title="Projeto Hydros">www.projetohydros.com</a>')?>
		</p>
		<p class="strong">
			<?=traduz('Seu consentimento')?>
		</p>
		<p>
			<?=traduz('Ao utilizar nosso site, você concorda com a nossa política de privacidade online.')?>
		</p>
		<p class="strong">
			<?=traduz('Alterações à nossa Política de Privacidade')?>
		</p>
		<p>
			<?=traduz('Se nós decidirmos mudar nossa política de privacidade, vamos atualizar data de modificação da política de privacidade abaixo.')?>
		</p>
		<p>
			<?=traduz('Esta política foi modificada pela última vez em <strong>10/06/2013</strong>.')?>
		</p>
		<p>
			<?=traduz('Esta política está em conformidade da Trust Guard PCI compliance.')?>
		</p>
	</div>
</div>