<div class="centro">

	<div class="metade comfundo">

		<h1><?=traduz('HYDROS INSPIRA')?></h1>

		<h2><?=traduz('Nesse espaço, você encontrará materiais feitos por pessoas que tiveram contato com o Projeto Hydros e ficaram sensibilizadas com a sua mensagem, de alguma forma. Portanto, se tiver algum produto ou manifestação artística que foi fruto do seu contato com o Projeto Hydros e que tenha atingido um determinado grupo de pessoas, como, por exemplo, um livro, vídeo, textos, música, exposição etc, envie para hydros@corecomunicacao.com.br. Nós iremos analisar o seu material e publicar aqui. O importante, realmente, é levar essa ideia adiante!')?></h2>

	</div>

</div>

<div class="borda">
	<div class="centro">
		<div class="metade resultado-premio">
			<h3><?=traduz('Concurso Mexichem Brasil 2012')?></h3>
			<p>
				<?=traduz("A Mexichem Brasil realizou um concurso com as escolas que receberam o Projeto Hydros e foram sensibilizadas por sua mensagem de preservação da água. Os jovens estudantes tinham que criar manifestações artísticas sozinhos ou em grupos. Aqui você confere os vencedores nacionais e o regionais.")?>
			</p>
			
			<h4><?=traduz('Vencedores Nacionais')?></h4>
			<a href="_imgs/inspira/premio/premio1.jpg" title="Thiago Mendes Carvalho e Mayan Silva" target="_blank"><img src="_imgs/inspira/premio/premio1-thumb.jpg" alt="Thiago Mendes Carvalho e Mayan Silva"><br>Thiago Mendes Carvalho e Mayan Silva</a>

			<h4><?=traduz('Vencedores Regionais')?></h4>
			<a href="_imgs/inspira/premio/premio3.jpg" title="Graziela Maria Brito da Costa" target="_blank"><img src="_imgs/inspira/premio/premio3-thumb.jpg" alt="Graziela Maria Brito da Costa"><br>Graziela Maria Brito da Costa<br>[Anápolis]</a>

			<a href="_imgs/inspira/premio/premio4.jpg" title="André Caetano dos Santos" target="_blank"><img src="_imgs/inspira/premio/premio4-thumb.jpg" alt="André Caetano dos Santos"><br>André Caetano dos Santos<br>[Maceió]</a>

			<a href="_imgs/inspira/premio/premio5.jpg" title="Thales Johnef da Silva" target="_blank"><img src="_imgs/inspira/premio/premio5-thumb.jpg" alt="Thales Johnef da Silva"><br>Thales Johnef da Silva<br>[São José dos Campos]</a>

			<a href="_imgs/inspira/premio/premio6.jpg" title="Alunos de 5ª a 8ª Série" target="_blank"><img src="_imgs/inspira/premio/premio6-thumb.jpg" alt="Alunos de 5ª a 8ª Série"><br><?=traduz('Alunos de 5ª a 8ª Série')?><br>[São Paulo]</a>

			<a href="_imgs/inspira/premio/premio7.jpg" title="Fabiana Maria da Silva" target="_blank"><img src="_imgs/inspira/premio/premio7-thumb.jpg" alt="Fabiana Maria da Silva"><br>Fabiana Maria da Silva<br>[Suape]</a>

			<a href="_imgs/inspira/premio/premio8.jpg" title="Giuliano Souto Costa" target="_blank"><img src="_imgs/inspira/premio/premio8-thumb.jpg" alt="Giuliano Souto Costa"><br>Giuliano Souto Costa<br>[Sumaré]</a>

			<a href="_imgs/inspira/premio/premio9.jpg" title="Brenda Gonçalves de Paula" target="_blank"><img src="_imgs/inspira/premio/premio9-thumb.jpg" alt="Brenda Gonçalves de Paula"><br>Brenda Gonçalves de Paula<br>[Uberaba]</a>

			<div style="height:80px;">
				<div class="compartilhar-inspira">
					<?=traduz('Compartilhe!')?>
					<ul>
						<li class="share-email">
							<a href="<?=$share['email']?>" title="<?=traduz('Compartilhe por e-mail!')?>">e-mail</a>
						</li>
						<li class="share-facebook">
							<a rel="nofollow" href="http://www.facebook.com/share.php" onclick="return fbs_click(this)" title="<?=traduz('Compartilhe pelo facebook!')?>">facebook</a>
						</li>
						<li class="share-twitter">
							<a rel="nofollow" href="" onclick="<?=$share['tweet']?>" title="<?=traduz('Compartilhe no Tumblr')?>">twitter</a>
						</li>
						<li class="share-tumblr">
							<a href="http://www.tumblr.com/share/link?url=<?php echo urlencode(current_url())?>&description=<?= urlencode($share['description_tumblr']) ?>&name=<?php echo urlencode($share['tumblr']) ?>" title="<?=traduz('Compartilhe no Tumblr!')?>">tumblr</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="borda">
	<div class="centro">
		<div class="metade">
			<h3><?=traduz('Concurso "O Homem e a água"')?></h3>
			<p>
				<?=traduz("O Grupo Empresarial Kaluz promoveu um concurso cultural de fotografias, em 2012, englobando todos os seus colaboradores ao redor do mundo, com o tema “O homem e a água”. Veja aqui as cinco imagens vencedoras que foram inspiradas também nas mensagens de sensibilização que o Projeto Hydros trouxe ao cotidiano dos profissionais.")?>
			</p>
			<img src="_imgs/inspira/img2-inspiraConcurso.jpg" alt="Hydros Inspira">
			<div class="barra-share-large">
				<a href="_arquivos/inspira/<?=$link_download_homemagua?>" target="_blank" title="<?=traduz('Faça o download!')?>" class="link-download"><?=traduz('Faça o download!')?></a>
			</div>
			<p class="p-verdana">
				<?=traduz('<strong>1º Lugar</strong> - María de los Ángeles Avilés Gamboa, da Mexichem, com o tema “Mar Morto, reviva!”')?><br>
				<?=traduz('<strong>2º Lugar</strong> - Emiel van den Boomen, da Wavin Holanda, com o tema “Banhando nas águas sagradas do templo Tirta Empul em Bali”')?><br>
				<?=traduz('<strong>3º Lugar</strong> - Liliana Echeverri Branch, da Mexichem Colombia S.A.S., com o tema “Os meninos do Pacífico são água”')?><br>
				<?=traduz('<strong>4º Lugar</strong> - Diego Alejandro David Vázquez, da Mexichem, com o tema “Com os pés em mais águas”')?><br>
				<?=traduz('<strong>5º Lugar</strong> - Filemón Peruyero Solís, da Mexichem TI, com o tema “Agua: meio de transporte e sustento diário”')?><br>
			</p>
			<div style="height:80px;">
				<div class="compartilhar-inspira">
					<?=traduz('Compartilhe!')?>
					<ul>
						<li class="share-email">
							<a href="<?=$share['email']?>" title="<?=traduz('Compartilhe por e-mail!')?>">e-mail</a>
						</li>
						<li class="share-facebook">
							<a rel="nofollow" href="http://www.facebook.com/share.php" onclick="return fbs_click(this)" title="<?=traduz('Compartilhe pelo facebook!')?>">facebook</a>
						</li>
						<li class="share-twitter">
							<a rel="nofollow" href="" onclick="<?=$share['tweet']?>" title="<?=traduz('Compartilhe no Tumblr')?>">twitter</a>
						</li>
						<li class="share-tumblr">
							<a href="http://www.tumblr.com/share/link?url=<?php echo urlencode(current_url())?>&description=<?= urlencode($share['description_tumblr']) ?>&name=<?php echo urlencode($share['tumblr']) ?>" title="<?=traduz('Compartilhe no Tumblr!')?>">tumblr</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="borda">

	<div class="centro">

		<div id="down-inspira">
			<?php switch ($this->session->userdata('language')) {
				case 'es':
					echo "<img src='_imgs/inspira/El-don-del-agua_ESP-big.jpg'>";	
					$link_download = "El-don-del-agua_ESP.pdf";
					break;
				case 'en':
					echo "<img src='_imgs/inspira/The-Gift-of-Water_ING-big.jpg'>";
					$link_download = "The-Gift-of-Water_ING.pdf";
					break;
				default:
				case 'pt':
					echo "<img src='_imgs/inspira/O-dom-da-agua_PORT-big.jpg'>";
					$link_download = "O-dom-da-agua_PORT.pdf";
					break;
			}?>
			<div class="barra-share-large">
				<a href="_arquivos/inspira/<?=$link_download?>" target="_blank" title="<?=traduz('Faça o download!')?>" class="link-download"><?=traduz('Faça o download!')?></a>
			</div>
		</div>

		<div class="metade largo">

			<h3><?=traduz('O Dom da Água – Regina Moya')?></h3>
			
			<p>
				<?=traduz("A artista plástica e escritora mexicana Regina Moya foi sensibilizada pelo Projeto Hydros e, a partir dele, escreveu e ilustrou o livro infantil “O Dom da Água”, que você confere aqui na íntegra. Para conhecer um pouco mais sobre o trabalho da artista acesse <a href='http://www.reginamoya.com' target='_blank'>http://www.reginamoya.com</a>.")?>
			</p>

			<div style="height:80px;">
				<div class="compartilhar-inspira">
					<?=traduz('Compartilhe!')?>
					<ul>
						<li class="share-email">
							<a href="<?=$share['email']?>" title="<?=traduz('Compartilhe por e-mail!')?>">e-mail</a>
						</li>
						<li class="share-facebook">
							<a rel="nofollow" href="http://www.facebook.com/share.php" onclick="return fbs_click(this)" title="<?=traduz('Compartilhe pelo facebook!')?>">facebook</a>
						</li>
						<li class="share-twitter">
							<a rel="nofollow" href="" onclick="<?=$share['tweet']?>" title="<?=traduz('Compartilhe no Tumblr')?>">twitter</a>
						</li>
						<li class="share-tumblr">
							<a href="http://www.tumblr.com/share/link?url=<?php echo urlencode(current_url())?>&description=<?= urlencode($share['description_tumblr']) ?>&name=<?php echo urlencode($share['tumblr']) ?>" title="<?=traduz('Compartilhe no Tumblr!')?>">tumblr</a>
						</li>
					</ul>
				</div>
			</div>	
		</div>
	</div>
</div>
