<!doctype html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="pt-BR"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="pt-BR"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="pt-BR"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="pt-BR"> <!--<![endif]-->
<head>
  <meta charset="utf-8">

  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <title><?echo (isset($og) && isset($og['title']) && $og['title']) ? $og['title'] : traduz('Projeto Hydros')?></title>
  <meta name="description" content="<?echo (isset($og) && isset($og['description']) && $og['description']) ? $og['description'] : traduz("O Projeto Hydros tenta mostrar o vínculo entre o planeta e o ser humano em relação à água, do qual ainda não estamos conscientes. Levantamos a questão: 'Como me relaciono com a água?'. Não sabemos. Está na hora de percebermos tudo o que fazemos com ela na construção da realidade de uma pessoa, de uma cidade, de um negócio ou de um país, e como cada uma dessas ações afeta o presente, o hoje, o agora.")?>">
  <meta name="keywords" content="<?=traduz('Projeto Hydros, Água, Sustentabilidade, Consumo de Água, Consumo Consciente, Dia Mundial da Água, Consumo Consciente de Água, Planeta Sustentável')?>" />
  <meta name="robots" content="index, follow" />
  <meta name="author" content="Trupe Design" />
  <meta name="copyright" content="2012 Trupe Design" />

  <meta name="viewport" content="width=device-width,initial-scale=1">

  <meta property="og:site_name" content="<?=traduz('Projeto Hydros')?>"/>
  <meta property="og:type" content="website"/>
  <meta property="og:url" content="<?=current_url()?>"/>
  <meta property="og:title" content="<?echo (isset($og) && isset($og['title']) && $og['title']) ? $og['title'] : traduz("Projeto Hydros")?>"/>
  <meta property="og:image" content="<?echo (isset($og) && isset($og['image']) && $og['image']) ? $og['image'] : base_url('_imgs/layout/'.prefixo('fb').'.jpg')?>"/>
  <meta property="og:description" content="<?echo (isset($og) && isset($og['description']) && $og['description']) ? $og['description'] : traduz("O Projeto Hydros tenta mostrar o vínculo entre o planeta e o ser humano em relação à água, do qual ainda não estamos conscientes. Levantamos a questão: 'Como me relaciono com a água?'. Não sabemos. Está na hora de percebermos tudo o que fazemos com ela na construção da realidade de uma pessoa, de uma cidade, de um negócio ou de um país, e como cada uma dessas ações afeta o presente, o hoje, o agora.")?>"/>

  <meta property="fb:admins" content="100002297057504" />

  <base href="<?= base_url() ?>">
  <script> var BASE = '<?= base_url() ?>'</script>

  <?CSS(array('reset', 'base', 'fontface/fonts', 'fancybox/fancybox', $this->router->class, $load_css))?>
  
  <?if(ENVIRONMENT == 'development'):?>
    
    <?JS(array('modernizr-2.0.6.min', 'less-1.3.0.min', 'jquery-1.8.0.min'))?>
    
  <?else:?>

    <?JS(array('modernizr-2.0.6.min'))?>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/jquery-1.8.0.min.js"><\/script>')</script>

  <?endif;?>

</head>
<body class="<?=$this->session->userdata('language')?>">
<div id="fb-root"></div>
<?
$app_id = '324783017548156';
switch($this->session->userdata('language')){
    case 'pt':
        $app_lang = 'pt_BR';
        break;
    case 'en':
        $app_lang = 'en_US';
        break;
    case 'es':
        $app_lang = 'es_MX';
        break;
    default:
        $app_lang = '';
}
?>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) {return;}
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/<?=$app_lang?>/all.js#xfbml=1&appId=<?=$app_id?>";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
