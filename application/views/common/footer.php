
  </div> <!-- fim da div main -->

  <footer>

    <div class="centro">

      <ul>
        <li class="espaco negrito"><a href="home" title="<?=traduz('HOME')?>"><?=traduz('HOME')?></a></li>

        <li class="negrito"><a href="projeto" title="<?=traduz('PROJETO HYDROS')?>"><?=traduz('PROJETO HYDROS')?></a></li>
        <li><a href="projeto/contexto" title="<?=traduz('contexto')?>">&raquo;&nbsp;<?=traduz('contexto')?></a></li>
        <li><a href="projeto/apresentacao" title="<?=traduz('o que é o projeto?')?>">&raquo;&nbsp;<?=traduz('o que é o projeto?')?></a></li>
        <li><a href="projeto/desafios" title="<?=traduz('quais são os desafios?')?>">&raquo;&nbsp;<?=traduz('quais são os desafios?')?></a></li>
        <li><a href="projeto/apoio" title="<?=traduz('quem apoia')?>">&raquo;&nbsp;<?=traduz('quem apoia?')?></a></li>
        <li><a href="projeto/participar" title="<?=traduz('como eu posso participar?')?>">&raquo;&nbsp;<?=traduz('como eu posso participar?')?></a></li>
      </ul>

      <ul>
        <li class="negrito"><a href="materiais" title="<?=traduz('MATERIAIS')?>"><?=traduz('MATERIAIS')?></a></li>
        <li><a href="materiais/videos" title="<?=traduz('vídeos')?>">&raquo;&nbsp;<?=traduz('vídeos')?></a></li>
        <li><a href="materiais/galeria" title="<?=traduz('galeria de imagens')?>">&raquo;&nbsp;<?=traduz('galeria de imagens')?></a></li>
        <li><a href="materiais/exposicao" title="<?=traduz('exposição hydros')?>">&raquo;&nbsp;<?=traduz('exposição hydros')?></a></li>
        <li><a href="materiais/apoio" title="<?=traduz('material de apoio')?>">&raquo;&nbsp;<?=traduz('material de apoio')?></a></li>
        <li><a href="materiais/cartilha" title="<?=traduz('Cartilha Ação Educativa')?>">&raquo;&nbsp;<?=traduz('Cartilha Projeto Hydros na Escola')?></a></li>
      </ul>

      <ul>
        <li class="negrito"><a href="novidades" title="<?=traduz('FIQUE POR DENTRO')?>"><?=traduz('FIQUE POR DENTRO')?></a></li>
        <li><a href="novidades/recursos" title="<?=traduz('mapa de recursos hídricos no mundo')?>">&raquo;&nbsp;<?=traduz('mapa de recursos hídricos no mundo')?></a></li>
        <li><a href="novidades/noticias" title="<?=traduz('notícias')?>">&raquo;&nbsp;<?=traduz('notícias')?></a></li>
        <li class="espaco"><a href="novidades/giro" title="<?=traduz('giro')?>">&raquo;&nbsp;<?=traduz('giro')?></a></li>
        <li class="negrito espaco"><a href="pegada" title="<?=traduz('PEGADA HYDROS')?>"><?=traduz('PEGADA HYDROS')?></a></li>
        <li class="negrito"><a href="contato" title="<?=traduz('CONTATO')?>"><?=traduz('CONTATO')?></a></li>
      </ul>      

      <ul>
        <li class="negrito"><a href="politica" title="<?=traduz('política de privacidade')?>">&bull;&nbsp;<?=traduz('política de privacidade')?></a></li>
        <li class="negrito"><a href="legal" title="<?=traduz('aviso legal')?>">&bull;&nbsp;<?=traduz('aviso legal')?></a></li>
      </ul>

      <div id="assinatura">
        &copy; <?=Date('Y')?> <?=traduz('Projeto Hydros')?> &bull; <?=traduz('Todos os direitos reservados')?> <br>
        <?=traduz('Coordenação:')?> <a href="" title="Core Comunicação" target="_blank">Core Comunicação</a> <br>
        <?=traduz('Criação de sites:')?> <a href="http://www.trupe.net" title="Criação de sites: Trupe Agência Criativa" target="_blank">Trupe Agência Criativa</a>
      </div>

    </div>
  
  </footer>
  
  <script type="text/javascript">
   
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-<?=$analytics?>']);
    _gaq.push(['_trackPageview']);
   
    (function() {
      var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
      ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
      var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();
   
  </script>
  
  <script src="http://platform.tumblr.com/v1/share.js"></script>
  <?JS(array('jtwt.min', 'cycle', 'fancybox', 'qtip/jquery.qtip-1.0.0-rc3.min', 'jquery.tinycarousel.min' ,'jquery-ui-1.10.1.custom.min', 'imagesloaded', 'front', $this->router->class, $load_js))?>

</body>
</html>
