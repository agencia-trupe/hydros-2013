<header>
<?php
switch ($this->session->userdata('language')) {
    case 'es':
        $linktwitter = "https://twitter.com/ProyectoHydros";
        $linktumblr = "http://proyectohydros.tumblr.com/";
        $linkfacebook = "https://www.facebook.com/ProyectoHydros";
        break;
    case 'en':
        $linktwitter = "https://twitter.com/HydrosProject";
        $linktumblr = "http://projecthydros.tumblr.com/";
        $linkfacebook = "https://www.facebook.com/ProjectHydros";
        break;
    case 'pt':
    default:
        $linktwitter = "https://twitter.com/ProjetoHydros";
        $linktumblr = "http://projetohydros.tumblr.com/";
        $linkfacebook = "https://www.facebook.com/ProjetoHydros?sk=wall";
        break;
}
?>
 	<div class="superior">
		<div class="centro <?=$this->session->userdata('language')?>">

			<div id="language-box">
				<ul>
					<li id="lang-pt-link"><a href="linguagem/index/pt" <?if($this->session->userdata('language') == 'pt')echo" class='ativo'"?> title="versão em português">português</a></li>
					<li id="lang-es-link"><a href="linguagem/index/es" <?if($this->session->userdata('language') == 'es')echo" class='ativo'"?> title="versión en español">español</a></li>
					<li id="lang-en-link"><a href="linguagem/index/en" <?if($this->session->userdata('language') == 'en')echo" class='ativo'"?> title="english version">english</a></li>
				</ul>
			</div>

			<div id="share-box">
				<?=traduz('Compartilhe e acompanhe o Projeto Hydros em todas as redes sociais!')?>
				<ul>
					<li class="share-email"><a href="mailto:?subject=<?=traduz("Conheça o Projeto Hydros")?>&body=<?=traduz("O Projeto Hydros é uma iniciativa que tem o objetivo de disseminar a conscientização sobre temas relacionados à água e ao desperdício dos nossos recursos naturais. Se você quer fazer parte do nosso time e passar a ideia adiante, torne-se um Embaixador da Água! É super rápido e fácil, e você já estará fazendo a diferença com apenas um clique. .... www.projetohydros.com")?>" title="e-mail">e-mail</a></li>
					<li class="share-facebook"><a target="_blank" href="<?=$linkfacebook?>" title="facebook">facebook</a></li>
					<li class="share-twitter"><a target="_blank" href="<?=$linktwitter?>" title="twitter">twitter</a></li>
					<li class="share-tumblr"><a target="_blank" href="<?=$linktumblr?>" title="tumblr">tumblr</a></li>
					<li class="share-vimeo"><a target="_blank" href="https://vimeo.com/user9750540" title="vimeo">vimeo</a></li>
					<li class="share-youtube"><a target="_blank" href="http://www.youtube.com/user/ProjetoHydros" title="youtube">youtube</a></li>
				</ul>
			</div>

			<nav>
				<ul class="superior">
					<li><a href="home" id="mn-home" <?if($this->router->class=='home')echo" class='ativo'"?>><?=traduz('HOME')?></a></li>
					<li><a href="projeto" id="mn-projeto" <?if($this->router->class=='projeto')echo" class='ativo'"?>><?=traduz('PROJETO HYDROS')?></a></li>
					<li><a href="materiais" id="mn-materiais" <?if($this->router->class=='materiais')echo" class='ativo'"?>><?=traduz('MATERIAIS')?></a></li>
					<li class="ultimo"><a href="contato" id="mn-contato" <?if($this->router->class=='contato')echo" class='ativo'"?>><?=traduz('CONTATO')?></a></li>
				</ul>

			</nav>
		</div> <!-- Fecha centro -->
	</div> <!-- Fecha superior -->

	<div class="superior-sub submenu <?if($this->router->class!='projeto')echo "hid"?>" id="sub-projeto">
		<ul class="centro">
			<li><a <?if($this->router->class=='projeto' && $this->router->method=='contexto')echo "class='ativo'"?> href="projeto/contexto" title=""><?=traduz('Contexto')?></a></li>
			<li><a <?if($this->router->class=='projeto' && $this->router->method=='apresentacao')echo "class='ativo'"?> href="projeto/apresentacao" title=""><?=traduz('O que é o projeto?')?></a></li>
			<li><a <?if($this->router->class=='projeto' && $this->router->method=='desafios')echo "class='ativo'"?> href="projeto/desafios" title=""><?=traduz('Quais são os desafios?')?></a></li>
			<li><a <?if($this->router->class=='projeto' && $this->router->method=='apoio')echo "class='ativo'"?> href="projeto/apoio" title=""><?=traduz('Quem apoia?')?></a></li>
			<li><a  class="ultimo<?if($this->router->class=='projeto' && $this->router->method=='participar')echo " ativo"?>" href="projeto/participar" title=""><?=traduz('Como eu posso participar?')?></a></li>
		</ul>
	</div>

	<div class="superior-sub submenu <?if($this->router->class!='materiais')echo "hid"?>" id="sub-materiais">
		<ul class="centro">
			<li><a <?if($this->router->class=='materiais' && $this->router->method=='videos')echo "class='ativo'"?> href="materiais/videos" title=""><?=traduz('Vídeos')?></a></li>
			<li><a <?if($this->router->class=='materiais' && ($this->router->method=='galeria' || $this->router->method=='imagem'))echo "class='ativo'"?> href="materiais/galeria" title=""><?=traduz('Galeria de Imagens')?></a></li>
			<li><a <?if($this->router->class=='materiais' && $this->router->method=='exposicao')echo "class='ativo'"?> href="materiais/exposicao" title=""><?=traduz('Exposição Hydros')?></a></li>
			<li><a <?if($this->router->class=='materiais' && ($this->router->method=='apoio' || $this->router->method=='material'))echo "class='ativo'"?> href="materiais/apoio" title=""><?=traduz('Material de Apoio')?></a></li>
			<li><a class="ultimo<?if($this->router->class=='materiais' && $this->router->method=='cartilha')echo " ativo"?>" href="materiais/cartilha" title=""><?=traduz('Cartilha Projeto Hydros Na Escola')?></a></li>
		</ul>
	</div>

	<div class="inferior">
		<div class="centro">
			<nav>
				<ul class="inferior">
					<li><a href="inspira"  id="mn-inspira" <?if($this->router->class=='inspira')echo" class='ativo'"?>><?=traduz('HYDROS E VOCÊ')?></a></li>
					<li><a href="novidades" id="mn-novidades" <?if($this->router->class=='novidades')echo" class='ativo'"?>><?=traduz('FIQUE POR DENTRO')?></a></li>
					<li class="ultimo"><a href="pegada" id="mn-pegada" <?if($this->router->class=='pegada')echo" class='ativo'"?>><?=traduz('PEGADA HYDROS')?></a></li>
				</ul>
			</nav>
		</div>
	</div>

	<div class="inferior-sub submenu <?if($this->router->class!='inspira')echo "hid"?>" id="sub-inspira">
		<ul class="centro">
			<li><a <?if($this->router->class=='inspira' && $this->router->method!='aquavitae' && $this->router->method!='concursocultural')echo "class='ativo'"?> href="inspira/index" title=""><?=traduz('Hydros Inspira')?></a></li>
			<li><a class="ultimo<?if($this->router->class=='inspira' && $this->router->method=='aquavitae')echo " ativo"?>" href="inspira/aquavitae" title=""><?=traduz('Revista Aqua Vitae')?></a></li>
		</ul>
	</div>

	<div class="inferior-sub submenu <?if($this->router->class!='novidades')echo "hid"?>" id="sub-novidades">
		<ul class="centro">
			<li><a <?if($this->router->class=='novidades' && $this->router->method=='recursos')echo "class='ativo'"?> href="novidades/recursos" title=""><?=traduz('Mapa de Recursos Hídricos no Mundo')?></a></li>
			<li><a <?if($this->router->class=='novidades' && ($this->router->method=='noticias' || $this->router->method=='ler'))echo "class='ativo'"?> href="novidades/noticias" title=""><?=traduz('Notícias da Água')?></a></li>
			<li><a class="ultimo<?if($this->router->class=='novidades' && ($this->router->method=='giro' || $this->router->method=='ler_giro'))echo " ativo"?>" href="novidades/giro" title=""><?=traduz('Giro pelo Projeto Hydros')?></a></li>
		</ul>
	</div>

</header>

<div class="main main-<?=$this->router->class?> main-<?=$this->router->class?>-<?=$this->router->method?> <?if($this->router->class!='home')echo"internas"?>">