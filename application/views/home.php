	<div id="faixa-slides">

	<div class="centro">

		<div id="animate">
			<?php foreach ($slides as $key => $value): ?>
				<div class="slide" <?if($key > 0)echo "style='display:none;'"?>>
					<img src="_imgs/home/<?=$value->imagem?>">
					<div class="frase"><a href="<?=$value->link?>" title="<?=$value->titulo?>"><?=$value->titulo?></a></div>
				</div>
			<?php endforeach ?>
		</div>

		<div id="chamada">
			<?php foreach ($chamadas as $key => $value): ?>
				<div class="conteudo-chamada" <?if($key > 0)echo"style='display:none;'"?>>
					<h2><?=$value->titulo?></h2>
					<p>
						<?=$value->texto?>
					</p>
					<a href="<?=$value->link?>" title="<?=$value->botao?>"><?=$value->botao?></a>
				</div>
			<?php endforeach ?>
		</div>

	</div>

</div>

<div id="paginacao">
	<div class="centro">
		<div class="pad">
		</div>
	</div>
</div>

<div id="faixa-materiais">

	<div class="centro">

		<div class="caixa-texto">
			<h2><?=traduz('Materiais para download')?></h2>
			<p>
				<?=traduz('Com os MATERIAIS DISPONÍVEIS você pode realizar ações concretas e se tornar um verdadeiro EMBAIXADOR DA ÁGUA!')?>
			</p>
		</div>

		<div class="caixa-links <?echo ($this->agent->is_mobile()) ? "ipad" : "noipad"?>">

			<a href="materiais/videos" title="<?=traduz('Vídeos')?>">
				<div class="imagem">
					<img src="_imgs/layout/img-home-videos.jpg">
				</div>
				<div class="texto">
					<?=traduz('vídeos')?>
				</div>
			</a>

			<a href="materiais/galeria" title="<?=traduz('Galeria de Imagens')?>">
				<div class="imagem">
					<img src="_imgs/layout/img-home-imagens.jpg">
				</div>
				<div class="texto">
					<?=traduz('galeria de <br> imagens')?>
				</div>
			</a>

			<a href="materiais/exposicao" title="<?=traduz('Exposição Hydros')?>">
				<div class="imagem">
					<img src="_imgs/layout/img-home-exposicoes.jpg">
				</div>
				<div class="texto">
					<?=traduz('exposição <br> hydros')?>
				</div>
			</a>

			<a href="materiais/apoio" class="semmargem" title="<?=traduz('Material de Apoio')?>">
				<div class="imagem">
					<img src="_imgs/layout/img-home-materialapoio.jpg">
				</div>
				<div class="texto">
					<?=traduz('material de <br> apoio')?>
				</div>
			</a>

		</div>

	</div>
</div>

<div id="faixa-interacao">

	<div class="centro">

		<div class="coluna esquerda">
			<a href="materiais/cartilha" class="chamada-cartilha" title="<?=traduz('Saiba como usar o Projeto Hydros na sua escola')?>">
				<h3><?=traduz('Saiba como usar o Projeto Hydros na sua escola')?></h3>
				<?php
				switch ($this->session->userdata('language')) {
					case 'en': echo "<img src='_imgs/layout/capa-tutorial-home-en.jpg' alt='".traduz('Saiba como usar o Projeto Hydros na sua escola')."'>"; break;
					case 'es': echo "<img src='_imgs/layout/capa-tutorial-home-es.jpg' alt='".traduz('Saiba como usar o Projeto Hydros na sua escola')."'>"; break;
					default:
					case 'pt': echo "<img src='_imgs/layout/capa-tutorial-home-pt.jpg' alt='".traduz('Saiba como usar o Projeto Hydros na sua escola')."'>"; break;
				}
				?>

				<p>
					<?=traduz('O tutorial “Como usar o Projeto Hydros na escola” é o material ideal para que você, profissional da área da Educação, saiba como utilizar a cartilha Projeto Hydros na Escola e possa conscietizar os seus alunos sobre o tema água.')?>
				</p>
			</a>

			<div id="caixa-twitter">
				<div id="fundo-azul"></div>
				<div class="coluna-esquerda">
					<h4><?=traduz('Siga-nos!')?></h4>
				</div>
				<div class="coluna-direita" id="lista-tweets">
					<?php if ($this->session->userdata('language') == 'es'): ?>
						<a class="twitter-timeline" data-chrome="noheader noborders" href="https://twitter.com/ProyectoHydros" data-widget-id="346674912448114688">Tweets de @ProyectoHydros</a>
						<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
					<?php elseif($this->session->userdata('language') == 'en'):?>
						<a class="twitter-timeline" href="https://twitter.com/HydrosProject" data-widget-id="346675611416268800">Tweets de @HydrosProject</a>
						<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
					<?php else: ?>
						<a class="twitter-timeline" data-chrome="noheader noborders" href="https://twitter.com/<?=traduz('projetoHydros')?>" data-widget-id="346672385015042048">Tweets de @<?=traduz('projetoHydros')?></a>
						<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
					<?php endif; ?>
				</div>
			</div>
		</div>

		<div class="coluna direita">

			<div class="chamadas">

				<h2><?=traduz('FIQUE POR DENTRO')?></h2>

				<a href="novidades/ler/<?=$noticia[0]->slug?>" class="chamada-home" title="<?=$noticia[0]->titulo?>">
					<h3><?=traduz('Notícias da água')?></h3>
					<?php if ($noticia[0]->imagem): ?>
						<img src="_imgs/noticias/home/<?=$noticia[0]->imagem?>">
					<?php endif ?>
					<div class="texto">
						<p class="titulo"><?=$noticia[0]->titulo?></p>
						<p>
							<?=word_limiter($noticia[0]->olho, 20)?>
						</p>
						<p class="saiba-mais"><?=traduz('leia mais')?> &raquo</p>
					</div>
				</a>

				<a href="novidades/recursos" class="chamada-home" title="<?=traduz('Mapa de recursos hídricos no mundo')?>">
					<h3><?=traduz('Mapa de recursos hídricos no mundo')?></h3>
					<img src="_imgs/layout/img-mapa-home.jpg">
					<div class="texto">
						<p class="titulo"></p>
						<p>
							<?=traduz('Veja aqui dados sobre os recursos hídricos de cada país, além de descobrir como esses recursos são utilizados em cada região.')?>
						</p>
						<p class="saiba-mais"><?=traduz('leia mais')?> &raquo</p>
					</div>
				</a>

			</div>

<?php
switch ($this->session->userdata('language')) {
    case 'es':
        $linkfacebook = "https://www.facebook.com/ProyectoHydros";
        break;
    case 'en':
        $linkfacebook = "https://www.facebook.com/ProjectHydros";
        break;
    case 'pt':
    default:
        $linkfacebook = "https://www.facebook.com/ProjetoHydros?sk=wall";
        break;
}
?>

			<div id="facebook-like">
				<div class="fb-like-box" data-href="<?=$linkfacebook?>" data-width="551" data-show-faces="true" data-stream="false" data-header="false" data-border-color="#fff"></div>
			</div>

			<div id="caixa-newsletter">

				<h4><?=traduz('Cadastre-se para receber novidades sobre o PROJETO HYDROS!')?></h4>

				<form method="post" action="" id="form-newsletter">
					<div id="move-newsletter">
						<input type="text" name="nome" id="input-nome" placeholder="<?=traduz('nome')?>" required>
						<input type="email" name="email" id="input-email" placeholder="<?=traduz('e-mail')?>">
						<input type="hidden" name="idioma" id="input-idioma" value="<?=$this->session->userdata('language')?>">
						<input type="submit" value="<?=traduz('ENVIAR')?>">
					</div>
					<div id="resposta-newsletter"></div>
				</form>

			</div>

		</div>

	</div>

</div>