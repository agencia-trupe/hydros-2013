<div class="centro">

	<h1><?=traduz('Contato')?></h1>

	<div id="resposta">
		<?=traduz('Obrigado por entrar em contato!<br>Responderemos assim que possível.')?>		
	</div>

	<div class="form">

		<form method="post" action="contato/enviar" id="form-contato">

			<label>
				<?=traduz('nome')?><br>
				<input type="text" name="nome" id="input-nome" required>
			</label>

			<label>
				<?=traduz('e-mail')?><br>
				<input type="email" name="email" id="input-email" required>
			</label>

			<label>
				<?=traduz('telefone')?><br>
				<input type="text" name="telefone" id="input-telefone">
			</label>

			<label>
				<?=traduz('mensagem')?><br>
				<textarea name="mensagem" id="input-mensagem" required></textarea>
			</label>

			<label class="check">
				<?=traduz('Quer receber nossa newsletter com todas as novidades e informações sobre o Projeto Hydros?')?><br>
				<label><input type="radio" name="newsletter" value="1" checked="true"> <?=traduz('SIM')?></label>
				<label><input type="radio" name="newsletter" value="0"> <?=traduz('NÃO')?></label>
			</label>

			<input type="text" name="address" style="height:0; padding:0; border:0; margin:0; overflow:hidden;">

			<input type="submit" value="<?=traduz('ENVIAR')?>">

		</form>

	</div>

</div>

<?php if ($this->session->flashdata('envio_status')): ?>
<script defer>
$('document').ready( function(){

	$('#resposta').fadeIn('slow', function(){
		$('#resposta').delay(5000).fadeOut('slow');
	})

});
</script>
<?php endif ?>