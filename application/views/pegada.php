<div class="centro">

	<div class="metade">

		<h1><?=traduz('PEGADA HYDROS')?></h1>

		<h2><?=traduz('Já imaginou calcular a quantidade de água que você gasta no seu dia a dia, ao lavar as mãos ou escovar os dentes, por exemplo?')?></h2>

		<p>
			<?=traduz('Agora você pode descobrir a sua pegada hídrica com o aplicativo do Projeto Hydros, o Pegada Hydros. Basta ter um tablet ou smartphone que tenha App Store (iPhone) ou Google Play (Android) como plataforma de distribuição de aplicativo e fazer o download gratuito do App.')?>
		</p>

		<div style="height:80px;">
			<div id="compartilhar-pegada1" class="compartilhar-pegada">
				<?=traduz('Compartilhe!')?>
				<ul>
					<li class="share-email">
						<a href="<?=$share['email']?>" title="<?=traduz('Compartilhe por e-mail!')?>">e-mail</a>
					</li>
					<li class="share-facebook">
						<a rel="nofollow" href="http://www.facebook.com/share.php" onclick="return fbs_click(this)" title="<?=traduz('Compartilhe pelo facebook!')?>">facebook</a>
					</li>
					<li class="share-twitter">
						<a rel="nofollow" href="" onclick="<?=$share['tweet']?>" title="<?=traduz('Compartilhe no Twitter')?>">twitter</a>
					</li>
					<li class="share-tumblr">
						<a href="http://www.tumblr.com/share/link?url=<?php echo urlencode(current_url())?>&description=<?= urlencode($share['description_tumblr']) ?>&name=<?php echo urlencode($share['tumblr']) ?>" title="<?=traduz('Compartilhe no Tumblr!')?>">tumblr</a>
					</li>
				</ul>
			</div>
		</div>			

	</div>

<?php
switch ($this->session->userdata('language')) {
	case 'en':
		$link_googleplay = "https://play.google.com/store/apps/details?id=com.intuitiveappz.hydros&feature=search_result&hl=en";
		$link_appstore = "https://itunes.apple.com/us/app/pegada-hydros/id646515029?ls=1&mt=8";
		break;
	case 'es':
		$link_googleplay = "https://play.google.com/store/apps/details?id=com.intuitiveappz.hydros&feature=search_result&hl=es";
		$link_appstore = "https://itunes.apple.com/us/app/pegada-hydros/id646515029?ls=1&mt=8";
		break;
	default:
	case 'pt':
		$link_googleplay = "https://play.google.com/store/apps/details?id=com.intuitiveappz.hydros&feature=search_result&hl=pt_BR";
		$link_appstore = "https://itunes.apple.com/us/app/pegada-hydros/id646515029?ls=1&mt=8";
		break;	
}
?>

	<div class="metade fundo-img <?=$this->session->userdata('language')?>">

		<a href="<?=$link_googleplay?>" target="_blank" id="google-play" title="<?=traduz('Google Play')?>"><?=traduz('Google Play')?></a>

		<a href="<?=$link_appstore?>" id="app-store" title="<?=traduz('App Store')?>"><?=traduz('App Store')?></a>

	</div>

</div>

<div class="borda">

	<div class="centro">

<?php
switch ($this->session->userdata('language')) {
	case 'en':
		$link_googleplay = "https://play.google.com/store/apps/details?id=com.intuitiveappz.gamehydros";
		$link_appstore = "https://itunes.apple.com/us/app/game-hydros/id651149694?ls=1&mt=8";
		$frase = "Learn how to properly use clean and water for reuse in the shortest time possible. Download the Hydros Game on your iOS or Android!";
		$titulo = "Hydros Game";
		break;
	case 'es':
		$link_googleplay = "https://play.google.com/store/apps/details?id=com.intuitiveappz.gamehydros";
		$link_appstore = "https://itunes.apple.com/us/app/game-hydros/id651149694?ls=1&mt=8";
		$frase = "Aprende a utilizar correctamente el agua limpia y para reutilización en el menor tiempo posible. Descarga el Game Hydros en tu iOS o Android!";
		$titulo = "Game Hydros";
		break;
	default:
	case 'pt':
		$link_googleplay = "https://play.google.com/store/apps/details?id=com.intuitiveappz.gamehydros";
		$link_appstore = "https://itunes.apple.com/us/app/game-hydros/id651149694?ls=1&mt=8";
		$frase = "Aprenda como utilizar corretamente água limpa e de reúso, no menor tempo possível. Baixe o Game Hydros no seu iOS ou Android!";
		$titulo = "Game Hydros";
		break;	
}
?>
		<div class="metade fundo-img2  <?=$this->session->userdata('language')?>">
			<a href="<?=$link_googleplay?>" target="_blank" id="google-play" title="<?=traduz('Google Play')?>"><?=traduz('Google Play')?></a>

			<a href="<?=$link_appstore?>" id="app-store" title="<?=traduz('App Store')?>"><?=traduz('App Store')?></a>		
		</div>
		<div class="metade largo">
			<h2><?=traduz('Game Hydros')?></h2>
			
			<p>
				<?=traduz('O Game Hydros ensina, de forma divertida e interativa, as formas de captação, reuso e descarte de água. Além disso, ele se passa em algumas das cidades que representam os locais de atuação do Projeto Hydros: Cidade do México, São Paulo, Nova Iorque, Tóquio e Amsterdã.  Em cada cidade, são 5 fases, que ficam mais difíceis, de acordo com a evolução do usuário no jogo.')?>
			</p>

			<p>
				<?=traduz('Usando o sensor de gravidade dos smartphones e tablets, o jogador direciona o fluxo da água pelos canos, para os lugares da casa em que cada tipo de água deve ser utilizado - de chuveiros e pias a irrigação de hortas e abastecimento de reservatórios de água de reuso.')?>
			</p>

			<p>
				<?=traduz('A partir de agora, você também tem o destino da água do planeta em suas mãos!')?>
			</p>

			<div style="height:80px;">
				<div id="compartilhar-pegada2" class="compartilhar-pegada">
					<?=traduz('Compartilhe!')?>
					<ul>
						<li class="share-email">
							<a href="mailto:?subject=<?=urlencode($titulo)?>&body=<?=urlencode($frase)?>" title="<?=traduz('Compartilhe por e-mail!')?>">e-mail</a>
						</li>
						<li class="share-facebook">
							<a rel="nofollow" href="http://www.facebook.com/share.php" onclick="return fbs_click_game(this,'<?=$frase?>','<?=$titulo?>', '<?=prefixo('')?>')" title="<?=traduz('Compartilhe pelo facebook!')?>">facebook</a>
						</li>
						<li class="share-twitter">
							<a rel="nofollow" href="" onclick="window.open('http://twitter.com/home?status=<?=$frase?>','Twitter','toolbar=0,status=0,width=626,height=436'); return false;" title="<?=traduz('Compartilhe no Twitter')?>">twitter</a>							
						</li>
						<li class="share-tumblr">
							<a href="http://www.tumblr.com/share/link?url=<?php echo urlencode(current_url())?>&description=<?= urlencode($frase) ?>&name=<?php echo urlencode($titulo) ?>" title="<?=traduz('Compartilhe no Tumblr!')?>">tumblr</a>
						</li>
					</ul>
				</div>
			</div>	

		</div>

	</div>

</div>