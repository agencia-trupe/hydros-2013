<div class="centro">
	<div class="metade">
		<h3><?=traduz('Veja imagens das manifestações artísticas de pequenos embaixadores.')?></h3>
		<p>
			<?=traduz("Por meio de um concuso cultural na Argentina, pequenos Embaixadores da Água foram sensibilizados e inspirados pela mensagem que o Projeto Hydros traz às pessoas. Aqui, você pode ver os vencedores segurando suas verdadeiras obras de arte!")?>
		</p>
		<img src="_imgs/inspira/concurso-argentina.jpg" alt="" />
	</div>
</div>