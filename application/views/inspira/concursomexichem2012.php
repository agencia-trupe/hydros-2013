<div class="centro">
	<div class="metade resultado-premio">
		<h3><?=traduz('Concurso Mexichem Brasil 2012')?></h3>
		<p>
			<?=traduz("A Mexichem Brasil realizou um concurso com as escolas que receberam o Projeto Hydros e foram sensibilizadas por sua mensagem de preservação da água. Os jovens estudantes tinham que criar manifestações artísticas sozinhos ou em grupos. Aqui você confere os vencedores nacionais e o regionais.")?>
		</p>
		
		<h4><?=traduz('Vencedores Nacionais')?></h4>
		<a href="_imgs/inspira/premio/premio1.jpg" title="Thiago Mendes Carvalho e Mayan Silva" target="_blank"><img src="_imgs/inspira/premio/premio1-thumb.jpg" alt="Thiago Mendes Carvalho e Mayan Silva"><br>Thiago Mendes Carvalho e Mayan Silva</a>

		<h4><?=traduz('Vencedores Regionais')?></h4>
		<a href="_imgs/inspira/premio/premio3.jpg" title="Graziela Maria Brito da Costa" target="_blank"><img src="_imgs/inspira/premio/premio3-thumb.jpg" alt="Graziela Maria Brito da Costa"><br>Graziela Maria Brito da Costa<br>[Anápolis]</a>

		<a href="_imgs/inspira/premio/premio4.jpg" title="André Caetano dos Santos" target="_blank"><img src="_imgs/inspira/premio/premio4-thumb.jpg" alt="André Caetano dos Santos"><br>André Caetano dos Santos<br>[Maceió]</a>

		<a href="_imgs/inspira/premio/premio5.jpg" title="Thales Johnef da Silva" target="_blank"><img src="_imgs/inspira/premio/premio5-thumb.jpg" alt="Thales Johnef da Silva"><br>Thales Johnef da Silva<br>[São José dos Campos]</a>

		<a href="_imgs/inspira/premio/premio6.jpg" title="Alunos de 5ª a 8ª Série" target="_blank"><img src="_imgs/inspira/premio/premio6-thumb.jpg" alt="Alunos de 5ª a 8ª Série"><br><?=traduz('Alunos de 5ª a 8ª Série')?><br>[São Paulo]</a>

		<a href="_imgs/inspira/premio/premio7.jpg" title="Fabiana Maria da Silva" target="_blank"><img src="_imgs/inspira/premio/premio7-thumb.jpg" alt="Fabiana Maria da Silva"><br>Fabiana Maria da Silva<br>[Suape]</a>

		<a href="_imgs/inspira/premio/premio8.jpg" title="Giuliano Souto Costa" target="_blank"><img src="_imgs/inspira/premio/premio8-thumb.jpg" alt="Giuliano Souto Costa"><br>Giuliano Souto Costa<br>[Sumaré]</a>

		<a href="_imgs/inspira/premio/premio9.jpg" title="Brenda Gonçalves de Paula" target="_blank"><img src="_imgs/inspira/premio/premio9-thumb.jpg" alt="Brenda Gonçalves de Paula"><br>Brenda Gonçalves de Paula<br>[Uberaba]</a>

		<div style="height:80px;">
			<div class="compartilhar-inspira">
				<?=traduz('Compartilhe!')?>
				<ul>
					<li class="share-email">
						<a href="<?=$share['email']?>" title="<?=traduz('Compartilhe por e-mail!')?>">e-mail</a>
					</li>
					<li class="share-facebook">
						<a rel="nofollow" href="http://www.facebook.com/share.php" onclick="return fbs_click(this)" title="<?=traduz('Compartilhe pelo facebook!')?>">facebook</a>
					</li>
					<li class="share-twitter">
						<a rel="nofollow" href="" onclick="<?=$share['tweet']?>" title="<?=traduz('Compartilhe no Tumblr')?>">twitter</a>
					</li>
					<li class="share-tumblr">
						<a href="http://www.tumblr.com/share/link?url=<?php echo urlencode(current_url())?>&description=<?= urlencode($share['description_tumblr']) ?>&name=<?php echo urlencode($share['tumblr']) ?>" title="<?=traduz('Compartilhe no Tumblr!')?>">tumblr</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>