<div class="borda">
	<div class="centro">
		<div class="metade menuinspira">
			<a href="inspira/concursoargentina" title="<?=traduz('Veja imagens das manifestações artísticas de pequenos embaixadores.')?>">
				<img src="_imgs/inspira/thumbs/thumb-concursoargentina.png" alt="<?=traduz('Veja imagens das manifestações artísticas de pequenos embaixadores.')?>">
				<br /><?=traduz('Veja imagens das manifestações artísticas de pequenos embaixadores.')?>
			</a>
			<a href="inspira/planeta" title="<?=traduz('Música "Planeta" tem inspiração no Hydros')?>">
				<img src="_imgs/inspira/thumbs/thumb-planeta.png" alt="<?=traduz('Música "Planeta" tem inspiração no Hydros')?>">
				<br /><?=traduz('Música "Planeta" tem inspiração no Hydros')?>
			</a>
			<a href="inspira/concursomexichem2012" title="<?=traduz('Concurso Mexichem Brasil 2012')?>" class="ultimo">
				<img src="_imgs/inspira/thumbs/thumb-concursomexichem2012.png" alt="<?=traduz('Concurso Mexichem Brasil 2012')?>">
				<br /><?=traduz('Concurso Mexichem Brasil 2012')?>
			</a>
			<a href="inspira/concursohomemeagua" title="<?=traduz('Concurso "O Homem e a água"')?>">
				<img src="_imgs/inspira/thumbs/thumb-concursohomemeagua.png" alt="<?=traduz('Concurso "O Homem e a água"')?>">
				<br /><?=traduz('Concurso "O Homem e a água"')?>
			</a>
			<a href="inspira/domdaagua" title="<?=traduz('O Dom da Água – Regina Moya')?>">
				<img src="_imgs/inspira/thumbs/thumb-domdaagua.png" alt="<?=traduz('O Dom da Água – Regina Moya')?>">
				<br /><?=traduz('O Dom da Água – Regina Moya')?>
			</a>
		</div>
	</div>
</div>