<div class="centro">
	<div id="down-inspira">
		<?php switch ($this->session->userdata('language')) {
			case 'es':
				echo "<img src='_imgs/inspira/El-don-del-agua_ESP-big.jpg'>";	
				$link_download = "El-don-del-agua_ESP.pdf";
				break;
			case 'en':
				echo "<img src='_imgs/inspira/The-Gift-of-Water_ING-big.jpg'>";
				$link_download = "The-Gift-of-Water_ING.pdf";
				break;
			default:
			case 'pt':
				echo "<img src='_imgs/inspira/O-dom-da-agua_PORT-big.jpg'>";
				$link_download = "O-dom-da-agua_PORT.pdf";
				break;
		}?>
		<div class="barra-share-large">
			<a href="_arquivos/inspira/<?=$link_download?>" target="_blank" title="<?=traduz('Faça o download!')?>" class="link-download"><?=traduz('Faça o download!')?></a>
		</div>
	</div>

	<div class="metade largo">

		<h3><?=traduz('O Dom da Água – Regina Moya')?></h3>
		
		<p>
			<?=traduz("A artista plástica e escritora mexicana Regina Moya foi sensibilizada pelo Projeto Hydros e, a partir dele, escreveu e ilustrou o livro infantil “O Dom da Água”, que você confere aqui na íntegra. Para conhecer um pouco mais sobre o trabalho da artista acesse <a href='http://www.reginamoya.com' target='_blank'>http://www.reginamoya.com</a>.")?>
		</p>

		<div style="height:80px;">
			<div class="compartilhar-inspira">
				<?=traduz('Compartilhe!')?>
				<ul>
					<li class="share-email">
						<a href="<?=$share['email']?>" title="<?=traduz('Compartilhe por e-mail!')?>">e-mail</a>
					</li>
					<li class="share-facebook">
						<a rel="nofollow" href="http://www.facebook.com/share.php" onclick="return fbs_click(this)" title="<?=traduz('Compartilhe pelo facebook!')?>">facebook</a>
					</li>
					<li class="share-twitter">
						<a rel="nofollow" href="" onclick="<?=$share['tweet']?>" title="<?=traduz('Compartilhe no Tumblr')?>">twitter</a>
					</li>
					<li class="share-tumblr">
						<a href="http://www.tumblr.com/share/link?url=<?php echo urlencode(current_url())?>&description=<?= urlencode($share['description_tumblr']) ?>&name=<?php echo urlencode($share['tumblr']) ?>" title="<?=traduz('Compartilhe no Tumblr!')?>">tumblr</a>
					</li>
				</ul>
			</div>
		</div>	
	</div>
</div>