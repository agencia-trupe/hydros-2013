<div class="centro">

	<div class="metade comfundo aqua">

		<h1><?=traduz('REVISTA AQUA VITAE &bull; A INSPIRAÇÃO HYDROS')?></h1>

		<p><?=traduz('A Aqua Vitae é uma revista criada pelo Grupo Empresarial Kaluz com o objetivo de debater ideias sobre a problemática do recurso hídrico.  Por isso, promoveu como uma de suas linhas de atuação, identificar especialistas mundiais sobre o tema, que encontram nas páginas da publicação a possibilidade de fortalecer com seus pensamentos, investigações e aportes, uma discussão séria e proativa acerca das diferentes facetas que o recurso hídrico tem para o desenvolvimento dos seres humanos, em harmonia com seu uso, manejo e conservação. Disponibilizamos as edições lançadas e que inspiraram a série fotográfica Hydros.')?></p>

		<div class="links">

		<!-- ***** REVISTAS AQUA VITAE EM PORTUGUÊS ***** -->
		<?php if ($this->session->userdata('language') == 'pt'): ?>

			<?php $ultimaEdicao = 17?>

			<?php if ($ultimaEdicao): ?>
				<?php for ($i=$ultimaEdicao; $i >= 1 ; $i--): ?>

					<?=$titulo = traduz("Revista Aqua Vitae").' #'.$i ?>

					<div class="edicao">
						<img src="_imgs/aquavitae/pt/aquavitae<?=$i?>.jpg" alt="<?=$titulo?>">
						<div class="barra-share-small">
							<a href="_arquivos/aquavitae/pt/aquavitae<?=$i?>.pdf" target="_blank" title="<?=traduz('Faça o download!')?>" class="link-download"></a>
							<ul>
								<li class="share-email">
									<a href="mailto:?subject=<?=urlencode($titulo)?>&body=<?=urlencode($titulo)?>" title="<?=traduz('Compartilhe por e-mail!')?>">e-mail</a>
								</li>
								<li class="share-facebook">
									<a rel="nofollow" href="http://www.facebook.com/share.php" data-fb-title="<?=$titulo?>" data-fb-description="<?=$titulo?>" onclick="return fbs_click(this)" title="<?=traduz('Compartilhe pelo facebook!')?>">facebook</a>
								</li>
								<li class="share-twitter">
									<a rel="nofollow" href="" onclick="window.open('http://twitter.com/home?status=<?=urlencode($titulo.' '.current_url()) ?>','Twitter','toolbar=0,status=0,width=626,height=436'); return false;" title="<?=traduz('Compartilhe no Twitter')?>">twitter</a>
								</li>
								<li class="share-tumblr">
									<a href="http://www.tumblr.com/share/link?url=<?=urlencode(current_url())?>&description=<?=urlencode($titulo) ?>&name=<?=urlencode($titulo) ?>" title="<?=traduz('Compartilhe no Tumblr!')?>">tumblr</a>
								</li>
							</ul>
						</div>
						#<?=$i?>
					</div>

				<?php endfor; ?>
			<?php endif; ?>
		<!-- ******************************************* -->

		<!-- ***** REVISTAS AQUA VITAE EM INGLÊS ***** -->
		<?php elseif($this->session->userdata('language') == 'en'): ?>

			<?php $ultimaEdicao = 0?>

			<?php if ($ultimaEdicao): ?>
				<?php for ($i=$ultimaEdicao; $i >= 1 ; $i--): ?>

					<?=$titulo = traduz("Revista Aqua Vitae").' #'.$i ?>

					<div class="edicao">
						<img src="_imgs/aquavitae/en/aquavitae<?=$i?>.jpg" alt="<?=$titulo?>">
						<div class="barra-share-small">
							<a href="_arquivos/aquavitae/en/aquavitae<?=$i?>.pdf" target="_blank" title="<?=traduz('Faça o download!')?>" class="link-download"></a>
							<ul>
								<li class="share-email">
									<a href="mailto:?subject=<?=urlencode($titulo)?>&body=<?=urlencode($titulo)?>" title="<?=traduz('Compartilhe por e-mail!')?>">e-mail</a>
								</li>
								<li class="share-facebook">
									<a rel="nofollow" href="http://www.facebook.com/share.php" data-fb-title="<?=$titulo?>" data-fb-description="<?=$titulo?>" onclick="return fbs_click(this)" title="<?=traduz('Compartilhe pelo facebook!')?>">facebook</a>
								</li>
								<li class="share-twitter">
									<a rel="nofollow" href="" onclick="window.open('http://twitter.com/home?status=<?=urlencode($titulo.' '.current_url()) ?>','Twitter','toolbar=0,status=0,width=626,height=436'); return false;" title="<?=traduz('Compartilhe no Twitter')?>">twitter</a>
								</li>
								<li class="share-tumblr">
									<a href="http://www.tumblr.com/share/link?url=<?=urlencode(current_url())?>&description=<?=urlencode($titulo) ?>&name=<?=urlencode($titulo) ?>" title="<?=traduz('Compartilhe no Tumblr!')?>">tumblr</a>
								</li>
							</ul>
						</div>
						#<?=$i?>
					</div>

				<?php endfor; ?>
			<?php endif; ?>
		<!-- ******************************************* -->

		<!-- ***** REVISTAS AQUA VITAE EM ESPANHOL ***** -->
		<?php elseif($this->session->userdata('language') == 'es'): ?>

			<?php $edicoes = array(17,16,12,10,5); ?>

				<?php foreach($edicoes as $i): ?>

					<?=$titulo = traduz("Revista Aqua Vitae").' #'.$i ?>

					<div class="edicao">
						<img src="_imgs/aquavitae/es/aquavitae<?=$i?>.jpg" alt="<?=$titulo?>">
						<div class="barra-share-small">
							<a href="_arquivos/aquavitae/es/aquavitae<?=$i?>.pdf" target="_blank" title="<?=traduz('Faça o download!')?>" class="link-download"></a>
							<ul>
								<li class="share-email">
									<a href="mailto:?subject=<?=urlencode($titulo)?>&body=<?=urlencode($titulo)?>" title="<?=traduz('Compartilhe por e-mail!')?>">e-mail</a>
								</li>
								<li class="share-facebook">
									<a rel="nofollow" href="http://www.facebook.com/share.php" data-fb-title="<?=$titulo?>" data-fb-description="<?=$titulo?>" onclick="return fbs_click(this)" title="<?=traduz('Compartilhe pelo facebook!')?>">facebook</a>
								</li>
								<li class="share-twitter">
									<a rel="nofollow" href="" onclick="window.open('http://twitter.com/home?status=<?=urlencode($titulo.' '.current_url()) ?>','Twitter','toolbar=0,status=0,width=626,height=436'); return false;" title="<?=traduz('Compartilhe no Twitter')?>">twitter</a>
								</li>
								<li class="share-tumblr">
									<a href="http://www.tumblr.com/share/link?url=<?=urlencode(current_url())?>&description=<?=urlencode($titulo) ?>&name=<?=urlencode($titulo) ?>" title="<?=traduz('Compartilhe no Tumblr!')?>">tumblr</a>
								</li>
							</ul>
						</div>
						#<?=$i?>
					</div>

				<?php endforeach; ?>
		<!-- ******************************************* -->
		<?php endif ?>



		</div>

	</div>

</div>