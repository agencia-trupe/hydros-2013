<div class="centro">
	<div class="metade">
		<h3><?=traduz('Música "Planeta" tem inspiração no Hydros')?></h3>
		<p>
			<?=traduz("A letra foi criada pela professora de Ciências da Escola Municipal Professora Lacy Luiza da Crus Flores, Alice Sitta Perera. Já a aluna Hillary do 9º ano ficou responsável por criar a melodia e cantar a música que fala nosso planeta, em especial sobre a água. A escola recebeu o Projeto Hydros em 2012 e resolveu ajudar de alguma maneira. O trabalho, que ficou maravilhoso, você confere aqui:")?>
		</p>

		<object width="640" height="360">
		  <param name="movie" value="https://www.youtube.com/v/AmKCCidylaE?version=3"></param>
		  <param name="allowFullScreen" value="true"></param>
		  <param name="allowScriptAccess" value="always"></param>
		  <embed src="https://www.youtube.com/v/AmKCCidylaE?version=3" type="application/x-shockwave-flash" allowfullscreen="true" allowScriptAccess="always" width="640" height="360"></embed>
		</object>

		<div class="musicaplaneta">
			<h4>Planeta</h4>
			<p class="estrofe">Planeta você precisa de água.</p>
			<p>Planeta você precisa de mim</p>
			<p>Você precisa de água limpa sim.</p>
			<p>As flores não existiriam</p>
			<p>Nem bancos na praça</p>
			<p>De onde podemos observar</p>
			<p>As cores exuberantes</p>
			<p  class="estrofe">Exalando seus perfumes dia e noite</p>
			<p>Planeta você precisa de mim</p>
			<p>Você precisa de água limpa sim.</p>
			<p>Planeta você precisa de mim</p>
			<p>Você precisa de água limpa sim.</p>
			<p>A escuridão e nada a enxergar</p>
			<p>Pois à noite somente a luz do luar</p>
			<p>Não haveria hidroelétricas para energia gerar</p>
			<p>Então você que me usar</p>
			<p>Uma mensagem vou deixar</p>
			<p>Para haver vida no planeta</p>
			<p>Você precisa me preservar.(bis)</p>
			<p>Planeta você precisa de mim</p>
			<p>Você precisa de água limpa sim.</p>
			<p>Planeta você precisa de mim</p>
			<p>Você precisa de água limpa sim.</p>
		</div>
	</div>
</div>