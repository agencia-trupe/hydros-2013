<div class="centro">

	<div class="metade concursocultural">

		<img src="_imgs/inspira/<?=prefixo("logo_concursocultural")?>.png" alt="<?=traduz('Concurso Cultural Água em 1 minuto')?>" />

		<h2><?=traduz('Se a sua escola já recebeu o Projeto Hydros e a sua mensagem de conscientização e sensibilização sobre a água, está na hora de mostrar o que você aprendeu! Participe do Concurso Cultural “Água em 1 minuto” promovido pelo Projeto Hydros em parceria com o Grupo Empresarial Kaluz.')?></h2>

	</div>

</div>

<div class="borda">
	<div class="centro">
		<div class="metade concursocultural">
			<p>
				<?=traduz("Você encontra os materiais referentes ao concurso aqui:")?>
			</p>

			<div class="concurso-download">
				<?php switch ($this->session->userdata('language')) {
				case 'es':
					$link_download = "ES_Reglamento_agua_en1minuto.pdf";
					break;
				case 'en':
					$link_download = "EN_Regulations_water_in1minut.pdf";
					break;
				default:
				case 'pt':
					$link_download = "PT_Regulamento_agua_em1minuto.pdf";
					break;
				}?>
				<a href="_arquivos/inspira/concursocultural/<?=$link_download?>" target="_blank" title="<?=traduz('Regulamento')?>"><?=traduz('Regulamento')?></a>
			</div>
			<div class="concurso-download">
				<?php switch ($this->session->userdata('language')) {
				case 'es':
					$link_download = "ES_flyer_agua_en1minuto_grafica.pdf";
					break;
				case 'en':
					$link_download = "EN_flyer_water_in1minut_grafica.pdf";
					break;
				default:
				case 'pt':
					$link_download = "PT_flyer_agua_em1minuto_grafica.pdf";
					break;
				}?>
					<a href="_arquivos/inspira/concursocultural/<?=$link_download?>" target="_blank" title="<?=traduz('Flyer')?>" class="link-download"><?=traduz('Flyer')?></a>
			</div>
			<div class="concurso-download">
				<?php switch ($this->session->userdata('language')) {
				case 'es':
					$link_download = "ES_Afiche_agua_en1minuto_A3.pdf";
					break;
				case 'en':
					$link_download = "EN_Cartaz_awater_in1minut_A3.pdf";
					break;
				default:
				case 'pt':
					$link_download = "PT_Cartaz_agua_em1minuto_A3.pdf";
					break;
				}?>
				<a href="_arquivos/inspira/concursocultural/<?=$link_download?>" target="_blank" title="<?=traduz('Cartaz A3')?>" class="link-download"><?=traduz('Cartaz A3')?></a>
			</div>
			<div class="concurso-download">
				<?php switch ($this->session->userdata('language')) {
				case 'es':
					$link_download = "ES_Afiche_agua_en1minuto_A4.pdf";
					break;
				case 'en':
					$link_download = "EN_Cartaz_awater_in1minut_A4.pdf";
					break;
				default:
				case 'pt':
					$link_download = "PT_Cartaz_agua_em1minuto_A4.pdf";
					break;
				}?>
					<a href="_arquivos/inspira/concursocultural/<?=$link_download?>" target="_blank" title="<?=traduz('Cartaz A4')?>"><?=traduz('Cartaz A4')?></a>
			</div>
		</div>
	</div>
</div>
<div class="borda">
	<div class="centro">
		<div class="metade concursocultural">
			<p>
				<?=traduz("Disponibilizamos abaixo todos os materiais do Projeto Hydros que podem, de alguma maneira, te inspirar para o concurso.")?>
			</p>

				<a href="materiais/videos" class="concurso-video">Vídeo</a>

			<p>
				<?=traduz('Se tiver alguma dúvida sobre o concurso, você pode nos contatar através do e-mail <a href="mailto:hydros@corecomunicacao.com.br">hydros@corecomunicacao.com.br</a> ou entrar em contato com o Embaixador da Água que entrou em contato com a sua escola.')?>
			</p>

			<div style="height:80px;">
				<div class="compartilhar-inspira">
					<?=traduz('Compartilhe!')?>
					<ul>
						<li class="share-email">
							<a href="<?=$share['email']?>" title="<?=traduz('Compartilhe por e-mail!')?>">e-mail</a>
						</li>
						<li class="share-facebook">
							<a rel="nofollow" href="http://www.facebook.com/share.php" onclick="return fbs_click(this)" title="<?=traduz('Compartilhe pelo facebook!')?>">facebook</a>
						</li>
						<li class="share-twitter">
							<a rel="nofollow" href="" onclick="<?=$share['tweet']?>" title="<?=traduz('Compartilhe no Tumblr')?>">twitter</a>
						</li>
						<li class="share-tumblr">
							<a href="http://www.tumblr.com/share/link?url=<?php echo urlencode(current_url())?>&description=<?= urlencode($share['description_tumblr']) ?>&name=<?php echo urlencode($share['tumblr']) ?>" title="<?=traduz('Compartilhe no Tumblr!')?>">tumblr</a>
						</li>
					</ul>
				</div>
			</div>

		</div>
	</div>
</div>