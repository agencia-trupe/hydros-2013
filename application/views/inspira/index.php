<div class="centro">

	<div class="metade comfundo">

		<h1><?=traduz('HYDROS INSPIRA')?></h1>

		<h2><?=traduz('Nesse espaço, você encontrará materiais feitos por pessoas que tiveram contato com o Projeto Hydros e ficaram sensibilizadas com a sua mensagem, de alguma forma. Portanto, se tiver algum produto ou manifestação artística que foi fruto do seu contato com o Projeto Hydros e que tenha atingido um determinado grupo de pessoas, como, por exemplo, um livro, vídeo, textos, música, exposição etc, envie para hydros@corecomunicacao.com.br. Nós iremos analisar o seu material e publicar aqui. O importante, realmente, é levar essa ideia adiante!')?></h2>

	</div>

</div>
