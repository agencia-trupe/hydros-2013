<div class="centro">
	<div class="metade">
		<h3><?=traduz('Concurso "O Homem e a água"')?></h3>
		<p>
			<?=traduz("O Grupo Empresarial Kaluz promoveu um concurso cultural de fotografias, em 2012, englobando todos os seus colaboradores ao redor do mundo, com o tema “O homem e a água”. Veja aqui as cinco imagens vencedoras que foram inspiradas também nas mensagens de sensibilização que o Projeto Hydros trouxe ao cotidiano dos profissionais.")?>
		</p>
		<img src="_imgs/inspira/img2-inspiraConcurso.jpg" alt="Hydros Inspira">
		<div class="barra-share-large">
			<a href="_arquivos/inspira/<?=$link_download_homemagua?>" target="_blank" title="<?=traduz('Faça o download!')?>" class="link-download"><?=traduz('Faça o download!')?></a>
		</div>
		<p class="p-verdana">
			<?=traduz('<strong>1º Lugar</strong> - María de los Ángeles Avilés Gamboa, da Mexichem, com o tema “Mar Morto, reviva!”')?><br>
			<?=traduz('<strong>2º Lugar</strong> - Emiel van den Boomen, da Wavin Holanda, com o tema “Banhando nas águas sagradas do templo Tirta Empul em Bali”')?><br>
			<?=traduz('<strong>3º Lugar</strong> - Liliana Echeverri Branch, da Mexichem Colombia S.A.S., com o tema “Os meninos do Pacífico são água”')?><br>
			<?=traduz('<strong>4º Lugar</strong> - Diego Alejandro David Vázquez, da Mexichem, com o tema “Com os pés em mais águas”')?><br>
			<?=traduz('<strong>5º Lugar</strong> - Filemón Peruyero Solís, da Mexichem TI, com o tema “Agua: meio de transporte e sustento diário”')?><br>
		</p>
		<div style="height:80px;">
			<div class="compartilhar-inspira">
				<?=traduz('Compartilhe!')?>
				<ul>
					<li class="share-email">
						<a href="<?=$share['email']?>" title="<?=traduz('Compartilhe por e-mail!')?>">e-mail</a>
					</li>
					<li class="share-facebook">
						<a rel="nofollow" href="http://www.facebook.com/share.php" onclick="return fbs_click(this)" title="<?=traduz('Compartilhe pelo facebook!')?>">facebook</a>
					</li>
					<li class="share-twitter">
						<a rel="nofollow" href="" onclick="<?=$share['tweet']?>" title="<?=traduz('Compartilhe no Tumblr')?>">twitter</a>
					</li>
					<li class="share-tumblr">
						<a href="http://www.tumblr.com/share/link?url=<?php echo urlencode(current_url())?>&description=<?= urlencode($share['description_tumblr']) ?>&name=<?php echo urlencode($share['tumblr']) ?>" title="<?=traduz('Compartilhe no Tumblr!')?>">tumblr</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>