<?php

$root = "application/language/";

$dirs = array('en', 'es', 'pt');

$fop = fopen("sem_traducao.php","a+");
fwrite($fop,"<?php\n");

$ocorrencias = array();

// Itera diretórios
foreach ($dirs as $key => $value) {
	// Itera arquivos dentro dos diretórios
	foreach (new DirectoryIterator($root.$value) as $fileInfo) {
		
		// Verifica se é um arquivo php
	    if($fileInfo->getExtension() == 'php'){
	    	//echo $fileInfo->getFilename().'<br>';

	    	$file = $root.$value.'/'.$fileInfo->getFilename();
			// get the file contents, assuming the file to be readable (and exist)
			$contents = file_get_contents($file);
			// escape special characters in the query
			$pattern = preg_quote(" = \"\";", '/');
			// finalise the regular expression, matching the whole line
			$pattern = "/^.*$pattern.*\$/m";
			// search, and store all matching occurences in $matches
			if(preg_match_all('# = ""; #i', $contents, $matches)){
				$retorno = "ARQUIVO : $file\n";
			    $retorno .= print_r($matches[0], 1);
			    $retorno .= "\n========================\n\n";
			    $fp = fopen("matches.txt","a+");
				fwrite($fp,$retorno);
				fclose($fp);
				
				fwrite($fop,"\n// ARQUIVO : $file\n");
				foreach ($matches[0] as $key => $lan) {
					$valor_final = "\$lang[".$lan."] = \"\";\n";
					if(!in_array($valor_final, $ocorrencias)){
						$ocorrencias[] = $valor_final;
						fwrite($fop,$valor_final);
					}
				}

			}else{
				$retorno = "ARQUIVO : $file\n sem ocorrencias";
			}
	    }
	}	
}

fwrite($fop,"?>");
fclose($fop);

echo "finalizado";

// $root = "application/views/";

// $dirs = array('','common','materiais','novidades','projeto');

// $fop = fopen("lang.php","a+");
// fwrite($fop,"<?php\n");

// $ocorrencias = array();

// // Itera diretórios
// foreach ($dirs as $key => $value) {
// 	// Itera arquivos dentro dos diretórios
// 	foreach (new DirectoryIterator($root.$value) as $fileInfo) {
		
// 		// Verifica se é um arquivo php
// 	    if($fileInfo->getExtension() == 'php'){
// 	    	//echo $fileInfo->getFilename().'<br>';

// 	    	$file = $root.$value.'/'.$fileInfo->getFilename();
// 			// get the file contents, assuming the file to be readable (and exist)
// 			$contents = file_get_contents($file);
// 			// escape special characters in the query
// 			$pattern = preg_quote("traduz", '/');
// 			// finalise the regular expression, matching the whole line
// 			$pattern = "/^.*$pattern.*\$/m";
// 			// search, and store all matching occurences in $matches
// 			if(preg_match_all('#traduz\((.+?)\)#i', $contents, $matches)){
// 				$retorno = "ARQUIVO : $file\n";
// 			    $retorno .= print_r($matches[1], 1);
// 			    $retorno .= "\n========================\n\n";
// 			    $fp = fopen("matches.txt","a+");
// 				fwrite($fp,$retorno);
// 				fclose($fp);
				
// 				fwrite($fop,"\n// ARQUIVO : $file\n");
// 				foreach ($matches[1] as $key => $lan) {
// 					$valor_final = "\$lang[".$lan."] = \"\";\n";
// 					if(!in_array($valor_final, $ocorrencias)){
// 						$ocorrencias[] = $valor_final;
// 						fwrite($fop,$valor_final);
// 					}
// 				}

// 			}else{
// 				$retorno = "ARQUIVO : $file\n sem ocorrencias";
// 			}
// 	    }
// 	}	
// }
/*
// fwrite($fop, "?>");
// fclose($fop);

// echo "finalizado";
*/
?>