<?php

$lang['email_must_be_array'] = 'O m�todo de valida��o de e-mail deve ser passado em forma de Array';
$lang['email_invalid_address'] = 'Endere�o de e-mail inv�lido: %s';
$lang['email_attachment_missing'] = 'N�o foi poss�vel localizar o anexo para o e-mail: %s';
$lang['email_attachment_unreadable'] = 'N�o foi poss�vel abrir o anexo: %s';
$lang['email_no_recipients'] = 'Voc� deve incluir destinat�rios: Para, Cc ou Cco';
$lang['email_send_failure_phpmail'] = 'N�o � poss�vel enviar e-mail usando a fun��o PHP mail (). O servidor n�o pode ser configurado para enviar e-mail utilizando este m�todo.';
$lang['email_send_failure_sendmail'] = 'N�o � poss�vel enviar e-mail usando a fun��o PHP Sendmail. O servidor n�o pode ser configurado para enviar e-mail utilizando este m�todo.';
$lang['email_send_failure_smtp'] = 'N�o � poss�vel enviar e-mail usando fun��o PHP SMTP. O servidor n�o pode ser configurado para enviar e-mail utilizando este m�todo.';
$lang['email_sent'] = 'Sua mensagem foi enviada com sucesso utilizando o seguinte protocolo: %s';
$lang['email_no_socket'] = 'N�o � poss�vel abrir um soquete para Sendmail. Por favor, verifique as configura��es.';
$lang['email_no_hostname'] = 'Voc� n�o especificou um nome de host SMTP.';
$lang['email_smtp_error'] = 'Os seguintes erros de SMTP foram encontrados: %s';
$lang['email_no_smtp_unpw'] = 'Erro: Voc� deve atribuir um nome de usu�rio e senha SMTP.';
$lang['email_failed_smtp_login'] = 'Falha ao enviar o comando AUTH LOGIN. Erro: %s';
$lang['email_smtp_auth_un'] = 'Falha ao autenticar o seu nome de usu�rio. Erro: %s';
$lang['email_smtp_auth_pw'] = 'Falha ao autenticar a senha. Erro: %s';
$lang['email_smtp_data_failure'] = 'N�o � poss�vel enviar dados: %s';
$lang['email_exit_status'] = 'C�digo de status de sa�da: %s';
?>