<?php

// ARQUIVO : application/views//home.php
$lang['Materiais para download'] = "";
$lang['Com os MATERIAIS DE APOIO você pode realizar ações concretas e se tornar um verdadeiro EMBAIXADOR DA ÁGUA!'] = "";
$lang['Vídeos'] = "";
$lang['vídeos'] = "";
$lang['Galeria de Imagens'] = "";
$lang['galeria de <br> imagens'] = "";
$lang['Exposição Hydros'] = "";
$lang['exposição <br> hydros'] = "";
$lang['Material de Apoio'] = "";
$lang['material de <br> apoio'] = "";
$lang['Saiba como usar o Projeto Hydros na sua escola'] = "";
$lang['A cartilha Projeto Hydros na Escola é o material ideal para que você, profissional da área da Educação, possa conscientizar os seus alunos sobre o tema água.'] = "";
$lang['Siga-nos!'] = "";
$lang['projetoHydros'] = "";
$lang['FIQUE POR DENTRO'] = "";
$lang['Notícias da água'] = "";
$lang['Conceito criado pela Convenção de Ramsar para denominar o conjunto de áreas alagáveis, como lagos, manguezais e pântanos.'] = "";
$lang['leia mais'] = "";
$lang['Mapa de recursos hídricos no mundo'] = "";
$lang['Veja aqui dados sobre os recursos hídricos de cada país, além de descobrir como esses recursos são utilizados em cada região.'] = "";
$lang['Cadastre-se para receber novidades sobre o PROJETO HYDROS!'] = "";
$lang['nome'] = "";
$lang['e-mail'] = "";
$lang['ENVIAR'] = "";

// ARQUIVO : application/views//pegada.php
$lang['PEGADA HYDROS'] = "";
$lang['Já imaginou calcular a quantidade de água que você gasta no seu dia a dia, ao lavar as mãos ou escovar os dentes, por exemplo? '] = "";
$lang['Agora você pode descobrir a sua pegada hídrica com o aplicativo do Projeto Hydros, o Pegada Hydros. Basta ter um tablet ou smartphone que tenha App Store (iPhone) ou Google Play (Android) como plataforma de distribuição de aplicativo e fazer o download gratuito do App.'] = "";
$lang['Google Play'] = "";
$lang['iTunes'] = "";
$lang['E aguarde que, logo mais, o aplicativo Pegada Hydros se desdobrará em um game que irá ensinar, de forma divertida e interativa, as formas de captação, reuso e descarte de água. Além disso, ele se passará em algumas das cidades que representam os locais de atuação do Projeto Hydros: Cidade do México, São Paulo, Nova Iorque, Tóquio e Amsterdã. Em cada cidade, serão 5 fases, que vão ficando mais difíceis, de acordo com a evolução do usuário no jogo.'] = "";
$lang['Usando o sensor de gravidade dos smartphones e tablets, o jogador terá de direcionar o fluxo da água pelos canos, para os lugares da casa em que a cada tipo de água deve ser utilizada - de chuveiros e pias a irrigação de hortas e abastecimentos de reservatórios de água de reuso.'] = "";

// ARQUIVO : application/views//contato.php
$lang['Contato'] = "";
$lang['telefone'] = "";
$lang['mensagem'] = "";
$lang['Quer receber nossa newsletter com todas as novidades e informações sobre o Projeto Hydros?'] = "";
$lang['SIM'] = "";
$lang['NÃO'] = "";

// ARQUIVO : application/views/common/menu.php
$lang['Compartilhe e acompanhe o Projeto Hydros em todos os canais!'] = "";
$lang["Conheça o Projeto Hydros"] = "";
$lang["O Projeto Hydros é uma iniciativa que tem o objetivo de disseminar a conscientização sobre temas relacionados à água e ao desperdício dos nossos recursos naturais. Se você quer fazer parte do nosso time e passar a ideia adiante, torne-se um Embaixador da Água! É super rápido e fácil, e você já estará fazendo a diferença com apenas um clique. .... www.projetohydros.com"] = "";
$lang[''] = "";
$lang['HOME'] = "";
$lang['PROJETO HYDROS'] = "";
$lang['MATERIAIS'] = "";
$lang['Contexto'] = "";
$lang['O que é o projeto?'] = "";
$lang['Quais são os desafios?'] = "";
$lang['Quem apoia?'] = "";
$lang['Como eu posso participar?'] = "";
$lang['cartilha Projeto Hydros Na Escola'] = "";
$lang['CONTATO'] = "";
$lang['Mapa de Recursos Hídricos no Mundo'] = "";
$lang['Notícias da Água'] = "";
$lang['Giro pelo Projeto Hydros'] = "";

// ARQUIVO : application/views/common/footer.php
$lang['contexto'] = "";
$lang['o que é o projeto?'] = "";
$lang['quais são os desafios?'] = "";
$lang['quem apoia'] = "";
$lang['como eu posso participar?'] = "";
$lang['galeria de imagens'] = "";
$lang['exposição hydros'] = "";
$lang['material de apoio'] = "";
$lang['cartilha Ação Educativa'] = "";
$lang['mapa de recursos hídricos no mundo'] = "";
$lang['notícias'] = "";
$lang['giro'] = "";
$lang['política de privacidade'] = "";
$lang['aviso legal'] = "";
$lang['Projeto Hydros'] = "";
$lang['Todos os direitos reservados'] = "";
$lang['Coordenação:'] = "";
$lang['Criação de sites:'] = "";

// ARQUIVO : application/views/common/header.php
$lang['site_keywords'] = "";

// ARQUIVO : application/views/materiais/videos.php
$lang['A água em nós'] = "";
$lang['Os vídeos “Água em nós”, composto por um filme mais completo e outros oito featurettes, são ferramentas de conscientização e sensibilização que mostram, de forma lúdica, a verdadeira relação que temos com a água em todos os seus aspectos.  Portanto, use-os à vontade: publique no seu mural do Facebook e outras redes sociais ou faça o download para usá-los no ambiente de trabalho, com a família e amigos.'] = "";
$lang['Faça a diferença agindo diferente!'] = "";
$lang['Faça o download!'] = "";
$lang['Compartilhe!'] = "";
$lang['Compartilhe por e-mail!'] = "";
$lang['Compartilhe pelo facebook!'] = "";
$lang['Compartilhe no Tumblr'] = "";
$lang['Compartilhe no Tumblr!'] = "";
$lang['Advertência Legal de Utilização de Conteúdo'] = "";
$lang['O conteúdo do hotsite é de utilização livre, desde que observadas as regras a seguir estabelecidas:'] = "";
$lang['O objetivo do hotsite é exclusivamente informativo. O material exposto não pode ser utilizado para qualquer finalidade e/ou propósito contrário a esse fim, nem poderá ser copiado e/ou reproduzido sem a divulgação clara e expressa da fonte, creditando a autoria do texto, fotografia e/ou vídeo, produção etc., cujo nome se encontra indicado junto a cada material exposto. Exemplo: "www.projetohydros.com / (nome completo do autor). Todos os direitos reservados".'] = "";
$lang['A alteração de conteúdo e/ou distorções de qualquer natureza, como montagens de imagens, de voz ou outras, é expressamente proibida, em qualquer hipótese, podendo ser punida na forma da lei.'] = "";
$lang['É vedada, também, a veiculação do conteúdo deste hotsite, mesmo com indicação de autor, com a finalidade de lucro pessoal e/ou corporativo, ou com associação à religião, política, discriminação, pedofilia e/ou outras práticas ilícitas.'] = "";
$lang['A indicação de fonte efetuada por terceiro que utilizar os textos, imagens ou vídeos deste hotsite deverá conter, necessariamente, o endereço do domínio e o nome completo do autor do texto, fotografia ou vídeo correspondente.'] = "";
$lang['Personagens da Turma do Cocoricó estrelam filmetes do Projeto Hydros'] = "";
$lang['Em 2012, o Projeto Hydros ganhou importantes aliados, por meio de uma iniciativa da Mexichem Brasil: a série “Pílulas de Cultura” estrelada pela Turma do Cocoricó. Veiculada na TV Cultura, a série foca o tema da água e é um ótimo exemplo de como o Projeto Hydros possui inúmeras possibilidades de interação. Reveja os vídeos.'] = "";
$lang['Água'] = "";
$lang['Não pode faltar água nesse mundo'] = "";
$lang['Entre nessa onda!'] = "";
$lang['Água limpa é vida!'] = "";
$lang['Tudo é água'] = "";

// ARQUIVO : application/views/materiais/galeria.php
$lang['Um dos grandes destaques do Projeto Hydros são as fotografias que compõem os quatro livros da série, de autoria de artistas latino-americanos e europeus. São mais de 200 imagens para você utilizar. Aqui você encontra a opção de compartilhá-las ou baixá-las em todos os tamanhos.'] = "";
$lang['DOWNLOAD DA GALERIA <br>DE IMAGENS COMPLETA'] = "";
$lang['Você pode fazer o download da <br>galeria completa em alta resolução.'] = "";
$lang['clique para iniciar o download'] = "";
$lang['DICA'] = "";
$lang['Para que sua iniciativa tenha um resultado mais positivo e impactante, sugerimos a impressão do material selecionado em papel fotográfico e em alta resolução.'] = "";
$lang['Clique nas imagens para ampliar e ver detalhes:'] = "";

// ARQUIVO : application/views/materiais/exposicao.php
$lang['Você pode utilizar de oito até 26 imagens dentre as mais de 200 extraídas da coleção de livros Hydros para criar sua própria exposição. Pense em um espaço disponível, como escolas, escritórios, estações do metrô, parques ou até mesmo museus. Disponha as fotos de uma forma que toda a mensagem embutida possa ser apreciada pelos visitantes.'] = "";
$lang['Selecione a exposição que deseja e faça o download!'] = "";
$lang['imagens'] = "";

// ARQUIVO : application/views/materiais/cartilha.php
$lang['Cartilha Projeto Hydros na Escola'] = "";
$lang['Pensando em sensibilizar e ensinar crianças nas escolas sobre a escassez da água, o Projeto Hydros criou a cartilha “Projeto Hydros na Escola”. Juntamente com as exposições e materiais de apoio (que podem ser utilizados em sala de aula), a cartilha foi criada para que suas atividades possam ser incluídas no planejamento escolar. Existem duas versões: uma mais completa e outra resumida, com ações mais pontuais.'] = "";

// ARQUIVO : application/views/materiais/videos_detalhe.php
$lang['Voltar'] = "";
$lang['VOLTAR'] = "";

// ARQUIVO : application/views/materiais/imagens_detalhes.php
$lang['Fotógrafo'] = "";
$lang['VOLTAR PARA GALERIA COMPLETA'] = "";

// ARQUIVO : application/views/materiais/apoio.php
$lang['Cada um de nós, Embaixadores da Água, pode também intervir em nossos espaços cotidianos, como no escritório, no carro, nas portas e nos espelhos de nossos ambientes e, até mesmo, em nossa própria casa.'] = "";
$lang['Para isso, projetamos uma campanha composta por diversas peças, que você pode selecionar individualmente ou em conjunto por cartazes. Use sua imaginação para conscientizar e sensibilizar mais pessoas, junto ao Projeto Hydros.'] = "";
$lang['Wallpaper'] = "";
$lang['ver'] = "";
$lang['Marcador de livro'] = "";
$lang['Testeira para computador'] = "";
$lang['Móbile'] = "";
$lang['Banner'] = "";
$lang['Toalha de bandeja'] = "";
$lang['Cartaz'] = "";
$lang['Adesivo'] = "";
$lang['Banner online'] = "";

// ARQUIVO : application/views/materiais/apoio_detalhe.php

// ARQUIVO : application/views/novidades/recursos.php
$lang['Saiba mais sobre os recursos hídricos disponíveis (água de superficie e subterrânea) e renováveis e como eles são usados em cada região (agricultura, doméstico e industrial), além da presença do Projeto Hydros em diversos países.'] = "";

// ARQUIVO : application/views/novidades/noticias.php
$lang['Acompanhe as últimas informações sobre a água no mundo.'] = "";

// ARQUIVO : application/views/novidades/giro.php
$lang['Saiba quais ações cada empresa do Grupo Empresarial Kaluz está desenvolvendo para implantar o Projeto Hydros entre seus principais públicos de interesse, como colaboradores, comunidade, escolas etc.'] = "";
$lang['VER MAIS'] = "";

// ARQUIVO : application/views/novidades/noticias_detalhes.php
$lang['voltar para a home de notícias'] = "";

// ARQUIVO : application/views/novidades/giro_detalhes.php
$lang['voltar para a home do Giro'] = "";

// ARQUIVO : application/views/projeto/contexto.php
$lang['Preservar a vida de todo o planeta é uma tarefa grande, que começa com ações muito simples'] = "";
$lang['O descaso do ser humano com a água está gerando preocupações alarmantes. De acordo com o Relatório Planeta Vivo 2010 da World Wildlife Fund (WWF), a Terra já ultrapassou 30% de sua capacidade de reposição dos recursos necessários para as nossas demandas. Porém, ainda podemos tentar reverter esse quadro, por meio de atitudes simples, em nosso dia a dia.'] = "";
$lang['Com a gestão adequada dos recursos hídricos, podemos amenizar o impacto das nossas ações na natureza e, assim, tentar garantir o fornecimento da quantidade e qualidade necessárias, para nós e para as próximas gerações. E você pode começar agora, integrando o grupo de “Embaixadores da Água” do Projeto Hydros.'] = "";

// ARQUIVO : application/views/projeto/apresentacao.php
$lang['Há tempos debatemos a forma como o homem se relaciona com a água'] = "";
$lang['Em 2008, a Fundação Kaluz e o Grupo Empresarial Kaluz, assim como as suas empresas Mexichem, Elementia e BX, lançaram uma série de quatro livros de fotografias tratando do tema água, com o objetivo de conscientizar e sensibilizar sobre a importância do uso correto desse elemento. As publicações chegaram às mãos de líderes nacionais, regionais e mundiais, que perceberam o potencial que o projeto trazia.'] = "";
$lang['Para ampliar a disseminação da preservação da água, nasceu o Portal Hydros. Interativo e trilíngue (espanhol, português e inglês), o site é o principal canal de comunicação entre o Projeto e seus Embaixadores. O portal tem inúmeras ferramentas que permitem a qualquer instituição ou cidadão comum, promover verdadeiras campanhas de sensibilização sobre a água.'] = "";
$lang['O <span class="txt-branco">Projeto Hydros</span> levanta a questão: <br> <span class="maior">\'Como me relaciono com a água?\'</span> <br> Trata-se de um grande chamado para que todos sejam “Embaixadores da água”, a <br> fim de disseminar o uso sustentável dos recursos hídricos.'] = "";
$lang['Em 2012, a ação foi lançada para todas as empresas do Grupo Kaluz, incentivando atitudes em todos os países nos quais atua, com foco na conscientização, preservação e sustentabilidade. No mesmo ano, o <strong>Projeto Hydros</strong> também chegou a escolas e comunidades próximas de suas unidades, abrindo um diálogo importante sobre o tema.'] = "";
$lang['O desafio para os próximos anos é disseminar para o maior numero possível de pessoas essa conscientização de que os nossos recursos naturais estão acabando e precisamos fazer algo para reverter essa situação. A ideia é que o <strong>Projeto Hydros</strong> saia dos portões das empresas do Grupo Kaluz e ganhe o mundo.'] = "";

// ARQUIVO : application/views/projeto/desafios.php
$lang['Uma questão de <br> consciência e atitude'] = "";
$lang['Um dos desafios para a preservação da água potável do mundo é a conscientização. Por isso, o objetivo de <strong>Projeto Hydros</strong> é fazer com que os resultados já alcançados ao longo de 2012, se expandam ainda mais e envolvam cada vez mais pessoas. Prova desse esforço: 2013 é o Ano Internacional para a Cooperação pela Água pela UNESCO.'] = "";
$lang['A ideia do <strong>Projeto Hydros</strong> é chamar a atenção da sociedade civil, empresas e estudantes em idade de formação escolar para as questões relacionadas à água e ao saneamento básico. E você pode colaborar, como um Embaixador da Água, abraçando a ideia do Projeto e incentivando todas as pessoas ao seu redor a debaterem o tema.'] = "";
$lang['Desafios para a preservação da água potável do mundo'] = "";
$lang['A UNESCO (Organização das Nações Unidas para a Educação, a Ciência e a Cultura) escolheu 2013 como o Ano Internacional para a Cooperação pela Água. O objetivo é promover uma maior interação entre nações e debater os desafios do manejo da água. Segundo a ONU-Água, existe um aumento da demanda pelo acesso, alocação e serviços relacionados a esse bem natural. O ano vai destacar iniciativas de sucesso sobre cooperação pela água.'] = "";
$lang['Saiba mais em'] = "";

// ARQUIVO : application/views/projeto/apoio.php
$lang['Seja você também um<br>Embaixador da Água!'] = "";
$lang['Simples mudanças de atitude podem fazer a diferença na preservação da água. Hoje já não é mais suficiente apenas fechar a torneira ao lavar a louça ou ao escovar os dentes, por exemplo. O grande desafio é a conscientização. Precisamos mostrar às pessoas que a escassez da água, especialmente a potável, aquela que consumimos, é um problema sério e que devemos tomar as atitudes corretas agora. Uma atitude inicial é tornar-se um <strong>Embaixador da Água</strong> do <strong>Projeto Hydros</strong>. '] = "";
$lang['Esta é a lista de pessoas e empresas que decidiram fazer parte desta iniciativa. Seja um deles e faça a sua parte!'] = "";
$lang['Empresas que apoiam o Projeto Hydros e agora são Embaixadoras da Água.'] = "";
$lang['voltar'] = "";
$lang['ver mais'] = "";
$lang['Seja um embaixador da água!'] = "";
$lang['PARTICIPE!'] = "";
$lang['Seja um'] = "";
$lang['Embaixador'] = "";
$lang['da Água!'] = "";
$lang['Pessoas que apoiam o Projeto Hydros e agora são Embaixadores da Água.'] = "";

// ARQUIVO : application/views/projeto/participar.php
$lang['Para ser um Embaixador da Água'] = "";
$lang['Para que você consiga utilizar todos os recursos disponíveis pelo Projeto Hydros da melhor forma possível, criamos dois tutoriais, com o passo a passo de como utilizar os materiais disponíveis neste site:'] = "";
$lang['Público Geral e Colaboradores'] = "";
$lang['um para o público em geral <br> e colaboradores do <br>Grupo Empresarial Kaluz'] = "";
$lang['clique para<br> iniciar o download'] = "";
$lang['Profissionais'] = "";
$lang['e outro para profissionais <br> da área de Educação'] = "";
$lang['Você está perto de tornar-se um <strong>Embaixador da Água</strong> e ter as ferramentas necessárias para sensibilizar e conscientizar as pessoas ao seu redor acerca de temas como a correta utilização e preservação da água.'] = "";
$lang['Participe do Projeto Hydros e faça a sua parte!'] = "";

// ARQUIVO : application/views/projeto/adesao.php
$lang['CADASTRO DE APOIO<br>AO PROJETO HYDROS'] = "";
$lang['Se você apoia o Projeto Hydros divulgue aqui<br> seu nome ou a marca de sua empresa.'] = "";
$lang['Cadastro de Pessoas'] = "";
$lang['cadastro de <br> <span class="maior">PESSOAS</span>'] = "";
$lang['Cadastro de Empresa'] = "";
$lang['cadastro de <br> <span class="maior">EMPRESAS</span>'] = "";
$lang['nome completo'] = "";
$lang['país'] = "";
$lang['CADASTRAR'] = "";
$lang['O Projeto Hydros não divulga publicamente os e-mails cadastrados nem fornece seus cadastros a terceiros. A solicitação do endereço de e-mail neste caso será usada apenas se for necessário confirmar alguma alteração. O Projeto Hydros se reserva o direito de excluir cadastros a seu critério.'] = "";
$lang['nome da empresa'] = "";
$lang['nome do<br> contato responsável'] = "";
$lang['website'] = "";
$lang['procurar imagem'] = "";
$lang['imagem da marca<br> da empresa'] = "";
$lang['Pronto, seu cadastro foi feito com sucesso.'] = "";
$lang['Você deu o primeiro passo para se tornar um Embaixador da Água.'] = "";
$lang['Quer saber mais? Acompanhe as novidades do Projeto Hydros sobre a água por meio de nossas redes sociais.'] = "";
$lang['Curta, compartilhe, siga e participe!'] = "";
$lang['A sua empresa deu o primeiro passo para se tornar uma Embaixadora da Água.'] = "";
$lang['Curta, compartilhe com seus colaboradores, siga e participe!'] = "";
?>